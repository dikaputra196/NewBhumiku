<!--/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */-->
<?php
//Required File
    require_once dirname(__FILE__)."/../components/templates/main.php";
    require_once dirname(__FILE__)."/../class/config.php";
    
    //Define Connection -> Database
    $db = new Database();
    $db->connect();
    
    //Call Template
    $template = new Template();
    
    //Start HTML
    $template->pageTitle="BHUMIKU Balai Pertemuan | Order Pembelian";
    
    //Start Content
    $template->contentTitle="<span class='glyphicon glyphicon-edit'></span> New Order Pembelian";
    $template->startContent();
?>
<!-- Box Form -->
<div class="row">
    <div class="col-md-12">
        <?php $template->startBox(); ?>
            <strong></strong>
        <?php $template->conBox();?>
                <!-- Form Order Pembelian -->
                <form class="form-horizontal" method="POST">
                    <div style="margin-left:15px">
                        <h4><u>Data P.O</u></h4>
                    </div>
                    
                    <!-- No. P.O -->
                    <div class="form-group">
                        <label class="col-sm-2 control-label">No. P.O</label>
                        
                        <div class="col-sm-3">
                            <input type="text" class="form-control" name="no_po" placeholder="Nomor P.O">
                        </div>
                    </div>
                    
                    <!-- Tanggal P.O -->
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Tanggal P.O</label>
                        
                        <div class="col-sm-3">
                            <input type="text" class="form-control datepicker" name="tanggal_po" placeholder="Tanggal P.O" required>
                        </div>
                    </div>
                    
                    <!-- Tanggal Pengiriman -->
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Tanggal Pengiriman</label>
                        
                        <div class="col-sm-3">
                            <input type="text" class="form-control datepicker" name="tanggal_pengiriman" placeholder="Tanggal Pengiriman">
                        </div>
                    </div>
                    
                    <!-- Catatan -->
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Catatan</label>
                        
                        <div class="col-sm-3">
                            <textarea class="form-control" name="catatan" placeholder="Catatan"></textarea>
                        </div>
                    </div>
                    
                    <!-- Jenis Pembelian -->
                    <div class="form-group">
                        <label class="col-sm-2 control-label" name="jenis_pembelian">Jenis Pembelian</label>
                        
                        <div class="col-sm-2">
                            <select class="form-control select2">
                                <option value=""> ---</option>
                                <option value="INVENTORY">Inventory</option>
                                <option value="EVENT">Event</option>
                            </select>
                        </div>
                    </div>
                    
                    <!-- Cara Pembayaran -->
                    <div class="form-group">
                        <label class="col-sm-2 control-label" name="cara_pembayaran">Cara Pembayaran</label>
                        
                        <div class="col-sm-2">
                            <select class="form-control select2">
                                <option value=""> ---</option>
                                <option value="TUNAI">Tunai</option>
                                <option value="BANK">Bank</option>
                                <option value="KREDIT">Kredit</option>
                            </select>
                        </div>
                    </div>
                    
                    <!-- Akun Pembelian -->
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Akun Pembelian</label>
                        
                        <div class="col-sm-2">
                            <select class="form-control select2" name="akun_pembelian">
                                <option value=""> ---</option>
                                <?php
                                    $db->select("tb_coa","kode_coa,nama_coa",NULL,"kode_parent='110000' ");
                                    $result = $db->getResult();
                                    foreach($result as $show_ap){
                                ?>
                                    <option value="<?= $show_ap["kode_coa"]; ?>"><?= $show_ap["nama_coa"]; ?></option>
                                <?php } ?>
                            </select>
                        </div>
                    </div>
                    
                    <!-- Akun Pembayaran -->
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Akun Pembayaran</label>
                        
                        <div class="col-sm-2">
                            <select class="form-control select2" name="akun_pembayaran">
                                <option value=""> ---</option>
                                <?php
                                    $db->select("tb_coa","kode_coa,nama_coa",NULL,"kode_parent='110000' ");
                                    $result_p = $db->getResult();
                                    foreach($result_p as $show_apb){
                                ?>
                                    <option value="<?= $show_apb["kode_coa"]; ?>"><?= $show_apb["nama_coa"]; ?></option>
                                <?php } ?>
                            </select>
                        </div>
                    </div>
                    
                    <div style="margin-left:15px">
                        <h4><u>Data Vendor / Supplier</u></h4>
                    </div>
                    
                    <!-- Nama Vendor -->
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Nama Vendor</label>
                        
                        <div class="col-sm-3">
                            <select class="select2 form-control" name="nama_vendor">
                                <option value=""> ---</option>
                                <?php
                                    $db->select("tb_vendor","id_vendor,nama_vendor,alamat_vendor",NULL,"is_active='1' ");
                                    $result_v = $db->getResult();
                                    foreach($result_v as $show_v){
                                ?>
                                    <option value="<?= $show_v["id_vendor"]; ?>"><?= $show_v["nama_vendor"]; ?></option>
                                <?php } ?>
                            </select>
                        </div>
                    </div>
                    
                    <!-- Alamat Vendor -->
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Alamat Vendor</label>
                        
                        <div class="col-sm-5">
                            <input type="text" class="form-control" name="alamat_vendor" placeholder="Alamat Vendor" readonly>
                        </div>
                    </div>
                    
                    <div style="margin-left:15px">
                        <h4><u>Data Event</u></h4>
                    </div>
                    
                    <!-- Nama Event -->
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Nama Event</label>
                        
                        <div class="col-sm-3">
                            <select class="select2 form-control" name="nama_event">
                                <option value=""> ---</option>
                                <?php
                                    $db->select("tb_booking","id_booking,nama_event",NULL,"is_final='0' and status_booking='TENTATIVE' ");
                                    $result_ne = $db->getResult();
                                    foreach($result_ne as $show_ne){
                                ?>
                                <option value="<?= $show_ne["id_booking"]; ?>"><?= $show_ne["nama_event"]; ?></option>
                                <?php } ?>
                            </select>
                        </div>
                    </div>
                    
                    <!-- Jumlah barang yang akan diorder -->
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Jumlah Barang yang di Order</label>
                        
                        <div class="col-sm-1">
                            <select class="select2 form-control" name="count_order" id="count_order">
                                <option value=""> ---</option>
                            </select>
                        </div>
                    </div>
                    
                    <div class="form-group">
                        <div class="col-md-12" id="tabel">
                        </div>
                    </div>
                </form>
        <?php $template->endConBox();?>
        <?php $template->endBox();?>
    </div>
</div>
<!-- End Box Form -->
<!-- End Content -->
<?php $template->endContent(); ?>

<!-- Place Script Here -->
    <!-- Datepicker -->
        <script>
            $(document).ready(function(){
                $("input.datepicker").Zebra_DatePicker();
            });
        </script>
    <!-- Select2 -->
    <script>
        $(document).ready(function(){
            $(".select2").select2();
        });
    </script>
    <!-- Option-->
<script type="text/javascript">
    $(document).ready(function(){
        for(no=1; no <=10; no++)
        {
            $("#count_order").append($('<option>',
            {
                value: no,
                text : ""+no
            }));
        }
    });
</script>    
    
<!--// End Script Place -->

<!-- End </body> -->
<?php $template->endBody(); ?>

<!-- End HTML -->
<?php $template->endHtml(); ?>