<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

//Required File
    require_once dirname(__FILE__)."/../components/templates/main.php";
    require_once dirname(__FILE__)."/../class/config.php";
    
    //Define Connection -> Database
    $db = new Database();
    $db->connect();
    
    //Call Template
    $template = new Template();
    
    //Start HTML
    $template->pageTitle="BHUMIKU Balai Pertemuan | Data Chart of Accounts";
    
    //Start Content
    $template->contentTitle="<span class='glyphicon glyphicon-check'></span> Data Chart of Accounts";
    $template->startContent();
?>

<!-- Start Box -->
<div class="row">
    <!-- Box New Chart of Accounts -->
    <div class="col-md-3 col-sm-6 col-xs-12">
        <div class="info-box">
            <a href="<?= MAIN_URL ?>/pages/form_tambah_coa.php"><span class="info-box-icon bg-aqua"><i class="fa fa-bar-chart"></i></span></a>
            
            <div class="info-box-content">
                <span class="info-box-text"><strong>New</strong></span>
                <span class="info-box-more">Menambah Data Accounts</span>
            </div>
        </div>
    </div>
</div>

<!-- Table Chart of Accounts List + Action -->
<div class="row">
    <div class="col-md-12">
        <?php $template->startBox(); ?>
            <strong>  </strong>
        <?php $template->conBox();?>
            <!-- Table -->
            <div class="col-md-12">
                <table class="table table-responsive table-striped table-hover">
                    <thead align="center" style="background-color:#00a65a; font-weight:bold;">
                        <tr style="color:white;">
                            <td>No.</td>
                            <td>Kode</td>
                            <td>Nama Perkiraan</td>
                            <td>Jenis</td>
                            <td>Normal Saldo</td>
                            <td colspan="2">Action</td>
                        </tr>
                    </thead>
                    <?php
                        $no=0;
                        $db->select("tb_coa","kode_coa,nama_coa,kode_parent,jenis_coa,saldo_normal",NULL,"is_active='1' ");
                        $result = $db->getResult();
                        foreach($result as $show_coa){
                            $no++;
                    ?>
                    <tbody id="tbody" align="center">
                        <td><?= $no; ?></td>
                        <td><?= $show_coa["kode_coa"]; ?></td>
                        <td align="left"><?= $show_coa["nama_coa"]; ?></td>
                        <td><?= $show_coa["jenis_coa"]; ?></td>
                        <td><?= $show_coa["saldo_normal"]; ?></td>
                        <td>
                            <a href="#myModal" data-toggle="modal" data-id="<?= $show_coa["kode_coa"]; ?>"><button class="btn btn-sm bg-olive"><span class="glyphicon glyphicon-edit"></span> Edit</button></a>
                        </td>
                        <td>
                            <a class="delete-item" href="javascript:void(0);" data-id="<?= $show_coa["kode_coa"]; ?>"><button class="btn btn-sm btn-danger"><span class="glyphicon glyphicon-remove"></span> Delete</button></a>
                        </td>
                    </tbody>
                    <?php
                        }
                    ?>
                </table>
            </div>
        <?php $template->endConBox();?>
        <?php $template->endBox();?>
    </div>
</div>


<!-- End Content -->
<?php $template->endContent(); ?>

<!-- Modal -->
<?php
    $template->startModal();
    $template->conModal();
    $template->footModal();
?>

<!-- Place Script Here -->
    <!-- Dialog -->
    <script>
    $(document).ready(function(){
      $(".delete-item").click(function(e){
          e.preventDefault();
          var kode_coa = $(this).attr('data-id');
          var parent = $(this).parent("td").parent("tr");
          bootbox.dialog({
            message: "Apakah anda yakin akan menghapus Data ini?",
            title: "<i class='glyphicon glyphicon-trash'></i> Hapus? ",
            buttons: {
            success: {
            label: "<i class='fa fa-times'></i> Tidak",
            className: "btn-success",
            callback: function(){
              $(".bootbox").modal("hide");
            }
          },
          danger: {
            label: "<i class='fa fa-check'></i> Hapus!",
            className: "btn-danger",
            callback: function(){
              $.ajax({
                type: "POST",
                url: "<?= MAIN_URL ?>/action/act_delete_data_coa.php",
                data: "kode_coa="+kode_coa
              })
              .done(function(response){
                bootbox.alert(response);
                parent.fadeOut('slow');
              })
              .fail(function(){
                bootbox.alert('Error.....');
              });
            }
          }
          }
          });
      });
    });
  </script>
  
  <script type="text/javascript">
    $(document).ready(function(){
        $('#myModal').on('show.bs.modal', function (e) {
            var rowid = $(e.relatedTarget).data('id');
            
            $.ajax({
                type : 'post',
                url : 'modal_edit_coa.php',
                data :  'rowid='+ rowid,
                success : function(data){
                $('.fetched-data').html(data);
                }
            });
         });
    });
  </script>
<!-- //End Script Place -->

<!-- End </body> -->
<?php $template->endBody(); ?>

<!-- End HTML -->
<?php $template->endHtml(); ?>