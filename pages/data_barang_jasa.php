<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

//Required File
    require_once dirname(__FILE__)."/../components/templates/main.php";
    require_once dirname(__FILE__)."/../class/config.php";
    require_once dirname(__FILE__)."/../class/manual_connect.php";
    
    //Call Template
    $template = new Template();
    
    //Start HTML
    $template->pageTitle="BHUMIKU Balai Pertemuan | Data Barang / Jasa";
    
    //Start Content
    $template->contentTitle="<span class='glyphicon glyphicon-check'></span> Data Barang / Jasa";
    $template->startContent();
?>

<!-- Start Box -->
<div class="row">
    <!-- Box New Barang dan Jasa -->
    <div class="col-md-3 col-sm-6 col-xs-12">
        <div class="info-box">
            <a href="<?= MAIN_URL ?>/pages/form_tambah_barangjasa.php"><span class="info-box-icon bg-aqua"><i class="fa fa-cubes"></i></span></a>
            
            <div class="info-box-content">
                <span class="info-box-text"><strong>New</strong></span>
                <span class="info-box-more">Menambah Data Barang / Jasa</span>
            </div>
        </div>
    </div>
</div>

<!-- Table Barang Jasa List + Action -->
<div class="row">
    <div class="col-md-12">
        <?php $template->startBox(); ?>
            <strong> Data Barang / Jasa </strong>
        <?php $template->conBox();?>
            <!-- Table -->
            <div class="col-md-12">
                <table class="table table-responsive table-striped table-hover">
                    <thead align="center" style="background-color:#00a65a; font-weight:bold;">
                        <tr style="color:white;">
                            <td>No.</td>
                            <td>ID</td>
                            <td>Kode</td>
                            <td>Nama</td>
                            <td>S.B</td>
                            <td>S.K</td>
                            <td>Jenis</td>
                            <td>Inv.</td>
                            <td>Stock</td>
                            <td>Konversi Satuan</td>
                            <td>Harga Rata - rata</td>
                            <td>Purchased</td>
                            <td colspan="2">Action</td>
                        </tr>
                    </thead>
                    <?php
                        $no=0;
                        $query = mysqli_query($con,"
                                                    SELECT
                                                    a.id_barang,
                                                    a.kode_barang,
                                                    a.nama_barang,
                                                    b.nama_satuan AS Satuan_Besar,
                                                    c.nama_satuan AS Satuan_Kecil,
                                                    a.jenis_barang,
                                                    IF(is_inventory='1','Ya','Tidak') AS INV,
                                                    a.on_stock,
                                                    a.konversi_satuan,
                                                    a.avg_price,
                                                    a.on_purchased
                                                    FROM
                                                    tb_barang a
                                                    INNER JOIN tb_satuan b
                                                    ON b.id_satuan=a.id_satuan_besar
                                                    INNER JOIN tb_satuan c
                                                    ON c.id_satuan = a.id_satuan_kecil
                                                    WHERE a.is_active='1'
                                ");
                        while($result = mysqli_fetch_array($query)){
                            $no++;
                    ?>
                    <tbody id="tbody" align="center">
                        <td><?= $no; ?></td>
                        <td><?= $result["id_barang"]; ?></td>
                        <td><?= $result["kode_barang"]; ?></td>
                        <td align="left"><?= $result["nama_barang"]; ?></td>
                        <td><?= $result["Satuan_Besar"]; ?></td>
                        <td><?= $result["Satuan_Kecil"]; ?></td>
                        <td><?= $result["jenis_barang"]; ?></td>
                        <td><?= $result["INV"]; ?></td>
                        <td><?= $result["on_stock"]; ?></td>
                        <td><?= $result["konversi_satuan"]; ?></td>
                        <td><?= $result["avg_price"]; ?></td>
                        <td><?= $result["on_purchased"]; ?></td>
                        <td>
                            <a href="#myModal" data-toggle="modal" data-id="<?= $result["id_barang"]; ?>"><button class="btn btn-sm bg-olive"><span class="glyphicon glyphicon-edit"></span> Edit</button></a>
                        </td>
                        <td>
                            <a class="delete-item" href="javascript:void(0)" data-id="<?= $result["id_barang"] ?>"><button class="btn btn-sm btn-danger"><span class="glyphicon glyphicon-remove"></span> Delete</button></a>
                        </td>
                    </tbody>
                    <?php
                        }
                    ?>
                </table>    
            </div>
        <?php $template->endConBox();?>
        <?php $template->endBox();?>
    </div>
</div>


<!-- End Content -->
<?php $template->endContent(); ?>

<!-- Modal -->
<?php
    $template->startModal();
    $template->conModal();
    $template->footModal();
?>

<!-- Place Script Here -->
    <!-- Dialog -->
    <script>
    $(document).ready(function(){
      $(".delete-item").click(function(e){
          e.preventDefault();
          var id_barang = $(this).attr('data-id');
          var parent = $(this).parent("td").parent("tr");
          bootbox.dialog({
            message: "Apakah anda yakin akan menghapus Data ini?",
            title: "<i class='glyphicon glyphicon-trash'></i> Hapus? ",
            buttons: {
            success: {
            label: "<i class='fa fa-times'></i> Tidak",
            className: "btn-success",
            callback: function(){
              $(".bootbox").modal("hide");
            }
          },
          danger: {
            label: "<i class='fa fa-check'></i> Hapus!",
            className: "btn-danger",
            callback: function(){
              $.ajax({
                type: "POST",
                url: "<?= MAIN_URL ?>/action/act_delete_data_barangjasa.php",
                data: "id_barang="+id_barang
              })
              .done(function(response){
                bootbox.alert(response);
                parent.fadeOut('slow');
              })
              .fail(function(){
                bootbox.alert('Error.....');
              });
            }
          }
          }
          });
      });
    });
  </script>
  
  <script type="text/javascript">
    $(document).ready(function(){
        $('#myModal').on('show.bs.modal', function (e) {
            var rowid = $(e.relatedTarget).data('id');
            
            $.ajax({
                type : 'post',
                url : 'modal_edit_barangjasa.php',
                data :  'rowid='+ rowid,
                success : function(data){
                $('.fetched-data').html(data);
                }
            });
         });
    });
  </script>
<!-- //End Script Place -->

<!-- End </body> -->
<?php $template->endBody(); ?>

<!-- End HTML -->
<?php $template->endHtml(); ?>