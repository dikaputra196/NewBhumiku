<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

//Required File
    require_once dirname(__FILE__)."/../class/config.php";
    
        
    //Define Connection -> Database
        $db = new Database();
        $db->connect(); 
        
        
        if($_REQUEST["rowid"]){
            $id = $_REQUEST['rowid'];
            $db->select("tb_jenis_setup","id_jenis_setup,nama_jenis_setup,deskripsi",NULL,"id_jenis_setup='$id'");
            $result = $db->getResult();
            foreach($result as $show_djs){

?>
            <form class="form-horizontal" method="POST" action="#">
                <div style="margin-left:15px">
                    <h4><u></u></h4>
                </div>
                <!-- ID Jenis Setup -->
                <div class="form-group">
                    <label class="col-sm-4 control-label">ID Jenis Setup</label>
                        
                    <div class="col-sm-4">
                        <input type="text" class="form-control" name="id_jenis_setup" placeholder="ID Jenis Setup" value="<?= $show_djs["id_jenis_setup"]; ?>" readonly>
                    </div>
                </div>
                
                <!-- Nama Jenis Setup -->
                <div class="form-group">
                    <label class="col-sm-4 control-label">Nama Jenis Setup</label>
                        
                    <div class="col-sm-5">
                        <input type="text" class="form-control" name="nama_jenis_setup" placeholder="Nama Jenis Setup" value="<?= $show_djs["nama_jenis_setup"]; ?>">
                    </div>
                </div>
                
                <!-- Deksripsi -->
                <div class="form-group">
                    <label class="col-sm-4 control-label">Deskripsi</label>
                        
                    <div class="col-sm-6">
                        <textarea class="form-control" name="deskripsi" placeholder="Deskripsi"><?= $show_djs["deskripsi"]; ?></textarea>
                    </div>
                </div>
                
                <!-- Button -->
                <div class="form-group">
                    <label class="col-sm-4 control-label"></label>
                        
                    <div class="col-sm-2">
                        <button type="submit" id="submit" name="submit" class="btn btn-success">Simpan</button>
                    </div>
                    <div class="col-sm-2">
                        <button type="reset" id="reset" name="reset" class="btn btn-warning">Reset</button>
                    </div>
                </div>
            </form>

        <?php }}?>


    <!-- Select2 -->
        <script>
            $(document).ready(function(){
                $(".select2").select2();
            });
        </script>