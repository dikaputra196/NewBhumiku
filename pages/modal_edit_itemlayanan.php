<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

//Required File
    require_once dirname(__FILE__)."/../class/config.php";
    
        
    //Define Connection -> Database
        $db = new Database();
        $db->connect(); 
        
        
        if($_REQUEST["rowid"]){
            $id = $_REQUEST['rowid'];
            $db->select("tb_item","id_item,kode_item,nama_item,price_item,deskripsi_item",NULL,"id_item='$id'");
            $result = $db->getResult();
            foreach($result as $show_dil){

?>
            <form class="form-horizontal" method="POST" action="#">
                <div style="margin-left:15px">
                    <h4><u></u></h4>
                </div>
                <!-- ID Item -->
                <div class="form-group">
                    <label class="col-sm-4 control-label">ID Item</label>
                        
                    <div class="col-sm-4">
                        <input type="text" class="form-control" name="id_item" placeholder="ID Item" value="<?= $show_dil["id_item"]; ?>" readonly>
                    </div>
                </div>
                
                <!-- Kode Item -->
                <div class="form-group">
                    <label class="col-sm-4 control-label">Kode Item</label>
                        
                    <div class="col-sm-4">
                        <input type="text" class="form-control" name="kode_item" placeholder="Kode Item" value="<?= $show_dil["kode_item"]; ?>" required>
                    </div>
                </div>
                
                <!-- Nama Item -->
                <div class="form-group">
                    <label class="col-sm-4 control-label">Nama Item</label>
                        
                    <div class="col-sm-5">
                        <input type="text" class="form-control" name="nama_item" placeholder="Nama Item" value="<?= $show_dil["nama_item"]; ?>" required>
                    </div>
                </div>
                
                <!-- Harga Item -->
                <div class="form-group">
                    <label class="col-sm-4 control-label">Harga Item</label>

                    <div class="col-sm-5">
                        <div class="input-group">
                            <span class="input-group-addon">Rp.</span>
                            <input type="number" class="form-control" name="harga_item" id="harga_item" value="<?= $show_dil["price_item"]; ?>" required>
                        </div>

                    </div>
                </div>
                
                <!-- Deskripsi Item -->
                <div class="form-group">
                    <label class="col-sm-4 control-label">Deskripsi Item</label>
                        
                    <div class="col-sm-6">
                        <textarea class="form-control" name="deskripsi_item" placeholder="Deskripsi Item"><?= $show_dil["deskripsi_item"]; ?></textarea>
                    </div>
                </div>
                
                <!-- Button -->
                <div class="form-group">
                    <label class="col-sm-4 control-label"></label>
                        
                    <div class="col-sm-2">
                        <button type="submit" id="submit" name="submit" class="btn btn-success">Simpan</button>
                    </div>
                    <div class="col-sm-2">
                        <button type="reset" id="reset" name="reset" class="btn btn-warning">Reset</button>
                    </div>
                </div>
            </form>
        <?php }}?>


    <!-- Select2 -->
        <script>
            $(document).ready(function(){
                $(".select2").select2();
            });
        </script>