<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

//Required File
    require_once dirname(__FILE__)."/../class/config.php";
    
        
    //Define Connection -> Database
        $db = new Database();
        $db->connect(); 
        
        
        if($_REQUEST["rowid"]){
            $id = $_REQUEST['rowid'];
            $db->select("tb_eo","id_eo,nama_eo,alamat_eo,telepon_eo,hp_eo,cp_eo,email_eo",NULL,"id_eo='$id'");
            $result = $db->getResult();
            foreach($result as $show_deo){

?>
            <form class="form-horizontal" method="POST" action="#">
                <div style="margin-left:15px">
                    <h4><u></u></h4>
                </div>
                <!-- ID Event Organizer -->
                <div class="form-group">
                    <label class="col-sm-4 control-label">ID Event Organizer</label>
                        
                    <div class="col-sm-3">
                        <input type="text" class="form-control" name="id_eo" placeholder="ID Event Organizer" value="<?= $show_deo["id_eo"]; ?>" readonly>
                    </div>
                </div>
                
                <!-- Nama Event Organizer -->
                <div class="form-group">
                    <label class="col-sm-4 control-label">Nama Event Organizer</label>
                        
                    <div class="col-sm-5">
                        <input type="text" class="form-control" name="nama_eo" placeholder="Nama Event Organizer" value="<?= $show_deo["nama_eo"]; ?>" required>
                    </div>
                </div>
                
                <!-- Alamat Event Organizer -->
                <div class="form-group">
                    <label class="col-sm-4 control-label">Alamat Event Organizer</label>
                        
                    <div class="col-sm-7">
                        <input type="text" class="form-control" name="alamat_eo" placeholder="Alamat Event Organizer" value="<?= $show_deo["alamat_eo"]; ?>" required>
                    </div>
                </div>
                
                <!-- Telepon Event Organizer -->
                <div class="form-group">
                    <label class="col-sm-4 control-label">Telepon Event Organizer</label>
                        
                    <div class="col-sm-5">
                        <input type="text" class="form-control" name="telepon_eo" placeholder="Telepon Event Organizer" value="<?= $show_deo["telepon_eo"] ?>" required>
                    </div>
                </div>
                
                <!-- HP Event Organizer -->
                <div class="form-group">
                    <label class="col-sm-4 control-label">HP Event Organizer</label>
                        
                    <div class="col-sm-5">
                        <input type="text" class="form-control" name="hp_eo" value="<?= $show_deo["hp_eo"]; ?>" placeholder="HP Event Organizer">
                    </div>
                </div>
                
                <!-- Contact Person Event Organizer -->
                <div class="form-group">
                    <label class="col-sm-4 control-label">Contact Person</label>
                        
                    <div class="col-sm-4">
                        <input type="text" class="form-control" name="cp_eo" value="<?= $show_deo["cp_eo"]; ?>" placeholder="Contact Person" required>
                    </div>
                </div>
                
                <!-- e-Mail Event Organizer -->
                <div class="form-group">
                    <label class="col-sm-4 control-label">e-Mail Event Organizer</label>
                        
                    <div class="col-sm-6">
                        <input type="text" class="form-control" name="email_eo" value="<?= $show_deo["email_eo"]; ?>" placeholder="Email Event Organizer">
                    </div>
                </div>
                
                <!-- Button -->
                <div class="form-group">
                    <label class="col-sm-4 control-label"></label>
                        
                    <div class="col-sm-2">
                        <button type="submit" id="submit" name="submit" class="btn btn-success">Simpan</button>
                    </div>
                    <div class="col-sm-2">
                        <button type="reset" id="reset" name="reset" class="btn btn-warning">Reset</button>
                    </div>
                </div>
            </form>
        <?php }}?>


<!-- Select2 -->
    <script>
        $(document).ready(function(){
            $(".select2").select2();
        });
    </script>