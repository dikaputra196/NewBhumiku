<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

//Required File
        require_once dirname(__FILE__)."/../components/templates/main.php";
        require_once dirname(__FILE__)."/../class/config.php";

        //Define Connection -> Database
        $db = new Database();
        $db->connect();

        //Call Template
        $template = new Template();

        //Start HTML
        $template->pageTitle="BHUMIKU Balai Pertemuan | New Jenis Setup";

        //Start Content
        $template->contentTitle="<span class='glyphicon glyphicon-edit'></span> New Jenis Setup";
        $template->startContent();
?>

<!-- Box Form -->
<div class="row">
    <div class="col-md-12">
        <?php $template->startBox(); ?>
            <strong></strong>
        <?php $template->conBox();?>
            <!-- Form New Jenis Setup -->
            <form class="form-horizontal" method="POST" action="<?= MAIN_URL ?>/action/act_save_jenissetup.php">
                <div style="margin-left:15px">
                    <h4><u></u></h4>
                </div>
                <!-- ID Jenis Setup -->
                <div class="form-group">
                    <label class="col-sm-2 control-label">ID Jenis Setup</label>
                        
                    <div class="col-sm-2">
                       <input type="text" class="form-control" name="id_jenis_setup" placeholder="ID Jenis Setup" readonly>
                    </div>
                </div>
                
                <!-- Nama Jenis Setup -->
                <div class="form-group">
                    <label class="col-sm-2 control-label">Nama Jenis Setup</label>
                        
                    <div class="col-sm-4">
                       <input type="text" class="form-control" name="nama_jenis_setup" placeholder="Nama Jenis Setup">
                    </div>
                </div>
                
                <!-- Deksripsi -->
                <div class="form-group">
                    <label class="col-sm-2 control-label">Deskripsi</label>
                        
                    <div class="col-sm-4">
                        <textarea class="form-control" name="deskripsi" placeholder="Deskripsi"></textarea>
                    </div>
                </div>
                
                <!-- Button -->
                <div class="form-group">
                    <label class="col-sm-2 control-label"></label>
                        
                    <div class="col-sm-1">
                        <button type="submit" id="submit" name="submit" class="btn btn-success">Simpan</button>
                    </div>
                    <div class="col-sm-1">
                        <button type="reset" id="reset" name="reset" class="btn btn-warning">Reset</button>
                    </div>
                </div>
            </form>
            
        <?php $template->endConBox();?>
        <?php $template->endBox();?>
    </div>
</div>
<!-- End Box Form -->
<!-- End Content -->
<?php $template->endContent(); ?>

    <!-- Place Script Here -->
    
        <!-- Datepicker -->
        <script>
            $(document).ready(function(){
                $("input.datepicker").Zebra_DatePicker();
            });
        </script>
    
        <!-- Select2 -->
        <script>
            $(document).ready(function(){
                $(".select2").select2();
            });
        </script>
        
    <!--// End Script Place -->
    
<!-- End </body> -->
<?php $template->endBody(); ?>

<!-- End HTML -->
<?php $template->endHtml(); ?>