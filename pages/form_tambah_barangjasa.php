<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

//Required File
        require_once dirname(__FILE__)."/../components/templates/main.php";
        require_once dirname(__FILE__)."/../class/config.php";

        //Define Connection -> Database
        $db = new Database();
        $db->connect();

        //Call Template
        $template = new Template();

        //Start HTML
        $template->pageTitle="BHUMIKU Balai Pertemuan | New Barang / Jasa";

        //Start Content
        $template->contentTitle="<span class='glyphicon glyphicon-edit'></span> New Barang / Jasa";
        $template->startContent();
?>

<!-- Box Form -->
<div class="row">
    <div class="col-md-12">
        <?php $template->startBox(); ?>
            <strong></strong>
        <?php $template->conBox();?>
            <!-- Form New Barang / Jasa -->
            <form class="form-horizontal" method="POST" action="<?= MAIN_URL ?>/action/act_save_databarangjasa.php">
                <div style="margin-left:15px">
                    <h4><u></u></h4>
                </div>
                <!-- Kode Barang -->
                <div class="form-group">
                    <label class="col-sm-2 control-label">Kode</label>
                        
                    <div class="col-sm-2">
                       <input type="text" class="form-control" name="kode_barang" placeholder="Kode Barang">
                    </div>
                </div>
                
                <!-- Nama Barang -->
                <div class="form-group">
                    <label class="col-sm-2 control-label">Nama</label>
                        
                    <div class="col-sm-4">
                       <input type="text" class="form-control" name="nama_barang" placeholder="Nama Barang">
                    </div>
                </div>
                
                <!-- Kode Satuan Besar -->
                <div class="form-group">
                    <label class="col-sm-2 control-label">Kode Satuan Besar</label>
                        
                    <div class="col-sm-3">
                        <select class="form-control select2" name="satuan_besar">
                            <option value=""> ---</option>
                            <?php
                                $db->select("tb_satuan","id_satuan,kode_satuan,nama_satuan",NULL,"is_active='1'");
                                $result = $db->getResult();
                                foreach($result as $satuan){
                            ?>
                            <option value="<?= $satuan['id_satuan']; ?>"><?= $satuan['kode_satuan']; ?> - <?= $satuan['nama_satuan']; ?></option>
                            <?php } ?>
                        </select>
                    </div>
                </div>
                
                <!-- Kode Satuan Kecil -->
                <div class="form-group">
                    <label class="col-sm-2 control-label">Kode Satuan Kecil</label>
                        
                    <div class="col-sm-3">
                        <select class="form-control select2" name="satuan_kecil">
                            <option value=""> ---</option>
                            <?php
                                $db->select("tb_satuan","id_satuan,kode_satuan,nama_satuan",NULL,"is_active='1'");
                                $results = $db->getResult();
                                foreach($results as $satuan_k){
                            ?>
                            <option value="<?= $satuan_k['id_satuan']; ?>"><?= $satuan_k['kode_satuan']; ?> - <?= $satuan_k['nama_satuan']; ?></option>
                            <?php } ?>
                        </select>
                    </div>
                </div>
                
                <!-- Nilai Konversi Satuan -->
                <div class="form-group">
                    <label class="col-sm-2 control-label">Nilai Konversi Satuan</label>
                        
                    <div class="col-sm-3">
                       <input type="number" class="form-control" name="konversi_satuan">
                    </div>
                </div>
                
                <!-- Jenis -->
                <div class="form-group">
                    <label class="col-sm-2 control-label">Jenis</label>
                        
                    <div class="col-sm-2">
                        <select class="form-control select2" name="jenis">
                            <option value=""> ---</option>
                            <option value="BARANG">Barang</option>
                            <option value="JASA">Jasa</option>
                        </select>
                    </div>
                </div>
                
                <!-- Harga Rata - rata -->
                <div class="form-group">
                    <label class="col-sm-2 control-label">Harga Rata - rata</label>
                        
                    <div class="col-sm-3">
                       <input type="number" class="form-control" name="harga_rata2">
                    </div>
                </div>
                
                <!-- On Stock -->
                <div class="form-group">
                    <label class="col-sm-2 control-label">On Stock</label>
                        
                    <div class="col-sm-3">
                       <input type="number" class="form-control" name="on_stock">
                    </div>
                </div>
                
                <!-- On Purchased -->
                <div class="form-group">
                    <label class="col-sm-2 control-label">On Purchased</label>
                        
                    <div class="col-sm-3">
                       <input type="number" class="form-control" name="on_purchased">
                    </div>
                </div>
                
                <!-- Inventory -->
                <div class="form-group">
                    <label class="col-sm-2 control-label"></label>
                        
                    <div class="col-sm-3">
                        <input type="checkbox" class="flat-red" id="inventory" name="inventory"> Masuk Inventory
                    </div>
                </div>
                
                <!-- Button -->
                <div class="form-group">
                    <label class="col-sm-2 control-label"></label>
                        
                    <div class="col-sm-1">
                        <button type="submit" id="submit" name="submit" class="btn btn-success">Simpan</button>
                    </div>
                    <div class="col-sm-1">
                        <button type="reset" id="reset" name="reset" class="btn btn-warning">Reset</button>
                    </div>
                </div>
            </form>
            
        <?php $template->endConBox();?>
        <?php $template->endBox();?>
    </div>
</div>
<!-- End Box Form -->
<!-- End Content -->
<?php $template->endContent(); ?>

    <!-- Place Script Here -->
    
        <!-- Datepicker -->
        <script>
            $(document).ready(function(){
                $("input.datepicker").Zebra_DatePicker();
            });
        </script>
    
        <!-- Select2 -->
        <script>
            $(document).ready(function(){
                $(".select2").select2();
            });
        </script>
        
        <!-- Flat red color scheme for iCheck -->
        <script>
            $('input[type="checkbox"].flat-red, input[type="radio"].flat-red').iCheck({
              checkboxClass: 'icheckbox_flat-green',
              radioClass   : 'iradio_flat-green'
            });
        </script>
    <!--// End Script Place -->
    
<!-- End </body> -->
<?php $template->endBody(); ?>

<!-- End HTML -->
<?php $template->endHtml(); ?>