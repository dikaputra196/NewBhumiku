<!--
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */ -->
<?php
    //Required File
    require_once dirname(__FILE__)."/../components/templates/main.php";
    require_once dirname(__FILE__)."/../class/config.php";

    //Define Connection -> Database
    $db = new Database();
    $db->connect();

    //Call Template
    $template = new Template();

    //Start HTML
    $template->pageTitle="BHUMIKU Balai Pertemuan | Edit Booking";

    //Start Content
    $template->contentTitle="<span class='glyphicon glyphicon-edit'></span> Edit Booking";
    $template->startContent();

    //Data from previous page
      if($_REQUEST["id_booking"]){

        //Data Booking
          $id_booking =  $db->escapeString($_REQUEST["id_booking"]);
          $db->select("tb_booking",'*',"tb_ruangan on tb_booking.id_ruangan = tb_ruangan.id_ruangan JOIN tb_jenis_setup on tb_booking.id_jenis_setup = tb_jenis_setup.id_jenis_setup JOIN tb_agent on tb_booking.id_agent = tb_agent.id_agent JOIN tb_eo on tb_booking.id_eo = tb_eo.id_eo","id_booking = '$id_booking'");
          $result = $db->getResult();

          foreach($result as $show_ub){
            $tanggal_pemesanan = $show_ub["TGL_BOOKING"];
            $nama_event = $show_ub["NAMA_EVENT"];
            $tanggal_event = $show_ub["TGL_EVENT"];
            $jam_mulai = $show_ub["JAM_MULAI"];
            $jam_selesai = $show_ub["JAM_SELESAI"];
            $id_ruangan = $show_ub["ID_RUANGAN"];
            $jumlah_undangan = $show_ub["JML_UNDANGAN"];
            $id_jenis_setup = $show_ub["ID_JENIS_SETUP"];
            $id_agent = $show_ub["ID_AGENT"];
            $id_eo = $show_ub["ID_EO"];
            $harga_sewa = $show_ub["HARGA_ROOM"];
            $nama_pemesan = $show_ub["NAMA_PEMESAN"];
            $alamat_pemesan = $show_ub["ALAMAT_PEMESAN"];
            $telepon_pemesan = $show_ub["TELEPON_PEMESAN"];
            $email_pemesan = $show_ub["EMAIL_PEMESAN"];
            $diskon_room = $show_ub["DISKON_ROOM"];
            $jaminan_kerusakan = $show_ub["JAMINAN_KERUSAKAN"];
            $catatan = $show_ub["REMARK_BOOKING"];
            $nama_ruangan = $show_ub["NAMA_RUANGAN"];
            $jenis_setup = $show_ub["NAMA_JENIS_SETUP"];
            $agent = $show_ub["NAMA_AGENT"];
            $eo = $show_ub["NAMA_EO"];
          }

        //Data Pembayaran
          $db->select("tb_payment_schedule",'*',NULL,"id_booking = '$id_booking'");
          $resultp=$db->getResult();
          $get = $db->getSql();
          $c = count($resultp);

          echo "<pre>";
          print_r($resultp);
          echo "<br>";
          print_r($c);
          echo "<br>";
          print_r($get);
          echo "<br>";

          foreach($resultp as $s){
            echo "<br>";
            echo $s[1];
            echo "<br>";
          }

          echo "</pre>";




      }

?>

<!-- Box Form -->
<div class="row">
    <div class="col-md-12">
        <?php $template->startBox(); ?>
            <strong></strong>
        <?php $template->conBox();?>
                <!-- Form Booking -->
                <form class="form-horizontal" method="POST">
                    <div style="margin-left:15px">
                        <h4><u>Informasi Event dan Ruangan</u></h4>
                    </div>
                    <!-- Tanggal Pemesanan -->
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Tanggal Pemesanan</label>

                        <div class="col-sm-3">
                            <input type="text" class="form-control datepicker" name="tanggal_pemesanan" placeholder="Tanggal Pemesanan" value="<?= $tanggal_pemesanan; ?>">
                        </div>
                    </div>

                    <!-- Nama Event -->
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Nama Event</label>

                        <div class="col-sm-5">
                            <input type="text" class="form-control" name="nama_event" placeholder="Nama Event" value="<?= $nama_event ?>">
                        </div>
                    </div>

                    <!-- Tanggal Event -->
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Tanggal Event</label>

                        <div class="col-sm-3">
                            <input type="text" class="form-control datepicker" name="tanggal_event" placeholder="Tanggal Event" value="<?= $tanggal_event ?>">
                        </div>
                    </div>

                    <!-- Waktu Mulai & Selesai -->
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Waktu</label>

                        <!-- Waktu Mulai -->
                        <div class="col-sm-2">
                            <small><i>Waktu Mulai</i></small>
                            <div class="input-group clockpicker">
                                <span class="input-group-addon">
                                    <span class="glyphicon glyphicon-time"></span>
                                </span>
                                <input type="text" class="form-control" name="jam_mulai" placeholder="Jam Mulai" value="<?= $jam_mulai; ?>">
                            </div>
                        </div>

                        <!-- Waktu Selesai -->
                        <div class="col-sm-2">
                            <small><i>Waktu Selesai</i></small>
                            <div class="input-group clockpicker">
                                <span class="input-group-addon">
                                    <span class="glyphicon glyphicon-time"></span>
                                </span>
                                <input type="text" class="form-control" name="jam_selesai" placeholder="Jam Selesai" value="<?= $jam_selesai; ?>">
                            </div>
                        </div>
                    </div>

                    <!-- Ruangan -->
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Ruangan</label>

                        <div class="col-sm-3">
                            <select class="form-control select2" name="ruangan">
                                <option value="<?= $id_ruangan; ?>"><?= $nama_ruangan; ?></option>
                                <option value=""> ---</option>
                                <?php
                                    $db->select("tb_ruangan","id_ruangan,nama_ruangan",NULL,"is_active='1'");
                                    $result_r = $db->getResult();
                                    foreach($result_r as $show_r){
                                ?>
                                <option value="<?= $show_r['id_ruangan']; ?>"><?= $show_r['nama_ruangan']; ?></option>
                                <?php
                                    }
                                ?>
                            </select>
                        </div>
                    </div>

                    <!-- Jumlah Undangan -->
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Jumlah Undangan</label>

                        <div class="col-sm-2">
                            <input type="number" class="form-control" name="jumlah_undangan" id="jumlah_undangan" value="<?= $jumlah_undangan; ?>">
                        </div>
                    </div>

                    <!-- Jenis Setup -->
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Jenis Setup</label>

                        <div class="col-sm-3">
                            <select class="form-control select2" name="jenis_setup">
                                <option value="<?= $id_jenis_setup ?>"><?= $jenis_setup; ?></option>
                                <option value=""> ---</option>
                                <?php
                                    $db->select("tb_jenis_setup","id_jenis_setup,nama_jenis_setup",NULL,"is_active='1'");
                                    $result_j = $db->getResult();
                                    foreach($result_j as $show_j){
                                ?>
                                <option value="<?= $show_j['id_jenis_setup']; ?>"><?= $show_j['nama_jenis_setup']; ?></option>
                                <?php
                                    }
                                ?>
                            </select>
                        </div>
                    </div>

                    <!-- Agent -->
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Agent</label>

                        <div class="col-sm-3">
                            <select class="form-control select2" name="agent">
                                <option value="<?= $id_agent ?>"><?= $agent; ?></option>
                                <option value=""> ---</option>
                                <?php
                                    $db->select("tb_agent","id_agent,nama_agent",NULL,"is_active='1'");
                                    $result_a = $db->getResult();
                                    foreach($result_a as $show_a){
                                ?>
                                <option value="<?= $show_a['id_agent']; ?>"> <?= $show_a['nama_agent']; ?> </option>
                                <?php
                                    }
                                ?>
                            </select>
                        </div>
                    </div>

                    <!-- Event Organizer -->
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Event Organizer</label>

                        <div class="col-sm-3">
                            <select class="form-control select2" name="event_organizer">
                                <option value="<?= $id_eo ?>"><?= $eo; ?></option>
                                <option value=""> ---</option>
                                <?php
                                    $db->select("tb_eo","id_eo,nama_eo",NULL,"is_active='1'");
                                    $result = $db->getResult();
                                    foreach($result as $show){
                                ?>
                                <option value="<?= $show['id_eo']; ?>"><?= $show['nama_eo']; ?></option>
                                <?php
                                    }
                                ?>
                            </select>
                        </div>
                    </div>

                    <!-- Harga Sewa -->
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Harga Sewa</label>

                        <div class="col-sm-3">
                            <div class="input-group">
                                <span class="input-group-addon">Rp.</span>
                                <input type="text" class="form-control" name="harga_sewa_r" readonly placeholder="Harga Sewa" value="<?= $harga_sewa; ?>">
                            </div>
                        </div>
                    </div>

                    <div style="margin-left:15px">
                        <h4><u>Informasi Pemesanan</u></h4>
                    </div>

                    <!-- Nama Pemesan -->
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Nama</label>

                        <div class="col-sm-4">
                                <input type="text" class="form-control" name="nama_pemesan" placeholder="Nama Pemesan" value="<?= $nama_pemesan; ?>">
                        </div>
                    </div>

                    <!-- Alamat Pemesan -->
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Alamat</label>

                        <div class="col-sm-6">
                                <input type="text" class="form-control" name="alamat_pemesan" placeholder="Alamat Pemesan" value="<?= $alamat_pemesan; ?>">
                        </div>
                    </div>

                    <!-- Telepon Pemesan -->
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Telepon</label>

                        <div class="col-sm-3">
                                <input type="text" class="form-control" name="telepon_pemesan" placeholder="Telepon Pemesan" value="<?= $telepon_pemesan; ?>">
                        </div>
                    </div>

                    <!-- Email Pemesan -->
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Email</label>

                        <div class="col-sm-4">
                                <input type="email" class="form-control" name="email_pemesan" placeholder="Email Pemesan"value="<?= $email_pemesan; ?>">
                        </div>
                    </div>

                    <div style="margin-left:15px">
                        <h4><u>Harga Termasuk</u></h4>
                    </div>

                    <!-- Count Item Included -->
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Jumlah Item Included</label>

                        <div class="col-sm-1">
                            <select class="form-control select2" name="count_item_in" id="count_item_in">
                                <option value=""> ---</option>
                            </select>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-2 control-label"></label>

                        <div class="col-sm-10" id="item_in">
                        </div>
                    </div>

                    <div style="margin-left:15px">
                        <h4><u>Penambahan Item dan Layanan</u></h4>
                    </div>

                    <!-- Count Penambahan Item dan Layanan -->
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Jumlah Item yang akan Ditambahkan</label>

                        <div class="col-sm-1">
                            <select class="form-control select2" name="count_item_ex" id="count_item_ex">
                                <option value=""> ---</option>
                            </select>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-2 control-label"></label>

                        <div class="col-sm-10" id="item_ex">
                        </div>
                    </div>

                    <!-- Jadwal Pembayaran -->
                    <div style="margin-left:15px">
                        <h4><u>Jadwal Pembayaran</u></h4>
                    </div>

                    <!-- Pembayaran 1 -->
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Pembayaran I</label>

                        <div class="col-sm-3">
                            <input type="text" class="form-control datepicker" name="pay_1">
                        </div>

                        <label class="col-sm-2 control-label">Alokasi I</label>

                        <div class="col-sm-2">
                            <div class="input-group">
                                <input type="number" class="form-control" name="alokasi_1">
                                <span class="input-group-addon">%</span>
                            </div>
                        </div>
                    </div>

                    <!-- Pembayaran 2 -->
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Pembayaran II</label>

                        <div class="col-sm-3">
                            <input type="text" class="form-control datepicker" name="pay_2">
                        </div>

                        <label class="col-sm-2 control-label">Alokasi II</label>

                        <div class="col-sm-2">
                            <div class="input-group">
                                <input type="number" class="form-control" name="alokasi_2">
                                <span class="input-group-addon">%</span>
                            </div>
                        </div>
                    </div>

                    <!-- Pembayaran 3 -->
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Pembayaran III</label>

                        <div class="col-sm-3">
                            <input type="text" class="form-control datepicker" name="pay_3">
                        </div>

                        <label class="col-sm-2 control-label">Alokasi III</label>

                        <div class="col-sm-2">
                            <div class="input-group">
                                <input type="number" class="form-control" name="alokasi_3">
                                <span class="input-group-addon">%</span>
                            </div>
                        </div>
                    </div>

                    <div style="margin-left:15px">
                        <h4><u>Diskon dan Jaminan</u></h4>
                    </div>

                    <!-- Harga Sewa Ruangan -->
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Harga Sewa Ruangan</label>

                        <div class="col-sm-4">
                            <div class="input-group">
                                <span class="input-group-addon">Rp.</span>
                                <input type="text" class="form-control" name="t_sewa_ruangan" readonly value="<?= $harga_sewa; ?>">
                            </div>
                        </div>
                    </div>

                    <!-- Diskon -->
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Diskon</label>

                        <div class="col-sm-4">
                            <div class="input-group">
                                <span class="input-group-addon">Rp.</span>
                                <input type="number" class="form-control" name="diskon" value="<?= $diskon_room; ?>">
                            </div>
                        </div>
                    </div>

                    <!-- Total Harga -->
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Total Harga</label>

                        <div class="col-sm-4">
                            <div class="input-group">
                                <span class="input-group-addon">Rp.</span>
                                <input type="text" class="form-control" name="total_harga" readonly>
                            </div>
                        </div>
                    </div>

                    <!-- Jaminan Kerusakan -->
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Jaminan Kerusakan</label>

                        <div class="col-sm-4">
                            <div class="input-group">
                                <span class="input-group-addon">Rp.</span>
                                <input type="text" class="form-control" name="jaminan_kerusakan" placeholder="Jaminan Kerusakan" value="<?= $jaminan_kerusakan; ?>">
                            </div>
                        </div>
                    </div>

                    <!-- Catatan -->
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Catatan</label>

                        <div class="col-sm-4">
                            <textarea class="form-control" name="catatan" placeholder="Catatan"><?= $catatan; ?></textarea>
                        </div>
                    </div>

                    <!-- Button -->
                    <div class="form-group">
                        <label class="col-sm-2 control-label"></label>

                        <div class="col-sm-1">
                            <button type="submit" id="submit" name="submit" class="btn btn-success">Simpan</button>
                        </div>
                        <div class="col-sm-1">
                            <button type="reset" id="reset" name="reset" class="btn btn-warning">Reset</button>
                        </div>
                    </div>
                </form>
        <?php $template->endConBox();?>
        <?php $template->endBox();?>
    </div>
</div>
<!-- End Box Form -->
<!-- End Content -->
<?php
    $template->endContent();
?>

<!-- Place Script Here -->
    <!-- Datepicker -->
    <script>
        $(document).ready(function(){
            $("input.datepicker").Zebra_DatePicker();
        });
    </script>

    <!-- Clockpicker -->
    <script>
        $(document).ready(function(){
            $(".clockpicker").clockpicker({donetext:"SET"});
        });
    </script>

    <!-- Select2 -->
    <script>
        $(document).ready(function(){
            $(".select2").select2();
        });
    </script>

    <!-- Option Item Included-->
    <script type="text/javascript">
        $(document).ready(function(){
            for(no=1; no <=6; no++)
            {
                $("#count_item_in").append($('<option>',
                {
                    value: no,
                    text : ""+no
                }));
            }
        });
    </script>

    <!-- Show Item Included -->
    <script type="text/javascript">
        $(document).ready(function(){
            $("#count_item_in").change(function(){
                var count_item_in = $("#count_item_in").val();
                $.ajax({
                    url: "<?= MAIN_URL ?>/action/show_select_item_in.php",
                    data: "count_item_in="+count_item_in,
                    cache: false,
                    success: function(msg)
                    {
                        $("#item_in").html(msg);
                    }
                });
            });
        });
    </script>

    <!-- Option Item Excluded-->
    <script type="text/javascript">
        $(document).ready(function(){
            for(no=1; no <=4; no++)
            {
                $("#count_item_ex").append($('<option>',
                {
                    value: no,
                    text : ""+no
                }));
            }
        });
    </script>

    <!-- Show Item Excluded -->
    <script type="text/javascript">
        $(document).ready(function(){
            $("#count_item_ex").change(function(){
                var count_item_ex = $("#count_item_ex").val();
                $.ajax({
                    url: "<?= MAIN_URL ?>/action/show_select_item_ex.php",
                    data: "count_item_ex="+count_item_ex,
                    cache: false,
                    success: function(msg)
                    {
                        $("#item_ex").html(msg);
                    }
                });
            });
        });
    </script>

<!--// End Script Place -->

<!-- End </body> -->
<?php $template->endBody(); ?>

<!-- End HTML -->
<?php
    $template->endHtml();
?>
