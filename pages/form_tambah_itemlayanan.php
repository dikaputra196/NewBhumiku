<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

//Required File
        require_once dirname(__FILE__)."/../components/templates/main.php";
        require_once dirname(__FILE__)."/../class/config.php";

        //Define Connection -> Database
        $db = new Database();
        $db->connect();

        //Call Template
        $template = new Template();

        //Start HTML
        $template->pageTitle="BHUMIKU Balai Pertemuan | New Item & Layanan";

        //Start Content
        $template->contentTitle="<span class='glyphicon glyphicon-edit'></span> New Item & Layanan";
        $template->startContent();
?>

<!-- Box Form -->
<div class="row">
    <div class="col-md-12">
        <?php $template->startBox(); ?>
            <strong></strong>
        <?php $template->conBox();?>
            <!-- Form New Satuan -->
            <form class="form-horizontal" method="POST" action="<?= MAIN_URL ?>/action/act_save_dataitemlayanan.php">
                <div style="margin-left:15px">
                    <h4><u></u></h4>
                </div>
                <!-- ID Item -->
                <div class="form-group">
                    <label class="col-sm-2 control-label">ID Item</label>
                        
                    <div class="col-sm-2">
                       <input type="text" class="form-control" name="id_item" placeholder="ID Item" readonly>
                    </div>
                </div>
                
                <!-- Kode Item -->
                <div class="form-group">
                    <label class="col-sm-2 control-label">Kode Item</label>
                        
                    <div class="col-sm-2">
                       <input type="text" class="form-control" name="kode_item" placeholder="Kode Item" required>
                    </div>
                </div>
                
                <!-- Nama Item -->
                <div class="form-group">
                    <label class="col-sm-2 control-label">Nama Item</label>
                        
                    <div class="col-sm-4">
                       <input type="text" class="form-control" name="nama_item" placeholder="Nama Item" required>
                    </div>
                </div>
                
                <!-- Harga Item -->
                <div class="form-group">
                    <label class="col-sm-2 control-label">Harga Item</label>

                    <div class="col-sm-3">
                        <div class="input-group">
                            <span class="input-group-addon">Rp.</span>
                            <input type="number" class="form-control" name="harga_item" id="harga_item" required>
                        </div>

                    </div>
                </div>
                
                <!-- Deskripsi Item -->
                <div class="form-group">
                    <label class="col-sm-2 control-label">Deskripsi Item</label>
                        
                    <div class="col-sm-4">
                        <textarea class="form-control" name="deskripsi_item" placeholder="Deskripsi Item"></textarea>
                    </div>
                </div>
                
                <!-- Button -->
                <div class="form-group">
                    <label class="col-sm-2 control-label"></label>
                        
                    <div class="col-sm-1">
                        <button type="submit" id="submit" name="submit" class="btn btn-success">Simpan</button>
                    </div>
                    <div class="col-sm-1">
                        <button type="reset" id="reset" name="reset" class="btn btn-warning">Reset</button>
                    </div>
                </div>
            </form>
            
        <?php $template->endConBox();?>
        <?php $template->endBox();?>
    </div>
</div>
<!-- End Box Form -->
<!-- End Content -->
<?php $template->endContent(); ?>

    <!-- Place Script Here -->
    
        <!-- Datepicker -->
        <script>
            $(document).ready(function(){
                $("input.datepicker").Zebra_DatePicker();
            });
        </script>
    
        <!-- Select2 -->
        <script>
            $(document).ready(function(){
                $(".select2").select2();
            });
        </script>
        
    <!--// End Script Place -->
    
<!-- End </body> -->
<?php $template->endBody(); ?>

<!-- End HTML -->
<?php $template->endHtml(); ?>