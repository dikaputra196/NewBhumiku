<!--
/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */-->
<?php
    //Required File
    require_once dirname(__FILE__)."/../components/templates/main.php";
    require_once dirname(__FILE__)."/../class/config.php";
    require_once dirname(__FILE__)."/../class/manual_connect.php";
    
    //Variable
    $kredit_awal=0;
    $debet_awal=0;
    
    //Select
    $query = mysqli_query($con, "SELECT
                                a.tgl_transaksi,
                                a.no_bukti,
                                c.nama_coa,
                                b.kode_coa,
                                b.posisi,
                                a.ref_jurnal,
                                b.jml_transaksi
                                FROM tb_jurnal a
                                JOIN
                                tb_detail_jurnal b
                                ON a.id_jurnal = b.id_jurnal
                                JOIN
                                tb_coa c
                                ON b.kode_coa = c.kode_coa ");
    
    //Call Template
    $template = new Template();
    
    //Start HTML
    $template->pageTitle="BHUMIKU Balai Pertemuan | Jurnal Umum";
    
    //Start Content
    $template->contentTitle="<span class='glyphicon glyphicon-check'></span> Jurnal Umum";
    $template->startContent();
?>

<!-- List -->
<div class="row">
    <div class="col-md-12">
        <?php $template->startBox(); ?>
            <strong></strong>
        <?php $template->conBox();?>
            <!-- Table -->
            <div class="col-md-12">
                <table class="table display table-responsive table-striped table-hover" id="tjurnal">
                        <thead>
                            <tr>
                                <td><strong>Tanggal</strong></td>
                                <td><strong>No.Bukti</strong></td>
                                <td><strong>Keterangan</strong></td>
                                <td><strong>Reff</strong></td>
                                <td><strong>Debet</strong></td>
                                <td><strong>Kredit</strong></td>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                                while($result_ju=mysqli_fetch_array($query)){
                            ?>
                                <tr>
                                    <td><?= $result_ju["tgl_transaksi"]; ?></td>
                                    <td><?= $result_ju["no_bukti"]; ?></td>
                                    <td><?= $result_ju["nama_coa"]; ?></td>
                                    <td><?= $result_ju["ref_jurnal"]; ?></td>
                                    <td>
                                        <?php
                                            if($result_ju["posisi"]==="D"){
                                                $total_debet=$debet_awal+=$result_ju["jml_transaksi"];
                                                echo number_format($result_ju["jml_transaksi"]);
                                            }
                                            else{
                                                echo "-";
                                            }
                                        ?>
                                    </td>
                                    <td>
                                        <?php
                                            if($result_ju["posisi"]=='K'){
                                                $total_kredit=$kredit_awal+=$result_ju["jml_transaksi"];
                                                echo number_format($result_ju["jml_transaksi"]);
                                            }
                                            else{
            					echo "-";
                                            }
                                        ?>
                                    </td>
                                </tr>
                            <?php } ?>
                        </tbody>
                        <tfoot>
                            <tr>
                                <td colspan="4"><strong>Total</strong></td>
                                <td><strong><?= number_format($total_debet); ?></strong</td>
                                <td><strong><?= number_format($total_kredit); ?></strong></td>
                            </tr>
                        </tfoot>
                    </table>
            </div>
        <?php $template->endConBox();?>
        <?php $template->endBox();?>
    </div>
</div>

<!-- End Content -->
<?php $template->endContent(); ?>

<!-- Place Script Here -->
    <!-- Data Tables -->
        <script>
            $(document).ready(function(){
                $("#tjurnal").dataTable({
                    "dom":'Bfrtip',
                    buttons: [
                        {
                            extend: 'print',
                            text: '<i class="fa fa-print"></i> Print'
                        },
                        { 
                            extend: 'excelHtml5',
                            text: '<i class="fa fa-file-excel-o"></i> Export to Excel'
                        }
                    ]
                });
            });
        </script>
<!-- //End Script Place -->

<!-- End </body> -->
<?php $template->endBody(); ?>

<!-- End HTML -->
<?php $template->endHtml(); ?>