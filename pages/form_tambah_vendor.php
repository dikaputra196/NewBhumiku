<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

//Required File
        require_once dirname(__FILE__)."/../components/templates/main.php";
        require_once dirname(__FILE__)."/../class/config.php";

        //Define Connection -> Database
        $db = new Database();
        $db->connect();

        //Call Template
        $template = new Template();

        //Start HTML
        $template->pageTitle="BHUMIKU Balai Pertemuan | New Vendor";

        //Start Content
        $template->contentTitle="<span class='glyphicon glyphicon-edit'></span> New Vendor";
        $template->startContent();
?>

<!-- Box Form -->
<div class="row">
    <div class="col-md-12">
        <?php $template->startBox(); ?>
            <strong></strong>
        <?php $template->conBox();?>
            <!-- Form New Vendor -->
            <form class="form-horizontal" method="POST" action="<?= MAIN_URL ?>/action/act_save_datavendor.php">
                <div style="margin-left:15px">
                    <h4><u></u></h4>
                </div>
                <!-- ID Vendor -->
                <div class="form-group">
                    <label class="col-sm-2 control-label">ID Vendor</label>
                        
                    <div class="col-sm-3">
                       <input type="text" class="form-control" name="id_vendor" placeholder="ID Vendor" readonly>
                    </div>
                </div>
                
                <!-- Nama Vendor -->
                <div class="form-group">
                    <label class="col-sm-2 control-label">Nama Vendor</label>
                        
                    <div class="col-sm-4">
                       <input type="text" class="form-control" name="nama_vendor" placeholder="Nama Vendor" required>
                    </div>
                </div>
                
                <!-- Alamat Vendor -->
                <div class="form-group">
                    <label class="col-sm-2 control-label">Alamat Vendor</label>
                        
                    <div class="col-sm-5">
                        <input type="text" class="form-control" name="alamat_vendor" placeholder="Alamat Vendor" required>
                    </div>
                </div>
                
                <!-- Telepon Vendor -->
                <div class="form-group">
                    <label class="col-sm-2 control-label">Telepon Vendor</label>
                        
                    <div class="col-sm-3">
                       <input type="text" class="form-control" name="telepon_vendor" placeholder="Telepon Vendor" required>
                    </div>
                </div>
                
                <!-- HP Vendor -->
                <div class="form-group">
                    <label class="col-sm-2 control-label">HP Vendor</label>
                        
                    <div class="col-sm-3">
                       <input type="text" class="form-control" name="HP_vendor" placeholder="HP Vendor">
                    </div>
                </div>
                
                <!-- Contact Person -->
                <div class="form-group">
                    <label class="col-sm-2 control-label">Contact Person</label>
                        
                    <div class="col-sm-4">
                       <input type="text" class="form-control" name="contact_person" placeholder="Contact Person" required>
                    </div>
                </div>
                
                <!-- eMail Vendor -->
                <div class="form-group">
                    <label class="col-sm-2 control-label">e-Mail Vendor</label>
                        
                    <div class="col-sm-4">
                       <input type="email" class="form-control" name="email_vendor" placeholder="Email Vendor">
                    </div>
                </div>
                
                <div style="margin-left:15px">
                    <h4><u>Informasi Pembayaran</u></h4>
                </div>
                
                <!-- NPWP -->
                <div class="form-group">
                    <label class="col-sm-2 control-label">NPWP</label>
                        
                    <div class="col-sm-3">
                       <input type="text" class="form-control" name="npwp" placeholder="NPWP" required>
                    </div>
                </div>
                
                <!-- BANK -->
                <div class="form-group">
                    <label class="col-sm-2 control-label">Bank</label>
                        
                    <div class="col-sm-3">
                       <input type="text" class="form-control" name="bank" placeholder="BANK">
                    </div>
                </div>
                
                <!-- No. Rekening -->
                <div class="form-group">
                    <label class="col-sm-2 control-label">No. Rekening</label>
                        
                    <div class="col-sm-3">
                       <input type="text" class="form-control" name="no_rekening" placeholder="No. Rekening">
                    </div>
                </div>
                
                <!-- Catatan -->
                <div class="form-group">
                    <label class="col-sm-2 control-label">Catatan</label>
                        
                    <div class="col-sm-4">
                        <textarea class="form-control" name="catatan" placeholder="Catatan"></textarea>
                    </div>
                </div>
                
                <!-- Button -->
                <div class="form-group">
                    <label class="col-sm-2 control-label"></label>
                        
                    <div class="col-sm-1">
                        <button type="submit" id="submit" name="submit" class="btn btn-success">Simpan</button>
                    </div>
                    <div class="col-sm-1">
                        <button type="reset" id="reset" name="reset" class="btn btn-warning">Reset</button>
                    </div>
                </div>
            </form>
            
        <?php $template->endConBox();?>
        <?php $template->endBox();?>
    </div>
</div>
<!-- End Box Form -->
<!-- End Content -->
<?php $template->endContent(); ?>

    <!-- Place Script Here -->
    
        <!-- Datepicker -->
        <script>
            $(document).ready(function(){
                $("input.datepicker").Zebra_DatePicker();
            });
        </script>
    
        <!-- Select2 -->
        <script>
            $(document).ready(function(){
                $(".select2").select2();
            });
        </script>
        
    <!--// End Script Place -->
    
<!-- End </body> -->
<?php $template->endBody(); ?>

<!-- End HTML -->
<?php $template->endHtml(); ?>