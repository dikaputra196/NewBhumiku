<!--
/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */ -->
<?php
    //Required File
    require_once dirname(__FILE__)."/../components/templates/main.php";
    require_once dirname(__FILE__)."/../class/config.php";
    
    //Define Connection -> Database
    $db = new Database();
    $db->connect();
    
    //Call Template
    $template = new Template();
    
    //Start HTML
    $template->pageTitle="BHUMIKU Balai Pertemuan | Info";
    
    //Start Content
    $template->contentTitle="<span class='glyphicon glyphicon-edit'></span> Info";
    $template->startContent();
?>

<!-- Data -->
<?php
    $db->select("tb_profil","id_profil,nama,alamat,telepon,fax,email,website,npwp,pkp,logo",NULL,"id_profil='1'");
    $result_in = $db->getResult();
    foreach($result_in as $show_in){
        $id_profil = $show_in["id_profil"];
        $nama = $show_in["nama"];
        $alamat = $show_in["alamat"];
        $telepon = $show_in["telepon"];
        $fax = $show_in["fax"];
        $email = $show_in["email"];
        $website = $show_in["website"];
        $npwp = $show_in["npwp"];
        $pkp = $show_in["pkp"];
        $logo = $show_in["logo"];
    }
?>

<!-- Box Form -->
<div class="row">
    <div class="col-md-12">
        <?php $template->startBox(); ?>
            <strong></strong>
        <?php $template->conBox();?>
                <!-- Form Booking -->
                <form class="form-horizontal" method="POST">
                    <div style="margin-left:15px">
                        <h4><u>Informasi Umum</u></h4>
                    </div>
                    
                    <!-- Nama -->
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Nama Perusahaan</label>
                        
                        <div class="col-md-3">
                            <input type="text" class="form-control" name="nama_perusahaan" value="<?= $nama; ?>">
                        </div>
                    </div>
                    
                    <!-- Alamat -->
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Alamat Perusahaan</label>
                        
                        <div class="col-md-5">
                            <input type="text" class="form-control" name="alamat_perusahaan" value="<?= $alamat; ?>">
                        </div>
                    </div>
                    
                    <!-- Telepon Perusahaan -->
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Telepon Perusahaan</label>
                        
                        <div class="col-md-2">
                            <input type="text" class="form-control" name="telepon_perusahaan" value="<?= $telepon; ?>">
                        </div>
                    </div>
                    
                    <!-- Fax Perusahaan -->
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Fax Perusahaan</label>
                        
                        <div class="col-md-2">
                            <input type="text" class="form-control" name="fax_perusahaan" value="<?= $fax; ?>">
                        </div>
                    </div>
                    
                    <!-- Email -->
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Email Perusahaan</label>
                        
                        <div class="col-md-3">
                            <input type="text" class="form-control" name="email_perusahaan" value="<?= $email; ?>">
                        </div>
                    </div>
                    
                    <!-- Website -->
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Website Perusahaan</label>
                        
                        <div class="col-md-3">
                            <input type="text" class="form-control" name="website_perusahaan" value="<?= $website; ?>">
                        </div>
                    </div>
                    
                    <div style="margin-left:15px">
                        <h4><u>Informasi Pajak</u></h4>
                    </div>
                    
                    <!-- NPWP -->
                    <div class="form-group">
                        <label class="col-sm-2 control-label">N.P.W.P</label>
                        
                        <div class="col-md-3">
                            <input type="text" class="form-control" name="npwp_perusahaan" value="<?= $npwp; ?>">
                        </div>
                    </div>
                    
                    <!-- Nomor PKP -->
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Nomor P.K.P</label>
                        
                        <div class="col-md-3">
                            <input type="text" class="form-control" name="pkp_perusahaan" value="<?= $pkp; ?>">
                        </div>
                    </div>
                    
                    <div style="margin-left:15px">
                        <h4><u>Logo</u></h4>
                    </div>
                    
                    <!-- Logo -->
                    <div class="form-group">
                        <label class="col-sm-2 control-label"></label>
                        
                        <div class="col-md-5">
                            <img src="<?= MAIN_URL ?>/gambar/<?= $logo; ?>" style="height:100px;width:100px;">
                        </div>
                    </div>
                    
                    <div class="form-group">
                        <label class="col-sm-2 control-label"></label>
                        
                        <div class="col-md-5">
                            <input type="file" name="logo"/>
                        </div>
                    </div>
                    
                    <!-- Button -->
                    <div class="form-group">
                        <label class="col-sm-2 control-label"></label>
                        
                        <div class="col-sm-1">
                            <button type="submit" id="submit" name="submit" class="btn btn-success">Simpan</button>
                        </div>
                        <div class="col-sm-1">
                            <button type="reset" id="reset" name="reset" class="btn btn-warning">Reset</button>
                        </div>
                    </div>
                    
                </form>
        <?php $template->endConBox();?>
        <?php $template->endBox();?>
    </div>
</div>
<!-- End Box Form -->
<!-- End Content -->
<?php
    $template->endContent();
?>

<!-- Place Script Here -->
    <!-- Datepicker -->
    <script>
        $(document).ready(function(){
            $("input.datepicker").Zebra_DatePicker();
        });
    </script>
        
    <!-- Select2 -->
    <script>
        $(document).ready(function(){
            $(".select2").select2();
        });
    </script>

<!--// End Script Place -->

<!-- End </body> -->
<?php $template->endBody(); ?>

<!-- End HTML -->
<?php
    $template->endHtml();
?>