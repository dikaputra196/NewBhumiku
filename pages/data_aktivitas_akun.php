<!--
/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */-->
<?php
    //Required File
    require_once dirname(__FILE__)."/../components/templates/main.php";
    require_once dirname(__FILE__)."/../class/config.php";
    require_once dirname(__FILE__)."/../class/manual_connect.php";
    
    //Call Template
    $template = new Template();
    
    //Define Connection -> Database
    $db = new Database();
    $db->connect();
    
    //Start HTML
    $template->pageTitle="BHUMIKU Balai Pertemuan | Aktivitas Akun";
    
    //Start Content
    $template->contentTitle="<span class='glyphicon glyphicon-check'></span> Aktivitas Akun";
    $template->startContent();
?>

<!-- List -->
<div class="row">
    <div class="col-md-12">
        <?php $template->startBox(); ?>
            <strong></strong>
        <?php $template->conBox();?>
            <!-- Search with Booking Date, Event Date, and Event Name -->
            <div class="row">
                <!-- Kode Akun -->
                <div class="col-md-4">
                    <div class="form-group">
                        <label>Kode Akun : </label>

                        <div class="input-group">
                            <div class="input-group-addon">
                                <i class="fa fa-building"></i>
                            </div>
                            <select class="form-control select2" name="kode_akun" id="kode_akun">
                                <option value=""> ---</option>
                                <?php
                                    $db->select("tb_coa","kode_coa,nama_coa,kode_parent,jenis_coa,saldo_normal",NULL,"kode_parent NOT IN(0)");
                                    $result_ka = $db->getResult();
                                    foreach($result_ka as $show_ka){
                                ?>
                                    <option value="<?= $show_ka["kode_coa"] ?>"><?= $show_ka["kode_coa"];?> - <?= $show_ka["nama_coa"]; ?></option>
                                <?php } ?>
                            </select>
                        </div>
                    </div>
                </div>
                
                <!-- Dari Tanggal -->
                <div class="col-md-3">
                    <div class="form-group">
                        <label>Dari Tanggal  : </label>

                        <div class="input-group">
                            <div class="input-group-addon">
                                <i class="fa fa-calendar"></i>
                            </div>
                            <input class="form-control datepicker" type="text" readonly="readonly" name="dari_tanggal" id="dari_tanggal">
                        </div>
                    </div>
                </div>
                <!-- Sampai Tanggal -->
                <div class="col-md-3">
                    <div class="form-group">
                        <label>Sampai Tanggal : </label>

                        <div class="input-group">
                            <div class="input-group-addon">
                                <i class="fa fa-calendar"></i>
                            </div>
                            <input class="form-control datepicker" type="text" readonly="readonly" name="sampai_tanggal" id="sampai_tanggal">
                        </div>
                    </div>
                </div>
                <!-- Button -->
                <div class="col-md-2">
                    <div class="form-group">
                        <label>&nbsp</label>
                        <div class="input-group">
                            
                            <button type="button" id="tampil" class="btn btn-info"> <i class="glyphicon glyphicon-search"></i>&nbsp; Cari</button
                        </div>
                        
                    </div>
                </div>
                
            </div>
            <!-- Table -->
            <div class="col-md-12">
                <table class="table display table-responsive table-striped table-hover" id="taktivitas">
                    <thead>
                            <tr>
                                <td><strong>Tanggal</strong></td>
                                <td><strong>Akun</strong></td>
                                <td><strong>No.Bukti</strong></td>
                                <td><strong>Keterangan</strong></td>
                                <td><strong>Reff</strong></td>
                                <td><strong>Debet</strong></td>
                                <td><strong>Kredit</strong></td>
                                <td><strong>Saldo</strong></td>
                            </tr>
                        </thead>
                        <tbody id="tbody">
                        </tbody>
                </table>
            </div>
        <?php $template->endConBox();?>
        <?php $template->endBox();?>
    </div>
</div>

<!-- End Content -->
<?php $template->endContent(); ?>

<!-- Place Script Here -->
   <!-- Zebra Date Picker -->
    <script>
        $(document).ready(function(){
            $("input.datepicker").Zebra_DatePicker();
        });
    </script>
    
    <!-- Select2 -->
    <script>
        $(document).ready(function(){
            $(".select2").select2();
        });
    </script>
    
    <!-- Onclick Action -->
    <script type="text/javascript">
      $(document).ready(function(){
        $("#tampil").click(function(){
          var kode_akun = $("#kode_akun").val();
          var dari_tanggal = $("#dari_tanggal").val();
          var sampai_tanggal = $("#sampai_tanggal").val();
          $("#tbody").html("<tr><td colspan=12><img src='<?=MAIN_URL?>/components/images/load.gif'></td></tr>")
          $.ajax({
            url: "<?= MAIN_URL ?>/action/get_data_aktivitasakun.php",
            data: "kode_akun="+kode_akun+"&dari_tanggal="+dari_tanggal+"&sampai_tanggal="+sampai_tanggal,
            success: function(msg)
              {
                $("#tbody").html(msg);
              }
          });
        });
      });
    </script>
<!-- //End Script Place -->

<!-- End </body> -->
<?php $template->endBody(); ?>

<!-- End HTML -->
<?php $template->endHtml(); ?>