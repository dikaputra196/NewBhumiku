<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

//Required File
    require_once dirname(__FILE__)."/../class/config.php";
    require_once dirname(__FILE__)."/../components/templates/main.php";
        
    //Call Template
        $template = new Template();
        
    //Define Connection -> Database
        $db = new Database();
        $db->connect(); 
        
        
        if($_REQUEST["rowid"]){
            $id = $_REQUEST['rowid'];
            $db->select("tb_satuan","id_satuan,kode_satuan,nama_satuan",NULL,"id_satuan='$id'");
            $result = $db->getResult();
            foreach($result as $show_ds){

?>
            
            <form class="form-horizontal" method="POST" action="<?= MAIN_URL ?>/action/act_update_data_satuan.php" id="form">
                <div style="margin-left:15px">
                    <h4><u></u></h4>
                </div>
                <input type="hidden" name="id_satuan" id="id_satuan" value="<?= $id ?>"/>
                <!-- Kode Satuan -->
                <div class="form-group">
                    <label class="col-sm-3 control-label">Kode Satuan</label>
                        
                    <div class="col-sm-2">
                        <input type="text" class="form-control" name="kode_satuan" placeholder="Kode Satuan" value="<?= $show_ds["kode_satuan"]; ?>">
                    </div>
                </div>
                
                <!-- Nama Satuan -->
                <div class="form-group">
                    <label class="col-sm-3 control-label">Nama Satuan</label>
                        
                    <div class="col-sm-4">
                        <input type="text" class="form-control" name="nama_satuan" placeholder="Nama Satuan" value="<?= $show_ds["nama_satuan"]; ?>">
                    </div>
                </div>
                
                <!-- Button -->
                <div class="form-group">
                    <label class="col-sm-3 control-label"></label>
                        
                    <div class="col-sm-2">
                        <button type="submit" id="submit" name="submit" class="btn btn-success">Simpan</button>
                    </div>
                    <div class="col-sm-2">
                        <button type="reset" id="reset" name="reset" class="btn btn-warning">Reset</button>
                    </div>
                </div>
            </form>
        <?php }}?>

    <!-- Select2 -->
        <script>
            $(document).ready(function(){
                $(".select2").select2();
            });
        </script>
        
        <script></script>