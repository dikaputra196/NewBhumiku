<!--
/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */ -->
<?php
    //Required File
    require_once dirname(__FILE__)."/../components/templates/main.php";
    require_once dirname(__FILE__)."/../class/config.php";
    
    //Define Connection -> Database
    $db = new Database();
    $db->connect();
    
    //Call Template
    $template = new Template();
    
    //Start HTML
    $template->pageTitle="BHUMIKU Balai Pertemuan | Mutasi Kas";
    
    //Start Content
    $template->contentTitle="<span class='glyphicon glyphicon-edit'></span> Mutasi Kas";
    $template->startContent();
?>

<!-- Box Form -->
<div class="row">
    <div class="col-md-12">
        <?php $template->startBox(); ?>
            <strong></strong>
        <?php $template->conBox();?>
                <!-- Form Mutasi Kas -->
                <form class="form-horizontal" method="POST" action="<?= MAIN_URL ?>/action/act_simpan_mutasikas.php">
                    <div style="margin-left:15px">
                        <h4><u>Informasi Transaksi</u></h4>
                    </div>
                    
                    <!-- Tanggal Transaksi -->
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Tanggal Transaksi</label>
                        
                        <div class="col-sm-3">
                            <input type="text" class="form-control datepicker" name="tanggal_transaksi" placeholder="Tanggal Transaksi">
                        </div>
                    </div>
                    
                    <!-- No. Bukti -->
                    <div class="form-group">
                        <label class="col-sm-2 control-label">No. Bukti</label>
                        
                        <div class="col-sm-3">
                            <input type="text" class="form-control" name="no_bukti" placeholder="No. Bukti"/>
                        </div>
                    </div>
                    
                    <!-- Keterangan -->
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Keterangan</label>
                        
                        <div class="col-sm-4">
                            <textarea class="form-control" name="keterangan"></textarea>
                        </div>
                    </div>
                    
                    <div style="margin-left:15px">
                        <h4><u>Akun - akun Termutasi</u></h4>
                    </div>
                    
                    <!-- Dari Akun -->
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Dari Akun</label>
                        
                        <div class="col-sm-5">
                            <select class="form-control select2" name="dari_akun">
                                <option value=""> ---</option>
                                <?php
                                    $db->select("tb_coa","kode_coa,nama_coa",NULL,"kode_parent > 0 AND kode_coa NOT IN ('110000','120000','130000','140000','150000','160000','210000','220000','230000','310000','410000','420000','430000','510000','520000','610000','620000','630000','640000','650000')");
                                    $result_da = $db->getResult();
                                    foreach($result_da as $show_da){
                                ?>
                                    <option value="<?= $show_da["kode_coa"]; ?>"><?= $show_da["kode_coa"]; ?> - <?= $show_da["nama_coa"]; ?></option>
                                <?php } ?>
                            </select>
                        </div>
                    </div>
                    
                    <!-- Ke Akun -->
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Ke Akun</label>
                        
                        <div class="col-sm-5">
                            <select class="form-control select2" name="ke_akun">
                                <option value=""> ---</option>
                                <?php
                                    $db->select("tb_coa","kode_coa,nama_coa",NULL,"kode_parent > 0 AND kode_coa NOT IN ('110000','120000','130000','140000','150000','160000','210000','220000','230000','310000','410000','420000','430000','510000','520000','610000','620000','630000','640000','650000')");
                                    $result_k = $db->getResult();
                                    foreach($result_k as $show_k){
                                ?>
                                    <option value="<?= $show_k["kode_coa"]; ?>"><?= $show_k["kode_coa"]; ?> - <?= $show_k["nama_coa"]; ?></option>
                                <?php } ?>
                            </select>
                        </div>
                    </div>
                    
                    <!-- Nominal Transaksi -->
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Nominal Transaksi</label>
                        
                        <div class="col-sm-3">
                            <div class="input-group">
                                <span class="input-group-addon">Rp.</span>
                                <input type="number" class="form-control" name="nominal_transaksi">
                            </div>
                        </div>
                    </div>
                    
                    <!-- Button -->
                    <div class="form-group">
                        <label class="col-sm-2 control-label"></label>
                        
                        <div class="col-sm-1">
                            <button type="submit" id="submit" name="submit" class="btn btn-success">Simpan</button>
                        </div>
                        <div class="col-sm-1">
                            <button type="reset" id="reset" name="reset" class="btn btn-warning">Reset</button>
                        </div>
                    </div>
                </form>
        <?php $template->endConBox();?>
        <?php $template->endBox();?>
    </div>
</div>
<!-- End Box Form -->
<!-- End Content -->
<?php $template->endContent(); ?>

<!-- Place Script Here -->
    <!-- Datepicker -->
    <script>
        $(document).ready(function(){
            $("input.datepicker").Zebra_DatePicker();
        });
    </script>
    
    <!-- Clockpicker -->
    <script>
        $(document).ready(function(){
            $(".clockpicker").clockpicker({donetext:"SET"});
        });
    </script>
    
    <!-- Select2 -->
    <script>
        $(document).ready(function(){
            $(".select2").select2();
        });
    </script>
<!--// End Script Place -->

<!-- End </body> -->
<?php $template->endBody(); ?>

<!-- End HTML -->
<?php $template->endHtml(); ?>