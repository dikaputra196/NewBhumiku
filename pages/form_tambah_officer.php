<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
    //Required File
        require_once dirname(__FILE__)."/../components/templates/main.php";
        require_once dirname(__FILE__)."/../class/config.php";

        //Define Connection -> Database
        $db = new Database();
        $db->connect();

        //Call Template
        $template = new Template();

        //Start HTML
        $template->pageTitle="BHUMIKU Balai Pertemuan | New User";

        //Start Content
        $template->contentTitle="<span class='glyphicon glyphicon-edit'></span> New User";
        $template->startContent();
?>

<!-- Box Form -->
<div class="row">
    <div class="col-md-12">
        <?php $template->startBox(); ?>
            <strong></strong>
        <?php $template->conBox();?>
            <!-- Form New User -->
            <form class="form-horizontal" method="POST" action="<?= MAIN_URL ?>/action/act_save_dataofficer.php">
                
                <!-- ID User -->
                <div class="form-group">
                    <label class="col-sm-2 control-label">ID User</label>
                        
                    <div class="col-sm-3">
                       <input type="text" class="form-control" name="id_user" placeholder="ID User" readonly>
                    </div>
                </div>
                
                <!-- Nama Lengkap -->
                <div class="form-group">
                    <label class="col-sm-2 control-label">Nama Lengkap</label>
                        
                    <div class="col-sm-5">
                       <input type="text" class="form-control" name="nama_lengkap" placeholder="Nama Lengkap" required>
                    </div>
                </div>
                
                <!-- Jabatan -->
                <div class="form-group">
                    <label class="col-sm-2 control-label">Jabatan</label>
                        
                    <div class="col-sm-3">
                        <select class="form-control select2" name="jabatan" required>
                            <option value=""> ---</option>
                            <option value="ACCOUNTING">ACCOUNTING</option>
                            <option value="ADMINISTRATOR">ADMINISTRATOR</option>
                            <option value="FINANCE">FINANCE</option>
                            <option value="MANAGER">MANAGER</option>
                            <option value="OPERATION">OPERATION</option>
                            <option value="RESERVATION">RESERVATION</option>
                        </select>
                    </div>
                </div>
                
                <!-- e-Mail -->
                <div class="form-group">
                    <label class="col-sm-2 control-label">E-Mail</label>
                        
                    <div class="col-sm-4">
                        <input type="email" class="form-control" name="email" placeholder="E-Mail" required>
                    </div>
                </div>
               
                <div style="margin-left:15px">
                    <h4><u>Informasi Login</u></h4>
                </div>
                
                <!-- Username -->
                <div class="form-group">
                    <label class="col-sm-2 control-label">Username</label>
                        
                    <div class="col-sm-4">
                       <input type="text" class="form-control" name="username" placeholder="Username" required>
                    </div>
                </div>
                
                <!-- Password -->
                <div class="form-group">
                    <label class="col-sm-2 control-label">Password</label>
                        
                    <div class="col-sm-4">
                       <input type="password" class="form-control" name="password" placeholder="Password" required>
                    </div>
                </div>
                
                <!-- Button -->
                <div class="form-group">
                    <label class="col-sm-2 control-label"></label>
                        
                    <div class="col-sm-1">
                        <button type="submit" id="submit" name="submit" class="btn btn-success">Simpan</button>
                    </div>
                    <div class="col-sm-1">
                        <button type="reset" id="reset" name="reset" class="btn btn-warning">Reset</button>
                    </div>
                </div>
            </form>
        
        <?php $template->endConBox();?>
        <?php $template->endBox();?>
    </div>
</div>
<!-- End Box Form -->
<!-- End Content -->
<?php $template->endContent(); ?>

    <!-- Place Script Here -->
    
        <!-- Datepicker -->
        <script>
            $(document).ready(function(){
                $("input.datepicker").Zebra_DatePicker();
            });
        </script>
    
        <!-- Select2 -->
        <script>
            $(document).ready(function(){
                $(".select2").select2();
            });
        </script>
        
    <!--// End Script Place -->
    
<!-- End </body> -->
<?php $template->endBody(); ?>

<!-- End HTML -->
<?php $template->endHtml(); ?>