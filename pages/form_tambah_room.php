<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
    //Required File
        require_once dirname(__FILE__)."/../components/templates/main.php";
        require_once dirname(__FILE__)."/../class/config.php";

        //Define Connection -> Database
        $db = new Database();
        $db->connect();

        //Call Template
        $template = new Template();

        //Start HTML
        $template->pageTitle="BHUMIKU Balai Pertemuan | New Room";

        //Start Content
        $template->contentTitle="<span class='glyphicon glyphicon-edit'></span> New Room";
        $template->startContent();
?>

<!-- Box Form -->
<div class="row">
    <div class="col-md-12">
        <?php $template->startBox(); ?>
            <strong></strong>
        <?php $template->conBox();?>
            <!-- Form New Room -->
            <form class="form-horizontal" method="POST" action="<?= MAIN_URL ?>/action/act_save_dataroom.php">
                <div style="margin-left:15px">
                    <h4><u>Informasi Umum</u></h4>
                </div>
                <!-- ID Room -->
                <div class="form-group">
                    <label class="col-sm-2 control-label">ID Ruangan</label>
                        
                    <div class="col-sm-3">
                       <input type="text" class="form-control" name="id_room" placeholder="ID Room" readonly>
                    </div>
                </div>
                
                <!-- Nama Room -->
                <div class="form-group">
                    <label class="col-sm-2 control-label">Nama Ruangan</label>
                        
                    <div class="col-sm-3">
                       <input type="text" class="form-control" name="nama_room" placeholder="Nama Room">
                    </div>
                </div>
                
                <!-- Kapasitas Room -->
                <div class="form-group">
                    <label class="col-sm-2 control-label">Kapasitas Ruangan</label>
                        
                    <div class="col-sm-3">
                        <input type="number" class="form-control" name="kapasitas_room" placeholder="Kapasitas Room">
                    </div>
                </div>
                
                <div style="margin-left:15px">
                    <h4><u>Dimensi Ruangan</u></h4>
                </div>
                
                <!-- Dimensi Room -->
                <div class="form-group">
                    <label class="col-sm-2 control-label">Dimensi Ruangan</label>
                        
                    <div class="col-sm-2">
                        <small><i>Panjang</i></small>
                        <div class="input-group">
                            <input type="number" class="form-control" name="panjang" id="panjang">
                            <span class="input-group-addon">m</span>
                        </div>
                    </div>
                    
                     <div class="col-sm-2">
                        <small><i>Lebar</i></small>
                        <div class="input-group">
                            <input type="number" class="form-control" name="lebar" id="lebar">
                            <span class="input-group-addon">m</span>
                        </div>
                    </div>
                </div>
                
                <div style="margin-left:15px">
                        <h4><u>Harga Sewa Standar</u></h4>
                </div>
                
                <!-- Full Day -->
                <div class="form-group">
                    <label class="col-sm-2 control-label">Full Day</label>
                        
                    <div class="col-sm-3">
                        <div class="input-group">
                            <span class="input-group-addon">Rp.</span>
                            <input type="number" class="form-control" name="full_day" id="full_day" placeholder="Harga Full Day">
                        </div>
                    </div>
                </div>
                
                <!-- Half Day -->
                <div class="form-group">
                    <label class="col-sm-2 control-label">Half Day</label>
                        
                    <div class="col-sm-3">
                        <div class="input-group">
                            <span class="input-group-addon">Rp.</span>
                            <input type="number" class="form-control" name="half_day" id="half_day" placeholder="Harga Half Day">
                        </div>
                    </div>
                </div>
                
                <!-- Hourly Extra -->
                <div class="form-group">
                    <label class="col-sm-2 control-label">Hourly Extra</label>
                        
                    <div class="col-sm-3">
                        <div class="input-group">
                            <span class="input-group-addon">Rp.</span>
                            <input type="number" class="form-control" name="hourly" id="hourly" placeholder="Hourly Extra">
                        </div>
                    </div>
                </div>
                
                <!-- Button -->
                <div class="form-group">
                    <label class="col-sm-2 control-label"></label>
                        
                    <div class="col-sm-1">
                        <button type="submit" id="submit" name="submit" class="btn btn-success">Simpan</button>
                    </div>
                    <div class="col-sm-1">
                        <button type="reset" id="reset" name="reset" class="btn btn-warning">Reset</button>
                    </div>
                </div>
            </form>
        
        <?php $template->endConBox();?>
        <?php $template->endBox();?>
    </div>
</div>
<!-- End Box Form -->
<!-- End Content -->
<?php $template->endContent(); ?>

    <!-- Place Script Here -->
    
        <!-- Datepicker -->
        <script>
            $(document).ready(function(){
                $("input.datepicker").Zebra_DatePicker();
            });
        </script>
    
    <!--// End Script Place -->
    
<!-- End </body> -->
<?php $template->endBody(); ?>

<!-- End HTML -->
<?php $template->endHtml(); ?>