<!--
/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */-->
<?php
    //Required File
    require_once dirname(__FILE__)."/../components/templates/main.php";
    require_once dirname(__FILE__)."/../class/config.php";
    
    //Define Connection -> Database
    $db = new Database();
    $db->connect();
    
    //Call Template
    $template = new Template();
    
    //Start HTML
    $template->pageTitle="BHUMIKU Balai Pertemuan | System Log";
    
    //Start Content
    $template->contentTitle="<span class='glyphicon glyphicon-check'></span> System Log";
    $template->startContent();
?>

<!-- Log -->
<div class="row">
    <div class="col-md-12">
        <?php $template->startBox(); ?>
            <strong></strong>
        <?php $template->conBox();?>
            <!-- Table -->
            <div class="col-md-12">
                <table class="table table-responsive table-striped table-hover" id="tsystem">
                        <thead>
                            <tr>
                                <td>Waktu</td>
                                <td>User</td>
                                <td>Jenis</td>
                                <td>Waktu</td>
                                <td>Detail</td>
                            </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
            </div>
        <?php $template->endConBox();?>
        <?php $template->endBox();?>
    </div>
</div>

<!-- End Content -->
<?php
    $template->endContent();
?>

<!-- Place Script Here -->

    <!-- Zebra Date Picker -->
    <script>
        $(document).ready(function(){
            $("input.datepicker").Zebra_DatePicker();
        });
    </script>

    <!-- Data Tables -->
    <script>
        $(document).ready(function(){
            $("#tsystem").dataTable({
                "dom":'Bfrtip',
                buttons: [
                    {
                        extend: 'print',
                        text: '<i class="fa fa-print"></i> Print'
                    },
                    { 
                        extend: 'excelHtml5',
                        text: '<i class="fa fa-file-excel-o"></i> Export to Excel'
                    },
                ],
                "bProcessing": true,
                "sAjaxSource": "<?=MAIN_URL?>/action/act_data_system_log.php",
                "aoColumns": [
                    {mData: 'tgl_history'},
                    {mData: 'nama_lengkap'},
                    {mData: 'type_history'},
                    {mData: 'tgl_history'},
                    {mData: 'detail_history'}
                ]
                
            });
        });
    </script>

    
    
<!-- //End Script Place -->

<!-- End </body> -->
<?php $template->endBody(); ?>

<!-- End HTML -->
<?php
    $template->endHtml();
?>