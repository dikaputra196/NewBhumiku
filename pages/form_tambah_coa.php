<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

//Required File
        require_once dirname(__FILE__)."/../components/templates/main.php";
        require_once dirname(__FILE__)."/../class/config.php";

        //Define Connection -> Database
        $db = new Database();
        $db->connect();

        //Call Template
        $template = new Template();

        //Start HTML
        $template->pageTitle="BHUMIKU Balai Pertemuan | New Chart of Accounts";

        //Start Content
        $template->contentTitle="<span class='glyphicon glyphicon-edit'></span> New Chart of Accounts";
        $template->startContent();
?>

<!-- Box Form -->
<div class="row">
    <div class="col-md-12">
        <?php $template->startBox(); ?>
            <strong></strong>
        <?php $template->conBox();?>
            <!-- Form New COA -->
            <form class="form-horizontal" method="POST" action="<?= MAIN_URL ?>/action/act_save_datacoa.php">
                <div style="margin-left:15px">
                    <h4><u></u></h4>
                </div>
                <!-- Induk / Parent Akun -->
                <div class="form-group">
                    <label class="col-sm-2 control-label">Induk / Parent Akun</label>
                        
                    <div class="col-sm-3">
                        <select class="form-control select2" name="parent_akun">
                            <option value=""> ---</option>
                            <?php
                                $db->select("tb_coa","kode_coa,nama_coa",NULL,"kode_coa IN('100000','110000','120000','130000','140000','150000','160000','200000','210000','220000','230000','300000','310000','320000','400000','410000','420000','430000','500000','510000','520000','600000','610000','620000','630000','640000','650000')");
                                $result = $db->getResult();
                                foreach($result as $show_pk){
                            ?>
                            <option value="<?= $show_pk["kode_coa"]; ?>"><?= $show_pk["kode_coa"]; ?> - <?= $show_pk["nama_coa"]; ?></option>
                            <?php
                                }
                            ?>
                        </select>
                    </div>
                </div>
                
                <!-- Kode Akun -->
                <div class="form-group">
                    <label class="col-sm-2 control-label">Kode Akun</label>
                        
                    <div class="col-sm-3">
                       <input type="text" class="form-control" name="kode_akun" placeholder="Kode Akun">
                    </div>
                </div>
                
                <!-- Nama Akun -->
                <div class="form-group">
                    <label class="col-sm-2 control-label">Nama Akun</label>
                        
                    <div class="col-sm-5">
                       <input type="text" class="form-control" name="nama_akun" placeholder="Nama Akun">
                    </div>
                </div>
                
                <!-- Jenis Akun -->
                <div class="form-group">
                    <label class="col-sm-2 control-label">Jenis Akun</label>
                        
                    <div class="col-sm-3">
                        <select class="form-control select2" name="jenis_akun">
                            <option value=""> ---</option>
                            <option value="HEADER">Header</option>
                            <option value="DETAIL">Detail</option>
                       </select>
                    </div>
                </div>
                
                <!-- Saldo Normal -->
                <div class="form-group">
                    <label class="col-sm-2 control-label">Saldo Normal</label>
                        
                    <div class="col-sm-3">
                        <select class="form-control select2" name="saldo_normal">
                            <option value=""> ---</option>
                            <option value="DEBET">Debet</option>
                            <option value="KREDIT">Kredit</option>
                       </select>
                    </div>
                </div>
                
                <!-- Button -->
                <div class="form-group">
                    <label class="col-sm-2 control-label"></label>
                        
                    <div class="col-sm-1">
                        <button type="submit" id="submit" name="submit" class="btn btn-success">Simpan</button>
                    </div>
                    <div class="col-sm-1">
                        <button type="reset" id="reset" name="reset" class="btn btn-warning">Reset</button>
                    </div>
                </div>
            </form>
            
        <?php $template->endConBox();?>
        <?php $template->endBox();?>
    </div>
</div>
<!-- End Box Form -->
<!-- End Content -->
<?php $template->endContent(); ?>

    <!-- Place Script Here -->
    
        <!-- Datepicker -->
        <script>
            $(document).ready(function(){
                $("input.datepicker").Zebra_DatePicker();
            });
        </script>
    
        <!-- Select2 -->
        <script>
            $(document).ready(function(){
                $(".select2").select2();
            });
        </script>
        
    <!--// End Script Place -->
    
<!-- End </body> -->
<?php $template->endBody(); ?>

<!-- End HTML -->
<?php $template->endHtml(); ?>