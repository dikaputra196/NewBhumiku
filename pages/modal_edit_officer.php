<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

    //Required File
        require_once dirname(__FILE__)."/../class/config.php";
        require_once dirname(__FILE__)."/../components/templates/main.php";
    
    //Call Template
        $template = new Template();
    
    //Define Connection -> Database
        $db = new Database();
        $db->connect(); 
        
        
        if($_REQUEST["rowid"]){
            $id = $_REQUEST['rowid'];
            $db->select("tb_user","id_user,nama_lengkap,jabatan,user_name,user_pass,email",NULL,"id_user='$id'");
            $result = $db->getResult();
            foreach($result as $show_du){

?>

            <form class="form-horizontal" method="POST" action="<?= MAIN_URL ?>/action/act_update_data_officer.php">
                
                <!-- ID User -->
                <div class="form-group">
                    <label class="col-sm-3 control-label">ID User</label>
                        
                    <div class="col-sm-3">
                        <input type="text" class="form-control" name="id_user" placeholder="ID User" readonly value="<?= $show_du["id_user"]; ?>">
                    </div>
                </div>
                
                <!-- Nama Lengkap -->
                <div class="form-group">
                    <label class="col-sm-3 control-label">Nama Lengkap</label>
                        
                    <div class="col-sm-6">
                       <input type="text" class="form-control" name="nama_lengkap" placeholder="Nama Lengkap" required value="<?= $show_du["nama_lengkap"]; ?>">
                    </div>
                </div>
                
                <!-- Jabatan -->
                <div class="form-group">
                    <label class="col-sm-3 control-label">Jabatan</label>
                        
                    <div class="col-sm-6">
                        <select class="form-control select2" name="jabatan" required>
                            <option value="<?= $show_du["jabatan"]; ?>"> <?= $show_du["jabatan"]; ?>"</option>
                            <option value=""> ---</option>
                            <option value="ACCOUNTING">ACCOUNTING</option>
                            <option value="ADMINISTRATOR">ADMINISTRATOR</option>
                            <option value="FINANCE">FINANCE</option>
                            <option value="MANAGER">MANAGER</option>
                            <option value="OPERATION">OPERATION</option>
                            <option value="RESERVATION">RESERVATION</option>
                        </select>
                    </div>
                </div>
                
                <!-- e-Mail -->
                <div class="form-group">
                    <label class="col-sm-3 control-label">E-Mail</label>
                        
                    <div class="col-sm-6">
                        <input type="email" class="form-control" name="email" placeholder="E-Mail" required value="<?= $show_du["email"]; ?>">
                    </div>
                </div>
               
                <div style="margin-left:15px">
                    <h4><u>Informasi Login</u></h4>
                </div>
                
                <!-- Username -->
                <div class="form-group">
                    <label class="col-sm-3 control-label">Username</label>
                        
                    <div class="col-sm-4">
                       <input type="text" class="form-control" name="username" placeholder="Username" required value="<?= $show_du["user_name"]; ?>">
                    </div>
                </div>
                
                <!-- Password -->
                <div class="form-group">
                    <label class="col-sm-3 control-label">Password</label>
                        
                    <div class="col-sm-4">
                       <input type="password" class="form-control" name="password" placeholder="Password" required value="<?= $show_du["user_pass"]; ?>">
                    </div>
                </div>
                
                <!-- Button -->
                <div class="form-group">
                    <label class="col-sm-3 control-label"></label>
                        
                    <div class="col-sm-2">
                        <button type="submit" id="submit" name="submit" class="btn btn-success">Simpan</button>
                    </div>
                    <div class="col-sm-2">
                        <button type="reset" id="reset" name="reset" class="btn btn-warning">Reset</button>
                    </div>
                </div>
            </form>
        <?php }}?>

    <script>
        $(document).ready(function(){
            $(".select2").select2();
        });
    </script>