<!--
/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */-->
<?php
    //Required File
    require_once dirname(__FILE__)."/../components/templates/main.php";
    require_once dirname(__FILE__)."/../class/config.php";
    require_once dirname(__FILE__)."/../class/manual_connect.php";
    
    //Call Template
    $template = new Template();
    
    //Start HTML
    $template->pageTitle="BHUMIKU Balai Pertemuan | Cancel Booking";
    
    //Start Content
    $template->contentTitle="<span class='glyphicon glyphicon-check'></span> Cancel Booking";
    $template->startContent();
?>
<!-- Search with Booking Date, Event Date, and Event Name -->
<div class="row">
    <div class="col-md-12">
        <?php $template->startBox(); ?>
            <strong> Data Booking </strong>
        <?php $template->conBox();?>
            <div class="row">
                <!-- Tanggal Booking -->
                <div class="col-md-3">
                    <div class="form-group">
                        <label>Tanggal Booking : </label>

                        <div class="input-group">
                            <div class="input-group-addon">
                                <i class="fa fa-calendar"></i>
                            </div>
                            <input class="form-control" type="text" readonly="readonly" id="tanggal_booking">
                        </div>
                    </div>
                </div>
                <!-- Tanggal Event -->
                <div class="col-md-3">
                    <div class="form-group">
                        <label>Tanggal Event : </label>

                        <div class="input-group">
                            <div class="input-group-addon">
                                <i class="fa fa-calendar"></i>
                            </div>
                            <input class="form-control" type="text" readonly="readonly" id="tanggal_event">
                        </div>
                    </div>
                </div>
                <!-- Nama Event -->
                <div class="col-md-6">
                    <div class="form-group">
                        <label>Nama Event : </label>

                        <div class="input-group">
                            <div class="input-group-addon">
                                <i class="fa fa-building"></i>
                            </div>
                            <input class="form-control" type="text" id="nama_event">
                        </div>
                    </div>
                </div>
            </div>
            <!-- Table -->
            <div class="col-md-12">
                    <table class="table table-responsive table-striped table-hover">
                        <thead align="center" style="background-color:#00a65a; font-weight:bold;">
                            <tr style="color:white;">
                                <td>No.</td>
                                <td>Tanggal</td>
                                <td>Nama Event</td>
                                <td>Ruangan</td>
                                <td>Waktu</td>
                                <td>Organizer</td>
                                <td>Pemesan</td>
                                <td>Action</td>
                            </tr>
                        </thead>
                        <tbody id="tbody">
                            <?php
                                $no=0;
                                $query = mysqli_query($con,"SELECT
                                                            a.id_booking,
                                                            a.tgl_event,
                                                            a.nama_event,
                                                            b.nama_ruangan,
                                                            CONCAT(jam_mulai,' - ', jam_selesai) AS Waktu,
                                                            c.nama_eo,
                                                            a.nama_pemesan,
                                                            a.status_booking
                                                            FROM
                                                            tb_booking a
                                                            INNER JOIN tb_eo c
                                                            ON c.id_eo = a.id_eo
                                                            INNER JOIN tb_ruangan b
                                                            ON b.id_ruangan = a.id_ruangan
                                                            where a.status_booking='TENTATIVE' ");
                                while($result_c = mysqli_fetch_array($query)){
                                    $no++;
                            ?>
                            <tr>
                                <td><?= $no; ?></td>
                                <td><?= $result_c["tgl_event"]; ?></td>
                                <td><?= $result_c["nama_event"] ?></td>
                                <td><?= $result_c["nama_ruangan"]; ?></td>
                                <td><?= $result_c["Waktu"]; ?></td>
                                <td><?= $result_c["nama_eo"]; ?></td>
                                <td><?= $result_c["nama_pemesan"]; ?></td>
                                <td><a class="delete-item" href="javascript:void(0)" data-id="<?= $result_c["id_booking"]; ?>"><button class="btn btn-sm btn-danger"><span class="glyphicon glyphicon-remove"></span> Delete</button></a></td>
                            </tr>
                            <?php } ?>
                        </tbody>
                    </table>
            </div>
        <?php $template->endConBox();?>
        <?php $template->endBox();?>
    </div>
</div>

<!-- End Content -->
<?php $template->endContent(); ?>

<!-- Place Script Here -->

    <!-- Zebra Date Picker -->
    <script>
        $(document).ready(function(){
            $("input.datepicker").Zebra_DatePicker();
        });
    </script>

    <!-- Dialog -->
    <script>
    $(document).ready(function(){
      $(".delete-item").click(function(e){
          e.preventDefault();
          var id_booking = $(this).attr('data-id');
          var parent = $(this).parent("td").parent("tr");
          bootbox.dialog({
            message: "Apakah anda yakin akan menghapus Data ini?",
            title: "<i class='glyphicon glyphicon-trash'></i> Hapus? ",
            buttons: {
            success: {
            label: "<i class='fa fa-times'></i> Tidak",
            className: "btn-success",
            callback: function(){
              $(".bootbox").modal("hide");
            }
          },
          danger: {
            label: "<i class='fa fa-check'></i> Hapus!",
            className: "btn-danger",
            callback: function(){
              $.ajax({
                type: "POST",
                url: "<?= MAIN_URL ?>/action/act_cancel_booking.php",
                data: "id_booking="+id_booking
              })
              .done(function(response){
                bootbox.alert(response);
                parent.fadeOut('slow');
              })
              .fail(function(){
                bootbox.alert('Error.....');
              });
            }
          }
          }
          });
      });
    });
  </script>
  
  <!-- Get Data from Nama Event -->
    <script type="text/javascript">
      $(document).ready(function(){
        $("#nama_event").keyup(function(){
          var nama_event = $("#nama_event").val();
          $("#tbody").html("<center><tr><td colspan='8'><img src='<?=MAIN_URL?>/components/images/load.gif'></td></tr></center>")
            $.ajax({
              url: "<?=MAIN_URL?>/action/act_get_data_nama_event_cb.php",
              data: "nama_event="+nama_event,
              cache: false,
              success: function(msg){
                $("#tbody").html(msg);
              }
            });
        });
      });
    </script>
  
    <!-- Get Data from Tanggal Booking -->
    <script type="text/javascript">
      $("#tanggal_booking").Zebra_DatePicker({
        format :'Y-m-d',
        onSelect: function() { 
            $(this).change();
            var tanggal_booking = $("#tanggal_booking").val();
            $("#tbody").html("<tr><td colspan='8'><center><img src='<?=MAIN_URL?>/components/images/load.gif'></center></td></tr>") 
            $.ajax({
            url: "<?=MAIN_URL?>/action/act_get_data_tanggal_booking_cb.php",
            data: "tanggal_booking="+tanggal_booking,
            cache: false,
            success: function(msg){
              $("#tbody").html(msg);
                }
            });
          }
      });
    </script>
    
    <!-- Get Data from Tanggal Event -->
    <script type="text/javascript">
      $("#tanggal_event").Zebra_DatePicker({
        format :'Y-m-d',
        onSelect: function() { 
            $(this).change();
            var tanggal_event = $("#tanggal_event").val();
            $("#tbody").html("<center><tr><td colspan='8'><img src='<?=MAIN_URL?>/components/images/load.gif'></td></tr></center>") 
            $.ajax({
            url: "<?=MAIN_URL?>/action/act_get_data_tanggal_event_cb.php",
            data: "tanggal_event="+tanggal_event,
            cache: false,
            success: function(msg){
              $("#tbody").html(msg);
                }
            });
          }
      });
    </script>
    
<!-- //End Script Place -->

<!-- End </body> -->
<?php $template->endBody(); ?>

<!-- End HTML -->
<?php $template->endHtml(); ?>