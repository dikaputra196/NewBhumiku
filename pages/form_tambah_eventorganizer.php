<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

//Required File
        require_once dirname(__FILE__)."/../components/templates/main.php";
        require_once dirname(__FILE__)."/../class/config.php";

        //Define Connection -> Database
        $db = new Database();
        $db->connect();

        //Call Template
        $template = new Template();

        //Start HTML
        $template->pageTitle="BHUMIKU Balai Pertemuan | New Event Organizer";

        //Start Content
        $template->contentTitle="<span class='glyphicon glyphicon-edit'></span> New Event Organizer";
        $template->startContent();
?>

<!-- Box Form -->
<div class="row">
    <div class="col-md-12">
        <?php $template->startBox(); ?>
            <strong></strong>
        <?php $template->conBox();?>
            <!-- Form New Event Organizer -->
            <form class="form-horizontal" method="POST" action="<?= MAIN_URL ?>/action/act_save_eventorganizer.php">
                <div style="margin-left:15px">
                    <h4><u></u></h4>
                </div>
                <!-- ID Event Organizer -->
                <div class="form-group">
                    <label class="col-sm-2 control-label">ID Event Organizer</label>
                        
                    <div class="col-sm-2">
                       <input type="text" class="form-control" name="id_eo" placeholder="ID Event Organizer" readonly>
                    </div>
                </div>
                
                <!-- Nama Event Organizer -->
                <div class="form-group">
                    <label class="col-sm-2 control-label">Nama Event Organizer</label>
                        
                    <div class="col-sm-4">
                       <input type="text" class="form-control" name="nama_eo" placeholder="Nama Event Organizer" required>
                    </div>
                </div>
                
                <!-- Alamat Event Organizer -->
                <div class="form-group">
                    <label class="col-sm-2 control-label">Alamat Event Organizer</label>
                        
                    <div class="col-sm-5">
                       <input type="text" class="form-control" name="alamat_eo" placeholder="Alamat Event Organizer" required>
                    </div>
                </div>
                
                <!-- Telepon Event Organizer -->
                <div class="form-group">
                    <label class="col-sm-2 control-label">Telepon Event Organizer</label>
                        
                    <div class="col-sm-3">
                       <input type="text" class="form-control" name="telepon_eo" placeholder="Telepon Event Organizer" required>
                    </div>
                </div>
                
                <!-- HP Event Organizer -->
                <div class="form-group">
                    <label class="col-sm-2 control-label">HP Event Organizer</label>
                        
                    <div class="col-sm-3">
                       <input type="text" class="form-control" name="hp_eo" placeholder="HP Event Organizer">
                    </div>
                </div>
                
                <!-- Contact Person Event Organizer -->
                <div class="form-group">
                    <label class="col-sm-2 control-label">Contact Person</label>
                        
                    <div class="col-sm-4">
                       <input type="text" class="form-control" name="cp_eo" placeholder="Contact Person" required>
                    </div>
                </div>
                
                <!-- e-Mail Event Organizer -->
                <div class="form-group">
                    <label class="col-sm-2 control-label">e-Mail Event Organizer</label>
                        
                    <div class="col-sm-5">
                       <input type="text" class="form-control" name="email_eo" placeholder="Email Event Organizer">
                    </div>
                </div>
                
                <!-- Button -->
                <div class="form-group">
                    <label class="col-sm-2 control-label"></label>
                        
                    <div class="col-sm-1">
                        <button type="submit" id="submit" name="submit" class="btn btn-success">Simpan</button>
                    </div>
                    <div class="col-sm-1">
                        <button type="reset" id="reset" name="reset" class="btn btn-warning">Reset</button>
                    </div>
                </div>
            </form>
            
        <?php $template->endConBox();?>
        <?php $template->endBox();?>
    </div>
</div>
<!-- End Box Form -->
<!-- End Content -->
<?php $template->endContent(); ?>

    <!-- Place Script Here -->
    
        <!-- Datepicker -->
        <script>
            $(document).ready(function(){
                $("input.datepicker").Zebra_DatePicker();
            });
        </script>
    
        <!-- Select2 -->
        <script>
            $(document).ready(function(){
                $(".select2").select2();
            });
        </script>
        
    <!--// End Script Place -->
    
<!-- End </body> -->
<?php $template->endBody(); ?>

<!-- End HTML -->
<?php $template->endHtml(); ?>