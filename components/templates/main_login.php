<?php
    class loginPage{
        public $pageTitle = "BHUMIKU Balai Pertemuan";
        public $contentTitle;
        public $contentDescription;
        public $activePage;
        public $logo="../images/icon-bhumiku.png";
        public $boxTitle;
        public $boxFooter;
        
        //Start Content
        public function startContent(){
            $this->startHtml();
            echo $this->contentTemplate();
        }
        
        //End Content
        public function endContent(){
            echo $this->footerTemplate();
        }
        
        //Start HTML
        public function startHtml(){
            echo "
                <!DOCTYPE html>
                <html>
                <head>
		<title>$this->pageTitle</title>
		<link rel='stylesheet' href='".MAIN_URL."/components/templates/style.css'>
		<link rel='stylesheet' href='".MAIN_URL."/components/templates/bg.css'>
                <!-- jQuery 3 -->
                <script src='".MAIN_URL."/components/jquery/dist/jquery.min.js'></script>
		<!-- Bootstrap 3.3.7 -->
                <link rel='stylesheet' href='".MAIN_URL."/components/bootstrap/dist/css/bootstrap.min.css'>
                <!-- Icon -->
                <link rel='icon' href='".MAIN_URL."/components/images/bhumiku-png.ico'>
                <!-- Bootstrap 3.3.7 -->
                <script src='".MAIN_URL."/components/bootstrap/dist/js/bootstrap.min.js'></script>
                <!-- Bootbox -->
                <script src='".MAIN_URL."/components/bootbox/bootbox.min.js'></script>

                <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
                <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
                <!--[if lt IE 9]>
                <script src='https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js'></script>
                <script src='https://oss.maxcdn.com/respond/1.4.2/respond.min.js'></script>
                <![endif]-->
                <!-- Google Font -->
                <link href='https://fonts.googleapis.com/css?family=Arimo' rel='stylesheet'>
                <style>
                    *{
                        font-family: 'Arimo', sans-serif;
                     }
                </style>
	</head>
            ";
        }
        
        public function endHtml(){
            echo "</html>";
        }
        
         private function contentTemplate(){
            ob_start();
            include "login_content.php";
            $val = ob_get_contents();
            ob_end_clean();

            return $val;
        }
    
        private function footerTemplate(){
            ob_start();
            include "login_footer.php";
            $val = ob_get_contents();
            ob_end_clean();

            return $val;
        }
    }
?>
