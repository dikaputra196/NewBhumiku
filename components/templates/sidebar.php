<!-- Left side column. contains the sidebar -->
  <aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      <!-- Sidebar user panel -->
      <div class="user-panel">
        <div class="pull-left image">
          <img src="<?=MAIN_URL?>/components/images/usr.png" class="img-circle" alt="User Image">
        </div>
        <div class="pull-left info">
          <a href="#"></a>
          <p><small><?= $_SESSION["nama_lengkap"]; ?></small></p>
          <a href="#"></a>
        </div>
      </div>
      <!-- sidebar menu: : style can be found in sidebar.less -->
      <ul class="sidebar-menu" data-widget="tree">
          
        <li class="header">MAIN NAVIGATION</li>
        
        <li class="treeview">
          <a href="#">
            <i class="fa fa-th-list"></i>
            <span>Master Data</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="<?= MAIN_URL ?>/pages/data_room.php"><i class="fa fa-home"></i> Data Room</a></li>
            <li><a href="<?= MAIN_URL ?>/pages/data_officer.php"><i class="fa fa-user"></i> Data Officer</a></li>
            <li><a href="<?= MAIN_URL ?>/pages/data_vendor.php"><i class="fa fa-institution"></i> Data Vendor</a></li>
            <li><a href="<?= MAIN_URL ?>/pages/data_agent.php"><i class="fa fa-users"></i> Data Agent</a></li>
            <li><a href="<?= MAIN_URL ?>/pages/data_satuan.php"><i class="fa fa-gg"></i> Data Satuan</a></li>
            <li><a href="<?= MAIN_URL ?>/pages/data_barang_jasa.php"><i class="fa fa-cubes"></i> Data Barang / Jasa</a></li>
            <li><a href="<?= MAIN_URL ?>/pages/data_item_layanan.php"><i class="fa fa-cubes"></i> Data Item Barang / Layanan</a></li>
            <li><a href="<?= MAIN_URL ?>/pages/data_jenis_setup.php"><i class="fa fa-houzz"></i> Data Jenis Setup Room</a></li>
            <li><a href="<?= MAIN_URL ?>/pages/data_event_organizer.php"><i class="fa fa-institution"></i> Data Event Organizer</a></li>
            <li><a href="<?= MAIN_URL ?>/pages/data_coa.php"><i class="fa fa-bar-chart"></i> Chart of Accounts</a></li>
          </ul>
        </li>
        
        <li class="treeview">
          <a href="#">
            <i class="fa fa-suitcase"></i>
            <span>Account Payable</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="<?= MAIN_URL ?>/pages/form_order_pembelian.php"><i class="fa fa-shopping-cart"></i> Order Pembelian</a></li>
            <li><a href="#"><i class="fa fa-download"></i> Penerimaan</a></li>
            <li><a href="#"><i class="fa fa-upload"></i> Pembayaran</a></li>
            <li><a href="#"><i class="fa fa-history"></i> Utang</a></li>
            <li><a href="<?= MAIN_URL ?>/pages/form_refund_dana.php"><i class="fa fa-share"></i> Refund Dana</a></li>
          </ul>
        </li>
        
        <li class="treeview">
          <a href="#">
            <i class="fa fa-book"></i> <span>General Ledger</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li class="treeview">
              <a href="#"><i class="fa fa-bank"></i> Kas dan Bank
                <span class="pull-right-container">
                  <i class="fa fa-angle-left pull-right"></i>
                </span>
              </a>
              <ul class="treeview-menu">
                <li><a href="<?= MAIN_URL ?>/pages/form_petty_cash.php"><i class="fa fa-money"></i> Petty Cash Event</a></li>
                <li><a href="<?= MAIN_URL ?>/pages/form_mutasi_kas.php"><i class="fa fa-sitemap"></i> Mutasi Kas</a></li>
                <li><a href="#"><i class="fa fa-bank"></i> Akun Bank</a></li>
              </ul>
            </li>
            
            <li class="treeview">
              <a href="#"><i class="fa fa-leanpub"></i> Jurnal
                <span class="pull-right-container">
                  <i class="fa fa-angle-left pull-right"></i>
                </span>
              </a>
              <ul class="treeview-menu">
                <li><a href="<?= MAIN_URL ?>/pages/data_jurnal_umum.php"><i class="fa fa-book"></i> Jurnal Umum</a></li>
                <li><a href="<?= MAIN_URL ?>/pages/data_aktivitas_akun.php"><i class="fa fa-bar-chart"></i> Aktivitas Akun</a></li>
                <li><a href="<?= MAIN_URL ?>/pages/form_entri_jurnal.php"><i class="fa fa-edit"></i> Entri Jurnal</a></li>
              </ul>
            </li>
            
            <li><a href="#"><i class="fa fa-dollar"></i> Cost of Sales</a></li>
            <li><a href="#"><i class="fa fa-line-chart"></i> Laba / Rugi</a></li>
            <li><a href="#"><i class="fa fa-balance-scale"></i> Neraca</a></li>
          </ul>
        </li>
        
        <li class="treeview">
          <a href="#">
            <i class="fa fa-file-text"></i>
            <span>Laporan</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="<?= MAIN_URL ?>/pages/system_log.php"><i class="fa fa-history"></i> System Log</a></li>
            <li><a href="#"><i class="fa fa-book"></i> Laporan Penjualan</a></li>
          </ul>
        </li>
        
        
        <li class="treeview" onclick="location.href='<?= MAIN_URL ?>/pages/form_info.php'">
          <a href="#">
                <i class="fa fa-info"></i>
                <span>Info Perusahaan</span>
          </a>
        </li>
      
      </ul>
    </section>
    <!-- /.sidebar -->
  </aside>
