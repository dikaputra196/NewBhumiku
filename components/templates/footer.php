        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->
    <!-- jQuery 3 -->
    <script src="<?= MAIN_URL ?>/components/jquery/dist/jquery.min.js"></script>
    <!-- Bootstrap 3.3.7 -->
    <script src="<?= MAIN_URL ?>/components/bootstrap/dist/js/bootstrap.min.js"></script>
    <!-- SlimScroll -->
    <script src="<?= MAIN_URL ?>/components/jquery-slimscroll/jquery.slimscroll.min.js"></script>
    <!-- FastClick -->
    <script src="<?= MAIN_URL ?>/components/fastclick/lib/fastclick.js"></script>
    <!-- AdminLTE App -->
    <script src="<?= MAIN_URL ?>/dist/js/adminlte.min.js"></script>
    <!-- Select2 -->
    <script src="<?= MAIN_URL ?>/components/select2/dist/js/select2.full.min.js"></script>
    <!-- InputMask -->
    <script src="<?= MAIN_URL ?>/plugins/input-mask/jquery.inputmask.js"></script>
    <script src="<?= MAIN_URL ?>/plugins/input-mask/jquery.inputmask.date.extensions.js"></script>
    <script src="<?= MAIN_URL ?>/plugins/input-mask/jquery.inputmask.extensions.js"></script>
    <!-- date-range-picker -->
    <script src="<?= MAIN_URL ?>/components/moment/min/moment.min.js"></script>
    <script src="<?= MAIN_URL ?>/components/bootstrap-daterangepicker/daterangepicker.js"></script>
    <!-- bootstrap datepicker -->
    <script src="<?= MAIN_URL ?>/components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
    <!-- bootstrap color picker -->
    <script src="<?= MAIN_URL ?>/components/bootstrap-colorpicker/dist/js/bootstrap-colorpicker.min.js"></script>
    <!-- bootstrap time picker -->
    <script src="<?= MAIN_URL ?>/plugins/timepicker/bootstrap-timepicker.min.js"></script>
    <!-- iCheck 1.0.1 -->
    <script src="<?= MAIN_URL ?>/plugins/iCheck/icheck.min.js"></script>
    <!-- Zebra Date Picker -->
    <script src="<?= MAIN_URL ?>/components/zebra-datepicker/dist/zebra_datepicker.min.js"></script>
    <!-- Bootbox -->
    <script src="<?= MAIN_URL ?>/components/bootbox/bootbox.min.js"></script>
    <!-- clockpicker -->
    <script src="<?= MAIN_URL ?>/components/clockpicker/dist/bootstrap-clockpicker.min.js"></script>
    <!-- DataTables -->
    <script src="<?= MAIN_URL ?>/components/datatables/datatables.min.js"></script>
    <script src="<?= MAIN_URL ?>/components/datatables/DataTables-1.10.16/js/dataTables.bootstrap.min.js"></script>
    <script src="<?= MAIN_URL ?>/components/datatables/Buttons-1.4.2/js/buttons.bootstrap.min.js"></script>
    <script src="<?= MAIN_URL ?>/components/datatables/Buttons-1.4.2/js/buttons.print.min.js"></script>
    <script src="<?= MAIN_URL ?>/components/datatables/Buttons-1.4.2/js/buttons.html5.min.js"></script>
    
    
    <!-- Jam -->
    <script type="text/javascript">
    function time() {
            var d = new Date();
            var weekday = new Array(7);
            weekday[0] = "Minggu";
            weekday[1] = "Senin";
            weekday[2] = "Selasa";
            weekday[3] = "Rabu";
            weekday[4] = "Kamis";
            weekday[5] = "Jumat";
            weekday[6] = "Sabtu";

            var month = new Array();
            month[0] = "Januari";
            month[1] = "Februari";
            month[2] = "Maret";
            month[3] = "April";
            month[4] = "Mei";
            month[5] = "Juni";
            month[6] = "Juli";
            month[7] = "Agustus";
            month[8] = "September";
            month[9] = "Oktober";
            month[10] = "November";
            month[11] = "Desember";

            var dy = d.getDate();
            var s = d.getSeconds();
            var m = d.getMinutes();
            var h = d.getHours();
            var n = weekday[d.getDay()];
            var y = d.getFullYear();
            var hr = month[d.getMonth()];
            clock.textContent =n + ", "+dy+" "+ hr+" "+ y + "  " + h + ":" + m + ":"+s;
        }
      setInterval(time, 1000);
    </script>
