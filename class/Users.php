<?php

/* 
 * 
 * 
 * Author : Nata
 */

    require_once dirname(__FILE__) . '/../class/manual_connect.php';

    class Users {
        
        public $id_user;
        public $nama_lengkap;
        public $jabatan;
        public $user_name;
        public $user_pass;
        public $is_active;
        public $last_update;

        public $isNewRecord = 1;
        
        public $password_confirm;

        
        
        public static function getTable(){
            return 'tb_user';    
        }
        
        public static function findByPk($value,$condition=[]){
            $condition['id_user'] = $value;
            return self::findOne($condition);
        }


        public static function findOne($condition=[]){

            $conn= manual_connect::mysqlConnection();
            $query = manual_connect::query(self::getTable(),$condition);

            $data = mysqli_fetch_array($query,MYSQLI_ASSOC);
            return self::parseObject($data);
           
        }

        public static function findAll($condition=[],$order=null,$limit=null,$offset=0){

            $conn= manual_connect::mysqlConnection();
            $query = manual_connect::mysqlConnection(self::getTable(),$condition,$order,$limit,$offset);

            $return = [];

            while($data = mysqli_fetch_array($query)){
                $return[] = self::parseObject($data);
            }
            return $return;
        }
        
        public static function parseObject($data){
            if($data==null)
                return new Users();

            $model = new Users();
            $model->id_user = $data['ID_USER'];
            $model->jabatan = $data['JABATAN'];
            $model->nama_lengkap = $data['NAMA_LENGKAP'];
            $model->user_name = $data['USER_NAME'];
            $model->user_pass = $data['USER_PASS'];
            $model->is_active = $data['IS_ACTIVE'];
            $model->last_update = $data['LAST_UPDATE'];
            
            $model->isNewRecord = 0;
            
            return $model;
        }
    
    }
?>