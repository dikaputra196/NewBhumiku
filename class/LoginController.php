<?php

/* 
 * 
 * 
 * Author : Nata
 */
    require_once dirname(__FILE__) . '/../class/manual_connect.php';
    require_once dirname(__FILE__) . '/../class/Users.php';
    require_once dirname(__FILE__) . '/../class/detect.php';
    
    class LoginController {
    public function login(){
        if(isset($_POST['login']))
        {
            $today = date('Y-m-d H:i:s');
            $last_login=  'NOW()';
            $user_name=$_POST['login']['username'];
            $user_pass=$_POST['login']['password'];
            $pass= manual_connect::passEncryp($user_pass);

            
            $user=  Users::findOne(['user_name'=>$user_name,'user_pass'=>$pass,'is_active' => 1]);
            $get = get_client_ip_env();
            
            if (!$user->isNewRecord) {
                    $qry= "insert into tb_history(TGL_HISTORY,ID_USER,TYPE_HISTORY,DETAIL_HISTORY) values('$today','$user->id_user','LOGIN','Login User : $user->nama_lengkap, IP Address = $get ')";
                    $conn=manual_connect::mysqlConnection();
                    $do=  mysqli_query($conn, $qry);
                    manual_connect::setSession($user->id_user, $user->user_name, $user->jabatan,$user->nama_lengkap);
                    
                    manual_connect::checkSession();
                }else{
                    manual_connect::setFlash('failed', 'Login Gagal password salah / username tidak ditemukan');
                    manual_connect::refresh();
                }

        }
    }
    public function change($model){
        if(isset($_POST['usr']))
        {
            $password=config::passEncryp($_POST['usr']['password']);
            $model->password = $password;
            if($model->save())
            {
                config::setFlash('success', 'Password berhasil dirubah');
                config::refresh();
            }
            else{
                config::setFlash('failed', 'Password gagal dirubah');
            }
         }
    
    }
    }