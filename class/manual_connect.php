<?php

	//Koneksi
	/*$con=mysqli_connect("localhost","root","");
	$select = mysqli_select_db($con,"db_bhumiku");
	//Check connection
	if(!$select)
		{
			echo "Failed to Connect to SQL: ".mysqli_connect_error();
		}*/
    
    class manual_connect{
        public function __construct() {
            date_default_timezone_set('Asia/Singapore');
            if(session_status() == PHP_SESSION_NONE){
                session_start();
            }
        }
        
        public function mysqlConnection() {
            $host = 'localhost';
            $user = 'root';
            $pass = '';
            $db = 'db_bhumiku';

            $connect = mysqli_connect($host, $user, $pass,$db) or die(mysqli_error());
            mysqli_set_charset($connect,'utf8');

            return $connect;

        }
        
        public function checkSession(){
            if($_SESSION['jabatan']=='ADMINISTRATOR'){
                self::redirect(MAIN_URL.'/beranda.php');
            }else{
                self::redirect(MAIN_URL);
            }
        }
        
        public function noSession(){
            if(!isset($_SESSION['id_user']) && !isset($_SESSION['user_name']) && !isset($_SESSION['nama_lengkap'])){
                self::setFlash('failed','Maaf Sesi Login anda habis , silahkan login kembali');
                self::redirect(MAIN_URL);
            }
        }
        
        public static function setSession($id_user, $user_name, $jabatan,$nama_lengkap){
            $_SESSION['id_user'] = $id_user;
            $_SESSION['user_name'] = $user_name;
            $_SESSION['jabatan'] = $jabatan;
            $_SESSION['nama_lengkap'] = $nama_lengkap;
        }
        
        public static function logout(){
            session_unset();
            unset($_SESSION['id_user']);
            unset($_SESSION['user_name']);
            unset($_SESSION['jabatan']);
            unset($_SESSION['nama_lengkap']);
            session_destroy();
        }
        
        public static function setFlash($name, $value) {
            $_SESSION['___FLASH_'] = [
                $name => $value,
                'value' => $value
            ];
        }
        
        public static function redirect($url, $statusCode = 302) {
        header('Location: ' . $url, true, $statusCode);
        }

        public static function refresh($statusCode = 302) {
            header('Location: ' . 'http://' . $_SERVER['HTTP_HOST'] . $_SERVER["REQUEST_URI"], true, $statusCode);
        }

        public static function currentUrl() {
            return 'http://' . $_SERVER['HTTP_HOST'] . strtok($_SERVER["REQUEST_URI"], '?');
        }

        public static function currentLink() {
            return $_SERVER['HTTP_HOST'] . $_SERVER["REQUEST_URI"];
        }


        public static function parseUrl($url, $query = []) {
            if ($query == null)
                return $url;
            return $url . '?' . http_build_query($query);
        }

        public static function encryptor($action, $string) {
            $output = false;

            $encrypt_method = "AES-256-CBC";
            //pls set your unique hashing key
            $secret_key = 'bhumiku';
            $secret_iv = 'bhumiku123';

            // hash
            $key = hash('sha256', $secret_key);

            // iv - encrypt method AES-256-CBC expects 16 bytes - else you will get a warning
            $iv = substr(hash('sha256', $secret_iv), 0, 16);

            //do the encyption given text/string/number
            if ($action == 'encrypt') {
                $output = openssl_encrypt($string, $encrypt_method, $key, 0, $iv);
                $output = base64_encode($output);
            } else if ($action == 'decrypt') {
                //decrypt the given text/string/number
                $output = openssl_decrypt(base64_decode($string), $encrypt_method, $key, 0, $iv);
            }

            return $output;
        }

        public static function passEncryp($value){
            $option = [
                'cost'=>11,
                'salt'=> substr(hash('sha256','bhumiku'),0,22)
            ];
            return password_hash($value, PASSWORD_BCRYPT, $option);

        }

        public static function selected($value, $compare) {
            if ($value == $compare)
                return 'selected';
            return;
        }

    public static function getFlash($name, $unset = true) {
        $value = isset($_SESSION['___FLASH_']) ? $_SESSION['___FLASH_'] : [];

        if (http_response_code() == 200) {
            if (isset($value[$name])) {
                if ($unset)
                    unset($_SESSION['___FLASH_']);
                return $value[$name];
            }
        }

        return null;
    }

    public static function showFlash($closeButton = true) {
        $name = null;
        $flashActive = isset($_SESSION['___FLASH_']) ? $_SESSION['___FLASH_'] : [];

        $class = '';

        if (isset($flashActive['success'])) {
            $name = 'success';
            $class = 'alert alert-success alert-dismissible';
        } else if (isset($flashActive['failed'])) {
            $name = 'failed';
            $class = 'alert alert-danger alert-dismissible';
        }

        $msg = self::getFlash($name);
        $alert = '<div class="' . $class . '">';

        if ($closeButton)
            $alert .= '<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>';
        
        $alert .= $msg;
        
        $alert .= '</div>';
        
        if($msg != null)
            echo $alert;
    }

        public static function parseSqlValue($value) {
            $link=self::mysqlConnection();
            return mysqli_real_escape_string($link,$value);
        }

        public static function deleteQuery($table, $condition = []) {
            $link=self::mysqlConnection();
            $qry = 'DELETE FROM ' . $table;
            $no = 0;
            if ($condition != null)
                $qry .= ' WHERE ';
            foreach ($condition as $field => $val) {
                if ($no != 0)
                    $qry .= ' AND ';
                $qry .= $field . '="' . Config::parseSqlValue($val) . '"';
                $no++;
            }

            return mysqli_query($link,$qry);
        }

        public static function maxQuery($table,$value){
            $link=self::mysqlConnection();
            $qry= 'SELECT MAX('.$value.') as max FROM '. $table;
            $query = mysqli_query($link, $qry);
            $data = mysqli_fetch_array($query);

            return $data['max'];
        }
        public static function countQuery($table, $condition = [], $limit = null, $offset = 0) {
            $link=self::mysqlConnection();
            $qry = 'SELECT COUNT(*) as nums FROM ' . $table;
            $no = 0;
            if ($condition != null)
                $qry .= ' WHERE ';

            if (is_array($condition)) {
                foreach ($condition as $field => $val) {
                    if ($no != 0)
                        $qry .= ' AND ';
                    $qry .= $field . '="' . Config::parseSqlValue($val) . '"';
                    $no++;
                }
            }else {
                $qry.= $condition;
            }

            if ($limit != null)
                $qry .= ' LIMIT ' . $limit;

            if ($offset != 0)
                $qry .= ' OFFSET ' . $offset;

            $qry .= ';';
            //print_r($qry);
            $query = mysqli_query($link,$qry);
            $data = mysqli_fetch_array($query);
            return $data['nums'];
        }

        public static function query($table, $condition = [], $order = null, $limit = null, $offset = 0, $select='*') {
            $link=self::mysqlConnection();
            $qry = 'SELECT '.$select.' FROM ' . $table;

            $no = 0;
            if ($condition != null)
                $qry .= ' WHERE ';

            if (is_array($condition)) {
                foreach ($condition as $field => $val) {
                    if ($no != 0)
                        $qry .= ' AND ';
                    $qry .= $field . '="' . manual_connect::parseSqlValue($val) . '"';
                    $no++;
                }
            }else {
                $qry.= $condition;
            }

            if ($order != null)
                $qry .= ' ORDER BY ' . $order;

            if ($limit != null)
                $qry .= ' LIMIT ' . $limit;

            if ($offset != 0)
                $qry .= ' OFFSET ' . $offset;

            $qry .= ';';

            //$query = mysqli_query($link,$qry);
            print_r($qry);

            return mysqli_query($link,$qry);
        }


        public static function queryRow($qry){
            $link=self::mysqlConnection();
            $query = mysqli_query($link,$qry);
            $data = mysqli_fetch_array($link,$query);

            return $data;
        }

        public static function checkList($condition)
        {
            if($condition == 1)
                return '<span class="glyphicon glyphicon-ok"></span>';
            else
                return '<span class="glyphicon glyphicon-remove"></span>';
        }


        public static function queryAll($qry){
            $link=self::mysqlConnection();
            $query = mysqli_query($link,$qry);
            $return = [];
            //print_r($qry);
            while($data = mysqli_fetch_array($query)){
                $return[] = $data;
            }
            return $return;
        }



        /**
         * HTML HELPER
         */
        public static function date($value,$separator="-")
        {
            $dtdate=  explode("-", $value);
            $day=$dtdate[0];
            $month=$dtdate[1];
            $year=$dtdate[2];
            $date=$year.$separator.$month.$separator.$day;
            return $date;
        }


        public static function subword($text,$limit,$endSeparator='...'){
            $arrText = explode(' ',$text);
            $tmpText = array();
            if($limit<count($arrText)){
                for($i=0;$i<$limit;$i++){
                    $tmpText[] = $arrText[$i];
                }
                $return = implode(' ',$tmpText).$endSeparator;
            }else{
                $return = $text;
            }
            return $return;
        }
        
    }

?>
