-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Dec 11, 2017 at 07:03 AM
-- Server version: 10.1.28-MariaDB
-- PHP Version: 7.1.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `db_bhumiku`
--

-- --------------------------------------------------------

--
-- Table structure for table `tb_agent`
--

CREATE TABLE `tb_agent` (
  `ID_AGENT` int(11) NOT NULL,
  `NAMA_AGENT` varchar(100) NOT NULL,
  `ALAMAT_AGENT` varchar(200) NOT NULL,
  `TELEPON_AGENT` varchar(30) NOT NULL,
  `HP_AGENT` varchar(50) NOT NULL,
  `CP_AGENT` varchar(100) NOT NULL,
  `EMAIL_AGENT` varchar(255) NOT NULL,
  `BANK_AGENT` varchar(100) NOT NULL,
  `NO_REKENING_AGENT` varchar(100) NOT NULL,
  `REMARK_AGENT` varchar(200) NOT NULL,
  `IS_ACTIVE` int(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_agent`
--

INSERT INTO `tb_agent` (`ID_AGENT`, `NAMA_AGENT`, `ALAMAT_AGENT`, `TELEPON_AGENT`, `HP_AGENT`, `CP_AGENT`, `EMAIL_AGENT`, `BANK_AGENT`, `NO_REKENING_AGENT`, `REMARK_AGENT`, `IS_ACTIVE`) VALUES
(1, 'test TEST', 'TEST', 'TEST', 'TEST', 'TEST', 'test', 'TEST', 'TEST', 'TEST', 1),
(2, 'TESAJKDA', 'DASDAS', 'DASKDJA;LSD', 'ASDLAKSLDA', 'DALSDASD', '', 'ASDASDA', 'ASDASDA', 'DASDASD', 0),
(3, 'CV. Tunjung Teknologi', 'Jl. Alam Sari No. 6 Padangsambian Kaja, Denpasar', '173719273', '9173173', 'Ngurah Ketut Hariadi', 'ngurah@tunjungteknologi.com', '-', '-', '-', 1);

-- --------------------------------------------------------

--
-- Table structure for table `tb_barang`
--

CREATE TABLE `tb_barang` (
  `ID_BARANG` int(11) NOT NULL,
  `KODE_BARANG` varchar(10) NOT NULL,
  `NAMA_BARANG` varchar(255) NOT NULL,
  `JENIS_BARANG` enum('BARANG','JASA') NOT NULL DEFAULT 'BARANG',
  `ID_SATUAN_BESAR` int(11) NOT NULL,
  `ID_SATUAN_KECIL` int(11) NOT NULL,
  `KONVERSI_SATUAN` float NOT NULL DEFAULT '1',
  `IS_INVENTORY` int(1) NOT NULL DEFAULT '1',
  `AVG_PRICE` float NOT NULL DEFAULT '0',
  `ON_STOCK` float NOT NULL DEFAULT '0',
  `ON_PURCHASED` float NOT NULL DEFAULT '0',
  `IS_ACTIVE` int(1) NOT NULL DEFAULT '1',
  `LAST_UPDATE` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_barang`
--

INSERT INTO `tb_barang` (`ID_BARANG`, `KODE_BARANG`, `NAMA_BARANG`, `JENIS_BARANG`, `ID_SATUAN_BESAR`, `ID_SATUAN_KECIL`, `KONVERSI_SATUAN`, `IS_INVENTORY`, `AVG_PRICE`, `ON_STOCK`, `ON_PURCHASED`, `IS_ACTIVE`, `LAST_UPDATE`) VALUES
(1, 'BR001', 'Kertas HVS 80 Gram', 'BARANG', 7, 1, 500, 0, 40000, 500, 0, 1, '2017-12-08 04:06:49'),
(2, 'JS001', 'TEST', 'BARANG', 2, 1, 1, 0, 0, 0, 0, 1, '2017-12-08 04:04:24'),
(5, 'tet', 'tet', 'JASA', 3, 5, 1, 1, 0, 0, 0, 0, '2016-07-14 03:27:37');

-- --------------------------------------------------------

--
-- Table structure for table `tb_booking`
--

CREATE TABLE `tb_booking` (
  `ID_BOOKING` int(11) NOT NULL,
  `TGL_BOOKING` date NOT NULL,
  `NO_BUKTI_PEMESANAN` varchar(30) DEFAULT NULL,
  `NAMA_EVENT` varchar(300) NOT NULL,
  `TGL_EVENT` date NOT NULL,
  `JAM_MULAI` time NOT NULL,
  `JAM_SELESAI` time NOT NULL,
  `ID_RUANGAN` int(11) NOT NULL,
  `JML_UNDANGAN` int(11) NOT NULL DEFAULT '0',
  `ID_JENIS_SETUP` int(11) NOT NULL,
  `ID_EO` int(11) NOT NULL,
  `ID_AGENT` int(11) NOT NULL DEFAULT '1',
  `NAMA_PEMESAN` varchar(100) NOT NULL,
  `ALAMAT_PEMESAN` varchar(300) NOT NULL,
  `TELEPON_PEMESAN` varchar(100) NOT NULL,
  `EMAIL_PEMESAN` varchar(100) NOT NULL,
  `STATUS_BOOKING` enum('TENTATIVE','DEFINITIVE','CANCELLED') NOT NULL DEFAULT 'TENTATIVE',
  `REMARK_BOOKING` text,
  `HARGA_ROOM` int(11) NOT NULL DEFAULT '0',
  `DISKON_ROOM` int(11) NOT NULL DEFAULT '0',
  `JAMINAN_KERUSAKAN` int(11) NOT NULL DEFAULT '0',
  `IS_FINAL` int(1) NOT NULL DEFAULT '0',
  `ID_USER` int(11) NOT NULL,
  `LAST_UPDATE` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_booking`
--

INSERT INTO `tb_booking` (`ID_BOOKING`, `TGL_BOOKING`, `NO_BUKTI_PEMESANAN`, `NAMA_EVENT`, `TGL_EVENT`, `JAM_MULAI`, `JAM_SELESAI`, `ID_RUANGAN`, `JML_UNDANGAN`, `ID_JENIS_SETUP`, `ID_EO`, `ID_AGENT`, `NAMA_PEMESAN`, `ALAMAT_PEMESAN`, `TELEPON_PEMESAN`, `EMAIL_PEMESAN`, `STATUS_BOOKING`, `REMARK_BOOKING`, `HARGA_ROOM`, `DISKON_ROOM`, `JAMINAN_KERUSAKAN`, `IS_FINAL`, `ID_USER`, `LAST_UPDATE`) VALUES
(1, '2016-05-01', 'B-160501-0001', 'WEDDING SAMSUL && ELGI', '2016-05-22', '18:00:00', '22:00:00', 4, 500, 1, 1, 1, 'SAMSUL GALAXY', 'none', '1234567', 'samsul@elgi.com', 'TENTATIVE', 'Minta perfect\r\n', 15000000, 0, 5000000, 1, 1, '2016-07-23 02:57:54'),
(2, '2016-05-01', 'B-160501-0002', 'WEDDING ANDRO && WINDA', '2016-05-22', '09:00:00', '13:00:00', 2, 200, 1, 1, 1, 'GOOGLE', 'none', '1234567', 'samsul@elgi.com', 'TENTATIVE', 'Minta perfect\r\n', 15000000, 0, 5000000, 1, 1, '2016-07-23 02:58:01'),
(3, '2016-05-01', 'B-160501-0003', 'SEMINAR MOBILE PROJECT', '2016-05-22', '14:00:00', '16:00:00', 3, 200, 1, 1, 1, 'LINUX', 'none', '1234567', 'samsul@elgi.com', 'TENTATIVE', 'Minta perfect\r\n', 15000000, 0, 5000000, 1, 1, '2016-07-23 02:58:02'),
(4, '2016-05-01', 'B-160501-0004', 'SEMINAR MOBILE PROJECT', '2016-05-29', '14:00:00', '16:00:00', 3, 200, 1, 1, 1, 'LINUX', 'none', '1234567', 'samsul@elgi.com', 'TENTATIVE', 'Minta perfect\r\n', 15000000, 0, 5000000, 1, 1, '2016-07-23 02:58:04'),
(5, '2016-05-01', 'B-160501-0005', 'SEMINAR MOBILE PROJECT', '2016-05-26', '14:00:00', '16:00:00', 3, 200, 1, 1, 1, 'LINUX', 'none', '1234567', 'samsul@elgi.com', 'TENTATIVE', 'Minta perfect\r\n', 15000000, 0, 5000000, 1, 1, '2016-07-23 02:58:06'),
(6, '2016-05-01', 'B-160501-0006', 'SEMINAR MOBILE PROJECT', '2016-05-07', '14:00:00', '16:00:00', 3, 200, 1, 1, 1, 'LINUX', 'none', '1234567', 'samsul@elgi.com', 'TENTATIVE', 'Minta perfect\r\n', 15000000, 0, 5000000, 1, 1, '2016-07-23 02:58:08'),
(7, '2016-05-26', 'B-160526-0007', 'NGURAH EVENT', '2016-05-30', '14:21:00', '17:21:00', 1, 200, 1, 1, 1, 'NGURAH KETUT HARIADI', 'Jl. Alam Sari No. 6 Denpasar', '08179313375', 'ngurah@tunjungteknologi.com', 'TENTATIVE', '-', 15000000, 0, 0, 0, 1, '2016-07-23 02:58:38'),
(8, '2016-05-26', 'B-160526-0008', 'NGURAH EVENT', '2016-05-30', '14:21:00', '17:21:00', 1, 200, 1, 1, 1, 'NGURAH KETUT HARIADI', 'Jl. Alam Sari No. 6 Denpasar', '08179313375', 'ngurah@tunjungteknologi.com', 'TENTATIVE', '-', 15000000, 0, 0, 0, 1, '2016-07-23 02:58:47'),
(9, '2016-05-26', 'B-160526-0009', 'DEVI EVENT', '2016-05-30', '14:00:00', '18:00:00', 2, 200, 2, 1, 1, 'DEVI', 'Jl. Jalan No 90', '7987931231', 'devi@test.com', 'TENTATIVE', '-', 10000000, 0, 0, 0, 1, '2016-07-23 02:58:56'),
(10, '2016-05-26', 'B-160526-0010', 'TEST', '2016-05-31', '17:35:00', '17:35:00', 1, 100, 1, 1, 1, 'JHSDKAS', 'sdaksjda', 'daskjdlad', 'oiojeqwe', 'TENTATIVE', '-', 12000000, 0, 0, 0, 1, '2016-07-23 02:59:00'),
(11, '2016-05-26', 'B-160526-0011', 'JSHDA', '2016-05-29', '17:40:00', '17:40:00', 1, 100, 1, 1, 1, 'ASJDHAKSD', 'asdkjalsd', '827913', 'jhdjhad', 'TENTATIVE', '-', 10000000, 0, 0, 0, 1, '2016-07-23 02:59:01'),
(12, '2016-05-26', 'B-160526-0012', 'UISDA', '2016-05-27', '17:42:00', '17:42:00', 1, 10, 1, 1, 1, 'SDKASJDA', 'sdkjasda', '182912', 'jsdklajdsa', 'TENTATIVE', '-', 20000000, 0, 0, 0, 1, '2016-07-23 02:59:03'),
(13, '2016-05-26', 'B-160526-0013', 'TEST LAGI', '2016-05-30', '17:45:00', '17:45:00', 1, 400, 1, 1, 1, 'AKJSDKLJAD', 'jaljsdkjasda', '87971283712', 'lajsdlkajsdkla', 'TENTATIVE', '-', 20000000, 0, 0, 0, 1, '2016-07-23 02:59:06'),
(14, '2016-05-27', 'B-160526-0014', 'POCO-POCO', '2016-06-05', '10:00:00', '12:00:00', 4, 500, 1, 1, 1, 'ADI', 'Jlan Merak', '871238', 'adi@adi.com', 'TENTATIVE', '-', 20000000, 0, 0, 0, 1, '2016-07-23 02:59:10'),
(15, '2016-07-16', 'P-160716-0001', 'ARISAN RUMAH', '2016-07-16', '12:01:00', '12:01:00', 1, 100, 1, 1, 1, 'MICHAEL VAN HOTTEN', 'Amsterdam Bakery No. 23', '08179313375', 'miranovitad@gmail.com', 'TENTATIVE', '-', 10000000, 0, 5000000, 0, 1, '2016-07-26 02:43:59'),
(16, '2016-07-18', 'P-160718-0002', 'BAGUS && AYU WEDDING', '2016-08-20', '18:00:00', '20:00:00', 4, 500, 1, 1, 1, 'BAGUS PUTRA WEDANA', 'Jl. Tunjung Sari No. 9xx Denpasar', '08179313375', 'bagus@tunjungteknologi.com', 'DEFINITIVE', '-', 20000000, 0, 0, 0, 1, '2016-07-27 01:43:14'),
(17, '2016-07-21', 'P-160721-0003', 'SEMINAR MAKANAN SEHAT', '2016-07-30', '10:00:00', '12:00:00', 4, 500, 2, 1, 1, 'PT. SEHAT SEJAHTERA', 'Jl. Gunung Agung Denpasar', '081784734', 'sehat@sejahtera.com', 'TENTATIVE', 'Setup ruangan seminar dengan meja di depan, sound system all wireless, lighting soft dan 2 unit projector', 20000000, 500000, 5000000, 0, 1, '2016-07-23 03:00:02'),
(18, '2016-07-21', 'P-160721-0004', 'TEST', '2016-07-31', '14:40:00', '14:40:00', 1, 100, 1, 1, 1, 'TEST', 'test', '1213', 'test', 'TENTATIVE', '', 10000000, 500000, 5000000, 0, 1, '2016-07-23 03:00:08'),
(19, '2016-07-21', 'P-160721-0005', 'TEST', '2016-07-31', '14:42:00', '14:42:00', 1, 200, 1, 1, 1, 'TEST', 'test', 'test', 'test', 'TENTATIVE', '', 10000000, 500000, 5000000, 0, 1, '2016-07-23 03:00:09'),
(20, '2016-07-21', 'P-160721-0006', 'TEST3', '2016-07-31', '14:45:00', '14:45:00', 2, 200, 2, 1, 1, 'TEST3', 'test3', 'test3', 'ngurah7683@yahoo.com;ngurah@tunjungteknologi.com', 'CANCELLED', '', 10000000, 1000000, 5000000, 0, 1, '2016-07-26 02:42:21'),
(21, '2016-07-21', 'P-160721-0007', 'GOLDEN HORSE SEMINAR', '2016-08-21', '10:00:00', '14:00:00', 4, 500, 2, 1, 1, 'PT. KUDA EMAS GEMILANG', 'Jl. Gunung Soputan Denpasar', '89123103', 'kuda@goldenhorse.com', 'TENTATIVE', 'Mohon dipersiapkan extra petugas keamanan', 50000000, 500000, 5000000, 0, 1, '2016-07-23 03:00:13'),
(22, '2016-07-23', 'P-160723-0008', 'TEST 23 JULI', '2016-07-30', '11:05:00', '11:05:00', 1, 100, 1, 1, 1, 'TEST', 'test', 'test', 'test', 'TENTATIVE', '', 10000000, 500000, 5000000, 0, 1, '2016-07-23 03:08:22'),
(23, '2016-07-23', 'P-160723-0009', 'SDASDASD', '2016-07-31', '11:12:00', '11:12:00', 1, 100, 2, 1, 1, 'SDADSA', 'dasdasd', 'sadad', 'asdasd', 'TENTATIVE', '', 10000000, 1000000, 5000000, 0, 1, '2016-07-23 03:13:07'),
(24, '2016-07-26', 'P-160726-0010', 'TES PENGIRIMAN EMAIL', '2016-07-26', '10:00:00', '10:00:00', 1, 100, 1, 1, 1, 'NGURAH KETUT HARIADI', 'Jl. Alam Sari No. 6 Denpasar', '08179313375', 'ngurah@tunjungteknologi.com', 'TENTATIVE', 'Test pengiriman email', 10000000, 500000, 1000000, 0, 1, '2016-07-26 02:01:31'),
(25, '2016-07-26', 'P-160726-0011', 'TEST EVENT', '2016-07-26', '10:03:00', '10:03:00', 1, 100, 1, 1, 1, 'NGURAH KETUT HARIADI', 'Jl. Alam Sari No. 6 Denpasar', '08179313375', 'ngurah@tunjungteknologi.com', 'TENTATIVE', 'test', 10000000, 10000, 100000, 0, 1, '2016-07-26 02:04:52'),
(26, '2016-07-26', 'P-160726-0012', 'PROGRAMMING SEMINAR', '2016-08-07', '07:32:00', '10:32:00', 3, 100, 1, 1, 1, 'NGURAH KETUT HARIADI', 'Jl. Alam Sari No. 6 Denpasar', '08179313375', 'ngurah@tunjungteknologi.com', 'TENTATIVE', '-', 15000000, 100000, 5000000, 0, 1, '2016-07-26 02:34:11'),
(27, '2016-08-13', 'P-160813-0001', 'TEST', '2016-09-15', '09:28:00', '09:28:00', 1, 200, 2, 1, 1, 'TEST', 'test', 'test', 'info@bhumiku.com', 'DEFINITIVE', '', 15000000, 200000, 5000000, 0, 1, '2016-08-13 02:01:06'),
(29, '2017-12-11', 'B-171211-0001', 'GitLab.com', '2017-12-31', '12:00:00', '17:00:00', 4, 150, 2, 1, 3, 'GitLab', 'US', '+1 362 298909', 'gitLab@gitlab.com', 'TENTATIVE', 'GitLab.com', 20000000, 1500000, 2000000, 0, 0, '2017-12-11 04:12:11'),
(30, '2017-12-11', 'B-171211-0002', 'GitLab.com', '2017-12-31', '12:00:00', '17:00:00', 4, 150, 2, 1, 3, 'GitLab', 'US', '+1 362 298909', 'gitLab@gitlab.com', 'TENTATIVE', 'GitLab.com', 20000000, 1500000, 2000000, 0, 0, '2017-12-11 04:14:03'),
(31, '2017-12-11', 'B-171211-0003', 'GitLab.com', '2017-12-31', '12:00:00', '17:00:00', 4, 150, 2, 1, 3, 'GitLab', 'US', '+1 362 298909', 'gitLab@gitlab.com', 'TENTATIVE', 'GitLab.com', 20000000, 1500000, 2000000, 0, 0, '2017-12-11 04:14:38');

-- --------------------------------------------------------

--
-- Table structure for table `tb_coa`
--

CREATE TABLE `tb_coa` (
  `kode_coa` varchar(6) NOT NULL,
  `nama_coa` varchar(200) NOT NULL,
  `kode_parent` varchar(6) NOT NULL,
  `jenis_coa` enum('HEADER','DETAIL') NOT NULL,
  `saldo_normal` enum('DEBET','KREDIT') NOT NULL DEFAULT 'DEBET',
  `is_active` int(1) NOT NULL DEFAULT '1',
  `last_update` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `is_mutasi` int(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_coa`
--

INSERT INTO `tb_coa` (`kode_coa`, `nama_coa`, `kode_parent`, `jenis_coa`, `saldo_normal`, `is_active`, `last_update`, `is_mutasi`) VALUES
('100000', 'AKTIVA LANCAR', '0', 'HEADER', 'DEBET', 1, '2016-07-21 01:44:09', 1),
('110000', 'Kas', '110000', 'HEADER', 'DEBET', 1, '2017-12-07 08:42:00', 1),
('110001', 'Kas', '110000', 'DETAIL', 'DEBET', 1, '2016-07-21 01:48:15', 1),
('110002', 'Kas Kecil Event', '110000', 'DETAIL', 'DEBET', 1, '2016-05-27 03:27:47', 1),
('120000', 'Bank', '100000', 'HEADER', 'DEBET', 1, '2016-07-20 05:13:22', 1),
('120001', 'BCA 8580778888 CV. Bhumiku', '120000', 'DETAIL', 'DEBET', 1, '2016-07-20 05:14:28', 1),
('130000', 'Piutang', '100000', 'HEADER', 'DEBET', 1, '2016-07-21 01:51:55', 1),
('130001', 'Piutang Sewa Ruangan', '130000', 'DETAIL', 'DEBET', 1, '2016-07-21 01:52:21', 1),
('130002', 'Piutang Karyawan', '130000', 'DETAIL', 'DEBET', 1, '2016-07-21 01:57:44', 1),
('130003', 'Piutang Giro/BG', '130000', 'DETAIL', 'DEBET', 1, '2016-07-21 01:58:18', 1),
('130004', 'Piutang Lain-lain', '130000', 'DETAIL', 'DEBET', 1, '2016-07-21 01:58:57', 1),
('140000', 'Persediaan', '100000', 'HEADER', 'DEBET', 1, '2016-07-21 02:01:34', 1),
('140001', 'Persediaan Barang Event', '140000', 'DETAIL', 'DEBET', 1, '2016-07-21 02:02:45', 1),
('140002', 'Perlengkapan Kantor', '140000', 'DETAIL', 'DEBET', 1, '2016-07-21 02:03:40', 1),
('150000', 'Investasi', '100000', 'HEADER', 'DEBET', 1, '2016-07-21 02:05:03', 1),
('150001', 'Investasi', '150000', 'DETAIL', 'DEBET', 1, '2016-07-21 02:05:24', 1),
('160000', 'Uang Muka', '100000', 'HEADER', 'DEBET', 1, '2016-07-21 02:13:32', 1),
('160001', 'Uang Muka Sewa Ruangan', '160000', 'DETAIL', 'DEBET', 1, '2016-07-21 02:13:57', 1),
('160002', 'Uang Muka Pembelian', '160000', 'DETAIL', 'DEBET', 1, '2016-07-21 02:14:26', 1),
('200000', 'AKTIVA TETAP', '0', 'HEADER', 'DEBET', 1, '2016-07-21 01:46:33', 1),
('210000', 'Tanah dan Gedung', '200000', 'HEADER', 'DEBET', 1, '2016-07-21 02:08:20', 1),
('210001', 'Tanah', '210000', 'DETAIL', 'DEBET', 1, '2016-07-21 02:15:12', 1),
('210002', 'Gedung Kantor', '210000', 'DETAIL', 'DEBET', 1, '2016-07-21 02:17:15', 1),
('210003', 'Gudang', '210000', 'DETAIL', 'DEBET', 1, '2016-07-21 02:16:05', 1),
('220000', 'Akumulasi Penyusutan', '200000', 'HEADER', 'DEBET', 1, '2016-07-21 02:09:06', 1),
('220001', 'Akumulasi Penyusutan Gedung Kantor', '220000', 'DETAIL', 'DEBET', 1, '2016-07-21 02:21:45', 1),
('220002', 'Akumulasi Panyusutan Perlengkapan', '220000', 'DETAIL', 'DEBET', 1, '2016-07-21 02:23:13', 1),
('230000', 'Perlengkapan', '200000', 'HEADER', 'DEBET', 1, '2016-07-21 02:23:01', 1),
('230001', 'Kursi dan Meja', '230000', 'DETAIL', 'DEBET', 1, '2016-07-21 02:18:56', 1),
('230002', 'Lighting && Sound System', '230000', 'DETAIL', 'DEBET', 1, '2016-07-21 02:20:15', 1),
('230003', 'Peralatan lain-lain', '230000', 'DETAIL', 'DEBET', 1, '2016-07-21 02:21:00', 1),
('300000', 'KEWAJIBAN', '0', 'HEADER', 'KREDIT', 1, '2016-07-21 01:46:07', 1),
('310000', 'Kewajiban Jangka Pendek', '300000', 'HEADER', 'KREDIT', 1, '2016-07-21 02:27:06', 1),
('310001', 'Utang Usaha', '310000', 'DETAIL', 'KREDIT', 1, '2016-07-21 02:28:39', 1),
('310002', 'Utang Lain-lain', '310000', 'DETAIL', 'KREDIT', 1, '2016-07-21 02:29:05', 1),
('310003', 'Utang Gaji', '310000', 'DETAIL', 'KREDIT', 1, '2016-07-21 02:30:29', 1),
('310004', 'Utang Jaminan Sewa Gedung', '310000', 'DETAIL', 'KREDIT', 1, '2016-07-21 02:36:05', 1),
('310005', 'Utang Pajak', '310000', 'DETAIL', 'KREDIT', 1, '2016-07-21 02:39:11', 1),
('310006', 'Pendapatan Diterima di Muka', '310000', 'DETAIL', 'KREDIT', 1, '2016-07-21 02:40:26', 1),
('320000', 'Kewajiban Jangka Panjang', '300000', 'HEADER', 'KREDIT', 1, '2016-07-21 02:27:33', 1),
('320001', 'Utang Bank', '320000', 'DETAIL', 'KREDIT', 1, '2016-07-21 02:40:53', 1),
('400000', 'EKUITAS', '0', 'HEADER', 'KREDIT', 1, '2016-07-21 03:01:00', 1),
('410000', 'Modal', '400000', 'HEADER', 'KREDIT', 1, '2016-07-21 02:56:53', 1),
('410001', 'Modal', '410000', 'DETAIL', 'KREDIT', 1, '2016-07-21 02:57:58', 1),
('420000', 'Laba Ditahan', '400000', 'HEADER', 'KREDIT', 1, '2016-07-21 02:56:42', 1),
('420001', 'Laba Ditahan', '420000', 'DETAIL', 'KREDIT', 1, '2016-07-21 02:58:25', 1),
('430000', 'Laba (Rugi) Bersih', '400000', 'HEADER', 'KREDIT', 1, '2016-07-21 02:57:31', 1),
('430001', 'Laba (Rugi) Bersih', '430000', 'DETAIL', 'KREDIT', 1, '2016-07-21 02:59:06', 1),
('500000', 'PENDAPATAN', '0', 'HEADER', 'KREDIT', 1, '2016-07-21 01:44:26', 1),
('510000', 'Pendapatan Usaha', '500000', 'HEADER', 'KREDIT', 1, '2016-07-21 01:45:12', 1),
('510001', 'Pendapatan Sewa Ruangan', '510000', 'DETAIL', 'KREDIT', 1, '2016-07-21 01:45:26', 1),
('510002', 'Retur Sewa', '510000', 'DETAIL', 'KREDIT', 1, '2016-07-21 02:53:35', 1),
('520000', 'Pendapatan Lain-lain', '500000', 'HEADER', 'KREDIT', 1, '2016-07-21 01:45:39', 1),
('520001', 'Pendapatan Bunga', '520000', 'DETAIL', 'KREDIT', 1, '2016-07-21 02:51:49', 1),
('520002', 'Diskon Pembelian', '520000', 'DETAIL', 'KREDIT', 1, '2016-07-21 02:52:27', 1),
('520003', 'Pendapatan Lain-lain', '520000', 'DETAIL', 'KREDIT', 1, '2016-07-21 02:54:11', 1),
('600000', 'BEBAN', '0', 'HEADER', 'DEBET', 1, '2016-07-21 01:43:48', 1),
('610000', 'Beban Operasional', '600000', 'HEADER', 'DEBET', 1, '2016-07-21 03:09:40', 1),
('610001', 'Biaya Listrik', '610000', 'DETAIL', 'DEBET', 1, '2016-07-21 03:17:01', 1),
('610002', 'Biaya Telepon', '610000', 'DETAIL', 'DEBET', 1, '2016-07-21 03:17:03', 1),
('610003', 'Biaya Air/PDAM', '610000', 'DETAIL', 'DEBET', 1, '2016-07-21 03:17:04', 1),
('610004', 'Biaya Keamanan', '610000', 'DETAIL', 'DEBET', 1, '2016-07-21 03:17:04', 1),
('610005', 'Biaya Transportasi', '610000', 'DETAIL', 'DEBET', 1, '2016-07-21 03:17:05', 1),
('610006', 'Biaya Operasional Lainnya', '610000', 'DETAIL', 'DEBET', 1, '2016-07-21 03:17:05', 1),
('620000', 'Beban Pajak', '600000', 'HEADER', 'DEBET', 1, '2016-07-21 03:10:06', 1),
('620001', 'PPN Pembelian', '620000', 'DETAIL', 'DEBET', 1, '2016-07-21 03:16:51', 1),
('620002', 'Biaya Pajak', '620000', 'DETAIL', 'DEBET', 1, '2016-07-21 03:17:32', 1),
('630000', 'Beban Gaji Karyawan', '600000', 'HEADER', 'DEBET', 1, '2016-07-21 03:10:34', 1),
('630001', 'Gaji Karyawan', '630000', 'DETAIL', 'DEBET', 1, '2016-07-21 03:18:01', 1),
('630002', 'Bonus Karyawan', '630000', 'DETAIL', 'DEBET', 1, '2016-07-21 03:18:13', 1),
('640000', 'Beban Promosi', '600000', 'HEADER', 'DEBET', 1, '2016-07-21 03:11:39', 1),
('640001', 'Biaya Promosi', '640000', 'DETAIL', 'DEBET', 1, '2016-07-21 03:19:13', 1),
('640002', 'Fee Marketing/Agent/EO', '640000', 'DETAIL', 'DEBET', 1, '2016-07-21 03:19:59', 1),
('650000', 'Beban Lain-lain', '600000', 'HEADER', 'DEBET', 1, '2016-07-21 03:12:16', 1),
('650001', 'Biaya Lain-lain', '650000', 'DETAIL', 'DEBET', 1, '2016-07-21 03:20:28', 1);

-- --------------------------------------------------------

--
-- Table structure for table `tb_customer`
--

CREATE TABLE `tb_customer` (
  `ID_CUSTOMER` int(11) NOT NULL,
  `NAMA_CUSTOMER` varchar(255) NOT NULL,
  `ALAMAT_CUSTOMER` varchar(255) NOT NULL,
  `HP_CUSTOMER` varchar(255) NOT NULL,
  `EMAIL_CUSTOMER` varchar(255) NOT NULL,
  `KTP_CUSTOMER` varchar(255) NOT NULL DEFAULT '-',
  `IS_ACTIVE` int(1) NOT NULL DEFAULT '1',
  `LAST_UPDATES` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_customer`
--

INSERT INTO `tb_customer` (`ID_CUSTOMER`, `NAMA_CUSTOMER`, `ALAMAT_CUSTOMER`, `HP_CUSTOMER`, `EMAIL_CUSTOMER`, `KTP_CUSTOMER`, `IS_ACTIVE`, `LAST_UPDATES`) VALUES
(1, 'test', 'test', '09817231', 'ngurah@ajsdkja.com', '5171010706830001', 1, '2017-01-31 05:00:25'),
(2, 'test', 'test', 'test', 'test', 'test', 0, '2017-01-31 05:04:02');

-- --------------------------------------------------------

--
-- Table structure for table `tb_detail_jurnal`
--

CREATE TABLE `tb_detail_jurnal` (
  `ID_DETAIL_JURNAL` int(11) NOT NULL,
  `ID_JURNAL` int(11) NOT NULL,
  `KODE_COA` varchar(10) NOT NULL,
  `POSISI` enum('D','K') NOT NULL,
  `JML_TRANSAKSI` float NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_detail_jurnal`
--

INSERT INTO `tb_detail_jurnal` (`ID_DETAIL_JURNAL`, `ID_JURNAL`, `KODE_COA`, `POSISI`, `JML_TRANSAKSI`) VALUES
(1, 2, '130001', 'D', 0),
(2, 2, '510001', 'K', 0),
(3, 3, '130001', 'D', 54000000),
(4, 3, '510001', 'K', 54000000),
(26, 13, '110001', 'D', 5000000),
(27, 13, '130001', 'K', 5000000),
(28, 14, '120001', 'D', 11000000),
(29, 14, '130001', 'K', 6000000),
(30, 14, '310004', 'K', 5000000),
(31, 15, '110001', 'D', 5800000),
(32, 15, '130001', 'K', 5800000),
(33, 16, '110001', 'D', 5000000),
(34, 16, '130001', 'K', 5000000),
(35, 17, '110001', 'D', 11000000),
(36, 17, '130001', 'K', 6000000),
(37, 17, '310004', 'K', 5000000),
(38, 18, '120001', 'D', 5800000),
(39, 18, '130001', 'K', 5800000),
(40, 19, '130001', 'D', 12050000),
(41, 19, '510001', 'K', 12050000),
(42, 20, '130001', 'D', 9010000),
(43, 20, '510001', 'K', 9010000),
(44, 21, '510001', 'D', 5000000),
(45, 21, '130001', 'K', 5000000),
(46, 22, '130001', 'D', 1000000),
(47, 23, '130001', 'D', 2000000),
(48, 24, '110001', 'D', 1000000),
(49, 26, '110001', 'D', 1000000),
(50, 26, '130001', 'K', 1000000),
(51, 27, '110002', 'D', 1000000),
(52, 27, '120001', 'K', 1000000),
(53, 28, '110002', 'D', 1000000),
(54, 28, '120001', 'K', 1000000),
(55, 29, '130001', 'D', 9510000),
(56, 29, '510001', 'K', 9510000),
(57, 30, '130001', 'D', 10000000),
(58, 30, '510001', 'K', 10000000),
(59, 31, '130001', 'D', 15900000),
(60, 31, '510001', 'K', 15900000),
(61, 32, '110001', 'D', 5000000),
(62, 32, '130001', 'K', 5000000),
(63, 33, '120001', 'D', 11000000),
(64, 33, '130001', 'K', 6000000),
(65, 33, '310004', 'K', 5000000),
(66, 34, '110001', 'D', 4900000),
(67, 34, '130001', 'K', 4900000),
(68, 35, '110001', 'D', 5000000),
(69, 35, '130001', 'K', 5000000),
(70, 36, '110001', 'D', 15000000),
(71, 36, '130001', 'K', 15000000),
(72, 36, '310004', 'K', 0),
(73, 37, '310004', 'D', 1000000),
(74, 37, '120001', 'K', 1000000),
(75, 38, '130001', 'D', 18000000),
(76, 38, '510001', 'K', 18000000),
(77, 39, '120001', 'D', 5000000),
(78, 39, '130001', 'K', 5000000),
(79, 40, '110001', 'D', 10000000),
(80, 40, '130001', 'K', 5000000),
(81, 40, '310004', 'K', 5000000),
(82, 41, '510001', 'D', 4490000),
(83, 41, '130001', 'K', 4490000),
(84, 42, '110001', 'D', 5000000),
(85, 42, '130001', 'K', 5000000);

-- --------------------------------------------------------

--
-- Table structure for table `tb_eo`
--

CREATE TABLE `tb_eo` (
  `ID_EO` int(11) NOT NULL,
  `NAMA_EO` varchar(100) NOT NULL,
  `ALAMAT_EO` varchar(200) NOT NULL,
  `TELEPON_EO` varchar(30) NOT NULL,
  `HP_EO` varchar(50) NOT NULL,
  `CP_EO` varchar(100) NOT NULL,
  `EMAIL_EO` varchar(255) NOT NULL,
  `BANK_EO` varchar(100) NOT NULL,
  `NO_REKENING_EO` varchar(100) NOT NULL,
  `REMARK_EO` varchar(200) NOT NULL,
  `IS_ACTIVE` int(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_eo`
--

INSERT INTO `tb_eo` (`ID_EO`, `NAMA_EO`, `ALAMAT_EO`, `TELEPON_EO`, `HP_EO`, `CP_EO`, `EMAIL_EO`, `BANK_EO`, `NO_REKENING_EO`, `REMARK_EO`, `IS_ACTIVE`) VALUES
(1, 'SEMARAK EVENT', 'Jl. Alam Sari No. 6 Denpasar', '987391723', '82739873', 'Budi', 'a@b.com', 'BCA', '123456', '-', 1),
(2, 'TEST', 'TEST', 'TEST', 'TEST', 'TEST', 'TEST@TEST.COM', 'BCA', '8739213', 'TEST', 1);

-- --------------------------------------------------------

--
-- Table structure for table `tb_history`
--

CREATE TABLE `tb_history` (
  `ID_HISTORY` int(11) NOT NULL,
  `TGL_HISTORY` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `ID_USER` int(11) NOT NULL,
  `TYPE_HISTORY` varchar(50) NOT NULL,
  `DETAIL_HISTORY` varchar(500) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_history`
--

INSERT INTO `tb_history` (`ID_HISTORY`, `TGL_HISTORY`, `ID_USER`, `TYPE_HISTORY`, `DETAIL_HISTORY`) VALUES
(1, '2016-05-19 09:39:11', 1, 'Login', 'Login User : NGURAH KETUT HARIADI, IP : 192.168.8.100'),
(2, '2016-05-19 09:44:34', 1, 'Login', 'Login User : NGURAH KETUT HARIADI, IP : 192.168.8.100'),
(3, '2016-05-19 09:47:46', 1, 'Login', 'Login User : NGURAH KETUT HARIADI, IP : 192.168.8.100'),
(4, '2016-05-19 09:52:13', 1, 'Login', 'Login User : NGURAH KETUT HARIADI, IP : 192.168.8.100'),
(5, '2016-05-19 09:52:52', 1, 'Login', 'Login User : NGURAH KETUT HARIADI, IP : 192.168.8.100'),
(6, '2016-05-19 09:53:12', 1, 'Login', 'Login User : NGURAH KETUT HARIADI, IP : 192.168.8.100'),
(7, '2016-05-19 09:55:30', 1, 'Login', 'Login User : NGURAH KETUT HARIADI, IP : 192.168.8.100'),
(8, '2016-05-19 09:59:11', 1, 'Login', 'Login User : NGURAH KETUT HARIADI, IP : 192.168.8.100'),
(9, '2016-05-19 09:59:26', 1, 'Logout', 'Logout User : NGURAH KETUT HARIADI, IP : 192.168.8.100'),
(10, '2016-05-19 13:12:06', 1, 'Login', 'Login User : NGURAH KETUT HARIADI, IP : 192.168.8.100'),
(11, '2016-05-19 13:13:04', 1, 'Logout', 'Logout User : NGURAH KETUT HARIADI, IP : 192.168.8.100'),
(12, '2016-05-19 13:13:42', 1, 'Login', 'Login User : NGURAH KETUT HARIADI, IP : 192.168.8.100'),
(13, '2016-05-19 13:13:55', 1, 'Logout', 'Logout User : NGURAH KETUT HARIADI, IP : 192.168.8.100'),
(14, '2016-05-19 13:17:21', 1, 'Login', 'Login User : NGURAH KETUT HARIADI, IP : 192.168.8.100'),
(15, '2016-05-19 13:19:21', 1, 'Logout', 'Logout User : NGURAH KETUT HARIADI, IP : 192.168.8.100'),
(16, '2016-05-19 13:21:33', 1, 'Login', 'Login User : NGURAH KETUT HARIADI, IP : 192.168.8.100'),
(17, '2016-05-19 13:21:48', 1, 'Logout', 'Logout User : NGURAH KETUT HARIADI, IP : 192.168.8.100'),
(18, '2016-05-19 13:22:20', 1, 'Login', 'Login User : NGURAH KETUT HARIADI, IP : 192.168.8.100'),
(19, '2016-05-19 13:22:46', 1, 'Logout', 'Logout User : NGURAH KETUT HARIADI, IP : 192.168.8.100'),
(20, '2016-05-19 13:28:39', 1, 'Login', 'Login User : NGURAH KETUT HARIADI, IP : 192.168.8.100'),
(21, '2016-05-19 13:31:39', 1, 'Logout', 'Logout User : NGURAH KETUT HARIADI, IP : 192.168.8.100'),
(22, '2016-05-20 04:26:58', 1, 'Login', 'Login User : NGURAH KETUT HARIADI, IP : 127.0.0.1'),
(23, '2016-05-20 04:35:09', 1, 'Logout', 'Logout User : NGURAH KETUT HARIADI, IP : 127.0.0.1'),
(24, '2016-05-20 04:43:05', 1, 'Login', 'Login User : NGURAH KETUT HARIADI, IP : 127.0.0.1'),
(25, '2016-05-20 04:44:02', 1, 'Logout', 'Logout User : NGURAH KETUT HARIADI, IP : 127.0.0.1'),
(26, '2016-05-20 04:53:06', 1, 'Login', 'Login User : NGURAH KETUT HARIADI, IP : 127.0.0.1'),
(27, '2016-05-20 04:53:24', 1, 'Logout', 'Logout User : NGURAH KETUT HARIADI, IP : 127.0.0.1'),
(28, '2016-05-20 04:56:59', 1, 'Login', 'Login User : NGURAH KETUT HARIADI, IP : 127.0.0.1'),
(29, '2016-05-20 08:22:02', 1, 'Logout', 'Logout User : NGURAH KETUT HARIADI, IP : 127.0.0.1'),
(30, '2016-05-22 02:04:21', 1, 'Login', 'Login User : NGURAH KETUT HARIADI, IP : 127.0.0.1'),
(31, '2016-05-22 02:04:43', 1, 'Logout', 'Logout User : NGURAH KETUT HARIADI, IP : 127.0.0.1'),
(32, '2016-05-22 02:31:28', 1, 'Login', 'Login User : NGURAH KETUT HARIADI, IP : 127.0.0.1'),
(33, '2016-05-22 02:31:49', 1, 'Logout', 'Logout User : NGURAH KETUT HARIADI, IP : 127.0.0.1'),
(34, '2016-05-22 02:34:39', 1, 'Login', 'Login User : NGURAH KETUT HARIADI, IP : 127.0.0.1'),
(35, '2016-05-22 02:35:10', 1, 'Logout', 'Logout User : NGURAH KETUT HARIADI, IP : 127.0.0.1'),
(36, '2016-05-22 02:36:40', 1, 'Login', 'Login User : NGURAH KETUT HARIADI, IP : 127.0.0.1'),
(37, '2016-05-22 02:36:58', 1, 'Logout', 'Logout User : NGURAH KETUT HARIADI, IP : 127.0.0.1'),
(38, '2016-05-22 02:37:42', 1, 'Login', 'Login User : NGURAH KETUT HARIADI, IP : 127.0.0.1'),
(39, '2016-05-22 02:38:02', 1, 'Logout', 'Logout User : NGURAH KETUT HARIADI, IP : 127.0.0.1'),
(40, '2016-05-22 02:42:28', 1, 'Login', 'Login User : NGURAH KETUT HARIADI, IP : 127.0.0.1'),
(41, '2016-05-22 02:42:45', 1, 'Logout', 'Logout User : NGURAH KETUT HARIADI, IP : 127.0.0.1'),
(42, '2016-05-22 02:50:37', 1, 'Login', 'Login User : NGURAH KETUT HARIADI, IP : 127.0.0.1'),
(43, '2016-05-22 02:51:13', 1, 'Logout', 'Logout User : NGURAH KETUT HARIADI, IP : 127.0.0.1'),
(44, '2016-05-22 02:56:38', 1, 'Login', 'Login User : NGURAH KETUT HARIADI, IP : 127.0.0.1'),
(45, '2016-05-22 02:57:20', 1, 'Logout', 'Logout User : NGURAH KETUT HARIADI, IP : 127.0.0.1'),
(46, '2016-05-22 03:09:48', 1, 'Login', 'Login User : NGURAH KETUT HARIADI, IP : 127.0.0.1'),
(47, '2016-05-22 03:10:27', 1, 'Logout', 'Logout User : NGURAH KETUT HARIADI, IP : 127.0.0.1'),
(48, '2016-05-22 03:10:44', 1, 'Login', 'Login User : NGURAH KETUT HARIADI, IP : 127.0.0.1'),
(49, '2016-05-22 03:11:00', 1, 'Logout', 'Logout User : NGURAH KETUT HARIADI, IP : 127.0.0.1'),
(50, '2016-05-22 03:11:32', 1, 'Login', 'Login User : NGURAH KETUT HARIADI, IP : 127.0.0.1'),
(51, '2016-05-22 03:12:50', 1, 'Logout', 'Logout User : NGURAH KETUT HARIADI, IP : 127.0.0.1'),
(52, '2016-05-22 03:14:38', 1, 'Login', 'Login User : NGURAH KETUT HARIADI, IP : 127.0.0.1'),
(53, '2016-05-22 03:16:53', 1, 'Logout', 'Logout User : NGURAH KETUT HARIADI, IP : 127.0.0.1'),
(54, '2016-05-22 03:17:18', 1, 'Login', 'Login User : NGURAH KETUT HARIADI, IP : 127.0.0.1'),
(55, '2016-05-22 04:24:59', 1, 'Login', 'Login User : NGURAH KETUT HARIADI, IP : 127.0.0.1'),
(56, '2016-05-22 04:25:47', 1, 'Logout', 'Logout User : NGURAH KETUT HARIADI, IP : 127.0.0.1'),
(57, '2016-05-22 04:25:58', 1, 'Login', 'Login User : NGURAH KETUT HARIADI, IP : 127.0.0.1'),
(58, '2016-05-22 04:59:12', 1, 'Login', 'Login User : NGURAH KETUT HARIADI, IP : 127.0.0.1'),
(59, '2016-05-22 04:59:28', 1, 'Logout', 'Logout User : NGURAH KETUT HARIADI, IP : 127.0.0.1'),
(60, '2016-05-22 05:01:48', 1, 'Login', 'Login User : NGURAH KETUT HARIADI, IP : 127.0.0.1'),
(61, '2016-05-22 05:02:09', 1, 'Logout', 'Logout User : NGURAH KETUT HARIADI, IP : 127.0.0.1'),
(62, '2016-05-22 05:02:48', 1, 'Login', 'Login User : NGURAH KETUT HARIADI, IP : 127.0.0.1'),
(63, '2016-05-22 05:05:26', 1, 'Logout', 'Logout User : NGURAH KETUT HARIADI, IP : 127.0.0.1'),
(64, '2016-05-22 05:33:06', 1, 'Login', 'Login User : NGURAH KETUT HARIADI, IP : 127.0.0.1'),
(65, '2016-05-22 05:33:22', 1, 'Logout', 'Logout User : NGURAH KETUT HARIADI, IP : 127.0.0.1'),
(66, '2016-05-22 05:34:43', 1, 'Login', 'Login User : NGURAH KETUT HARIADI, IP : 127.0.0.1'),
(67, '2016-05-22 05:34:52', 1, 'Logout', 'Logout User : NGURAH KETUT HARIADI, IP : 127.0.0.1'),
(68, '2016-05-22 05:35:24', 1, 'Login', 'Login User : NGURAH KETUT HARIADI, IP : 127.0.0.1'),
(69, '2016-05-22 05:35:46', 1, 'Logout', 'Logout User : NGURAH KETUT HARIADI, IP : 127.0.0.1'),
(70, '2016-05-22 05:36:26', 1, 'Login', 'Login User : NGURAH KETUT HARIADI, IP : 127.0.0.1'),
(71, '2016-05-22 05:36:52', 1, 'Logout', 'Logout User : NGURAH KETUT HARIADI, IP : 127.0.0.1'),
(72, '2016-05-22 05:37:19', 1, 'Login', 'Login User : NGURAH KETUT HARIADI, IP : 127.0.0.1'),
(73, '2016-05-22 05:39:29', 1, 'Logout', 'Logout User : NGURAH KETUT HARIADI, IP : 127.0.0.1'),
(74, '2016-05-22 05:39:34', 1, 'Login', 'Login User : NGURAH KETUT HARIADI, IP : 127.0.0.1'),
(75, '2016-05-22 05:39:47', 1, 'Logout', 'Logout User : NGURAH KETUT HARIADI, IP : 127.0.0.1'),
(76, '2016-05-22 05:41:11', 1, 'Login', 'Login User : NGURAH KETUT HARIADI, IP : 127.0.0.1'),
(77, '2016-05-22 05:41:42', 1, 'Logout', 'Logout User : NGURAH KETUT HARIADI, IP : 127.0.0.1'),
(78, '2016-05-22 05:42:38', 1, 'Login', 'Login User : NGURAH KETUT HARIADI, IP : 127.0.0.1'),
(79, '2016-05-22 05:42:45', 1, 'Logout', 'Logout User : NGURAH KETUT HARIADI, IP : 127.0.0.1'),
(80, '2016-05-22 05:45:39', 1, 'Login', 'Login User : NGURAH KETUT HARIADI, IP : 127.0.0.1'),
(81, '2016-05-22 05:46:43', 1, 'Logout', 'Logout User : NGURAH KETUT HARIADI, IP : 127.0.0.1'),
(82, '2016-05-22 05:51:24', 1, 'Login', 'Login User : NGURAH KETUT HARIADI, IP : 127.0.0.1'),
(83, '2016-05-22 05:51:33', 1, 'Logout', 'Logout User : NGURAH KETUT HARIADI, IP : 127.0.0.1'),
(84, '2016-05-22 05:51:53', 1, 'Login', 'Login User : NGURAH KETUT HARIADI, IP : 127.0.0.1'),
(85, '2016-05-22 05:52:00', 1, 'Logout', 'Logout User : NGURAH KETUT HARIADI, IP : 127.0.0.1'),
(86, '2016-05-22 05:52:22', 1, 'Login', 'Login User : NGURAH KETUT HARIADI, IP : 127.0.0.1'),
(87, '2016-05-22 05:52:27', 1, 'Logout', 'Logout User : NGURAH KETUT HARIADI, IP : 127.0.0.1'),
(88, '2016-05-22 05:53:42', 1, 'Login', 'Login User : NGURAH KETUT HARIADI, IP : 127.0.0.1'),
(89, '2016-05-22 05:53:53', 1, 'Logout', 'Logout User : NGURAH KETUT HARIADI, IP : 127.0.0.1'),
(90, '2016-05-22 05:59:23', 1, 'Login', 'Login User : NGURAH KETUT HARIADI, IP : 127.0.0.1'),
(91, '2016-05-22 05:59:39', 1, 'Logout', 'Logout User : NGURAH KETUT HARIADI, IP : 127.0.0.1'),
(92, '2016-05-22 06:00:20', 1, 'Login', 'Login User : NGURAH KETUT HARIADI, IP : 127.0.0.1'),
(93, '2016-05-22 06:00:27', 1, 'Logout', 'Logout User : NGURAH KETUT HARIADI, IP : 127.0.0.1'),
(94, '2016-05-22 06:03:03', 1, 'Login', 'Login User : NGURAH KETUT HARIADI, IP : 127.0.0.1'),
(95, '2016-05-22 06:03:11', 1, 'Logout', 'Logout User : NGURAH KETUT HARIADI, IP : 127.0.0.1'),
(96, '2016-05-22 06:05:20', 1, 'Login', 'Login User : NGURAH KETUT HARIADI, IP : 127.0.0.1'),
(97, '2016-05-22 06:06:18', 1, 'Logout', 'Logout User : NGURAH KETUT HARIADI, IP : 127.0.0.1'),
(98, '2016-05-22 06:19:37', 1, 'Login', 'Login User : NGURAH KETUT HARIADI, IP : 127.0.0.1'),
(99, '2016-05-22 06:21:35', 1, 'Login', 'Login User : NGURAH KETUT HARIADI, IP : 127.0.0.1'),
(100, '2016-05-22 06:22:09', 1, 'Logout', 'Logout User : NGURAH KETUT HARIADI, IP : 127.0.0.1'),
(101, '2016-05-22 06:22:46', 1, 'Login', 'Login User : NGURAH KETUT HARIADI, IP : 127.0.0.1'),
(102, '2016-05-22 06:24:59', 1, 'Logout', 'Logout User : NGURAH KETUT HARIADI, IP : 127.0.0.1'),
(103, '2016-05-22 06:25:20', 1, 'Login', 'Login User : NGURAH KETUT HARIADI, IP : 127.0.0.1'),
(104, '2016-05-24 08:53:38', 1, 'Login', 'Login User : NGURAH KETUT HARIADI, IP : 192.168.8.100'),
(105, '2016-05-24 08:54:05', 1, 'Logout', 'Logout User : NGURAH KETUT HARIADI, IP : 192.168.8.100'),
(106, '2016-05-24 08:55:07', 1, 'Login', 'Login User : NGURAH KETUT HARIADI, IP : 192.168.8.100'),
(107, '2016-05-24 08:57:29', 1, 'Logout', 'Logout User : NGURAH KETUT HARIADI, IP : 192.168.8.100'),
(108, '2016-05-24 08:58:19', 1, 'Login', 'Login User : NGURAH KETUT HARIADI, IP : 192.168.8.100'),
(109, '2016-05-24 08:58:29', 1, 'Logout', 'Logout User : NGURAH KETUT HARIADI, IP : 192.168.8.100'),
(110, '2016-05-24 08:59:35', 1, 'Login', 'Login User : NGURAH KETUT HARIADI, IP : 192.168.8.100'),
(111, '2016-05-24 09:00:00', 1, 'Logout', 'Logout User : NGURAH KETUT HARIADI, IP : 192.168.8.100'),
(112, '2016-05-24 09:01:32', 1, 'Login', 'Login User : NGURAH KETUT HARIADI, IP : 192.168.8.100'),
(113, '2016-05-24 09:01:43', 1, 'Logout', 'Logout User : NGURAH KETUT HARIADI, IP : 192.168.8.100'),
(114, '2016-05-24 09:02:10', 1, 'Login', 'Login User : NGURAH KETUT HARIADI, IP : 192.168.8.100'),
(115, '2016-05-24 09:49:40', 1, 'Logout', 'Logout User : NGURAH KETUT HARIADI, IP : 192.168.8.100'),
(116, '2016-05-25 12:34:07', 1, 'Login', 'Login User : NGURAH KETUT HARIADI, IP : 192.168.8.100'),
(117, '2016-05-25 12:35:06', 1, 'Logout', 'Logout User : NGURAH KETUT HARIADI, IP : 192.168.8.100'),
(118, '2016-05-25 12:38:03', 1, 'Login', 'Login User : NGURAH KETUT HARIADI, IP : 192.168.8.100'),
(119, '2016-05-25 12:38:30', 1, 'Logout', 'Logout User : NGURAH KETUT HARIADI, IP : 192.168.8.100'),
(120, '2016-05-25 13:12:29', 1, 'Login', 'Login User : NGURAH KETUT HARIADI, IP : 127.0.0.1'),
(121, '2016-05-25 13:12:53', 1, 'Logout', 'Logout User : NGURAH KETUT HARIADI, IP : 127.0.0.1'),
(122, '2016-05-25 13:44:05', 1, 'Login', 'Login User : NGURAH KETUT HARIADI, IP : 127.0.0.1'),
(123, '2016-05-25 13:44:56', 1, 'Logout', 'Logout User : NGURAH KETUT HARIADI, IP : 127.0.0.1'),
(124, '2016-05-25 13:45:53', 1, 'Login', 'Login User : NGURAH KETUT HARIADI, IP : 127.0.0.1'),
(125, '2016-05-25 13:49:15', 1, 'Logout', 'Logout User : NGURAH KETUT HARIADI, IP : 127.0.0.1'),
(126, '2016-05-25 13:55:24', 1, 'Login', 'Login User : NGURAH KETUT HARIADI, IP : 127.0.0.1'),
(127, '2016-05-25 13:55:59', 1, 'Logout', 'Logout User : NGURAH KETUT HARIADI, IP : 127.0.0.1'),
(128, '2016-05-25 14:10:05', 1, 'Login', 'Login User : NGURAH KETUT HARIADI, IP : 127.0.0.1'),
(129, '2016-05-25 14:10:50', 1, 'Logout', 'Logout User : NGURAH KETUT HARIADI, IP : 127.0.0.1'),
(130, '2016-05-25 14:13:02', 1, 'Login', 'Login User : NGURAH KETUT HARIADI, IP : 127.0.0.1'),
(131, '2016-05-25 14:13:41', 1, 'Logout', 'Logout User : NGURAH KETUT HARIADI, IP : 127.0.0.1'),
(132, '2016-05-25 14:40:37', 1, 'Login', 'Login User : NGURAH KETUT HARIADI, IP : 192.168.8.100'),
(133, '2016-05-25 15:09:07', 1, 'Login', 'Login User : NGURAH KETUT HARIADI, IP : 192.168.8.100'),
(134, '2016-05-25 15:09:46', 1, 'Logout', 'Logout User : NGURAH KETUT HARIADI, IP : 192.168.8.100'),
(135, '2016-05-26 09:16:28', 1, 'Login', 'Login User : NGURAH KETUT HARIADI, IP : 127.0.0.1'),
(136, '2016-05-26 09:18:36', 1, 'Logout', 'Logout User : NGURAH KETUT HARIADI, IP : 127.0.0.1'),
(137, '2016-05-26 09:21:09', 1, 'Login', 'Login User : NGURAH KETUT HARIADI, IP : 127.0.0.1'),
(138, '2016-05-26 09:24:50', 1, 'Logout', 'Logout User : NGURAH KETUT HARIADI, IP : 127.0.0.1'),
(139, '2016-05-26 09:28:50', 1, 'Login', 'Login User : NGURAH KETUT HARIADI, IP : 127.0.0.1'),
(140, '2016-05-26 09:31:20', 1, 'Logout', 'Logout User : NGURAH KETUT HARIADI, IP : 127.0.0.1'),
(141, '2016-05-26 09:35:42', 1, 'Login', 'Login User : NGURAH KETUT HARIADI, IP : 127.0.0.1'),
(142, '2016-05-26 09:40:01', 1, 'Login', 'Login User : NGURAH KETUT HARIADI, IP : 127.0.0.1'),
(143, '2016-05-26 09:41:03', 1, 'Logout', 'Logout User : NGURAH KETUT HARIADI, IP : 127.0.0.1'),
(144, '2016-05-26 09:42:37', 1, 'Login', 'Login User : NGURAH KETUT HARIADI, IP : 127.0.0.1'),
(145, '2016-05-26 09:43:37', 1, 'Logout', 'Logout User : NGURAH KETUT HARIADI, IP : 127.0.0.1'),
(146, '2016-05-26 09:45:40', 1, 'Login', 'Login User : NGURAH KETUT HARIADI, IP : 127.0.0.1'),
(147, '2016-05-26 09:46:47', 1, 'Logout', 'Logout User : NGURAH KETUT HARIADI, IP : 127.0.0.1'),
(148, '2016-05-26 09:50:50', 1, 'Login', 'Login User : NGURAH KETUT HARIADI, IP : 127.0.0.1'),
(149, '2016-05-26 09:51:39', 1, 'Logout', 'Logout User : NGURAH KETUT HARIADI, IP : 127.0.0.1'),
(150, '2016-05-26 10:47:44', 1, 'Login', 'Login User : NGURAH KETUT HARIADI, IP : 127.0.0.1'),
(151, '2016-05-26 10:48:00', 1, 'Logout', 'Logout User : NGURAH KETUT HARIADI, IP : 127.0.0.1'),
(152, '2016-05-26 10:48:43', 1, 'Login', 'Login User : NGURAH KETUT HARIADI, IP : 127.0.0.1'),
(153, '2016-05-26 10:49:21', 1, 'Logout', 'Logout User : NGURAH KETUT HARIADI, IP : 127.0.0.1'),
(154, '2016-05-26 10:51:53', 1, 'Login', 'Login User : NGURAH KETUT HARIADI, IP : 127.0.0.1'),
(155, '2016-05-26 10:52:13', 1, 'Logout', 'Logout User : NGURAH KETUT HARIADI, IP : 127.0.0.1'),
(156, '2016-05-26 10:52:57', 1, 'Login', 'Login User : NGURAH KETUT HARIADI, IP : 127.0.0.1'),
(157, '2016-05-26 10:53:49', 1, 'Logout', 'Logout User : NGURAH KETUT HARIADI, IP : 127.0.0.1'),
(158, '2016-05-26 10:54:11', 1, 'Login', 'Login User : NGURAH KETUT HARIADI, IP : 127.0.0.1'),
(159, '2016-05-26 10:54:27', 1, 'Logout', 'Logout User : NGURAH KETUT HARIADI, IP : 127.0.0.1'),
(160, '2016-05-26 10:55:11', 1, 'Login', 'Login User : NGURAH KETUT HARIADI, IP : 127.0.0.1'),
(161, '2016-05-26 10:56:15', 1, 'Logout', 'Logout User : NGURAH KETUT HARIADI, IP : 127.0.0.1'),
(162, '2016-05-26 10:57:44', 1, 'Login', 'Login User : NGURAH KETUT HARIADI, IP : 127.0.0.1'),
(163, '2016-05-26 10:58:47', 1, 'Logout', 'Logout User : NGURAH KETUT HARIADI, IP : 127.0.0.1'),
(164, '2016-05-26 11:03:53', 1, 'Login', 'Login User : NGURAH KETUT HARIADI, IP : 127.0.0.1'),
(165, '2016-05-26 11:08:07', 1, 'Logout', 'Logout User : NGURAH KETUT HARIADI, IP : 127.0.0.1'),
(166, '2016-05-26 11:12:17', 1, 'Login', 'Login User : NGURAH KETUT HARIADI, IP : 127.0.0.1'),
(167, '2016-05-26 11:13:07', 1, 'Logout', 'Logout User : NGURAH KETUT HARIADI, IP : 127.0.0.1'),
(168, '2016-05-26 11:17:08', 1, 'Login', 'Login User : NGURAH KETUT HARIADI, IP : 127.0.0.1'),
(169, '2016-05-26 11:17:18', 1, 'Logout', 'Logout User : NGURAH KETUT HARIADI, IP : 127.0.0.1'),
(170, '2016-05-26 11:17:45', 1, 'Login', 'Login User : NGURAH KETUT HARIADI, IP : 127.0.0.1'),
(171, '2016-05-26 11:18:03', 1, 'Logout', 'Logout User : NGURAH KETUT HARIADI, IP : 127.0.0.1'),
(172, '2016-05-26 11:18:20', 1, 'Login', 'Login User : NGURAH KETUT HARIADI, IP : 127.0.0.1'),
(173, '2016-05-26 11:18:35', 1, 'Logout', 'Logout User : NGURAH KETUT HARIADI, IP : 127.0.0.1'),
(174, '2016-05-26 23:18:57', 1, 'Login', 'Login User : NGURAH KETUT HARIADI, IP : 192.168.8.100'),
(175, '2016-05-26 23:22:29', 1, 'Logout', 'Logout User : NGURAH KETUT HARIADI, IP : 192.168.8.100'),
(176, '2016-05-26 23:22:47', 1, 'Login', 'Login User : NGURAH KETUT HARIADI, IP : 192.168.8.100'),
(177, '2016-05-26 23:36:29', 1, 'Login', 'Login User : NGURAH KETUT HARIADI, IP : 192.168.8.100'),
(178, '2016-05-26 23:37:11', 1, 'Logout', 'Logout User : NGURAH KETUT HARIADI, IP : 192.168.8.100'),
(179, '2016-05-26 23:44:46', 1, 'Logout', 'Logout User : NGURAH KETUT HARIADI, IP : 192.168.8.100'),
(180, '2016-05-26 23:45:05', 3, 'Login', 'Login User : JONI, IP : 192.168.8.100'),
(181, '2016-05-26 23:45:16', 3, 'Logout', 'Logout User : JONI, IP : 192.168.8.100'),
(182, '2016-05-26 23:47:38', 1, 'Login', 'Login User : NGURAH KETUT HARIADI, IP : 192.168.8.100'),
(183, '2016-05-26 23:47:46', 1, 'Logout', 'Logout User : NGURAH KETUT HARIADI, IP : 192.168.8.100'),
(184, '2016-05-26 23:47:54', 3, 'Login', 'Login User : JONI, IP : 192.168.8.100'),
(185, '2016-05-26 23:48:05', 3, 'Logout', 'Logout User : JONI, IP : 192.168.8.100'),
(186, '2016-05-26 23:49:03', 3, 'Login', 'Login User : JONI, IP : 192.168.8.100'),
(187, '2016-05-27 00:08:34', 3, 'Logout', 'Logout User : JONI, IP : 127.0.0.1'),
(188, '2016-05-27 00:08:39', 4, 'Login', 'Login User : COKI, IP : 127.0.0.1'),
(189, '2016-05-27 02:21:39', 1, 'Login', 'Login User : NGURAH KETUT HARIADI, IP : 192.168.206.191'),
(190, '2016-05-27 02:22:24', 1, 'Logout', 'Logout User : NGURAH KETUT HARIADI, IP : 192.168.206.191'),
(191, '2016-05-27 02:22:33', 1, 'Login', 'Login User : NGURAH KETUT HARIADI, IP : 192.168.206.191'),
(192, '2016-05-27 02:23:06', 1, 'Logout', 'Logout User : NGURAH KETUT HARIADI, IP : 192.168.206.191'),
(193, '2016-05-27 02:23:13', 4, 'Login', 'Login User : COKI, IP : 192.168.206.191'),
(194, '2016-05-27 02:23:30', 4, 'Logout', 'Logout User : COKI, IP : 192.168.206.191'),
(195, '2016-05-27 02:23:34', 1, 'Login', 'Login User : NGURAH KETUT HARIADI, IP : 192.168.206.191'),
(196, '2016-05-27 02:23:43', 1, 'Logout', 'Logout User : NGURAH KETUT HARIADI, IP : 192.168.206.191'),
(197, '2016-05-27 02:36:52', 1, 'Login', 'Login User : NGURAH KETUT HARIADI, IP : 192.168.206.191'),
(198, '2016-05-27 02:57:41', 1, 'Logout', 'Logout User : NGURAH KETUT HARIADI, IP : 192.168.206.191'),
(199, '2016-05-27 02:57:53', 3, 'Login', 'Login User : JONI, IP : 192.168.206.191'),
(200, '2016-05-27 03:10:00', 3, 'Logout', 'Logout User : JONI, IP : 192.168.206.191'),
(201, '2016-05-27 03:10:13', 4, 'Login', 'Login User : COKI, IP : 192.168.206.191'),
(202, '2016-05-27 03:14:57', 4, 'Logout', 'Logout User : COKI, IP : 192.168.206.191'),
(203, '2016-05-27 03:15:00', 1, 'Login', 'Login User : NGURAH KETUT HARIADI, IP : 192.168.206.191'),
(204, '2016-05-27 03:15:39', 1, 'Logout', 'Logout User : NGURAH KETUT HARIADI, IP : 192.168.206.191'),
(205, '2016-05-27 03:15:45', 1, 'Login', 'Login User : NGURAH KETUT HARIADI, IP : 192.168.206.191'),
(206, '2016-05-27 04:05:02', 1, 'Logout', 'Logout User : NGURAH KETUT HARIADI, IP : 192.168.206.191'),
(207, '2016-05-27 10:00:54', 1, 'Login', 'Login User : NGURAH KETUT HARIADI, IP : 127.0.0.1'),
(208, '2016-05-27 10:01:03', 1, 'Logout', 'Logout User : NGURAH KETUT HARIADI, IP : 127.0.0.1'),
(209, '2016-06-15 07:29:25', 1, 'Login', 'Login User : NGURAH KETUT HARIADI, IP : 192.168.8.100'),
(210, '2016-06-15 07:31:43', 1, 'Logout', 'Logout User : NGURAH KETUT HARIADI, IP : 192.168.8.100'),
(211, '2016-06-15 08:15:47', 1, 'Login', 'Login User : NGURAH KETUT HARIADI, IP : 192.168.8.100'),
(212, '2016-06-15 08:42:03', 1, 'Logout', 'Logout User : NGURAH KETUT HARIADI, IP : 192.168.8.100'),
(213, '2016-06-15 08:43:02', 4, 'Login', 'Login User : COKI, IP : 192.168.8.100'),
(214, '2016-06-15 08:45:24', 4, 'Logout', 'Logout User : COKI, IP : 192.168.8.100'),
(215, '2016-06-15 08:45:28', 1, 'Login', 'Login User : NGURAH KETUT HARIADI, IP : 192.168.8.100'),
(216, '2016-06-15 09:08:01', 1, 'Logout', 'Logout User : NGURAH KETUT HARIADI, IP : 192.168.8.100'),
(217, '2016-07-14 01:09:31', 1, 'Login', 'Login User : NGURAH KETUT HARIADI, IP : 127.0.0.1'),
(218, '2016-07-14 01:10:20', 1, 'Logout', 'Logout User : NGURAH KETUT HARIADI, IP : 127.0.0.1'),
(219, '2016-07-14 01:12:51', 1, 'Login', 'Login User : NGURAH KETUT HARIADI, IP : 127.0.0.1'),
(220, '2016-07-14 01:13:28', 1, 'Logout', 'Logout User : NGURAH KETUT HARIADI, IP : 127.0.0.1'),
(221, '2016-07-14 01:15:41', 1, 'Login', 'Login User : NGURAH KETUT HARIADI, IP : 127.0.0.1'),
(222, '2016-07-14 01:15:57', 1, 'Logout', 'Logout User : NGURAH KETUT HARIADI, IP : 127.0.0.1'),
(223, '2016-07-14 01:16:26', 1, 'Login', 'Login User : NGURAH KETUT HARIADI, IP : 127.0.0.1'),
(224, '2016-07-14 01:18:07', 1, 'Logout', 'Logout User : NGURAH KETUT HARIADI, IP : 127.0.0.1'),
(225, '2016-07-14 01:47:21', 1, 'Login', 'Login User : NGURAH KETUT HARIADI, IP : 127.0.0.1'),
(226, '2016-07-14 01:47:55', 1, 'Logout', 'Logout User : NGURAH KETUT HARIADI, IP : 127.0.0.1'),
(227, '2016-07-14 01:49:28', 1, 'Login', 'Login User : NGURAH KETUT HARIADI, IP : 127.0.0.1'),
(228, '2016-07-14 01:50:05', 1, 'Logout', 'Logout User : NGURAH KETUT HARIADI, IP : 127.0.0.1'),
(229, '2016-07-14 01:50:45', 1, 'Login', 'Login User : NGURAH KETUT HARIADI, IP : 127.0.0.1'),
(230, '2016-07-14 01:51:00', 1, 'Logout', 'Logout User : NGURAH KETUT HARIADI, IP : 127.0.0.1'),
(231, '2016-07-14 01:51:41', 1, 'Login', 'Login User : NGURAH KETUT HARIADI, IP : 127.0.0.1'),
(232, '2016-07-14 01:52:16', 1, 'Logout', 'Logout User : NGURAH KETUT HARIADI, IP : 127.0.0.1'),
(233, '2016-07-14 02:23:33', 1, 'Login', 'Login User : NGURAH KETUT HARIADI, IP : 127.0.0.1'),
(234, '2016-07-14 02:24:10', 1, 'Logout', 'Logout User : NGURAH KETUT HARIADI, IP : 127.0.0.1'),
(235, '2016-07-14 02:42:07', 1, 'Login', 'Login User : NGURAH KETUT HARIADI, IP : 127.0.0.1'),
(236, '2016-07-14 02:43:26', 1, 'Logout', 'Logout User : NGURAH KETUT HARIADI, IP : 127.0.0.1'),
(237, '2016-07-14 03:01:44', 1, 'Login', 'Login User : NGURAH KETUT HARIADI, IP : 127.0.0.1'),
(238, '2016-07-14 03:03:17', 1, 'Logout', 'Logout User : NGURAH KETUT HARIADI, IP : 127.0.0.1'),
(239, '2016-07-14 03:04:12', 1, 'Login', 'Login User : NGURAH KETUT HARIADI, IP : 127.0.0.1'),
(240, '2016-07-14 03:04:24', 1, 'Logout', 'Logout User : NGURAH KETUT HARIADI, IP : 127.0.0.1'),
(241, '2016-07-14 03:05:02', 1, 'Login', 'Login User : NGURAH KETUT HARIADI, IP : 127.0.0.1'),
(242, '2016-07-14 03:05:29', 1, 'Logout', 'Logout User : NGURAH KETUT HARIADI, IP : 127.0.0.1'),
(243, '2016-07-14 03:06:26', 1, 'Login', 'Login User : NGURAH KETUT HARIADI, IP : 127.0.0.1'),
(244, '2016-07-14 03:06:47', 1, 'Logout', 'Logout User : NGURAH KETUT HARIADI, IP : 127.0.0.1'),
(245, '2016-07-14 03:07:02', 1, 'Login', 'Login User : NGURAH KETUT HARIADI, IP : 127.0.0.1'),
(246, '2016-07-14 03:07:53', 1, 'Logout', 'Logout User : NGURAH KETUT HARIADI, IP : 127.0.0.1'),
(247, '2016-07-14 03:09:23', 1, 'Login', 'Login User : NGURAH KETUT HARIADI, IP : 127.0.0.1'),
(248, '2016-07-14 03:13:45', 1, 'Login', 'Login User : NGURAH KETUT HARIADI, IP : 127.0.0.1'),
(249, '2016-07-14 03:14:26', 1, 'Logout', 'Logout User : NGURAH KETUT HARIADI, IP : 127.0.0.1'),
(250, '2016-07-14 03:15:38', 1, 'Login', 'Login User : NGURAH KETUT HARIADI, IP : 127.0.0.1'),
(251, '2016-07-14 03:15:47', 1, 'Logout', 'Logout User : NGURAH KETUT HARIADI, IP : 127.0.0.1'),
(252, '2016-07-14 03:20:51', 1, 'Login', 'Login User : NGURAH KETUT HARIADI, IP : 127.0.0.1'),
(253, '2016-07-14 03:21:52', 1, 'Logout', 'Logout User : NGURAH KETUT HARIADI, IP : 127.0.0.1'),
(254, '2016-07-14 03:27:15', 1, 'Login', 'Login User : NGURAH KETUT HARIADI, IP : 127.0.0.1'),
(255, '2016-07-14 03:27:47', 1, 'Logout', 'Logout User : NGURAH KETUT HARIADI, IP : 127.0.0.1'),
(256, '2016-07-14 04:24:18', 1, 'Login', 'Login User : NGURAH KETUT HARIADI, IP : 127.0.0.1'),
(257, '2016-07-14 04:26:17', 1, 'Logout', 'Logout User : NGURAH KETUT HARIADI, IP : 127.0.0.1'),
(258, '2016-07-14 04:33:34', 1, 'Login', 'Login User : NGURAH KETUT HARIADI, IP : 127.0.0.1'),
(259, '2016-07-14 04:34:42', 1, 'Logout', 'Logout User : NGURAH KETUT HARIADI, IP : 127.0.0.1'),
(260, '2016-07-14 04:52:00', 1, 'Login', 'Login User : NGURAH KETUT HARIADI, IP : 127.0.0.1'),
(261, '2016-07-14 04:52:41', 1, 'Logout', 'Logout User : NGURAH KETUT HARIADI, IP : 127.0.0.1'),
(262, '2016-07-14 05:11:42', 1, 'Login', 'Login User : NGURAH KETUT HARIADI, IP : 127.0.0.1'),
(263, '2016-07-14 05:11:55', 1, 'Logout', 'Logout User : NGURAH KETUT HARIADI, IP : 127.0.0.1'),
(264, '2016-07-14 05:12:46', 1, 'Login', 'Login User : NGURAH KETUT HARIADI, IP : 127.0.0.1'),
(265, '2016-07-14 05:13:29', 1, 'Login', 'Login User : NGURAH KETUT HARIADI, IP : 127.0.0.1'),
(266, '2016-07-14 05:17:42', 1, 'Logout', 'Logout User : NGURAH KETUT HARIADI, IP : 127.0.0.1'),
(267, '2016-07-14 07:22:44', 1, 'Login', 'Login User : NGURAH KETUT HARIADI, IP : 127.0.0.1'),
(268, '2016-07-14 07:55:19', 1, 'Login', 'Login User : NGURAH KETUT HARIADI, IP : 10.1.1.123'),
(269, '2016-07-14 07:58:54', 1, 'Logout', 'Logout User : NGURAH KETUT HARIADI, IP : 10.1.1.123'),
(270, '2016-07-14 07:58:59', 6, 'Login', 'Login User : FITRIA AGUSTINA, IP : 10.1.1.123'),
(271, '2016-07-14 08:13:36', 6, 'Logout', 'Logout User : FITRIA AGUSTINA, IP : 10.1.1.123'),
(272, '2016-07-16 04:01:09', 1, 'Login', 'Login User : NGURAH KETUT HARIADI, IP : 127.0.0.1'),
(273, '2016-07-16 05:57:28', 1, 'Login', 'Login User : NGURAH KETUT HARIADI, IP : 127.0.0.1'),
(274, '2016-07-16 05:57:45', 1, 'Logout', 'Logout User : NGURAH KETUT HARIADI, IP : 127.0.0.1'),
(275, '2016-07-16 06:01:42', 1, 'Login', 'Login User : NGURAH KETUT HARIADI, IP : 127.0.0.1'),
(276, '2016-07-16 06:02:02', 1, 'Logout', 'Logout User : NGURAH KETUT HARIADI, IP : 127.0.0.1'),
(277, '2016-07-16 06:02:47', 1, 'Login', 'Login User : NGURAH KETUT HARIADI, IP : 127.0.0.1'),
(278, '2016-07-16 06:03:06', 1, 'Logout', 'Logout User : NGURAH KETUT HARIADI, IP : 127.0.0.1'),
(279, '2016-07-16 06:05:24', 1, 'Login', 'Login User : NGURAH KETUT HARIADI, IP : 127.0.0.1'),
(280, '2016-07-16 06:09:24', 1, 'Logout', 'Logout User : NGURAH KETUT HARIADI, IP : 127.0.0.1'),
(281, '2016-07-18 03:46:22', 1, 'Login', 'Login User : NGURAH KETUT HARIADI, IP : 127.0.0.1'),
(282, '2016-07-18 03:47:31', 1, 'Login', 'Login User : NGURAH KETUT HARIADI, IP : 127.0.0.1'),
(283, '2016-07-18 03:47:42', 1, 'Logout', 'Logout User : NGURAH KETUT HARIADI, IP : 127.0.0.1'),
(284, '2016-07-18 03:50:26', 1, 'Login', 'Login User : NGURAH KETUT HARIADI, IP : 127.0.0.1'),
(285, '2016-07-18 03:50:40', 1, 'Logout', 'Logout User : NGURAH KETUT HARIADI, IP : 127.0.0.1'),
(286, '2016-07-18 03:52:20', 1, 'Login', 'Login User : NGURAH KETUT HARIADI, IP : 127.0.0.1'),
(287, '2016-07-18 03:53:27', 1, 'Logout', 'Logout User : NGURAH KETUT HARIADI, IP : 127.0.0.1'),
(288, '2016-07-18 03:59:45', 1, 'Login', 'Login User : NGURAH KETUT HARIADI, IP : 127.0.0.1'),
(289, '2016-07-18 04:00:37', 1, 'Login', 'Login User : NGURAH KETUT HARIADI, IP : 127.0.0.1'),
(290, '2016-07-18 04:00:49', 1, 'Logout', 'Logout User : NGURAH KETUT HARIADI, IP : 127.0.0.1'),
(291, '2016-07-18 04:01:22', 1, 'Login', 'Login User : NGURAH KETUT HARIADI, IP : 127.0.0.1'),
(292, '2016-07-18 04:01:46', 1, 'Logout', 'Logout User : NGURAH KETUT HARIADI, IP : 127.0.0.1'),
(293, '2016-07-18 04:03:23', 1, 'Login', 'Login User : NGURAH KETUT HARIADI, IP : 127.0.0.1'),
(294, '2016-07-18 04:03:54', 1, 'Logout', 'Logout User : NGURAH KETUT HARIADI, IP : 127.0.0.1'),
(295, '2016-07-18 04:05:33', 1, 'Login', 'Login User : NGURAH KETUT HARIADI, IP : 127.0.0.1'),
(296, '2016-07-18 04:05:57', 1, 'Logout', 'Logout User : NGURAH KETUT HARIADI, IP : 127.0.0.1'),
(297, '2016-07-18 04:09:15', 1, 'Login', 'Login User : NGURAH KETUT HARIADI, IP : 127.0.0.1'),
(298, '2016-07-18 04:09:27', 1, 'Logout', 'Logout User : NGURAH KETUT HARIADI, IP : 127.0.0.1'),
(299, '2016-07-18 04:11:17', 1, 'Login', 'Login User : NGURAH KETUT HARIADI, IP : 127.0.0.1'),
(300, '2016-07-18 04:11:33', 1, 'Logout', 'Logout User : NGURAH KETUT HARIADI, IP : 127.0.0.1'),
(301, '2016-07-18 04:12:00', 1, 'Login', 'Login User : NGURAH KETUT HARIADI, IP : 127.0.0.1'),
(302, '2016-07-18 04:12:16', 1, 'Logout', 'Logout User : NGURAH KETUT HARIADI, IP : 127.0.0.1'),
(303, '2016-07-18 04:14:16', 1, 'Login', 'Login User : NGURAH KETUT HARIADI, IP : 127.0.0.1'),
(304, '2016-07-18 04:14:35', 1, 'Logout', 'Logout User : NGURAH KETUT HARIADI, IP : 127.0.0.1'),
(305, '2016-07-18 04:17:13', 1, 'Login', 'Login User : NGURAH KETUT HARIADI, IP : 127.0.0.1'),
(306, '2016-07-18 04:19:23', 1, 'Logout', 'Logout User : NGURAH KETUT HARIADI, IP : 127.0.0.1'),
(307, '2016-07-18 04:20:37', 1, 'Login', 'Login User : NGURAH KETUT HARIADI, IP : 127.0.0.1'),
(308, '2016-07-18 04:20:48', 1, 'Logout', 'Logout User : NGURAH KETUT HARIADI, IP : 127.0.0.1'),
(309, '2016-07-18 04:24:02', 1, 'Login', 'Login User : NGURAH KETUT HARIADI, IP : 127.0.0.1'),
(310, '2016-07-18 04:24:17', 1, 'Logout', 'Logout User : NGURAH KETUT HARIADI, IP : 127.0.0.1'),
(311, '2016-07-18 04:24:36', 1, 'Login', 'Login User : NGURAH KETUT HARIADI, IP : 127.0.0.1'),
(312, '2016-07-18 04:24:53', 1, 'Logout', 'Logout User : NGURAH KETUT HARIADI, IP : 127.0.0.1'),
(313, '2016-07-18 04:27:34', 1, 'Login', 'Login User : NGURAH KETUT HARIADI, IP : 127.0.0.1'),
(314, '2016-07-18 04:28:05', 1, 'Logout', 'Logout User : NGURAH KETUT HARIADI, IP : 127.0.0.1'),
(315, '2016-07-18 04:28:39', 1, 'Login', 'Login User : NGURAH KETUT HARIADI, IP : 127.0.0.1'),
(316, '2016-07-18 04:29:00', 1, 'Logout', 'Logout User : NGURAH KETUT HARIADI, IP : 127.0.0.1'),
(317, '2016-07-18 04:36:17', 1, 'Login', 'Login User : NGURAH KETUT HARIADI, IP : 127.0.0.1'),
(318, '2016-07-18 04:36:29', 1, 'Logout', 'Logout User : NGURAH KETUT HARIADI, IP : 127.0.0.1'),
(319, '2016-07-18 04:37:03', 1, 'Login', 'Login User : NGURAH KETUT HARIADI, IP : 127.0.0.1'),
(320, '2016-07-18 04:37:19', 1, 'Logout', 'Logout User : NGURAH KETUT HARIADI, IP : 127.0.0.1'),
(321, '2016-07-18 04:37:52', 1, 'Login', 'Login User : NGURAH KETUT HARIADI, IP : 127.0.0.1'),
(322, '2016-07-18 04:38:42', 1, 'Login', 'Login User : NGURAH KETUT HARIADI, IP : 127.0.0.1'),
(323, '2016-07-18 04:38:52', 1, 'Logout', 'Logout User : NGURAH KETUT HARIADI, IP : 127.0.0.1'),
(324, '2016-07-18 04:39:14', 1, 'Login', 'Login User : NGURAH KETUT HARIADI, IP : 127.0.0.1'),
(325, '2016-07-18 04:40:07', 1, 'Login', 'Login User : NGURAH KETUT HARIADI, IP : 127.0.0.1'),
(326, '2016-07-18 04:40:27', 1, 'Login', 'Login User : NGURAH KETUT HARIADI, IP : 127.0.0.1'),
(327, '2016-07-18 04:40:37', 1, 'Logout', 'Logout User : NGURAH KETUT HARIADI, IP : 127.0.0.1'),
(328, '2016-07-18 04:44:24', 1, 'Login', 'Login User : NGURAH KETUT HARIADI, IP : 127.0.0.1'),
(329, '2016-07-18 04:45:46', 1, 'Logout', 'Logout User : NGURAH KETUT HARIADI, IP : 127.0.0.1'),
(330, '2016-07-18 04:48:07', 1, 'Login', 'Login User : NGURAH KETUT HARIADI, IP : 127.0.0.1'),
(331, '2016-07-18 04:50:11', 1, 'Logout', 'Logout User : NGURAH KETUT HARIADI, IP : 127.0.0.1'),
(332, '2016-07-18 05:02:06', 1, 'Login', 'Login User : NGURAH KETUT HARIADI, IP : 127.0.0.1'),
(333, '2016-07-18 05:02:19', 1, 'Logout', 'Logout User : NGURAH KETUT HARIADI, IP : 127.0.0.1'),
(334, '2016-07-18 05:03:27', 1, 'Login', 'Login User : NGURAH KETUT HARIADI, IP : 127.0.0.1'),
(335, '2016-07-18 05:03:36', 1, 'Logout', 'Logout User : NGURAH KETUT HARIADI, IP : 127.0.0.1'),
(336, '2016-07-18 05:05:14', 1, 'Login', 'Login User : NGURAH KETUT HARIADI, IP : 127.0.0.1'),
(337, '2016-07-18 05:05:32', 1, 'Logout', 'Logout User : NGURAH KETUT HARIADI, IP : 127.0.0.1'),
(338, '2016-07-18 05:06:06', 1, 'Login', 'Login User : NGURAH KETUT HARIADI, IP : 127.0.0.1'),
(339, '2016-07-18 05:06:38', 1, 'Logout', 'Logout User : NGURAH KETUT HARIADI, IP : 127.0.0.1'),
(340, '2016-07-18 05:09:37', 1, 'Login', 'Login User : NGURAH KETUT HARIADI, IP : 127.0.0.1'),
(341, '2016-07-18 05:11:02', 1, 'Logout', 'Logout User : NGURAH KETUT HARIADI, IP : 127.0.0.1'),
(342, '2016-07-18 05:22:47', 1, 'Login', 'Login User : NGURAH KETUT HARIADI, IP : 192.168.8.101'),
(343, '2016-07-18 05:23:19', 1, 'Logout', 'Logout User : NGURAH KETUT HARIADI, IP : 192.168.8.101'),
(344, '2016-07-18 05:24:34', 1, 'Login', 'Login User : NGURAH KETUT HARIADI, IP : 192.168.8.101'),
(345, '2016-07-18 05:48:19', 1, 'Logout', 'Logout User : NGURAH KETUT HARIADI, IP : 192.168.8.101'),
(346, '2016-07-18 06:07:42', 1, 'Login', 'Login User : NGURAH KETUT HARIADI, IP : 127.0.0.1'),
(347, '2016-07-18 06:08:04', 1, 'Logout', 'Logout User : NGURAH KETUT HARIADI, IP : 127.0.0.1'),
(348, '2016-07-18 06:10:41', 1, 'Login', 'Login User : NGURAH KETUT HARIADI, IP : 127.0.0.1'),
(349, '2016-07-18 06:11:19', 1, 'Logout', 'Logout User : NGURAH KETUT HARIADI, IP : 127.0.0.1'),
(350, '2016-07-18 06:12:16', 1, 'Login', 'Login User : NGURAH KETUT HARIADI, IP : 127.0.0.1'),
(351, '2016-07-18 06:12:56', 1, 'Logout', 'Logout User : NGURAH KETUT HARIADI, IP : 127.0.0.1'),
(352, '2016-07-18 06:42:06', 1, 'Login', 'Login User : NGURAH KETUT HARIADI, IP : 192.168.8.101'),
(353, '2016-07-18 06:42:22', 1, 'Logout', 'Logout User : NGURAH KETUT HARIADI, IP : 192.168.8.101'),
(354, '2016-07-18 06:45:46', 1, 'Login', 'Login User : NGURAH KETUT HARIADI, IP : 192.168.8.101'),
(355, '2016-07-18 06:46:22', 1, 'Logout', 'Logout User : NGURAH KETUT HARIADI, IP : 192.168.8.101'),
(356, '2016-07-18 07:09:20', 1, 'Login', 'Login User : NGURAH KETUT HARIADI, IP : 127.0.0.1'),
(357, '2016-07-18 07:09:27', 1, 'Logout', 'Logout User : NGURAH KETUT HARIADI, IP : 127.0.0.1'),
(358, '2016-07-18 07:14:42', 1, 'Login', 'Login User : NGURAH KETUT HARIADI, IP : 192.168.8.101'),
(359, '2016-07-18 07:14:52', 1, 'Logout', 'Logout User : NGURAH KETUT HARIADI, IP : 192.168.8.101'),
(360, '2016-07-18 07:15:48', 1, 'Login', 'Login User : NGURAH KETUT HARIADI, IP : 192.168.8.101'),
(361, '2016-07-18 07:15:52', 1, 'Logout', 'Logout User : NGURAH KETUT HARIADI, IP : 192.168.8.101'),
(362, '2016-07-18 07:21:15', 1, 'Login', 'Login User : NGURAH KETUT HARIADI, IP : 192.168.8.101'),
(363, '2016-07-18 07:21:30', 1, 'Logout', 'Logout User : NGURAH KETUT HARIADI, IP : 192.168.8.101'),
(364, '2016-07-18 09:52:14', 1, 'Login', 'Login User : NGURAH KETUT HARIADI, IP : 192.168.8.101'),
(365, '2016-07-18 09:52:34', 1, 'Logout', 'Logout User : NGURAH KETUT HARIADI, IP : 192.168.8.101'),
(366, '2016-07-18 09:56:07', 1, 'Login', 'Login User : NGURAH KETUT HARIADI, IP : 192.168.8.101'),
(367, '2016-07-18 09:56:32', 1, 'Logout', 'Logout User : NGURAH KETUT HARIADI, IP : 192.168.8.101'),
(368, '2016-07-18 09:59:31', 1, 'Login', 'Login User : NGURAH KETUT HARIADI, IP : 192.168.8.101'),
(369, '2016-07-18 13:40:31', 1, 'Logout', 'Logout User : NGURAH KETUT HARIADI, IP : 192.168.8.101'),
(370, '2016-07-20 01:43:14', 1, 'Login', 'Login User : NGURAH KETUT HARIADI, IP : 192.168.8.101'),
(371, '2016-07-20 01:51:31', 1, 'Login', 'Login User : NGURAH KETUT HARIADI, IP : 192.168.8.101'),
(372, '2016-07-20 01:51:55', 1, 'Logout', 'Logout User : NGURAH KETUT HARIADI, IP : 192.168.8.101'),
(373, '2016-07-20 01:52:55', 1, 'Login', 'Login User : NGURAH KETUT HARIADI, IP : 192.168.8.101'),
(374, '2016-07-20 01:53:11', 1, 'Logout', 'Logout User : NGURAH KETUT HARIADI, IP : 192.168.8.101'),
(375, '2016-07-20 01:57:58', 1, 'Login', 'Login User : NGURAH KETUT HARIADI, IP : 192.168.8.101'),
(376, '2016-07-20 01:59:09', 1, 'Login', 'Login User : NGURAH KETUT HARIADI, IP : 192.168.8.101'),
(377, '2016-07-20 02:04:54', 1, 'Logout', 'Logout User : NGURAH KETUT HARIADI, IP : 192.168.8.101'),
(378, '2016-07-20 02:07:26', 1, 'Login', 'Login User : NGURAH KETUT HARIADI, IP : 192.168.8.101'),
(379, '2016-07-20 02:07:41', 1, 'Logout', 'Logout User : NGURAH KETUT HARIADI, IP : 192.168.8.101'),
(380, '2016-07-20 02:10:44', 1, 'Login', 'Login User : NGURAH KETUT HARIADI, IP : 192.168.8.101'),
(381, '2016-07-20 02:10:59', 1, 'Logout', 'Logout User : NGURAH KETUT HARIADI, IP : 192.168.8.101'),
(382, '2016-07-20 02:12:11', 1, 'Login', 'Login User : NGURAH KETUT HARIADI, IP : 192.168.8.101'),
(383, '2016-07-20 02:12:26', 1, 'Logout', 'Logout User : NGURAH KETUT HARIADI, IP : 192.168.8.101'),
(384, '2016-07-20 02:13:00', 1, 'Login', 'Login User : NGURAH KETUT HARIADI, IP : 192.168.8.101'),
(385, '2016-07-20 02:13:16', 1, 'Logout', 'Logout User : NGURAH KETUT HARIADI, IP : 192.168.8.101'),
(386, '2016-07-20 02:26:16', 1, 'Login', 'Login User : NGURAH KETUT HARIADI, IP : 192.168.8.101'),
(387, '2016-07-20 02:26:35', 1, 'Logout', 'Logout User : NGURAH KETUT HARIADI, IP : 192.168.8.101'),
(388, '2016-07-20 02:27:21', 1, 'Login', 'Login User : NGURAH KETUT HARIADI, IP : 192.168.8.101'),
(389, '2016-07-20 02:28:16', 1, 'Logout', 'Logout User : NGURAH KETUT HARIADI, IP : 192.168.8.101'),
(390, '2016-07-20 02:29:06', 1, 'Login', 'Login User : NGURAH KETUT HARIADI, IP : 192.168.8.101'),
(391, '2016-07-20 02:29:17', 1, 'Logout', 'Logout User : NGURAH KETUT HARIADI, IP : 192.168.8.101'),
(392, '2016-07-20 02:31:26', 1, 'Login', 'Login User : NGURAH KETUT HARIADI, IP : 192.168.8.101'),
(393, '2016-07-20 02:31:37', 1, 'Logout', 'Logout User : NGURAH KETUT HARIADI, IP : 192.168.8.101'),
(394, '2016-07-20 02:34:14', 1, 'Login', 'Login User : NGURAH KETUT HARIADI, IP : 192.168.8.101'),
(395, '2016-07-20 02:35:19', 1, 'Logout', 'Logout User : NGURAH KETUT HARIADI, IP : 192.168.8.101'),
(396, '2016-07-20 02:36:05', 1, 'Login', 'Login User : NGURAH KETUT HARIADI, IP : 192.168.8.101'),
(397, '2016-07-20 02:46:26', 1, 'Logout', 'Logout User : NGURAH KETUT HARIADI, IP : 192.168.8.101'),
(398, '2016-07-20 02:57:48', 1, 'Login', 'Login User : NGURAH KETUT HARIADI, IP : 192.168.8.101'),
(399, '2016-07-20 02:58:16', 1, 'Logout', 'Logout User : NGURAH KETUT HARIADI, IP : 192.168.8.101'),
(400, '2016-07-20 03:00:53', 1, 'Login', 'Login User : NGURAH KETUT HARIADI, IP : 192.168.8.101'),
(401, '2016-07-20 03:01:14', 1, 'Logout', 'Logout User : NGURAH KETUT HARIADI, IP : 192.168.8.101'),
(402, '2016-07-20 03:03:00', 1, 'Login', 'Login User : NGURAH KETUT HARIADI, IP : 192.168.8.101'),
(403, '2016-07-20 03:03:51', 1, 'Logout', 'Logout User : NGURAH KETUT HARIADI, IP : 192.168.8.101'),
(404, '2016-07-20 03:04:44', 1, 'Login', 'Login User : NGURAH KETUT HARIADI, IP : 192.168.8.101'),
(405, '2016-07-20 03:05:12', 1, 'Logout', 'Logout User : NGURAH KETUT HARIADI, IP : 192.168.8.101'),
(406, '2016-07-20 03:07:50', 1, 'Login', 'Login User : NGURAH KETUT HARIADI, IP : 192.168.8.101'),
(407, '2016-07-20 03:08:28', 1, 'Logout', 'Logout User : NGURAH KETUT HARIADI, IP : 192.168.8.101'),
(408, '2016-07-20 03:09:12', 1, 'Login', 'Login User : NGURAH KETUT HARIADI, IP : 192.168.8.101'),
(409, '2016-07-20 03:09:55', 1, 'Logout', 'Logout User : NGURAH KETUT HARIADI, IP : 192.168.8.101'),
(410, '2016-07-20 03:11:00', 1, 'Login', 'Login User : NGURAH KETUT HARIADI, IP : 192.168.8.101'),
(411, '2016-07-20 03:12:07', 1, 'Logout', 'Logout User : NGURAH KETUT HARIADI, IP : 192.168.8.101'),
(412, '2016-07-20 03:16:16', 1, 'Login', 'Login User : NGURAH KETUT HARIADI, IP : 192.168.8.101'),
(413, '2016-07-20 03:17:50', 1, 'Logout', 'Logout User : NGURAH KETUT HARIADI, IP : 192.168.8.101'),
(414, '2016-07-20 03:19:38', 1, 'Login', 'Login User : NGURAH KETUT HARIADI, IP : 192.168.8.101'),
(415, '2016-07-20 03:20:12', 1, 'Logout', 'Logout User : NGURAH KETUT HARIADI, IP : 192.168.8.101'),
(416, '2016-07-20 03:43:16', 1, 'Login', 'Login User : NGURAH KETUT HARIADI, IP : 192.168.8.101'),
(417, '2016-07-20 03:43:51', 1, 'Logout', 'Logout User : NGURAH KETUT HARIADI, IP : 192.168.8.101'),
(418, '2016-07-20 04:12:39', 1, 'Login', 'Login User : NGURAH KETUT HARIADI, IP : 192.168.8.101'),
(419, '2016-07-20 04:13:07', 1, 'Logout', 'Logout User : NGURAH KETUT HARIADI, IP : 192.168.8.101'),
(420, '2016-07-20 05:26:28', 1, 'Login', 'Login User : NGURAH KETUT HARIADI, IP : 192.168.8.101'),
(421, '2016-07-20 05:26:46', 1, 'Logout', 'Logout User : NGURAH KETUT HARIADI, IP : 192.168.8.101'),
(422, '2016-07-20 05:27:27', 1, 'Login', 'Login User : NGURAH KETUT HARIADI, IP : 192.168.8.101'),
(423, '2016-07-20 05:28:23', 1, 'Logout', 'Logout User : NGURAH KETUT HARIADI, IP : 192.168.8.101'),
(424, '2016-07-20 05:29:33', 1, 'Login', 'Login User : NGURAH KETUT HARIADI, IP : 192.168.8.101'),
(425, '2016-07-20 05:29:57', 1, 'Logout', 'Logout User : NGURAH KETUT HARIADI, IP : 192.168.8.101'),
(426, '2016-07-20 05:44:48', 1, 'Login', 'Login User : NGURAH KETUT HARIADI, IP : 192.168.8.101'),
(427, '2016-07-20 05:45:23', 1, 'Logout', 'Logout User : NGURAH KETUT HARIADI, IP : 192.168.8.101'),
(428, '2016-07-20 05:46:00', 1, 'Login', 'Login User : NGURAH KETUT HARIADI, IP : 192.168.8.101'),
(429, '2016-07-20 05:46:03', 1, 'Logout', 'Logout User : NGURAH KETUT HARIADI, IP : 192.168.8.101'),
(430, '2016-07-20 05:58:00', 1, 'Login', 'Login User : NGURAH KETUT HARIADI, IP : 192.168.8.101'),
(431, '2016-07-20 05:58:28', 1, 'Logout', 'Logout User : NGURAH KETUT HARIADI, IP : 192.168.8.101'),
(432, '2016-07-20 05:58:43', 1, 'Login', 'Login User : NGURAH KETUT HARIADI, IP : 192.168.8.101'),
(433, '2016-07-20 05:58:57', 1, 'Logout', 'Logout User : NGURAH KETUT HARIADI, IP : 192.168.8.101'),
(434, '2016-07-20 05:59:19', 1, 'Login', 'Login User : NGURAH KETUT HARIADI, IP : 192.168.8.101'),
(435, '2016-07-20 05:59:44', 1, 'Logout', 'Logout User : NGURAH KETUT HARIADI, IP : 192.168.8.101'),
(436, '2016-07-20 06:00:18', 1, 'Login', 'Login User : NGURAH KETUT HARIADI, IP : 192.168.8.101'),
(437, '2016-07-20 06:01:20', 1, 'Logout', 'Logout User : NGURAH KETUT HARIADI, IP : 192.168.8.101'),
(438, '2016-07-20 08:06:26', 1, 'Login', 'Login User : NGURAH KETUT HARIADI, IP : 192.168.8.101'),
(439, '2016-07-20 08:06:53', 1, 'Logout', 'Logout User : NGURAH KETUT HARIADI, IP : 192.168.8.101'),
(440, '2016-07-20 08:57:41', 1, 'Login', 'Login User : NGURAH KETUT HARIADI, IP : 192.168.8.101'),
(441, '2016-07-20 08:59:42', 1, 'Login', 'Login User : NGURAH KETUT HARIADI, IP : 192.168.8.101'),
(442, '2016-07-20 08:59:59', 1, 'Logout', 'Logout User : NGURAH KETUT HARIADI, IP : 192.168.8.101'),
(443, '2016-07-20 09:00:47', 1, 'Login', 'Login User : NGURAH KETUT HARIADI, IP : 192.168.8.101'),
(444, '2016-07-20 09:01:57', 1, 'Logout', 'Logout User : NGURAH KETUT HARIADI, IP : 192.168.8.101'),
(445, '2016-07-20 09:04:07', 1, 'Login', 'Login User : NGURAH KETUT HARIADI, IP : 192.168.8.101'),
(446, '2016-07-20 09:04:35', 1, 'Logout', 'Logout User : NGURAH KETUT HARIADI, IP : 192.168.8.101'),
(447, '2016-07-20 09:05:19', 1, 'Login', 'Login User : NGURAH KETUT HARIADI, IP : 192.168.8.101'),
(448, '2016-07-20 09:06:01', 1, 'Logout', 'Logout User : NGURAH KETUT HARIADI, IP : 192.168.8.101'),
(449, '2016-07-20 09:10:29', 1, 'Login', 'Login User : NGURAH KETUT HARIADI, IP : 192.168.8.101'),
(450, '2016-07-20 09:11:16', 1, 'Logout', 'Logout User : NGURAH KETUT HARIADI, IP : 192.168.8.101'),
(451, '2016-07-20 09:11:39', 1, 'Login', 'Login User : NGURAH KETUT HARIADI, IP : 192.168.8.101'),
(452, '2016-07-20 09:39:24', 1, 'Logout', 'Logout User : NGURAH KETUT HARIADI, IP : 192.168.8.101'),
(453, '2016-07-20 09:41:25', 1, 'Login', 'Login User : NGURAH KETUT HARIADI, IP : 192.168.8.101'),
(454, '2016-07-20 09:42:34', 1, 'Logout', 'Logout User : NGURAH KETUT HARIADI, IP : 192.168.8.101'),
(455, '2016-07-20 09:44:09', 1, 'Login', 'Login User : NGURAH KETUT HARIADI, IP : 192.168.8.101'),
(456, '2016-07-20 09:44:47', 1, 'Logout', 'Logout User : NGURAH KETUT HARIADI, IP : 192.168.8.101'),
(457, '2016-07-20 12:01:51', 1, 'Login', 'Login User : NGURAH KETUT HARIADI, IP : 192.168.8.101'),
(458, '2016-07-20 12:02:24', 1, 'Logout', 'Logout User : NGURAH KETUT HARIADI, IP : 192.168.8.101'),
(459, '2016-07-20 12:02:56', 1, 'Login', 'Login User : NGURAH KETUT HARIADI, IP : 192.168.8.101'),
(460, '2016-07-20 12:03:31', 1, 'Logout', 'Logout User : NGURAH KETUT HARIADI, IP : 192.168.8.101'),
(461, '2016-07-20 12:04:06', 1, 'Login', 'Login User : NGURAH KETUT HARIADI, IP : 192.168.8.101'),
(462, '2016-07-20 12:04:54', 1, 'Logout', 'Logout User : NGURAH KETUT HARIADI, IP : 192.168.8.101'),
(463, '2016-07-20 12:05:36', 1, 'Login', 'Login User : NGURAH KETUT HARIADI, IP : 192.168.8.101'),
(464, '2016-07-20 12:06:32', 1, 'Logout', 'Logout User : NGURAH KETUT HARIADI, IP : 192.168.8.101'),
(465, '2016-07-21 01:31:24', 1, 'Login', 'Login User : NGURAH KETUT HARIADI, IP : 192.168.8.100'),
(466, '2016-07-21 01:31:45', 1, 'Logout', 'Logout User : NGURAH KETUT HARIADI, IP : 192.168.8.100'),
(467, '2016-07-21 01:32:22', 1, 'Login', 'Login User : NGURAH KETUT HARIADI, IP : 192.168.8.100'),
(468, '2016-07-21 01:35:39', 1, 'Logout', 'Logout User : NGURAH KETUT HARIADI, IP : 192.168.8.100'),
(469, '2016-07-21 01:36:25', 1, 'Login', 'Login User : NGURAH KETUT HARIADI, IP : 192.168.8.100'),
(470, '2016-07-21 03:21:02', 1, 'Logout', 'Logout User : NGURAH KETUT HARIADI, IP : 192.168.8.100'),
(471, '2016-07-21 03:31:52', 1, 'Login', 'Login User : NGURAH KETUT HARIADI, IP : 192.168.8.100'),
(472, '2016-07-21 03:32:29', 1, 'Logout', 'Logout User : NGURAH KETUT HARIADI, IP : 192.168.8.100'),
(473, '2016-07-21 03:33:31', 1, 'Login', 'Login User : NGURAH KETUT HARIADI, IP : 192.168.8.100'),
(474, '2016-07-21 03:34:02', 1, 'Logout', 'Logout User : NGURAH KETUT HARIADI, IP : 192.168.8.100'),
(475, '2016-07-21 03:34:33', 1, 'Login', 'Login User : NGURAH KETUT HARIADI, IP : 192.168.8.100'),
(476, '2016-07-21 03:35:08', 1, 'Logout', 'Logout User : NGURAH KETUT HARIADI, IP : 192.168.8.100'),
(477, '2016-07-21 06:00:17', 1, 'Login', 'Login User : NGURAH KETUT HARIADI, IP : 192.168.8.100'),
(478, '2016-07-21 06:01:18', 1, 'Logout', 'Logout User : NGURAH KETUT HARIADI, IP : 192.168.8.100'),
(479, '2016-07-21 06:13:02', 1, 'Login', 'Login User : NGURAH KETUT HARIADI, IP : 192.168.8.100'),
(480, '2016-07-21 06:13:19', 1, 'Logout', 'Logout User : NGURAH KETUT HARIADI, IP : 192.168.8.100'),
(481, '2016-07-21 06:14:27', 1, 'Login', 'Login User : NGURAH KETUT HARIADI, IP : 192.168.8.100'),
(482, '2016-07-21 06:15:18', 1, 'Logout', 'Logout User : NGURAH KETUT HARIADI, IP : 192.168.8.100'),
(483, '2016-07-21 06:16:51', 1, 'Login', 'Login User : NGURAH KETUT HARIADI, IP : 192.168.8.100'),
(484, '2016-07-21 06:17:54', 1, 'Login', 'Login User : NGURAH KETUT HARIADI, IP : 192.168.8.100'),
(485, '2016-07-21 06:18:43', 1, 'Login', 'Login User : NGURAH KETUT HARIADI, IP : 192.168.8.100'),
(486, '2016-07-21 06:19:15', 1, 'Logout', 'Logout User : NGURAH KETUT HARIADI, IP : 192.168.8.100'),
(487, '2016-07-21 06:20:21', 1, 'Login', 'Login User : NGURAH KETUT HARIADI, IP : 192.168.8.100'),
(488, '2016-07-21 06:20:43', 1, 'Logout', 'Logout User : NGURAH KETUT HARIADI, IP : 192.168.8.100'),
(489, '2016-07-21 06:21:48', 1, 'Login', 'Login User : NGURAH KETUT HARIADI, IP : 192.168.8.100'),
(490, '2016-07-21 06:22:05', 1, 'Logout', 'Logout User : NGURAH KETUT HARIADI, IP : 192.168.8.100'),
(491, '2016-07-21 06:23:48', 1, 'Login', 'Login User : NGURAH KETUT HARIADI, IP : 192.168.8.100'),
(492, '2016-07-21 06:31:58', 1, 'Logout', 'Logout User : NGURAH KETUT HARIADI, IP : 192.168.8.100'),
(493, '2016-07-21 06:32:42', 1, 'Login', 'Login User : NGURAH KETUT HARIADI, IP : 192.168.8.100'),
(494, '2016-07-21 06:33:06', 1, 'Logout', 'Logout User : NGURAH KETUT HARIADI, IP : 192.168.8.100'),
(495, '2016-07-21 06:33:31', 1, 'Login', 'Login User : NGURAH KETUT HARIADI, IP : 192.168.8.100'),
(496, '2016-07-21 06:33:34', 1, 'Logout', 'Logout User : NGURAH KETUT HARIADI, IP : 192.168.8.100'),
(497, '2016-07-21 06:34:24', 1, 'Login', 'Login User : NGURAH KETUT HARIADI, IP : 192.168.8.100'),
(498, '2016-07-21 06:39:05', 1, 'Logout', 'Logout User : NGURAH KETUT HARIADI, IP : 192.168.8.100'),
(499, '2016-07-21 06:40:27', 1, 'Login', 'Login User : NGURAH KETUT HARIADI, IP : 192.168.8.100'),
(500, '2016-07-21 06:41:51', 1, 'Logout', 'Logout User : NGURAH KETUT HARIADI, IP : 192.168.8.100'),
(501, '2016-07-21 06:42:52', 1, 'Login', 'Login User : NGURAH KETUT HARIADI, IP : 192.168.8.100'),
(502, '2016-07-21 06:45:13', 1, 'Login', 'Login User : NGURAH KETUT HARIADI, IP : 192.168.8.100'),
(503, '2016-07-21 06:47:32', 1, 'Logout', 'Logout User : NGURAH KETUT HARIADI, IP : 192.168.8.100'),
(504, '2016-07-21 06:50:21', 1, 'Login', 'Login User : NGURAH KETUT HARIADI, IP : 192.168.8.100'),
(505, '2016-07-21 07:23:53', 1, 'Login', 'Login User : NGURAH KETUT HARIADI, IP : 192.168.8.100'),
(506, '2016-07-21 07:27:41', 1, 'Logout', 'Logout User : NGURAH KETUT HARIADI, IP : 192.168.8.100'),
(507, '2016-07-21 07:32:14', 1, 'Login', 'Login User : NGURAH KETUT HARIADI, IP : 192.168.8.100'),
(508, '2016-07-21 07:33:38', 1, 'Logout', 'Logout User : NGURAH KETUT HARIADI, IP : 192.168.8.100'),
(509, '2016-07-21 07:52:14', 1, 'Login', 'Login User : NGURAH KETUT HARIADI, IP : 192.168.8.100'),
(510, '2016-07-21 07:54:43', 1, 'Logout', 'Logout User : NGURAH KETUT HARIADI, IP : 192.168.8.100'),
(511, '2016-07-21 07:59:03', 1, 'Login', 'Login User : NGURAH KETUT HARIADI, IP : 192.168.8.100'),
(512, '2016-07-21 08:05:00', 1, 'Login', 'Login User : NGURAH KETUT HARIADI, IP : 192.168.8.100'),
(513, '2016-07-21 08:09:06', 1, 'Login', 'Login User : NGURAH KETUT HARIADI, IP : 192.168.8.100'),
(514, '2016-07-21 08:11:42', 1, 'Login', 'Login User : NGURAH KETUT HARIADI, IP : 192.168.8.100'),
(515, '2016-07-21 08:12:02', 1, 'Logout', 'Logout User : NGURAH KETUT HARIADI, IP : 192.168.8.100'),
(516, '2016-07-21 08:15:42', 1, 'Login', 'Login User : NGURAH KETUT HARIADI, IP : 192.168.8.100'),
(517, '2016-07-21 08:18:36', 1, 'Logout', 'Logout User : NGURAH KETUT HARIADI, IP : 192.168.8.100'),
(518, '2016-07-21 08:19:56', 1, 'Login', 'Login User : NGURAH KETUT HARIADI, IP : 192.168.8.100'),
(519, '2016-07-21 08:26:13', 1, 'Login', 'Login User : NGURAH KETUT HARIADI, IP : 192.168.8.100'),
(520, '2016-07-21 09:00:13', 1, 'Login', 'Login User : NGURAH KETUT HARIADI, IP : 192.168.8.100'),
(521, '2016-07-21 09:01:32', 1, 'Login', 'Login User : NGURAH KETUT HARIADI, IP : 192.168.8.100'),
(522, '2016-07-21 09:01:56', 1, 'Logout', 'Logout User : NGURAH KETUT HARIADI, IP : 192.168.8.100'),
(523, '2016-07-21 09:03:14', 1, 'Login', 'Login User : NGURAH KETUT HARIADI, IP : 192.168.8.100');
INSERT INTO `tb_history` (`ID_HISTORY`, `TGL_HISTORY`, `ID_USER`, `TYPE_HISTORY`, `DETAIL_HISTORY`) VALUES
(524, '2016-07-21 09:04:28', 1, 'Logout', 'Logout User : NGURAH KETUT HARIADI, IP : 192.168.8.100'),
(525, '2016-07-21 09:06:09', 1, 'Login', 'Login User : NGURAH KETUT HARIADI, IP : 192.168.8.100'),
(526, '2016-07-21 09:10:31', 1, 'Login', 'Login User : NGURAH KETUT HARIADI, IP : 192.168.8.100'),
(527, '2016-07-21 09:11:37', 1, 'Logout', 'Logout User : NGURAH KETUT HARIADI, IP : 192.168.8.100'),
(528, '2016-07-21 09:18:21', 1, 'Login', 'Login User : NGURAH KETUT HARIADI, IP : 192.168.8.100'),
(529, '2016-07-21 09:18:39', 1, 'Logout', 'Logout User : NGURAH KETUT HARIADI, IP : 192.168.8.100'),
(530, '2016-07-21 11:33:33', 1, 'Login', 'Login User : NGURAH KETUT HARIADI, IP : 127.0.0.1'),
(531, '2016-07-21 11:34:08', 1, 'Logout', 'Logout User : NGURAH KETUT HARIADI, IP : 127.0.0.1'),
(532, '2016-07-23 01:34:33', 1, 'Login', 'Login User : NGURAH KETUT HARIADI, IP : 192.168.8.101'),
(533, '2016-07-23 01:35:23', 1, 'Logout', 'Logout User : NGURAH KETUT HARIADI, IP : 192.168.8.101'),
(534, '2016-07-23 01:35:49', 1, 'Login', 'Login User : NGURAH KETUT HARIADI, IP : 192.168.8.101'),
(535, '2016-07-23 01:35:58', 1, 'Logout', 'Logout User : NGURAH KETUT HARIADI, IP : 192.168.8.101'),
(536, '2016-07-23 01:37:24', 1, 'Login', 'Login User : NGURAH KETUT HARIADI, IP : 192.168.8.101'),
(537, '2016-07-23 01:37:37', 1, 'Logout', 'Logout User : NGURAH KETUT HARIADI, IP : 192.168.8.101'),
(538, '2016-07-23 01:39:14', 1, 'Login', 'Login User : NGURAH KETUT HARIADI, IP : 192.168.8.101'),
(539, '2016-07-23 01:39:38', 1, 'Logout', 'Logout User : NGURAH KETUT HARIADI, IP : 192.168.8.101'),
(540, '2016-07-23 01:41:35', 1, 'Login', 'Login User : NGURAH KETUT HARIADI, IP : 192.168.8.101'),
(541, '2016-07-23 01:41:52', 1, 'Logout', 'Logout User : NGURAH KETUT HARIADI, IP : 192.168.8.101'),
(542, '2016-07-23 01:43:01', 1, 'Login', 'Login User : NGURAH KETUT HARIADI, IP : 192.168.8.101'),
(543, '2016-07-23 01:43:17', 1, 'Logout', 'Logout User : NGURAH KETUT HARIADI, IP : 192.168.8.101'),
(544, '2016-07-23 01:51:47', 1, 'Login', 'Login User : NGURAH KETUT HARIADI, IP : 192.168.8.101'),
(545, '2016-07-23 01:52:21', 1, 'Logout', 'Logout User : NGURAH KETUT HARIADI, IP : 192.168.8.101'),
(546, '2016-07-23 01:52:48', 1, 'Login', 'Login User : NGURAH KETUT HARIADI, IP : 192.168.8.101'),
(547, '2016-07-23 01:53:02', 1, 'Logout', 'Logout User : NGURAH KETUT HARIADI, IP : 192.168.8.101'),
(548, '2016-07-23 01:58:41', 1, 'Login', 'Login User : NGURAH KETUT HARIADI, IP : 192.168.8.101'),
(549, '2016-07-23 01:59:07', 1, 'Logout', 'Logout User : NGURAH KETUT HARIADI, IP : 192.168.8.101'),
(550, '2016-07-23 01:59:29', 1, 'Login', 'Login User : NGURAH KETUT HARIADI, IP : 192.168.8.101'),
(551, '2016-07-23 02:00:41', 1, 'Login', 'Login User : NGURAH KETUT HARIADI, IP : 192.168.8.101'),
(552, '2016-07-23 02:00:53', 1, 'Logout', 'Logout User : NGURAH KETUT HARIADI, IP : 192.168.8.101'),
(553, '2016-07-23 02:01:11', 1, 'Login', 'Login User : NGURAH KETUT HARIADI, IP : 192.168.8.101'),
(554, '2016-07-23 02:01:32', 1, 'Logout', 'Logout User : NGURAH KETUT HARIADI, IP : 192.168.8.101'),
(555, '2016-07-23 02:01:57', 1, 'Login', 'Login User : NGURAH KETUT HARIADI, IP : 192.168.8.101'),
(556, '2016-07-23 02:02:04', 1, 'Logout', 'Logout User : NGURAH KETUT HARIADI, IP : 192.168.8.101'),
(557, '2016-07-23 02:02:31', 1, 'Login', 'Login User : NGURAH KETUT HARIADI, IP : 192.168.8.101'),
(558, '2016-07-23 02:02:39', 1, 'Logout', 'Logout User : NGURAH KETUT HARIADI, IP : 192.168.8.101'),
(559, '2016-07-23 02:02:56', 1, 'Login', 'Login User : NGURAH KETUT HARIADI, IP : 192.168.8.101'),
(560, '2016-07-23 02:03:16', 1, 'Logout', 'Logout User : NGURAH KETUT HARIADI, IP : 192.168.8.101'),
(561, '2016-07-23 02:04:19', 1, 'Login', 'Login User : NGURAH KETUT HARIADI, IP : 192.168.8.101'),
(562, '2016-07-23 02:04:30', 1, 'Logout', 'Logout User : NGURAH KETUT HARIADI, IP : 192.168.8.101'),
(563, '2016-07-23 02:05:51', 1, 'Login', 'Login User : NGURAH KETUT HARIADI, IP : 192.168.8.101'),
(564, '2016-07-23 02:06:07', 1, 'Logout', 'Logout User : NGURAH KETUT HARIADI, IP : 192.168.8.101'),
(565, '2016-07-23 02:08:12', 1, 'Login', 'Login User : NGURAH KETUT HARIADI, IP : 192.168.8.101'),
(566, '2016-07-23 02:08:26', 1, 'Logout', 'Logout User : NGURAH KETUT HARIADI, IP : 192.168.8.101'),
(567, '2016-07-23 02:12:11', 1, 'Login', 'Login User : NGURAH KETUT HARIADI, IP : 192.168.8.101'),
(568, '2016-07-23 02:13:16', 1, 'Logout', 'Logout User : NGURAH KETUT HARIADI, IP : 192.168.8.101'),
(569, '2016-07-23 02:13:51', 1, 'Login', 'Login User : NGURAH KETUT HARIADI, IP : 192.168.8.101'),
(570, '2016-07-23 02:14:46', 1, 'Logout', 'Logout User : NGURAH KETUT HARIADI, IP : 192.168.8.101'),
(571, '2016-07-23 02:21:47', 1, 'Login', 'Login User : NGURAH KETUT HARIADI, IP : 192.168.8.101'),
(572, '2016-07-23 02:22:16', 1, 'Logout', 'Logout User : NGURAH KETUT HARIADI, IP : 192.168.8.101'),
(573, '2016-07-23 02:23:11', 1, 'Login', 'Login User : NGURAH KETUT HARIADI, IP : 192.168.8.101'),
(574, '2016-07-23 02:23:25', 1, 'Logout', 'Logout User : NGURAH KETUT HARIADI, IP : 192.168.8.101'),
(575, '2016-07-23 02:24:24', 1, 'Login', 'Login User : NGURAH KETUT HARIADI, IP : 192.168.8.101'),
(576, '2016-07-23 02:29:06', 1, 'Logout', 'Logout User : NGURAH KETUT HARIADI, IP : 192.168.8.101'),
(577, '2016-07-23 03:00:26', 1, 'Login', 'Login User : NGURAH KETUT HARIADI, IP : 192.168.8.101'),
(578, '2016-07-23 03:00:49', 1, 'Logout', 'Logout User : NGURAH KETUT HARIADI, IP : 192.168.8.101'),
(579, '2016-07-23 03:02:13', 1, 'Login', 'Login User : NGURAH KETUT HARIADI, IP : 192.168.8.101'),
(580, '2016-07-23 03:02:53', 1, 'Logout', 'Logout User : NGURAH KETUT HARIADI, IP : 192.168.8.101'),
(581, '2016-07-23 03:05:39', 1, 'Login', 'Login User : NGURAH KETUT HARIADI, IP : 192.168.8.101'),
(582, '2016-07-23 03:12:13', 1, 'Login', 'Login User : NGURAH KETUT HARIADI, IP : 192.168.8.101'),
(583, '2016-07-23 03:14:23', 1, 'Logout', 'Logout User : NGURAH KETUT HARIADI, IP : 192.168.8.101'),
(584, '2016-07-23 03:16:07', 1, 'Login', 'Login User : NGURAH KETUT HARIADI, IP : 192.168.8.101'),
(585, '2016-07-23 03:17:26', 1, 'Logout', 'Logout User : NGURAH KETUT HARIADI, IP : 192.168.8.101'),
(586, '2016-07-23 03:20:28', 1, 'Login', 'Login User : NGURAH KETUT HARIADI, IP : 192.168.8.101'),
(587, '2016-07-23 03:20:56', 1, 'Logout', 'Logout User : NGURAH KETUT HARIADI, IP : 192.168.8.101'),
(588, '2016-07-23 04:06:16', 1, 'Login', 'Login User : NGURAH KETUT HARIADI, IP : 192.168.8.101'),
(589, '2016-07-23 04:07:05', 1, 'Logout', 'Logout User : NGURAH KETUT HARIADI, IP : 192.168.8.101'),
(590, '2016-07-24 03:01:53', 1, 'Login', 'Login User : NGURAH KETUT HARIADI, IP : 127.0.0.1'),
(591, '2016-07-24 03:02:03', 1, 'Logout', 'Logout User : NGURAH KETUT HARIADI, IP : 127.0.0.1'),
(592, '2016-07-24 03:03:31', 1, 'Login', 'Login User : NGURAH KETUT HARIADI, IP : 127.0.0.1'),
(593, '2016-07-24 03:11:47', 1, 'Logout', 'Logout User : NGURAH KETUT HARIADI, IP : 127.0.0.1'),
(594, '2016-07-24 03:21:33', 1, 'Login', 'Login User : NGURAH KETUT HARIADI, IP : 127.0.0.1'),
(595, '2016-07-24 03:59:31', 1, 'Login', 'Login User : NGURAH KETUT HARIADI, IP : 192.168.8.101'),
(596, '2016-07-24 04:31:01', 1, 'Logout', 'Logout User : NGURAH KETUT HARIADI, IP : 192.168.8.101'),
(597, '2016-07-24 08:18:31', 1, 'Login', 'Login User : NGURAH KETUT HARIADI, IP : 192.168.8.101'),
(598, '2016-07-24 08:18:47', 1, 'Logout', 'Logout User : NGURAH KETUT HARIADI, IP : 192.168.8.101'),
(599, '2016-07-24 08:22:44', 1, 'Login', 'Login User : NGURAH KETUT HARIADI, IP : 192.168.8.101'),
(600, '2016-07-24 08:23:07', 1, 'Logout', 'Logout User : NGURAH KETUT HARIADI, IP : 192.168.8.101'),
(601, '2016-07-24 08:25:42', 1, 'Login', 'Login User : NGURAH KETUT HARIADI, IP : 192.168.8.101'),
(602, '2016-07-24 08:26:16', 1, 'Logout', 'Logout User : NGURAH KETUT HARIADI, IP : 192.168.8.101'),
(603, '2016-07-24 08:28:39', 1, 'Login', 'Login User : NGURAH KETUT HARIADI, IP : 192.168.8.101'),
(604, '2016-07-24 08:29:18', 1, 'Logout', 'Logout User : NGURAH KETUT HARIADI, IP : 192.168.8.101'),
(605, '2016-07-24 08:30:47', 1, 'Login', 'Login User : NGURAH KETUT HARIADI, IP : 192.168.8.101'),
(606, '2016-07-24 08:31:20', 1, 'Logout', 'Logout User : NGURAH KETUT HARIADI, IP : 192.168.8.101'),
(607, '2016-07-24 08:31:56', 1, 'Login', 'Login User : NGURAH KETUT HARIADI, IP : 192.168.8.101'),
(608, '2016-07-24 08:32:15', 1, 'Logout', 'Logout User : NGURAH KETUT HARIADI, IP : 192.168.8.101'),
(609, '2016-07-24 08:32:57', 1, 'Login', 'Login User : NGURAH KETUT HARIADI, IP : 192.168.8.101'),
(610, '2016-07-24 08:33:12', 1, 'Logout', 'Logout User : NGURAH KETUT HARIADI, IP : 192.168.8.101'),
(611, '2016-07-24 08:33:25', 1, 'Login', 'Login User : NGURAH KETUT HARIADI, IP : 192.168.8.101'),
(612, '2016-07-24 08:42:36', 1, 'Logout', 'Logout User : NGURAH KETUT HARIADI, IP : 192.168.8.101'),
(613, '2016-07-24 08:47:58', 1, 'Login', 'Login User : NGURAH KETUT HARIADI, IP : 192.168.8.101'),
(614, '2016-07-24 08:49:44', 1, 'Logout', 'Logout User : NGURAH KETUT HARIADI, IP : 192.168.8.101'),
(615, '2016-07-24 08:50:45', 1, 'Login', 'Login User : NGURAH KETUT HARIADI, IP : 192.168.8.101'),
(616, '2016-07-24 08:55:27', 1, 'Logout', 'Logout User : NGURAH KETUT HARIADI, IP : 192.168.8.101'),
(617, '2016-07-24 09:38:07', 1, 'Login', 'Login User : NGURAH KETUT HARIADI, IP : 192.168.8.101'),
(618, '2016-07-24 09:38:36', 1, 'Logout', 'Logout User : NGURAH KETUT HARIADI, IP : 192.168.8.101'),
(619, '2016-07-24 10:08:18', 1, 'Login', 'Login User : NGURAH KETUT HARIADI, IP : 192.168.8.101'),
(620, '2016-07-24 10:21:43', 1, 'Login', 'Login User : NGURAH KETUT HARIADI, IP : 192.168.8.101'),
(621, '2016-07-24 10:22:08', 1, 'Logout', 'Logout User : NGURAH KETUT HARIADI, IP : 192.168.8.101'),
(622, '2016-07-24 10:22:24', 1, 'Login', 'Login User : NGURAH KETUT HARIADI, IP : 192.168.8.101'),
(623, '2016-07-24 10:23:53', 1, 'Logout', 'Logout User : NGURAH KETUT HARIADI, IP : 192.168.8.101'),
(624, '2016-07-25 01:44:20', 1, 'Login', 'Login User : NGURAH KETUT HARIADI, IP : 192.168.8.101'),
(625, '2016-07-25 01:59:02', 1, 'Logout', 'Logout User : NGURAH KETUT HARIADI, IP : 192.168.8.101'),
(626, '2016-07-25 02:40:13', 1, 'Login', 'Login User : NGURAH KETUT HARIADI, IP : 192.168.8.101'),
(627, '2016-07-25 02:40:48', 1, 'Logout', 'Logout User : NGURAH KETUT HARIADI, IP : 192.168.8.101'),
(628, '2016-07-25 02:42:01', 1, 'Login', 'Login User : NGURAH KETUT HARIADI, IP : 192.168.8.101'),
(629, '2016-07-25 02:42:34', 1, 'Logout', 'Logout User : NGURAH KETUT HARIADI, IP : 192.168.8.101'),
(630, '2016-07-25 02:50:43', 1, 'Login', 'Login User : NGURAH KETUT HARIADI, IP : 192.168.8.101'),
(631, '2016-07-25 02:50:59', 1, 'Logout', 'Logout User : NGURAH KETUT HARIADI, IP : 192.168.8.101'),
(632, '2016-07-25 02:51:57', 1, 'Login', 'Login User : NGURAH KETUT HARIADI, IP : 192.168.8.101'),
(633, '2016-07-25 02:52:14', 1, 'Logout', 'Logout User : NGURAH KETUT HARIADI, IP : 192.168.8.101'),
(634, '2016-07-25 02:54:26', 1, 'Login', 'Login User : NGURAH KETUT HARIADI, IP : 192.168.8.101'),
(635, '2016-07-25 02:54:47', 1, 'Logout', 'Logout User : NGURAH KETUT HARIADI, IP : 192.168.8.101'),
(636, '2016-07-25 02:55:17', 1, 'Login', 'Login User : NGURAH KETUT HARIADI, IP : 192.168.8.101'),
(637, '2016-07-25 02:55:52', 1, 'Logout', 'Logout User : NGURAH KETUT HARIADI, IP : 192.168.8.101'),
(638, '2016-07-25 02:57:46', 1, 'Login', 'Login User : NGURAH KETUT HARIADI, IP : 192.168.8.101'),
(639, '2016-07-25 03:07:36', 1, 'Logout', 'Logout User : NGURAH KETUT HARIADI, IP : 192.168.8.101'),
(640, '2016-07-25 03:10:09', 1, 'Login', 'Login User : NGURAH KETUT HARIADI, IP : 192.168.8.101'),
(641, '2016-07-25 03:10:36', 1, 'Logout', 'Logout User : NGURAH KETUT HARIADI, IP : 192.168.8.101'),
(642, '2016-07-25 03:27:28', 1, 'Login', 'Login User : NGURAH KETUT HARIADI, IP : 192.168.8.101'),
(643, '2016-07-25 03:33:18', 1, 'Login', 'Login User : NGURAH KETUT HARIADI, IP : 192.168.8.101'),
(644, '2016-07-25 03:34:11', 1, 'Logout', 'Logout User : NGURAH KETUT HARIADI, IP : 192.168.8.101'),
(645, '2016-07-25 03:36:08', 1, 'Login', 'Login User : NGURAH KETUT HARIADI, IP : 192.168.8.101'),
(646, '2016-07-25 03:37:02', 1, 'Logout', 'Logout User : NGURAH KETUT HARIADI, IP : 192.168.8.101'),
(647, '2016-07-25 03:39:24', 1, 'Login', 'Login User : NGURAH KETUT HARIADI, IP : 192.168.8.101'),
(648, '2016-07-25 03:42:06', 1, 'Login', 'Login User : NGURAH KETUT HARIADI, IP : 192.168.8.101'),
(649, '2016-07-25 03:42:44', 1, 'Logout', 'Logout User : NGURAH KETUT HARIADI, IP : 192.168.8.101'),
(650, '2016-07-25 03:45:04', 1, 'Login', 'Login User : NGURAH KETUT HARIADI, IP : 192.168.8.101'),
(651, '2016-07-25 03:46:18', 1, 'Logout', 'Logout User : NGURAH KETUT HARIADI, IP : 192.168.8.101'),
(652, '2016-07-25 03:47:14', 1, 'Login', 'Login User : NGURAH KETUT HARIADI, IP : 192.168.8.101'),
(653, '2016-07-25 03:48:01', 1, 'Logout', 'Logout User : NGURAH KETUT HARIADI, IP : 192.168.8.101'),
(654, '2016-07-25 03:48:08', 1, 'Login', 'Login User : NGURAH KETUT HARIADI, IP : 192.168.8.101'),
(655, '2016-07-25 03:48:57', 1, 'Logout', 'Logout User : NGURAH KETUT HARIADI, IP : 192.168.8.101'),
(656, '2016-07-25 04:04:05', 1, 'Login', 'Login User : NGURAH KETUT HARIADI, IP : 192.168.8.101'),
(657, '2016-07-25 04:05:04', 1, 'Logout', 'Logout User : NGURAH KETUT HARIADI, IP : 192.168.8.101'),
(658, '2016-07-25 04:07:10', 1, 'Login', 'Login User : NGURAH KETUT HARIADI, IP : 192.168.8.101'),
(659, '2016-07-25 04:08:22', 1, 'Logout', 'Logout User : NGURAH KETUT HARIADI, IP : 192.168.8.101'),
(660, '2016-07-25 04:10:17', 1, 'Login', 'Login User : NGURAH KETUT HARIADI, IP : 192.168.8.101'),
(661, '2016-07-25 04:11:24', 1, 'Logout', 'Logout User : NGURAH KETUT HARIADI, IP : 192.168.8.101'),
(662, '2016-07-25 04:14:30', 1, 'Login', 'Login User : NGURAH KETUT HARIADI, IP : 192.168.8.101'),
(663, '2016-07-25 04:16:27', 1, 'Logout', 'Logout User : NGURAH KETUT HARIADI, IP : 192.168.8.101'),
(664, '2016-07-25 06:25:46', 1, 'Login', 'Login User : NGURAH KETUT HARIADI, IP : 192.168.8.101'),
(665, '2016-07-25 06:25:54', 1, 'Logout', 'Logout User : NGURAH KETUT HARIADI, IP : 192.168.8.101'),
(666, '2016-07-25 06:27:32', 1, 'Login', 'Login User : NGURAH KETUT HARIADI, IP : 192.168.8.101'),
(667, '2016-07-25 06:28:04', 1, 'Logout', 'Logout User : NGURAH KETUT HARIADI, IP : 192.168.8.101'),
(668, '2016-07-25 06:36:07', 1, 'Login', 'Login User : NGURAH KETUT HARIADI, IP : 192.168.8.101'),
(669, '2016-07-25 06:38:08', 1, 'Logout', 'Logout User : NGURAH KETUT HARIADI, IP : 192.168.8.101'),
(670, '2016-07-25 07:17:25', 3, 'Login', 'Login User : JONI, IP : 192.168.8.101'),
(671, '2016-07-25 07:17:58', 3, 'Logout', 'Logout User : JONI, IP : 192.168.8.101'),
(672, '2016-07-25 09:13:13', 1, 'Login', 'Login User : NGURAH KETUT HARIADI, IP : 192.168.8.101'),
(673, '2016-07-25 09:14:06', 1, 'Logout', 'Logout User : NGURAH KETUT HARIADI, IP : 192.168.8.101'),
(674, '2016-07-25 09:16:01', 1, 'Login', 'Login User : NGURAH KETUT HARIADI, IP : 192.168.8.101'),
(675, '2016-07-25 09:19:02', 1, 'Logout', 'Logout User : NGURAH KETUT HARIADI, IP : 192.168.8.101'),
(676, '2016-07-25 09:20:02', 1, 'Login', 'Login User : NGURAH KETUT HARIADI, IP : 192.168.8.101'),
(677, '2016-07-26 02:00:07', 1, 'Login', 'Login User : NGURAH KETUT HARIADI, IP : 192.168.8.100'),
(678, '2016-07-26 02:03:24', 1, 'Login', 'Login User : NGURAH KETUT HARIADI, IP : 192.168.8.100'),
(679, '2016-07-26 02:32:04', 1, 'Login', 'Login User : NGURAH KETUT HARIADI, IP : 192.168.8.100'),
(680, '2016-07-26 02:41:43', 1, 'Login', 'Login User : NGURAH KETUT HARIADI, IP : 192.168.8.100'),
(681, '2016-07-26 02:43:05', 1, 'Logout', 'Logout User : NGURAH KETUT HARIADI, IP : 192.168.8.100'),
(682, '2016-07-26 02:43:41', 1, 'Login', 'Login User : NGURAH KETUT HARIADI, IP : 192.168.8.100'),
(683, '2016-07-26 02:56:03', 1, 'Login', 'Login User : NGURAH KETUT HARIADI, IP : 192.168.8.100'),
(684, '2016-07-26 02:57:45', 1, 'Logout', 'Logout User : NGURAH KETUT HARIADI, IP : 192.168.8.100'),
(685, '2016-07-26 03:02:14', 1, 'Login', 'Login User : NGURAH KETUT HARIADI, IP : 192.168.8.100'),
(686, '2016-07-26 03:04:15', 1, 'Login', 'Login User : NGURAH KETUT HARIADI, IP : 192.168.8.100'),
(687, '2016-07-27 01:42:34', 1, 'Login', 'Login User : NGURAH KETUT HARIADI, IP : 192.168.8.103'),
(688, '2016-07-27 01:43:47', 1, 'Logout', 'Logout User : NGURAH KETUT HARIADI, IP : 192.168.8.103'),
(689, '2016-07-27 04:33:23', 1, 'Login', 'Login User : NGURAH KETUT HARIADI, IP : 192.168.8.103'),
(690, '2016-07-27 04:33:38', 1, 'Logout', 'Logout User : NGURAH KETUT HARIADI, IP : 192.168.8.103'),
(691, '2016-07-27 04:34:36', 1, 'Login', 'Login User : NGURAH KETUT HARIADI, IP : 192.168.8.103'),
(692, '2016-07-27 04:35:38', 1, 'Logout', 'Logout User : NGURAH KETUT HARIADI, IP : 192.168.8.103'),
(693, '2016-07-27 04:54:29', 1, 'Login', 'Login User : NGURAH KETUT HARIADI, IP : 192.168.8.103'),
(694, '2016-07-27 04:57:17', 1, 'Logout', 'Logout User : NGURAH KETUT HARIADI, IP : 192.168.8.103'),
(695, '2016-08-10 02:41:04', 1, 'Login', 'Login User : NGURAH KETUT HARIADI, IP : 192.168.8.100'),
(696, '2016-08-10 02:48:19', 1, 'Logout', 'Logout User : NGURAH KETUT HARIADI, IP : 192.168.8.100'),
(697, '2016-08-12 01:48:46', 1, 'Login', 'Login User : NGURAH KETUT HARIADI, IP : 192.168.8.100'),
(698, '2016-08-12 02:07:25', 1, 'Logout', 'Logout User : NGURAH KETUT HARIADI, IP : 192.168.8.100'),
(699, '2016-08-13 01:27:07', 1, 'Login', 'Login User : NGURAH KETUT HARIADI, IP : 127.0.0.1'),
(700, '2016-08-13 02:25:29', 1, 'Logout', 'Logout User : NGURAH KETUT HARIADI, IP : 127.0.0.1'),
(701, '2016-08-15 08:56:25', 1, 'Login', 'Login User : NGURAH KETUT HARIADI, IP : 192.168.8.100'),
(702, '2016-08-15 09:24:56', 1, 'Logout', 'Logout User : NGURAH KETUT HARIADI, IP : 192.168.8.100'),
(703, '2016-09-15 05:33:35', 1, 'Login', 'Login User : NGURAH KETUT HARIADI, IP : 192.168.8.102'),
(704, '2016-09-15 05:36:12', 1, 'Logout', 'Logout User : NGURAH KETUT HARIADI, IP : 192.168.8.102'),
(705, '2016-10-05 01:27:15', 1, 'Login', 'Login User : NGURAH KETUT HARIADI, IP : 10.1.1.103'),
(706, '2016-10-05 01:29:11', 1, 'Logout', 'Logout User : NGURAH KETUT HARIADI, IP : 10.1.1.103'),
(707, '2016-10-18 07:29:05', 1, 'Login', 'Login User : NGURAH KETUT HARIADI, IP : 192.168.8.100'),
(708, '2016-10-18 07:34:18', 1, 'Logout', 'Logout User : NGURAH KETUT HARIADI, IP : 192.168.8.100'),
(709, '2016-10-31 14:20:13', 1, 'Login', 'Login User : NGURAH KETUT HARIADI, IP : 192.168.8.100'),
(710, '2016-10-31 14:43:28', 1, 'Logout', 'Logout User : NGURAH KETUT HARIADI, IP : 192.168.8.100'),
(711, '2016-11-01 07:41:24', 1, 'Login', 'Login User : NGURAH KETUT HARIADI, IP : 192.168.8.100'),
(712, '2016-11-01 13:02:20', 1, 'Login', 'Login User : NGURAH KETUT HARIADI, IP : 192.168.8.101'),
(713, '2016-11-01 13:48:57', 1, 'Logout', 'Logout User : NGURAH KETUT HARIADI, IP : 192.168.8.101'),
(714, '2016-11-02 03:41:36', 1, 'Login', 'Login User : NGURAH KETUT HARIADI, IP : 192.168.8.100'),
(715, '2016-11-02 04:02:43', 1, 'Login', 'Login User : NGURAH KETUT HARIADI, IP : 192.168.8.100'),
(716, '2016-11-02 04:03:04', 1, 'Logout', 'Logout User : NGURAH KETUT HARIADI, IP : 192.168.8.100'),
(717, '2016-12-17 08:06:08', 1, 'Login', 'Login User : NGURAH KETUT HARIADI, IP : 192.168.8.100'),
(718, '2016-12-17 10:02:22', 1, 'Logout', 'Logout User : NGURAH KETUT HARIADI, IP : 192.168.8.100'),
(719, '2016-12-19 04:37:12', 1, 'Login', 'Login User : NGURAH KETUT HARIADI, IP : 192.168.8.100'),
(720, '2016-12-19 04:44:40', 1, 'Logout', 'Logout User : NGURAH KETUT HARIADI, IP : 192.168.8.100'),
(721, '2017-04-11 07:59:31', 1, 'Login', 'Login User : NGURAH KETUT HARIADI, IP : 192.168.8.100'),
(722, '2017-04-11 08:34:57', 1, 'Login', 'Login User : NGURAH KETUT HARIADI, IP : 192.168.8.101'),
(723, '2017-12-04 20:52:20', 1, 'Login', 'Login User : NGURAH KETUT HARIADI, IP Address = 127.0.0.1'),
(724, '2017-12-05 19:13:22', 1, 'Login', 'Login User : NGURAH KETUT HARIADI, IP Address = 127.0.0.1'),
(725, '2017-12-07 04:28:53', 1, 'Login', 'Login User : NGURAH KETUT HARIADI, IP Address = 127.0.0.1'),
(726, '2017-12-07 04:32:42', 1, 'Login', 'Login User : NGURAH KETUT HARIADI, IP Address = 127.0.0.1'),
(727, '2017-12-06 17:00:50', 1, 'Login', 'Login User : NGURAH KETUT HARIADI, IP Address = 127.0.0.1'),
(728, '2017-12-06 19:22:14', 1, 'Login', 'Login User : NGURAH KETUT HARIADI, IP Address = 127.0.0.1'),
(729, '2017-12-08 03:12:17', 1, 'Login', 'Login User : NGURAH KETUT HARIADI, IP Address = 127.0.0.1'),
(730, '2017-12-08 03:21:03', 1, 'Login', 'Login User : NGURAH KETUT HARIADI, IP Address = 127.0.0.1'),
(731, '2017-12-08 05:11:42', 1, 'Login', 'Login User : NGURAH KETUT HARIADI, IP Address : 127.0.0.1 '),
(732, '2017-12-11 03:10:01', 1, 'Login', 'Login User : NGURAH KETUT HARIADI, IP Address = 127.0.0.1');

-- --------------------------------------------------------

--
-- Table structure for table `tb_inventaris`
--

CREATE TABLE `tb_inventaris` (
  `ID_INVENTARIS` int(11) NOT NULL,
  `KODE_INVENTARIS` varchar(255) NOT NULL,
  `NAMA_INVENTARIS` varchar(255) NOT NULL,
  `JENIS_INVENTARIS` varchar(255) NOT NULL,
  `TANGGAL_PEMBELIAN` date NOT NULL,
  `NILAI_PEROLEHAN` int(11) NOT NULL,
  `UMUR_MANFAAT` int(11) NOT NULL,
  `LOKASI_INVENTORY` varchar(255) NOT NULL,
  `IS_AMORTISASI` int(11) NOT NULL,
  `IS_ACTIVE` int(1) NOT NULL,
  `LAST_UPDATES` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `ID_USER` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tb_item`
--

CREATE TABLE `tb_item` (
  `ID_ITEM` int(11) NOT NULL,
  `KODE_ITEM` varchar(10) NOT NULL,
  `NAMA_ITEM` varchar(200) NOT NULL,
  `PRICE_ITEM` int(11) NOT NULL,
  `DESKRIPSI_ITEM` varchar(255) NOT NULL,
  `ID_USER` int(11) NOT NULL,
  `IS_ACTIVE` int(1) NOT NULL DEFAULT '1',
  `LAST_UPDATE` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_item`
--

INSERT INTO `tb_item` (`ID_ITEM`, `KODE_ITEM`, `NAMA_ITEM`, `PRICE_ITEM`, `DESKRIPSI_ITEM`, `ID_USER`, `IS_ACTIVE`, `LAST_UPDATE`) VALUES
(1, 'KUR001', 'KURSI BANGKET', 10, 'Kursi bangket standart \r\nTanpa cover dan pita \r\nHarga sewa per hari/event', 1, 1, '2017-01-30 04:21:36'),
(2, 'KUR002', 'KURSI LIPAT', 7000, 'Kursi yang bisa dilipat', 1, 1, '2017-12-07 07:32:25'),
(3, 'MJA001', 'MEJA BUNDAR', 30000, 'Meja yang bentuknya bundar', 1, 1, '2016-05-19 13:30:06'),
(4, 'KAR001', 'KARPETx', 120000, 'Karpet biasa', 1, 1, '2017-12-08 08:32:36'),
(5, 'test', 'test', 123444, 'test', 1, 0, '2017-01-30 04:18:22');

-- --------------------------------------------------------

--
-- Table structure for table `tb_item_add`
--

CREATE TABLE `tb_item_add` (
  `ID_ITEM_ADD` int(11) NOT NULL,
  `ID_BOOKING` int(11) NOT NULL,
  `ID_ITEM` int(11) NOT NULL,
  `JML_ITEM` int(11) NOT NULL DEFAULT '0',
  `PRICE_ITEM` int(11) NOT NULL DEFAULT '0',
  `IS_INCLUDED` enum('YES','NO') NOT NULL DEFAULT 'YES',
  `ID_USER` int(11) NOT NULL,
  `LAST_UPDATE` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_item_add`
--

INSERT INTO `tb_item_add` (`ID_ITEM_ADD`, `ID_BOOKING`, `ID_ITEM`, `JML_ITEM`, `PRICE_ITEM`, `IS_INCLUDED`, `ID_USER`, `LAST_UPDATE`) VALUES
(1, 8, 1, 50, 0, 'YES', 1, '2016-05-26 09:24:30'),
(2, 8, 2, 50, 0, 'YES', 1, '2016-05-26 09:24:30'),
(3, 8, 3, 50, 0, 'YES', 1, '2016-05-26 09:24:30'),
(4, 9, 1, 100, 0, 'YES', 1, '2016-05-26 09:30:18'),
(5, 10, 2, 1, 0, 'YES', 1, '2016-05-26 09:36:32'),
(6, 11, 1, 20, 0, 'YES', 1, '2016-05-26 09:40:42'),
(7, 11, 1, 1, 0, 'YES', 1, '2016-05-26 09:40:42'),
(8, 12, 1, 100, 0, 'YES', 1, '2016-05-26 09:43:12'),
(9, 13, 1, 100, 0, 'YES', 1, '2016-05-26 09:46:39'),
(10, 13, 2, 200, 0, 'YES', 1, '2016-05-26 09:46:39'),
(11, 13, 3, 30, 0, 'YES', 1, '2016-05-26 09:46:39'),
(12, 13, 1, 100, 10000, 'NO', 1, '2016-05-26 09:46:39'),
(13, 13, 2, 300, 7000, 'NO', 1, '2016-05-26 09:46:39'),
(14, 14, 1, 100, 0, 'YES', 1, '2016-05-27 02:52:32'),
(15, 14, 4, 5, 0, 'YES', 1, '2016-05-27 02:52:32'),
(16, 14, 1, 100, 10000, 'NO', 1, '2016-05-27 02:52:32'),
(17, 14, 4, 5, 120000, 'NO', 1, '2016-05-27 02:52:32'),
(18, 15, 1, 1, 0, 'YES', 1, '2016-07-16 04:02:20'),
(19, 15, 1, 0, 0, 'YES', 1, '2016-07-16 04:02:20'),
(20, 15, 1, 1, 10000, 'NO', 1, '2016-07-16 04:02:20'),
(21, 15, 1, 100, 10000, 'NO', 1, '2016-07-16 04:02:20'),
(22, 15, 3, 10, 30000, 'NO', 1, '2016-07-16 04:02:20'),
(23, 16, 1, 200, 0, 'YES', 1, '2016-07-18 05:47:12'),
(24, 16, 3, 20, 0, 'YES', 1, '2016-07-18 05:47:12'),
(25, 16, 1, 300, 10000, 'NO', 1, '2016-07-18 05:47:13'),
(26, 16, 3, 10, 30000, 'NO', 1, '2016-07-18 05:47:13'),
(27, 17, 1, 200, 0, 'YES', 1, '2016-07-21 06:38:27'),
(28, 17, 3, 10, 0, 'YES', 1, '2016-07-21 06:38:27'),
(29, 17, 1, 300, 10000, 'NO', 1, '2016-07-21 06:38:27'),
(30, 17, 3, 30, 30000, 'NO', 1, '2016-07-21 06:38:27'),
(31, 18, 1, 100, 0, 'YES', 1, '2016-07-21 06:41:38'),
(32, 18, 3, 10, 0, 'YES', 1, '2016-07-21 06:41:38'),
(33, 18, 1, 100, 10000, 'NO', 1, '2016-07-21 06:41:38'),
(34, 18, 3, 10, 30000, 'NO', 1, '2016-07-21 06:41:38'),
(35, 19, 1, 100, 0, 'YES', 1, '2016-07-21 06:44:00'),
(36, 19, 3, 10, 0, 'YES', 1, '2016-07-21 06:44:00'),
(37, 19, 1, 100, 10000, 'NO', 1, '2016-07-21 06:44:00'),
(38, 19, 3, 10, 30000, 'NO', 1, '2016-07-21 06:44:00'),
(39, 19, 4, 2, 120000, 'NO', 1, '2016-07-21 06:44:00'),
(40, 20, 1, 100, 0, 'YES', 1, '2016-07-21 06:46:33'),
(41, 20, 3, 10, 0, 'YES', 1, '2016-07-21 06:46:33'),
(42, 20, 1, 100, 10000, 'NO', 1, '2016-07-21 06:46:34'),
(43, 20, 3, 20, 30000, 'NO', 1, '2016-07-21 06:46:34'),
(44, 20, 4, 10, 120000, 'NO', 1, '2016-07-21 06:46:34'),
(45, 21, 1, 300, 0, 'YES', 1, '2016-07-21 06:53:03'),
(46, 21, 3, 30, 0, 'YES', 1, '2016-07-21 06:53:04'),
(47, 21, 4, 10, 0, 'YES', 1, '2016-07-21 06:53:04'),
(48, 21, 1, 300, 10000, 'NO', 1, '2016-07-21 06:53:04'),
(49, 21, 3, 10, 30000, 'NO', 1, '2016-07-21 06:53:04'),
(50, 21, 4, 10, 120000, 'NO', 1, '2016-07-21 06:53:04'),
(51, 22, 1, 1, 0, 'YES', 1, '2016-07-23 03:07:01'),
(52, 22, 1, 1, 10000, 'NO', 1, '2016-07-23 03:07:01'),
(53, 22, 2, 20, 7000, 'NO', 1, '2016-07-23 03:07:01'),
(54, 22, 4, 20, 120000, 'NO', 1, '2016-07-23 03:07:01'),
(55, 23, 4, 1, 0, 'YES', 1, '2016-07-23 03:13:07'),
(56, 23, 1, 1, 10000, 'NO', 1, '2016-07-23 03:13:07'),
(57, 20, 1, 100, 10000, 'NO', 1, '2016-07-24 03:01:57'),
(58, 20, 3, 20, 30000, 'NO', 1, '2016-07-24 03:01:57'),
(59, 20, 4, 10, 120000, 'NO', 1, '2016-07-24 03:01:57'),
(60, 20, 1, 100, 10000, 'NO', 1, '2016-07-24 03:03:36'),
(61, 20, 1, 100, 10000, 'NO', 1, '2016-07-24 03:03:36'),
(62, 20, 3, 20, 30000, 'NO', 1, '2016-07-24 03:03:36'),
(63, 20, 3, 20, 30000, 'NO', 1, '2016-07-24 03:03:37'),
(64, 20, 4, 10, 120000, 'NO', 1, '2016-07-24 03:03:37'),
(65, 20, 4, 10, 120000, 'NO', 1, '2016-07-24 03:03:37'),
(66, 20, 1, 100, 10000, 'NO', 1, '2016-07-24 03:21:57'),
(67, 20, 1, 100, 10000, 'NO', 1, '2016-07-24 03:21:57'),
(68, 20, 1, 100, 10000, 'NO', 1, '2016-07-24 03:21:57'),
(69, 20, 1, 100, 10000, 'NO', 1, '2016-07-24 03:21:57'),
(70, 20, 3, 20, 30000, 'NO', 1, '2016-07-24 03:21:57'),
(71, 20, 3, 20, 30000, 'NO', 1, '2016-07-24 03:21:57'),
(72, 20, 3, 20, 30000, 'NO', 1, '2016-07-24 03:21:57'),
(73, 20, 3, 20, 30000, 'NO', 1, '2016-07-24 03:21:57'),
(74, 20, 4, 10, 120000, 'NO', 1, '2016-07-24 03:21:57'),
(75, 20, 4, 10, 120000, 'NO', 1, '2016-07-24 03:21:57'),
(76, 20, 4, 10, 120000, 'NO', 1, '2016-07-24 03:21:57'),
(77, 20, 4, 10, 120000, 'NO', 1, '2016-07-24 03:21:57'),
(78, 24, 1, 1, 0, 'YES', 1, '2016-07-26 02:01:31'),
(79, 24, 1, 1, 10000, 'NO', 1, '2016-07-26 02:01:32'),
(80, 25, 1, 1, 0, 'YES', 1, '2016-07-26 02:04:52'),
(81, 25, 1, 1, 10000, 'NO', 1, '2016-07-26 02:04:52'),
(82, 26, 1, 1, 0, 'YES', 1, '2016-07-26 02:34:12'),
(83, 26, 1, 100, 10000, 'NO', 1, '2016-07-26 02:34:12'),
(84, 20, 1, 100, 10000, 'NO', 1, '2016-07-26 02:41:49'),
(85, 20, 1, 100, 10000, 'NO', 1, '2016-07-26 02:41:49'),
(86, 20, 1, 100, 10000, 'NO', 1, '2016-07-26 02:41:49'),
(87, 20, 1, 100, 10000, 'NO', 1, '2016-07-26 02:41:49'),
(88, 20, 1, 100, 10000, 'NO', 1, '2016-07-26 02:41:49'),
(89, 20, 1, 100, 10000, 'NO', 1, '2016-07-26 02:41:49'),
(90, 20, 1, 100, 10000, 'NO', 1, '2016-07-26 02:41:49'),
(91, 20, 1, 100, 10000, 'NO', 1, '2016-07-26 02:41:49'),
(92, 20, 3, 20, 30000, 'NO', 1, '2016-07-26 02:41:49'),
(93, 20, 3, 20, 30000, 'NO', 1, '2016-07-26 02:41:49'),
(94, 20, 3, 20, 30000, 'NO', 1, '2016-07-26 02:41:49'),
(95, 20, 3, 20, 30000, 'NO', 1, '2016-07-26 02:41:49'),
(96, 20, 3, 20, 30000, 'NO', 1, '2016-07-26 02:41:49'),
(97, 20, 3, 20, 30000, 'NO', 1, '2016-07-26 02:41:49'),
(98, 20, 3, 20, 30000, 'NO', 1, '2016-07-26 02:41:49'),
(99, 20, 3, 20, 30000, 'NO', 1, '2016-07-26 02:41:49'),
(100, 20, 4, 10, 120000, 'NO', 1, '2016-07-26 02:41:49'),
(101, 20, 4, 10, 120000, 'NO', 1, '2016-07-26 02:41:50'),
(102, 20, 4, 10, 120000, 'NO', 1, '2016-07-26 02:41:50'),
(103, 20, 4, 10, 120000, 'NO', 1, '2016-07-26 02:41:50'),
(104, 20, 4, 10, 120000, 'NO', 1, '2016-07-26 02:41:50'),
(105, 20, 4, 10, 120000, 'NO', 1, '2016-07-26 02:41:50'),
(106, 20, 4, 10, 120000, 'NO', 1, '2016-07-26 02:41:50'),
(107, 20, 4, 10, 120000, 'NO', 1, '2016-07-26 02:41:50'),
(108, 20, 1, 100, 10000, 'NO', 1, '2016-07-26 02:42:21'),
(109, 20, 1, 100, 10000, 'NO', 1, '2016-07-26 02:42:21'),
(110, 20, 1, 100, 10000, 'NO', 1, '2016-07-26 02:42:21'),
(111, 20, 1, 100, 10000, 'NO', 1, '2016-07-26 02:42:21'),
(112, 20, 1, 100, 10000, 'NO', 1, '2016-07-26 02:42:21'),
(113, 20, 1, 100, 10000, 'NO', 1, '2016-07-26 02:42:21'),
(114, 20, 1, 100, 10000, 'NO', 1, '2016-07-26 02:42:21'),
(115, 20, 1, 100, 10000, 'NO', 1, '2016-07-26 02:42:21'),
(116, 20, 1, 100, 10000, 'NO', 1, '2016-07-26 02:42:21'),
(117, 20, 1, 100, 10000, 'NO', 1, '2016-07-26 02:42:21'),
(118, 20, 1, 100, 10000, 'NO', 1, '2016-07-26 02:42:21'),
(119, 20, 1, 100, 10000, 'NO', 1, '2016-07-26 02:42:21'),
(120, 20, 1, 100, 10000, 'NO', 1, '2016-07-26 02:42:21'),
(121, 20, 1, 100, 10000, 'NO', 1, '2016-07-26 02:42:21'),
(122, 20, 1, 100, 10000, 'NO', 1, '2016-07-26 02:42:21'),
(123, 20, 1, 100, 10000, 'NO', 1, '2016-07-26 02:42:21'),
(124, 20, 3, 20, 30000, 'NO', 1, '2016-07-26 02:42:21'),
(125, 20, 3, 20, 30000, 'NO', 1, '2016-07-26 02:42:21'),
(126, 20, 3, 20, 30000, 'NO', 1, '2016-07-26 02:42:21'),
(127, 20, 3, 20, 30000, 'NO', 1, '2016-07-26 02:42:21'),
(128, 20, 3, 20, 30000, 'NO', 1, '2016-07-26 02:42:21'),
(129, 20, 3, 20, 30000, 'NO', 1, '2016-07-26 02:42:22'),
(130, 20, 3, 20, 30000, 'NO', 1, '2016-07-26 02:42:22'),
(131, 20, 3, 20, 30000, 'NO', 1, '2016-07-26 02:42:22'),
(132, 20, 3, 20, 30000, 'NO', 1, '2016-07-26 02:42:22'),
(133, 20, 3, 20, 30000, 'NO', 1, '2016-07-26 02:42:22'),
(134, 20, 3, 20, 30000, 'NO', 1, '2016-07-26 02:42:22'),
(135, 20, 3, 20, 30000, 'NO', 1, '2016-07-26 02:42:22'),
(136, 20, 3, 20, 30000, 'NO', 1, '2016-07-26 02:42:22'),
(137, 20, 3, 20, 30000, 'NO', 1, '2016-07-26 02:42:22'),
(138, 20, 3, 20, 30000, 'NO', 1, '2016-07-26 02:42:22'),
(139, 20, 3, 20, 30000, 'NO', 1, '2016-07-26 02:42:22'),
(140, 20, 4, 10, 120000, 'NO', 1, '2016-07-26 02:42:22'),
(141, 20, 4, 10, 120000, 'NO', 1, '2016-07-26 02:42:22'),
(142, 20, 4, 10, 120000, 'NO', 1, '2016-07-26 02:42:22'),
(143, 20, 4, 10, 120000, 'NO', 1, '2016-07-26 02:42:22'),
(144, 20, 4, 10, 120000, 'NO', 1, '2016-07-26 02:42:22'),
(145, 20, 4, 10, 120000, 'NO', 1, '2016-07-26 02:42:22'),
(146, 20, 4, 10, 120000, 'NO', 1, '2016-07-26 02:42:22'),
(147, 20, 4, 10, 120000, 'NO', 1, '2016-07-26 02:42:22'),
(148, 20, 4, 10, 120000, 'NO', 1, '2016-07-26 02:42:22'),
(149, 20, 4, 10, 120000, 'NO', 1, '2016-07-26 02:42:22'),
(150, 20, 4, 10, 120000, 'NO', 1, '2016-07-26 02:42:22'),
(151, 20, 4, 10, 120000, 'NO', 1, '2016-07-26 02:42:22'),
(152, 20, 4, 10, 120000, 'NO', 1, '2016-07-26 02:42:22'),
(153, 20, 4, 10, 120000, 'NO', 1, '2016-07-26 02:42:22'),
(154, 20, 4, 10, 120000, 'NO', 1, '2016-07-26 02:42:22'),
(155, 20, 4, 10, 120000, 'NO', 1, '2016-07-26 02:42:22'),
(156, 15, 1, 1, 10000, 'NO', 1, '2016-07-26 02:43:59'),
(157, 15, 1, 100, 10000, 'NO', 1, '2016-07-26 02:43:59'),
(158, 15, 3, 10, 30000, 'NO', 1, '2016-07-26 02:43:59'),
(159, 27, 1, 1, 0, 'YES', 1, '2016-08-13 01:33:05'),
(160, 27, 2, 0, 0, 'YES', 1, '2016-08-13 01:33:05'),
(161, 27, 3, 10, 0, 'YES', 1, '2016-08-13 01:33:05'),
(162, 27, 1, 100, 10000, 'NO', 1, '2016-08-13 01:33:05'),
(163, 27, 2, 100, 7000, 'NO', 1, '2016-08-13 01:33:05'),
(164, 27, 3, 10, 30000, 'NO', 1, '2016-08-13 01:33:05'),
(165, 27, 4, 10, 120000, 'NO', 1, '2016-08-13 01:33:06'),
(166, 27, 1, 100, 10000, 'NO', 1, '2016-08-13 02:01:06'),
(167, 27, 2, 100, 7000, 'NO', 1, '2016-08-13 02:01:06'),
(168, 27, 3, 10, 30000, 'NO', 1, '2016-08-13 02:01:06'),
(169, 27, 4, 10, 120000, 'NO', 1, '2016-08-13 02:01:06'),
(170, 21, 1, 300, 10000, 'NO', 1, '2016-08-13 02:17:33'),
(171, 21, 3, 10, 30000, 'NO', 1, '2016-08-13 02:17:33'),
(172, 21, 4, 10, 120000, 'NO', 1, '2016-08-13 02:17:33'),
(173, 21, 1, 1, 10000, 'NO', 1, '2016-08-13 02:17:33'),
(174, 26, 1, 100, 10000, 'NO', 1, '2016-08-15 08:56:40'),
(176, 31, 1, 12, 0, 'YES', 0, '2017-12-11 04:14:38'),
(177, 31, 2, 34, 0, 'YES', 0, '2017-12-11 04:14:38'),
(178, 31, 3, 56, 0, 'YES', 0, '2017-12-11 04:14:38'),
(179, 31, 4, 2, 120000, 'NO', 0, '2017-12-11 04:14:38');

-- --------------------------------------------------------

--
-- Table structure for table `tb_item_included`
--

CREATE TABLE `tb_item_included` (
  `ID_ITEM_INCLUDED` int(11) NOT NULL,
  `ID_BOOKING` int(11) NOT NULL,
  `ID_ITEM` int(11) NOT NULL,
  `JML_ITEM` int(11) NOT NULL,
  `ID_VENDOR` int(11) NOT NULL,
  `PRICE_VENDOR` int(11) NOT NULL DEFAULT '0',
  `ID_USER` int(11) NOT NULL,
  `LAST_UPDATE` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tb_jenis_setup`
--

CREATE TABLE `tb_jenis_setup` (
  `ID_JENIS_SETUP` int(11) NOT NULL,
  `NAMA_JENIS_SETUP` varchar(100) NOT NULL,
  `DESKRIPSI` text NOT NULL,
  `IS_ACTIVE` int(1) NOT NULL DEFAULT '1',
  `LAST_UPDATE` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_jenis_setup`
--

INSERT INTO `tb_jenis_setup` (`ID_JENIS_SETUP`, `NAMA_JENIS_SETUP`, `DESKRIPSI`, `IS_ACTIVE`, `LAST_UPDATE`) VALUES
(1, 'SEMI STANDING', 'Diisi kursi hanya sebagian, sebagian lagi pengunjung akan berdiri', 1, '2017-12-07 07:38:26'),
(2, 'SEMINAR', 'Setup table dan kursi seperti ruangan seminar, tanpa stage. Meja moderator dan narasumber disediakan.', 1, '2016-05-19 09:23:22'),
(3, 'TEST', 'TEST', 1, '2017-01-26 09:33:41');

-- --------------------------------------------------------

--
-- Table structure for table `tb_jurnal`
--

CREATE TABLE `tb_jurnal` (
  `ID_JURNAL` int(11) NOT NULL,
  `NO_BUKTI` varchar(20) DEFAULT NULL,
  `TGL_TRANSAKSI` date NOT NULL,
  `AYAT_JURNAL` varchar(200) NOT NULL,
  `JENIS_JURNAL` varchar(100) NOT NULL,
  `REF_JURNAL` varchar(30) NOT NULL,
  `ID_USER` int(11) NOT NULL,
  `LAST_UPDATE` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_jurnal`
--

INSERT INTO `tb_jurnal` (`ID_JURNAL`, `NO_BUKTI`, `TGL_TRANSAKSI`, `AYAT_JURNAL`, `JENIS_JURNAL`, `REF_JURNAL`, `ID_USER`, `LAST_UPDATE`) VALUES
(2, '1', '2016-07-21', 'Sewa Ruangan ABHIPRAYA untuk Event test3 pada 2016-07-31', 'PENDAPATAN SEWA', '20', 1, '2016-07-24 08:18:56'),
(3, '1', '2016-07-21', 'Sewa Ruangan GRAND BALLROOM untuk Event GOLDEN HORSE SEMINAR pada 2016-08-21', 'PENDAPATAN SEWA', '21', 1, '2016-07-24 08:18:57'),
(13, 'B-160721-0001', '2016-07-21', 'Pembayaran Sewa Ruangan Termin 1', 'PEMBAYARAN SEWA', '20', 1, '2016-07-21 09:03:23'),
(14, 'B-160721-0001', '2016-07-21', 'Pembayaran Sewa Ruangan Termin 2 dan Jaminan Kerusakan', 'PEMBAYARAN SEWA', '20', 1, '2016-07-21 09:03:52'),
(15, 'B-160721-0010', '2016-07-21', 'Pembayaran Sewa Ruangan Termin 3', 'PEMBAYARAN SEWA', '20', 1, '2016-07-21 09:06:16'),
(16, 'B-160721-0010', '2016-07-21', 'Pembayaran Sewa Ruangan Termin 1', 'PEMBAYARAN SEWA', '20', 1, '2016-07-21 09:10:41'),
(17, 'B-160723-0001', '2016-07-23', 'Pembayaran Sewa Ruangan Termin 2 dan Jaminan Kerusakan', 'PEMBAYARAN SEWA', '20', 1, '2016-07-23 02:12:21'),
(18, 'B-160723-0002', '2016-07-23', 'Pembayaran Sewa Ruangan Termin 3', 'PEMBAYARAN SEWA', '20', 1, '2016-07-23 02:13:58'),
(19, '1', '2016-07-23', 'Sewa Ruangan ABHINAYA untuk Event Test 23 juli pada 2016-07-30', 'PENDAPATAN SEWA', '22', 1, '2016-07-24 08:19:01'),
(20, 'P-160723-0009', '2016-07-23', 'Sewa Ruangan ABHINAYA untuk Event sdasdasd pada 2016-07-31', 'PENDAPATAN SEWA', '23', 1, '2016-07-23 03:13:08'),
(21, 'P-160721-0006', '2016-07-24', 'Adjustment Sewa Ruangan ABHIPRAYA untuk Event TEST3 pada 2016-07-31', 'PENDAPATAN SEWA', '20', 1, '2016-07-24 03:21:57'),
(22, 'sdad', '2016-07-25', 'sdasdas', 'JURNAL MANUAL', '-', 1, '2016-07-25 03:36:31'),
(23, '23123', '2016-07-25', '231232', 'JURNAL MANUAL', '-', 1, '2016-07-25 03:39:51'),
(24, '131231', '2016-07-25', 'saeqwe', 'JURNAL MANUAL', '-', 1, '2016-07-25 03:42:28'),
(25, '23131', '2016-07-25', '312da', 'JURNAL MANUAL', '-', 1, '2016-07-25 03:45:52'),
(26, '891793723', '2016-07-25', 'Test Entri Jurnal Manual', 'JURNAL MANUAL', '-', 1, '2016-07-25 03:47:43'),
(27, '12345', '2016-07-25', 'Petty Cash Event XXX', 'JURNAL MANUAL', '-', 1, '2016-07-25 06:36:41'),
(28, 'PC-160725-0001', '2016-07-25', 'Test Petty Cash Entry', 'PETTY CASH', '1', 1, '2016-07-25 09:16:32'),
(29, 'P-160726-0010', '2016-07-26', 'Sewa Ruangan ABHINAYA untuk Event Tes Pengiriman Email pada 2016-07-26', 'PENDAPATAN SEWA', '24', 1, '2016-07-26 02:01:32'),
(30, 'P-160726-0011', '2016-07-26', 'Sewa Ruangan ABHINAYA untuk Event TEST EVENT pada 2016-07-26', 'PENDAPATAN SEWA', '25', 1, '2016-07-26 02:04:52'),
(31, 'P-160726-0012', '2016-07-26', 'Sewa Ruangan PASEBAN untuk Event PROGRAMMING SEMINAR pada 2016-08-07', 'PENDAPATAN SEWA', '26', 1, '2016-07-26 02:34:14'),
(32, 'B-160726-0001', '2016-07-26', 'Pembayaran Sewa Ruangan Termin 1', 'PEMBAYARAN SEWA', '26', 1, '2016-07-26 02:56:35'),
(33, 'B-160726-0002', '2016-07-26', 'Pembayaran Sewa Ruangan Termin 2 dan Jaminan Kerusakan', 'PEMBAYARAN SEWA', '26', 1, '2016-07-26 03:02:42'),
(34, 'B-160726-0003', '2016-07-26', 'Pembayaran Sewa Ruangan Termin 3', 'PEMBAYARAN SEWA', '26', 1, '2016-07-26 03:05:12'),
(35, 'B-160727-0001', '2016-07-27', 'Pembayaran Sewa Ruangan Termin 1', 'PEMBAYARAN SEWA', '16', 1, '2016-07-27 01:42:50'),
(36, 'B-160727-0002', '2016-07-27', 'Pembayaran Sewa Ruangan Termin 2 dan Jaminan Kerusakan', 'PEMBAYARAN SEWA', '16', 1, '2016-07-27 01:43:14'),
(37, 'RF-160727-0001', '2016-07-27', 'Refund Sewa Bagus Ayu', 'REFUND', '1', 1, '2016-07-27 04:55:02'),
(38, 'P-160813-0001', '2016-08-13', 'Sewa Ruangan ABHINAYA untuk Event test pada 2016-09-15', 'PENDAPATAN SEWA', '27', 1, '2016-08-13 01:33:06'),
(39, 'B-160813-0001', '2016-08-13', 'Pembayaran Sewa Ruangan Termin 1', 'PEMBAYARAN SEWA', '27', 1, '2016-08-13 01:37:05'),
(40, 'B-160813-0002', '2016-08-13', 'Pembayaran Sewa Ruangan Termin 2 dan Jaminan Kerusakan', 'PEMBAYARAN SEWA', '27', 1, '2016-08-13 01:40:52'),
(41, 'P-160721-0007', '2016-08-13', 'Adjustment Sewa Ruangan GRAND BALLROOM untuk Event GOLDEN HORSE SEMINAR pada 2016-08-21', 'PENDAPATAN SEWA', '21', 1, '2016-08-13 02:17:33'),
(42, 'B-161018-0001', '2016-10-18', 'Pembayaran Sewa Ruangan Termin 1', 'PEMBAYARAN SEWA', '21', 1, '2016-10-18 07:30:25'),
(43, 'B-171211-0003', '2017-12-11', 'Sewa Ruangan GRAND BALLROOM untuk Event GitLab.com pada 2017-12-11 ', 'PENDAPATAN SEWA', '31', 0, '2017-12-11 04:14:38');

-- --------------------------------------------------------

--
-- Table structure for table `tb_payment_schedule`
--

CREATE TABLE `tb_payment_schedule` (
  `ID_PAYMENT_SCHEDULE` int(11) NOT NULL,
  `ID_BOOKING` int(11) NOT NULL,
  `NO_BUKTI` varchar(255) NOT NULL DEFAULT '-',
  `TGL_PAYMENT_SCHEDULE` date NOT NULL,
  `ALOKASI_PAYMENT` float NOT NULL DEFAULT '0',
  `STATUS_PAYMENT` enum('OUTSTANDING','PAID') NOT NULL DEFAULT 'OUTSTANDING',
  `TGL_PAYMENT_RECEIVED` date DEFAULT NULL,
  `JML_PAYMENT_RECEIVED` int(11) NOT NULL DEFAULT '0',
  `CARA_PAYMENT` enum('TUNAI','BANK') NOT NULL DEFAULT 'TUNAI',
  `ID_USER` int(11) DEFAULT NULL,
  `LAST_UPDATE` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_payment_schedule`
--

INSERT INTO `tb_payment_schedule` (`ID_PAYMENT_SCHEDULE`, `ID_BOOKING`, `NO_BUKTI`, `TGL_PAYMENT_SCHEDULE`, `ALOKASI_PAYMENT`, `STATUS_PAYMENT`, `TGL_PAYMENT_RECEIVED`, `JML_PAYMENT_RECEIVED`, `CARA_PAYMENT`, `ID_USER`, `LAST_UPDATE`) VALUES
(1, 15, '-', '2016-07-16', 30, 'PAID', '2016-07-20', 5000000, 'TUNAI', 1, '2016-07-20 08:37:29'),
(2, 15, 'B-160720-0001', '2016-07-22', 30, 'PAID', '2016-07-20', 8000000, 'TUNAI', 1, '2016-07-20 09:01:14'),
(3, 15, 'B-160720-0002', '2016-07-31', 40, 'PAID', '2016-07-20', 3310000, 'TUNAI', 1, '2016-07-20 09:10:55'),
(4, 16, 'B-160727-0001', '2016-07-18', 30, 'PAID', '2016-07-27', 5000000, 'TUNAI', 1, '2016-07-27 01:42:50'),
(5, 16, 'B-160727-0002', '2016-07-26', 30, 'PAID', '2016-07-27', 15000000, 'TUNAI', 1, '2016-07-27 01:43:14'),
(6, 16, '-', '2016-08-31', 40, 'OUTSTANDING', NULL, 0, 'TUNAI', NULL, '2016-07-20 08:37:30'),
(7, 17, '-', '2016-07-21', 30, 'OUTSTANDING', NULL, 0, 'TUNAI', NULL, '2016-07-21 06:38:27'),
(8, 17, '-', '2016-07-28', 40, 'OUTSTANDING', NULL, 0, 'TUNAI', NULL, '2016-07-21 06:38:27'),
(9, 17, '-', '2016-07-30', 30, 'OUTSTANDING', NULL, 0, 'TUNAI', NULL, '2016-07-21 06:38:27'),
(10, 18, '-', '2016-07-21', 30, 'OUTSTANDING', NULL, 0, 'TUNAI', NULL, '2016-07-21 06:41:38'),
(11, 18, '-', '2016-07-26', 40, 'OUTSTANDING', NULL, 0, 'TUNAI', NULL, '2016-07-21 06:41:38'),
(12, 18, '-', '2016-07-31', 30, 'OUTSTANDING', NULL, 0, 'TUNAI', NULL, '2016-07-21 06:41:38'),
(13, 19, '-', '2016-07-21', 30, 'OUTSTANDING', NULL, 0, 'TUNAI', NULL, '2016-07-21 06:44:00'),
(14, 19, '-', '2016-07-25', 40, 'OUTSTANDING', NULL, 0, 'TUNAI', NULL, '2016-07-21 06:44:00'),
(15, 19, '-', '2016-07-31', 30, 'OUTSTANDING', NULL, 0, 'TUNAI', NULL, '2016-07-21 06:44:00'),
(16, 20, 'B-160721-0010', '2016-07-21', 30, 'PAID', '2016-07-21', 5000000, 'TUNAI', 1, '2016-07-21 09:10:41'),
(17, 20, 'B-160723-0001', '2016-07-25', 40, 'PAID', '2016-07-23', 11000000, 'TUNAI', 1, '2016-07-23 02:12:21'),
(18, 20, 'B-160723-0002', '2016-07-31', 30, 'PAID', '2016-07-23', 5800000, 'BANK', 1, '2016-07-23 02:13:58'),
(19, 21, 'B-161018-0001', '2016-07-21', 30, 'PAID', '2016-10-18', 5000000, 'TUNAI', 1, '2016-10-18 07:30:25'),
(20, 21, '-', '2016-07-31', 40, 'OUTSTANDING', NULL, 0, 'TUNAI', NULL, '2016-07-21 06:53:04'),
(21, 21, '-', '2016-08-21', 30, 'OUTSTANDING', NULL, 0, 'TUNAI', NULL, '2016-07-21 06:53:04'),
(22, 22, '-', '2016-07-23', 30, 'OUTSTANDING', NULL, 0, 'TUNAI', NULL, '2016-07-23 03:07:01'),
(23, 22, '-', '2016-07-27', 40, 'OUTSTANDING', NULL, 0, 'TUNAI', NULL, '2016-07-23 03:07:01'),
(24, 22, '-', '2016-07-31', 30, 'OUTSTANDING', NULL, 0, 'TUNAI', NULL, '2016-07-23 03:07:01'),
(25, 23, '-', '2016-07-23', 30, 'OUTSTANDING', NULL, 0, 'TUNAI', NULL, '2016-07-23 03:13:07'),
(26, 23, '-', '2016-07-23', 40, 'OUTSTANDING', NULL, 0, 'TUNAI', NULL, '2016-07-23 03:13:07'),
(27, 23, '-', '2016-07-23', 30, 'OUTSTANDING', NULL, 0, 'TUNAI', NULL, '2016-07-23 03:13:08'),
(28, 24, '-', '2016-07-26', 30, 'OUTSTANDING', NULL, 0, 'TUNAI', NULL, '2016-07-26 02:01:32'),
(29, 24, '-', '2016-07-30', 40, 'OUTSTANDING', NULL, 0, 'TUNAI', NULL, '2016-07-26 02:01:32'),
(30, 24, '-', '2016-08-06', 30, 'OUTSTANDING', NULL, 0, 'TUNAI', NULL, '2016-07-26 02:01:32'),
(31, 25, '-', '2016-07-26', 10, 'OUTSTANDING', NULL, 0, 'TUNAI', NULL, '2016-07-26 02:04:52'),
(32, 25, '-', '2016-07-26', 20, 'OUTSTANDING', NULL, 0, 'TUNAI', NULL, '2016-07-26 02:04:52'),
(33, 25, '-', '2016-07-26', 70, 'OUTSTANDING', NULL, 0, 'TUNAI', NULL, '2016-07-26 02:04:52'),
(34, 26, 'B-160726-0001', '2016-07-26', 30, 'PAID', '2016-07-26', 5000000, 'TUNAI', 1, '2016-07-26 02:56:35'),
(35, 26, 'B-160726-0002', '2016-07-30', 40, 'PAID', '2016-07-26', 11000000, 'BANK', 1, '2016-07-26 03:02:41'),
(36, 26, 'B-160726-0003', '2016-08-07', 30, 'PAID', '2016-07-26', 4900000, 'TUNAI', 1, '2016-07-26 03:05:12'),
(37, 27, 'B-160813-0001', '2016-08-13', 30, 'PAID', '2016-08-13', 5000000, 'BANK', 1, '2016-08-13 01:37:05'),
(38, 27, 'B-160813-0002', '2016-08-31', 40, 'PAID', '2016-08-13', 10000000, 'TUNAI', 1, '2016-08-13 01:40:52'),
(39, 27, '-', '2016-09-15', 30, 'OUTSTANDING', NULL, 0, 'TUNAI', NULL, '2016-08-13 01:33:06'),
(40, 31, 'B-171211-0003', '2017-12-11', 40, 'OUTSTANDING', NULL, 0, 'TUNAI', NULL, '2017-12-11 04:14:38'),
(41, 31, 'B-171211-0003', '2017-12-20', 30, 'OUTSTANDING', NULL, 0, 'TUNAI', NULL, '2017-12-11 04:14:38'),
(42, 31, 'B-171211-0003', '2017-12-29', 30, 'OUTSTANDING', NULL, 0, 'TUNAI', NULL, '2017-12-11 04:14:38');

-- --------------------------------------------------------

--
-- Table structure for table `tb_pembayaran`
--

CREATE TABLE `tb_pembayaran` (
  `ID_PEMBAYARAN` int(11) NOT NULL,
  `NO_HARIAN` int(11) NOT NULL DEFAULT '0',
  `NO_BUKTI` varchar(255) NOT NULL DEFAULT '-',
  `TGL_PEMBAYARAN` date NOT NULL,
  `CARA_PEMBAYARAN` enum('TUNAI','BANK') NOT NULL DEFAULT 'TUNAI',
  `JUMLAH_PEMBAYARAN` int(11) NOT NULL,
  `ID_JURNAL` int(11) DEFAULT NULL,
  `ID_BOOKING` int(11) NOT NULL,
  `ID_USER` int(11) NOT NULL,
  `WAKTU_POSTING` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_pembayaran`
--

INSERT INTO `tb_pembayaran` (`ID_PEMBAYARAN`, `NO_HARIAN`, `NO_BUKTI`, `TGL_PEMBAYARAN`, `CARA_PEMBAYARAN`, `JUMLAH_PEMBAYARAN`, `ID_JURNAL`, `ID_BOOKING`, `ID_USER`, `WAKTU_POSTING`) VALUES
(1, 1, 'B-160720-0001', '2016-07-20', 'TUNAI', 8000000, NULL, 15, 1, '2016-07-21 08:02:45'),
(2, 2, 'B-160720-0002', '2016-07-21', 'TUNAI', 3310000, NULL, 15, 1, '2016-07-23 01:46:38'),
(15, 3, 'B-160721-0010', '2016-07-21', 'TUNAI', 5000000, NULL, 20, 1, '2016-07-23 01:49:26'),
(16, 1, 'B-160723-0001', '2016-07-23', 'TUNAI', 11000000, NULL, 20, 1, '2016-07-23 02:12:21'),
(17, 2, 'B-160723-0002', '2016-07-23', 'BANK', 5800000, NULL, 20, 1, '2016-07-23 02:13:58'),
(18, 1, 'B-160726-0001', '2016-07-26', 'TUNAI', 5000000, NULL, 26, 1, '2016-07-26 02:56:35'),
(19, 2, 'B-160726-0002', '2016-07-26', 'BANK', 11000000, NULL, 26, 1, '2016-07-26 03:02:41'),
(20, 3, 'B-160726-0003', '2016-07-26', 'TUNAI', 4900000, NULL, 26, 1, '2016-07-26 03:05:12'),
(21, 1, 'B-160727-0001', '2016-07-27', 'TUNAI', 5000000, NULL, 16, 1, '2016-07-27 01:42:50'),
(22, 2, 'B-160727-0002', '2016-07-27', 'TUNAI', 15000000, NULL, 16, 1, '2016-07-27 01:43:14'),
(23, 1, 'B-160813-0001', '2016-08-13', 'BANK', 5000000, NULL, 27, 1, '2016-08-13 01:37:05'),
(24, 2, 'B-160813-0002', '2016-08-13', 'TUNAI', 10000000, NULL, 27, 1, '2016-08-13 01:40:52'),
(25, 1, 'B-161018-0001', '2016-10-18', 'TUNAI', 5000000, NULL, 21, 1, '2016-10-18 07:30:25');

-- --------------------------------------------------------

--
-- Table structure for table `tb_petty_cash`
--

CREATE TABLE `tb_petty_cash` (
  `ID_PETTY_CASH` int(11) NOT NULL,
  `NO_HARIAN` int(11) NOT NULL,
  `NO_BUKTI` varchar(20) NOT NULL,
  `TGL_TRANSAKSI` date NOT NULL,
  `ID_BOOKING` int(11) NOT NULL,
  `JML_TRANSAKSI` int(11) NOT NULL DEFAULT '0',
  `ID_USER` int(11) NOT NULL,
  `WAKTU_POSTING` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_petty_cash`
--

INSERT INTO `tb_petty_cash` (`ID_PETTY_CASH`, `NO_HARIAN`, `NO_BUKTI`, `TGL_TRANSAKSI`, `ID_BOOKING`, `JML_TRANSAKSI`, `ID_USER`, `WAKTU_POSTING`) VALUES
(1, 1, 'PC-160725-0001', '2016-07-25', 21, 1000000, 1, '2016-07-25 09:16:32');

-- --------------------------------------------------------

--
-- Table structure for table `tb_profil`
--

CREATE TABLE `tb_profil` (
  `ID_PROFIL` int(11) NOT NULL,
  `NAMA` varchar(100) NOT NULL,
  `ALAMAT` varchar(300) NOT NULL,
  `TELEPON` varchar(50) NOT NULL,
  `FAX` varchar(50) NOT NULL,
  `EMAIL` varchar(100) NOT NULL,
  `WEBSITE` varchar(100) NOT NULL,
  `NPWP` varchar(100) NOT NULL,
  `PKP` varchar(100) NOT NULL,
  `LOGO` longblob
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_profil`
--

INSERT INTO `tb_profil` (`ID_PROFIL`, `NAMA`, `ALAMAT`, `TELEPON`, `FAX`, `EMAIL`, `WEBSITE`, `NPWP`, `PKP`, `LOGO`) VALUES
(1, 'BHUMIKU', 'Jl. Gunung Soputan 49 Denpasar - Bali', '(0361) 480514', '(0361) 483339', 'info@bhumiku.com', 'www.bhumiku.com', '-', '-', 0x69636f6e2d6268756d696b752e706e67);

-- --------------------------------------------------------

--
-- Table structure for table `tb_refund`
--

CREATE TABLE `tb_refund` (
  `ID_REFUND` int(11) NOT NULL,
  `NO_HARIAN` int(11) NOT NULL,
  `NO_BUKTI` varchar(20) NOT NULL,
  `TGL_TRANSAKSI` date NOT NULL,
  `ID_BOOKING` int(11) NOT NULL,
  `JML_TRANSAKSI` int(11) NOT NULL DEFAULT '0',
  `ID_USER` int(11) NOT NULL,
  `WAKTU_POSTING` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_refund`
--

INSERT INTO `tb_refund` (`ID_REFUND`, `NO_HARIAN`, `NO_BUKTI`, `TGL_TRANSAKSI`, `ID_BOOKING`, `JML_TRANSAKSI`, `ID_USER`, `WAKTU_POSTING`) VALUES
(1, 1, 'RF-160727-0001', '2016-07-27', 16, 1000000, 1, '2016-07-27 04:55:02');

-- --------------------------------------------------------

--
-- Table structure for table `tb_ruangan`
--

CREATE TABLE `tb_ruangan` (
  `ID_RUANGAN` int(11) NOT NULL,
  `NAMA_RUANGAN` varchar(100) NOT NULL,
  `KAPASITAS_RUANGAN` int(11) NOT NULL DEFAULT '0',
  `PANJANG_RUANGAN` float NOT NULL DEFAULT '0',
  `LEBAR_RUANGAN` float NOT NULL DEFAULT '0',
  `PARENT_RUANGAN` int(11) NOT NULL DEFAULT '0',
  `HARGA_SEWA` int(11) NOT NULL DEFAULT '0',
  `HARGA_HALF_DAY` int(11) NOT NULL DEFAULT '0',
  `HARGA_HOURLY` int(11) NOT NULL DEFAULT '0',
  `IS_ACTIVE` int(1) NOT NULL DEFAULT '1',
  `LAST_UPDATE` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_ruangan`
--

INSERT INTO `tb_ruangan` (`ID_RUANGAN`, `NAMA_RUANGAN`, `KAPASITAS_RUANGAN`, `PANJANG_RUANGAN`, `LEBAR_RUANGAN`, `PARENT_RUANGAN`, `HARGA_SEWA`, `HARGA_HALF_DAY`, `HARGA_HOURLY`, `IS_ACTIVE`, `LAST_UPDATE`) VALUES
(1, 'ABHINAYA', 100, 12, 6, 4, 10000000, 3000000, 0, 1, '2017-01-26 08:28:53'),
(2, 'ABHIPRAYA', 100, 17, 6, 4, 15000000, 0, 0, 1, '2017-01-24 08:45:18'),
(3, 'PASEBAN', 100, 21, 21, 4, 15000000, 0, 0, 1, '2017-01-24 08:45:18'),
(4, 'GRAND BALLROOM', 100, 26, 26, 0, 20000000, 0, 0, 1, '2016-07-14 04:34:06'),
(5, 'BALIKU SANTI', 10, 10, 10, 4, 10000000, 0, 0, 0, '2017-01-24 08:45:22'),
(6, 'TEST', 100, 1, 1, 1, 0, 0, 0, 1, '2017-01-26 08:29:31'),
(7, 'TEST1', 121, 121, 121, 0, 0, 0, 0, 1, '2017-01-26 08:34:41');

-- --------------------------------------------------------

--
-- Table structure for table `tb_satuan`
--

CREATE TABLE `tb_satuan` (
  `ID_SATUAN` int(11) NOT NULL,
  `KODE_SATUAN` varchar(10) NOT NULL,
  `NAMA_SATUAN` varchar(30) NOT NULL,
  `IS_ACTIVE` int(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_satuan`
--

INSERT INTO `tb_satuan` (`ID_SATUAN`, `KODE_SATUAN`, `NAMA_SATUAN`, `IS_ACTIVE`) VALUES
(1, 'Pcs', 'Pieces', 1),
(2, 'Pkg', 'Package', 1),
(3, 'Kg', 'Kilogram', 1),
(4, 'Box', 'Box', 1),
(5, 'Mtr', 'Meter', 1),
(6, 'Roll', 'Roll', 1),
(7, 'Eks', 'Eksemplar', 1),
(8, 'test', 'test', 0);

-- --------------------------------------------------------

--
-- Table structure for table `tb_smtp`
--

CREATE TABLE `tb_smtp` (
  `ID_SMTP` int(11) NOT NULL,
  `SMTP_HOST` varchar(255) NOT NULL,
  `SMTP_PORT` varchar(10) NOT NULL,
  `SMTP_USERNAME` varchar(255) NOT NULL,
  `SMTP_PASSWORD` varchar(255) NOT NULL,
  `SMTP_AUTH` enum('satDefault','satNone','satSASL') NOT NULL DEFAULT 'satDefault',
  `EMAIL_FROM` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_smtp`
--

INSERT INTO `tb_smtp` (`ID_SMTP`, `SMTP_HOST`, `SMTP_PORT`, `SMTP_USERNAME`, `SMTP_PASSWORD`, `SMTP_AUTH`, `EMAIL_FROM`) VALUES
(1, 'mail.bhumiku.com', '25', 'noreply@bhumiku.com', 'Norepl4y', 'satDefault', 'noreply@bhumiku.com');

-- --------------------------------------------------------

--
-- Table structure for table `tb_user`
--

CREATE TABLE `tb_user` (
  `ID_USER` int(11) NOT NULL,
  `NAMA_LENGKAP` varchar(100) NOT NULL,
  `JABATAN` enum('RESERVATION','OPERATION','FINANCE','ACCOUNTING','MANAGER','ADMINISTRATOR') NOT NULL,
  `EMAIL` varchar(255) NOT NULL,
  `HP` varchar(100) NOT NULL,
  `USER_NAME` varchar(20) NOT NULL,
  `USER_PASS` varchar(100) NOT NULL,
  `IS_ACTIVE` int(1) NOT NULL DEFAULT '1',
  `LAST_UPDATE` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_user`
--

INSERT INTO `tb_user` (`ID_USER`, `NAMA_LENGKAP`, `JABATAN`, `EMAIL`, `HP`, `USER_NAME`, `USER_PASS`, `IS_ACTIVE`, `LAST_UPDATE`) VALUES
(1, 'NGURAH KETUT HARIADI', 'ADMINISTRATOR', 'ngurah@tunjungteknologi.com', '-', 'admin', '4549323194a26feb1f4595023e742ed4', 1, '2017-12-08 05:11:41'),
(2, 'ADMINISTRATOR', 'ADMINISTRATOR', 'admin@bhumiku.com', '-', 'admin1', 'fcea920f7412b5da7be0cf42b8c93759', 1, '2017-01-24 06:40:51'),
(3, 'JONI', 'ACCOUNTING', '-', '-', 'joni', 'e10adc3949ba59abbe56e057f20f883e', 0, '2017-01-24 07:25:45'),
(4, 'COKI', 'RESERVATION', '-', '-', 'coki', '4549323194a26feb1f4595023e742ed4', 0, '2017-01-24 06:40:52'),
(5, 'DEVI', 'RESERVATION', '-', '-', 'devi', 'e10adc3949ba59abbe56e057f20f883e', 0, '2017-01-24 06:40:54'),
(6, 'FITRIA AGUSTINA', 'ADMINISTRATOR', 'info@bhumiku.com', '-', 'fitria', 'ef208a5dfcfc3ea9941d7a6c43841784', 1, '2017-01-24 06:40:54'),
(7, 'TEST', 'ADMINISTRATOR', 'TEST', 'TEST', 'test', '4549323194a26feb1f4595023e742ed4', 1, '2017-01-24 07:46:20');

-- --------------------------------------------------------

--
-- Table structure for table `tb_vendor`
--

CREATE TABLE `tb_vendor` (
  `ID_VENDOR` int(11) NOT NULL,
  `NAMA_VENDOR` varchar(100) NOT NULL,
  `ALAMAT_VENDOR` varchar(200) NOT NULL,
  `TELEPON_VENDOR` varchar(30) NOT NULL,
  `HP_VENDOR` varchar(50) NOT NULL,
  `CP_VENDOR` varchar(50) NOT NULL,
  `EMAIL_VENDOR` varchar(255) NOT NULL,
  `NPWP_VENDOR` varchar(30) NOT NULL DEFAULT '-',
  `BANK_VENDOR` varchar(100) NOT NULL,
  `REMARK_VENDOR` varchar(200) NOT NULL,
  `NO_REKENING_VENDOR` varchar(100) NOT NULL,
  `IS_ACTIVE` int(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_vendor`
--

INSERT INTO `tb_vendor` (`ID_VENDOR`, `NAMA_VENDOR`, `ALAMAT_VENDOR`, `TELEPON_VENDOR`, `HP_VENDOR`, `CP_VENDOR`, `EMAIL_VENDOR`, `NPWP_VENDOR`, `BANK_VENDOR`, `REMARK_VENDOR`, `NO_REKENING_VENDOR`, `IS_ACTIVE`) VALUES
(1, 'TUNJUNG TEKNOLOGI', 'JL. ALAM SARI NO. 6 DENPASAR', '08179313375', '08179313375', 'NGURAH', 'ngurah@tunjungteknologi.com', '9999', 'BCA', '-', '6115001594', 1),
(2, 'TEST1234', 'TEST', 'TEST', 'TEST', 'TEST', '', '-', '-', 'TEST', '-', 0),
(3, 'TEST', 'TEST', 'TEST', 'TEST', 'TEST', 'TEST', '-', 'TEST', 'TEST', 'TEST', 0);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tb_agent`
--
ALTER TABLE `tb_agent`
  ADD PRIMARY KEY (`ID_AGENT`);

--
-- Indexes for table `tb_barang`
--
ALTER TABLE `tb_barang`
  ADD PRIMARY KEY (`ID_BARANG`,`AVG_PRICE`);

--
-- Indexes for table `tb_booking`
--
ALTER TABLE `tb_booking`
  ADD PRIMARY KEY (`ID_BOOKING`),
  ADD KEY `FK_BOOKING_RUANGAN` (`ID_RUANGAN`),
  ADD KEY `FK_EO_BOOKING` (`ID_EO`),
  ADD KEY `SK_JENIS_BOOKING` (`ID_JENIS_SETUP`),
  ADD KEY `FK_BOOKING_USER` (`ID_USER`),
  ADD KEY `FK_BOOKING_AGENT` (`ID_AGENT`);

--
-- Indexes for table `tb_coa`
--
ALTER TABLE `tb_coa`
  ADD PRIMARY KEY (`kode_coa`);

--
-- Indexes for table `tb_customer`
--
ALTER TABLE `tb_customer`
  ADD PRIMARY KEY (`ID_CUSTOMER`);

--
-- Indexes for table `tb_detail_jurnal`
--
ALTER TABLE `tb_detail_jurnal`
  ADD PRIMARY KEY (`ID_DETAIL_JURNAL`),
  ADD KEY `FK_JURNAL_DETAIL` (`ID_JURNAL`),
  ADD KEY `FK_COA_DETJURNAL` (`KODE_COA`);

--
-- Indexes for table `tb_eo`
--
ALTER TABLE `tb_eo`
  ADD PRIMARY KEY (`ID_EO`);

--
-- Indexes for table `tb_history`
--
ALTER TABLE `tb_history`
  ADD PRIMARY KEY (`ID_HISTORY`),
  ADD KEY `FK_USER_HISTORY` (`ID_USER`);

--
-- Indexes for table `tb_inventaris`
--
ALTER TABLE `tb_inventaris`
  ADD PRIMARY KEY (`ID_INVENTARIS`);

--
-- Indexes for table `tb_item`
--
ALTER TABLE `tb_item`
  ADD PRIMARY KEY (`ID_ITEM`),
  ADD KEY `FK_ITEM_USER` (`ID_USER`);

--
-- Indexes for table `tb_item_add`
--
ALTER TABLE `tb_item_add`
  ADD PRIMARY KEY (`ID_ITEM_ADD`),
  ADD KEY `FK_BOOKING_ITEMADD` (`ID_BOOKING`),
  ADD KEY `FK_ITEMADD_USER` (`ID_USER`),
  ADD KEY `FK_ITEM_ADD` (`ID_ITEM`);

--
-- Indexes for table `tb_item_included`
--
ALTER TABLE `tb_item_included`
  ADD PRIMARY KEY (`ID_ITEM_INCLUDED`),
  ADD KEY `FK_BOOKING_ITEMINC` (`ID_BOOKING`);

--
-- Indexes for table `tb_jenis_setup`
--
ALTER TABLE `tb_jenis_setup`
  ADD PRIMARY KEY (`ID_JENIS_SETUP`);

--
-- Indexes for table `tb_jurnal`
--
ALTER TABLE `tb_jurnal`
  ADD PRIMARY KEY (`ID_JURNAL`);

--
-- Indexes for table `tb_payment_schedule`
--
ALTER TABLE `tb_payment_schedule`
  ADD PRIMARY KEY (`ID_PAYMENT_SCHEDULE`);

--
-- Indexes for table `tb_pembayaran`
--
ALTER TABLE `tb_pembayaran`
  ADD PRIMARY KEY (`ID_PEMBAYARAN`);

--
-- Indexes for table `tb_petty_cash`
--
ALTER TABLE `tb_petty_cash`
  ADD PRIMARY KEY (`ID_PETTY_CASH`);

--
-- Indexes for table `tb_profil`
--
ALTER TABLE `tb_profil`
  ADD PRIMARY KEY (`ID_PROFIL`);

--
-- Indexes for table `tb_refund`
--
ALTER TABLE `tb_refund`
  ADD PRIMARY KEY (`ID_REFUND`);

--
-- Indexes for table `tb_ruangan`
--
ALTER TABLE `tb_ruangan`
  ADD PRIMARY KEY (`ID_RUANGAN`);

--
-- Indexes for table `tb_satuan`
--
ALTER TABLE `tb_satuan`
  ADD PRIMARY KEY (`ID_SATUAN`);

--
-- Indexes for table `tb_smtp`
--
ALTER TABLE `tb_smtp`
  ADD PRIMARY KEY (`ID_SMTP`);

--
-- Indexes for table `tb_user`
--
ALTER TABLE `tb_user`
  ADD PRIMARY KEY (`ID_USER`);

--
-- Indexes for table `tb_vendor`
--
ALTER TABLE `tb_vendor`
  ADD PRIMARY KEY (`ID_VENDOR`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tb_agent`
--
ALTER TABLE `tb_agent`
  MODIFY `ID_AGENT` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `tb_barang`
--
ALTER TABLE `tb_barang`
  MODIFY `ID_BARANG` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `tb_booking`
--
ALTER TABLE `tb_booking`
  MODIFY `ID_BOOKING` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=32;

--
-- AUTO_INCREMENT for table `tb_customer`
--
ALTER TABLE `tb_customer`
  MODIFY `ID_CUSTOMER` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `tb_detail_jurnal`
--
ALTER TABLE `tb_detail_jurnal`
  MODIFY `ID_DETAIL_JURNAL` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=86;

--
-- AUTO_INCREMENT for table `tb_eo`
--
ALTER TABLE `tb_eo`
  MODIFY `ID_EO` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `tb_history`
--
ALTER TABLE `tb_history`
  MODIFY `ID_HISTORY` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=733;

--
-- AUTO_INCREMENT for table `tb_inventaris`
--
ALTER TABLE `tb_inventaris`
  MODIFY `ID_INVENTARIS` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tb_item`
--
ALTER TABLE `tb_item`
  MODIFY `ID_ITEM` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `tb_item_add`
--
ALTER TABLE `tb_item_add`
  MODIFY `ID_ITEM_ADD` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=180;

--
-- AUTO_INCREMENT for table `tb_item_included`
--
ALTER TABLE `tb_item_included`
  MODIFY `ID_ITEM_INCLUDED` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tb_jenis_setup`
--
ALTER TABLE `tb_jenis_setup`
  MODIFY `ID_JENIS_SETUP` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `tb_jurnal`
--
ALTER TABLE `tb_jurnal`
  MODIFY `ID_JURNAL` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=44;

--
-- AUTO_INCREMENT for table `tb_payment_schedule`
--
ALTER TABLE `tb_payment_schedule`
  MODIFY `ID_PAYMENT_SCHEDULE` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=43;

--
-- AUTO_INCREMENT for table `tb_pembayaran`
--
ALTER TABLE `tb_pembayaran`
  MODIFY `ID_PEMBAYARAN` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;

--
-- AUTO_INCREMENT for table `tb_petty_cash`
--
ALTER TABLE `tb_petty_cash`
  MODIFY `ID_PETTY_CASH` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `tb_profil`
--
ALTER TABLE `tb_profil`
  MODIFY `ID_PROFIL` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `tb_refund`
--
ALTER TABLE `tb_refund`
  MODIFY `ID_REFUND` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `tb_ruangan`
--
ALTER TABLE `tb_ruangan`
  MODIFY `ID_RUANGAN` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `tb_satuan`
--
ALTER TABLE `tb_satuan`
  MODIFY `ID_SATUAN` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `tb_smtp`
--
ALTER TABLE `tb_smtp`
  MODIFY `ID_SMTP` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `tb_user`
--
ALTER TABLE `tb_user`
  MODIFY `ID_USER` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `tb_vendor`
--
ALTER TABLE `tb_vendor`
  MODIFY `ID_VENDOR` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `tb_booking`
--
ALTER TABLE `tb_booking`
  ADD CONSTRAINT `FK_BOOKING_AGENT` FOREIGN KEY (`ID_AGENT`) REFERENCES `tb_agent` (`ID_AGENT`) ON DELETE NO ACTION ON UPDATE CASCADE,
  ADD CONSTRAINT `FK_BOOKING_RUANGAN` FOREIGN KEY (`ID_RUANGAN`) REFERENCES `tb_ruangan` (`ID_RUANGAN`) ON DELETE NO ACTION ON UPDATE CASCADE,
  ADD CONSTRAINT `FK_EO_BOOKING` FOREIGN KEY (`ID_EO`) REFERENCES `tb_eo` (`ID_EO`) ON DELETE NO ACTION ON UPDATE CASCADE,
  ADD CONSTRAINT `SK_JENIS_BOOKING` FOREIGN KEY (`ID_JENIS_SETUP`) REFERENCES `tb_jenis_setup` (`ID_JENIS_SETUP`) ON DELETE NO ACTION ON UPDATE CASCADE;

--
-- Constraints for table `tb_detail_jurnal`
--
ALTER TABLE `tb_detail_jurnal`
  ADD CONSTRAINT `FK_COA_DETJURNAL` FOREIGN KEY (`KODE_COA`) REFERENCES `tb_coa` (`kode_coa`) ON DELETE NO ACTION ON UPDATE CASCADE,
  ADD CONSTRAINT `FK_JURNAL_DETAIL` FOREIGN KEY (`ID_JURNAL`) REFERENCES `tb_jurnal` (`ID_JURNAL`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `tb_history`
--
ALTER TABLE `tb_history`
  ADD CONSTRAINT `FK_USER_HISTORY` FOREIGN KEY (`ID_USER`) REFERENCES `tb_user` (`ID_USER`) ON DELETE NO ACTION ON UPDATE CASCADE;

--
-- Constraints for table `tb_item`
--
ALTER TABLE `tb_item`
  ADD CONSTRAINT `FK_ITEM_USER` FOREIGN KEY (`ID_USER`) REFERENCES `tb_user` (`ID_USER`) ON DELETE NO ACTION ON UPDATE CASCADE;

--
-- Constraints for table `tb_item_add`
--
ALTER TABLE `tb_item_add`
  ADD CONSTRAINT `FK_BOOKING_ITEMADD` FOREIGN KEY (`ID_BOOKING`) REFERENCES `tb_booking` (`ID_BOOKING`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `FK_ITEM_ADD` FOREIGN KEY (`ID_ITEM`) REFERENCES `tb_item` (`ID_ITEM`) ON DELETE NO ACTION ON UPDATE CASCADE;

--
-- Constraints for table `tb_item_included`
--
ALTER TABLE `tb_item_included`
  ADD CONSTRAINT `FK_BOOKING_ITEMINC` FOREIGN KEY (`ID_BOOKING`) REFERENCES `tb_booking` (`ID_BOOKING`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
