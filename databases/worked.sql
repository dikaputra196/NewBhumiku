USE db_bhumiku

SELECT * FROM tb_barang

SELECT* FROM tb_satuan

SELECT
a.kode_barang,
a.nama_barang,
b.nama_satuan AS Satuan_Besar,
c.nama_satuan AS Satuan_Kecil,
a.jenis_barang,
IF(is_inventory='1','Ya','Tidak') AS INV,
a.on_stock,
a.on_purchased
FROM
tb_barang a
INNER JOIN tb_satuan b
ON b.id_satuan=a.id_satuan_besar
INNER JOIN tb_satuan c
ON c.id_satuan = a.id_satuan_kecil
WHERE a.is_active='1'