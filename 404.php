<!DOCTYPE html>
<?php
    //Required File
    require_once dirname(__FILE__)."/components/templates/main.php";
    require_once dirname(__FILE__)."/class/config.php";
    
    //Connect to Database
    $db = new Database();
    $db->connect();
    
    //Call Template
    $template = new Template();
    
    //Start HTML
    $template->pageTitle="BHUMIKU Balai Pertemuan | 404 Not Found";
    $template->startHtml();
    
    //Start Content
    $template->startContent();
?>
<!-- 404 Error -->
<div class="error-page">
    <h2 class="headline text-yellow"> 404</h2>
    <div class="error-content">
        <h3><i class="fa fa-warning text-yellow"></i> Oops! Halaman Tidak Ditemukan.</h3>
        <p>
            Halaman yang anda tuju tidak ditemukan
            Anda dapat <a href="<?=MAIN_URL?>/beranda.php">Kembali ke Halaman Utama</a>.
        </p>
    </div>
</div>
<!-- //End 404 Error -->

<!-- End Content -->
<?php $template->endContent(); ?>

<!-- Place Script Here -->

<!-- //End Script Place -->

<!-- End HTML -->
<?php $template->endHtml(); ?>