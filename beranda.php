<!--
/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */-->
<?php
    //Required File
    require_once dirname(__FILE__)."/components/templates/main.php";
    require_once dirname(__FILE__)."/class/config.php";
    require_once dirname(__FILE__)."/class/manual_connect.php";
    
    //Define Connection -> Database
    $db = new Database();
    $db->connect();
    $config = new manual_connect();
    
    //Call Template
    $template = new Template();
    
    //
    $config->noSession();
    $currentLink = manual_connect::encryptor('encrypt', $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI']);
    
    //Start HTML
    $template->pageTitle="BHUMIKU Balai Pertemuan | Beranda";
    
    //Start Content
    $template->contentTitle="<span class='glyphicon glyphicon-check'></span> Beranda";
    $template->startContent();
    
?>

<!-- Start Box -->
<div class="row">
    <!-- Box New Booking -->
    <div class="col-md-3 col-sm-6 col-xs-12">
        <div class="info-box">
            <a href="<?= MAIN_URL ?>/pages/form_new_booking.php"><span class="info-box-icon bg-aqua"><i class="fa fa-calendar-plus-o"></i></span></a>
            
            <div class="info-box-content">
                <span class="info-box-text"><strong>New</strong></span>
                <span class="info-box-more">Membuat Booking Baru</span>
            </div>
        </div>
    </div>
    <!-- Box Edit -->
    <div class="col-md-3 col-sm-6 col-xs-12">
        <div class="info-box">
            <a href="<?= MAIN_URL ?>/pages/data_edit_booking.php"><span class="info-box-icon bg-olive"><i class="fa fa-edit"></i></span></a>
            
            <div class="info-box-content">
                <span class="info-box-text"><strong>Edit</strong></span>
                <span class="info-box-more">Edit Booking</span>
            </div>
        </div>
    </div>
    <!-- Box Pembayaran -->
    <div class="col-md-3 col-sm-6 col-xs-12">
        <div class="info-box">
            <a href="<?= MAIN_URL ?>/pages/form_pembayaran.php"><span class="info-box-icon bg-navy"><i class="fa fa-download"></i></span></a>
            
            <div class="info-box-content">
                <span class="info-box-text"><strong>Pembayaran</strong></span>
                <span class="info-box-more">Pembayaran Booking</span>
            </div>
        </div>
    </div>
    <!-- Box Cancel -->
    <div class="col-md-3 col-sm-6 col-xs-12">
        <div class="info-box">
            <a href="<?= MAIN_URL ?>/pages/data_cancel_booking.php"><span class="info-box-icon bg-red"><i class="fa fa-ban"></i></span></a>
            
            <div class="info-box-content">
                <span class="info-box-text"><strong>Batal</strong></span>
                <span class="info-box-more">Pembatalan Booking</span>
            </div>
        </div>
    </div>
</div>
<!-- End Box -->

<!-- Search with Booking Date, Event Date, and Event Name -->
<div class="row">
    <div class="col-md-12">
        <?php $template->startBox(); ?>
            <strong> Data Booking </strong>
        <?php $template->conBox();?>
            <div class="row">
                <!-- Tanggal Booking -->
                <div class="col-md-3">
                    <div class="form-group">
                        <label>Tanggal Booking : </label>

                        <div class="input-group">
                            <div class="input-group-addon">
                                <i class="fa fa-calendar"></i>
                            </div>
                            <input class="form-control" type="text" readonly="readonly" id="tanggal_booking">
                        </div>
                    </div>
                </div>
                <!-- Tanggal Event -->
                <div class="col-md-3">
                    <div class="form-group">
                        <label>Tanggal Event : </label>

                        <div class="input-group">
                            <div class="input-group-addon">
                                <i class="fa fa-calendar"></i>
                            </div>
                            <input class="form-control" type="text" readonly="readonly" id="tanggal_event">
                        </div>
                    </div>
                </div>
                <!-- Nama Event -->
                <div class="col-md-6">
                    <div class="form-group">
                        <label>Nama Event : </label>

                        <div class="input-group">
                            <div class="input-group-addon">
                                <i class="fa fa-building"></i>
                            </div>
                            <input class="form-control" type="text" id="nama_event">
                        </div>
                    </div>
                </div>
            </div>
            <!-- Table -->
            <div class="col-md-12">
                    <table class="table table-responsive table-striped table-hover">
                        <thead align="center" style="background-color:#00a65a; font-weight:bold;">
                            <tr style="color:white;">
                                <td>No.</td>
                                <td>Tanggal</td>
                                <td>Nama Event</td>
                                <td>Ruangan</td>
                                <td>Waktu</td>
                                <td>Organizer</td>
                                <td>Pemesan</td>
                                <td>Status</td>
                            </tr>
                        </thead>
                        <tbody id="tbody">
                        </tbody>
                    </table>
            </div>
        <?php $template->endConBox();?>
        <?php $template->endBox();?>
    </div>
</div>

<!-- End Content -->
<?php $template->endContent(); ?>

<!-- Place Script Here -->

<!-- Zebra Date Picker -->
<script>
    $(document).ready(function(){
        $("input.datepicker").Zebra_DatePicker();
    });
</script>

<!-- Get Data from Tanggal Booking -->
<script type="text/javascript">
  $("#tanggal_booking").Zebra_DatePicker({
    format :'Y-m-d',
    onSelect: function() { 
        $(this).change();
        var tanggal_booking = $("#tanggal_booking").val();
        $("#tbody").html("<tr><td colspan='8'><center><img src='<?=MAIN_URL?>/components/images/load.gif'></center></td></tr>") 
        $.ajax({
        url: "<?=MAIN_URL?>/action/act_get_data_tanggal_booking.php",
        data: "tanggal_booking="+tanggal_booking,
        cache: false,
        success: function(msg){
          $("#tbody").html(msg);
            }
        });
      }
  });
</script>

<!-- Get Data from Tanggal Event -->
<script type="text/javascript">
  $("#tanggal_event").Zebra_DatePicker({
    format :'Y-m-d',
    onSelect: function() { 
        $(this).change();
        var tanggal_event = $("#tanggal_event").val();
        $("#tbody").html("<center><tr><td colspan='8'><img src='<?=MAIN_URL?>/components/images/load.gif'></td></tr></center>") 
        $.ajax({
        url: "<?=MAIN_URL?>/action/act_get_data_tanggal_event.php",
        data: "tanggal_event="+tanggal_event,
        cache: false,
        success: function(msg){
          $("#tbody").html(msg);
            }
        });
      }
  });
</script>

<!-- Get Data from Nama Event -->
<script type="text/javascript">
  $(document).ready(function(){
    $("#nama_event").keyup(function(){
      var nama_event = $("#nama_event").val();
      $("#tbody").html("<center><tr><td colspan='8'><img src='<?=MAIN_URL?>/components/images/load.gif'></td></tr></center>")
        $.ajax({
          url: "<?=MAIN_URL?>/action/act_get_data_nama_event.php",
          data: "nama_event="+nama_event,
          cache: false,
          success: function(msg){
            $("#tbody").html(msg);
          }
        });
    });
  });
</script>

<!-- Logout -->
<script>
  $(document).ready(function(){
    $(".logout").click(function(){
      $.ajax({
          url: "logout.php"
      })
      .done(function(response){
        bootbox.alert(response,function(){
          if(response){
            window.location.href= "index.php";
          }else{
            window.location.href= "beranda.php";
          }
        });
      });
    });
  });
</script>

<!-- //End Script Place -->

<!-- End </body> -->
<?php $template->endBody(); ?>

<!-- End HTML -->
<?php $template->endHtml(); ?>