<?php
    error_reporting(0);
/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
    //Required File
        require_once dirname(__FILE__)."/../class/manual_connect.php";
        require_once dirname(__FILE__)."/../components/templates/main.php";

        //Define Connection -> Database

        //Data from previous page
        #Informasi Pemesanan
            $tanggal_pemesanan = mysqli_real_escape_string($con,$_POST["tanggal_pemesanan"]);
            $nama_event = mysqli_real_escape_string($con,$_POST["nama_event"]);
            $tanggal_event = mysqli_real_escape_string($con,$_POST["tanggal_event"]);
            $jam_mulai = mysqli_real_escape_string($con,$_POST["jam_mulai"]);
            $jam_selesai = mysqli_real_escape_string($con,$_POST["jam_selesai"]);
            $ruangan = mysqli_real_escape_string($con,$_POST["ruangan"]);
            $jumlah_undangan = mysqli_real_escape_string($con,$_POST["jumlah_undangan"]);
            $jenis_setup = mysqli_real_escape_string($con,$_POST["jenis_setup"]);
            $agent = mysqli_real_escape_string($con,$_POST["agent"]);
            $event_organizer = mysqli_real_escape_string($con,$_POST["event_organizer"]);
            $harga_sewa_r = mysqli_real_escape_string($con,$_POST["harga_sewa_r"]);
        
        #Info Pemesan
            $nama_pemesan = mysqli_real_escape_string($con,$_POST["nama_pemesan"]);
            $alamat_pemesan = mysqli_real_escape_string($con,$_POST["alamat_pemesan"]);
            $telepon_pemesan = mysqli_real_escape_string($con,$_POST["telepon_pemesan"]);
            $email_pemesan = mysqli_real_escape_string($con,$_POST["email_pemesan"]);
        
        #Payment    
            $pay_1 = mysqli_real_escape_string($con,$_POST["pay_1"]);
            $alokasi_1 = mysqli_real_escape_string($con,$_POST["alokasi_1"]);
            $pay_2 = mysqli_real_escape_string($con,$_POST["pay_2"]);
            $alokasi_2 = mysqli_real_escape_string($con,$_POST["alokasi_2"]);
            $pay_3 = mysqli_real_escape_string($con,$_POST["pay_3"]);
            $alokasi_3 = mysqli_real_escape_string($con,$_POST["alokasi_3"]);
        
        #Total"
            $t_sewa_ruangan = mysqli_real_escape_string($con,$_POST["t_sewa_ruangan"]);
            $diskon = mysqli_real_escape_string($con,$_POST["diskon"]);
            $total_harga = mysqli_real_escape_string($con,$_POST["total_harga"]);
            $jaminan_kerusakan = mysqli_real_escape_string($con,$_POST["jaminan_kerusakan"]);
            $catatan = mysqli_real_escape_string($con,$_POST["catatan"]);
        
        //Create Nomer Bukti Pemesanan
           $today = date("ymd");
            $query = mysqli_query($con,"SELECT MAX(id_booking),NO_BUKTI_PEMESANAN AS maxID FROM tb_booking WHERE NO_BUKTI_PEMESANAN LIKE('%".$today."%') GROUP BY id_booking ORDER BY ID_BOOKING DESC") or die(mysqli_error($con));
            $hasil = mysqli_fetch_array($query);
            $idMax = $hasil['maxID'];
            $noUrut = (int) substr($idMax,9, 4);
            $maxid=explode("-", $hasil['maxID']);
            $noUrut=$maxid[2];
            $noUrut++;
                #No Bukti Pemesanan
                $NewID = "B"."-".$today."-".sprintf('%04s',$noUrut);
        
        
        //Process
            
            $query1 = mysqli_query($con,"INSERT INTO tb_booking
					(TGL_BOOKING,
					NO_BUKTI_PEMESANAN,
					NAMA_EVENT,
					TGL_EVENT,
					JAM_MULAI,
					JAM_SELESAI,
					ID_RUANGAN,
					JML_UNDANGAN,
					ID_JENIS_SETUP,
					ID_EO,
					ID_AGENT,
					NAMA_PEMESAN,
					ALAMAT_PEMESAN,
					TELEPON_PEMESAN,
					EMAIL_PEMESAN,
					REMARK_BOOKING,
					HARGA_ROOM,
					DISKON_ROOM,
					JAMINAN_KERUSAKAN,
					LAST_UPDATE)
					values
					('$tanggal_pemesanan',
					'$NewID',
					'$nama_event',
					'$tanggal_event',
					'$jam_mulai',
					'$jam_selesai',
					'$ruangan',
					'$jumlah_undangan',
					'$jenis_setup',
					'$event_organizer',
					'$agent',
					'$nama_pemesan',
					'$alamat_pemesan',
					'$telepon_pemesan',
					'$email_pemesan',
					'$catatan',
					'$t_sewa_ruangan',
					'$diskon',
					'$jaminan_kerusakan',
					NOW() )") or die(mysqli_error($con));
              
            #last Increment
            $last_id = mysqli_insert_id($con);
        
            //Included Item
               $id_book = $last_id;
                $jml = count($_POST["item_included"]);
                for($i=0;$i<$jml;$i++){
                    $id_item = $_POST["item_included"][$i];
                    $jumlah_in = $_POST["jumlah_in"][$i];

                    $dt = [$id_book,$id_item,$jumlah_in,"YES"];
                    $value = implode("','",$dt);
                    $query = "INSERT INTO TB_ITEM_ADD (id_booking,id_item,jml_item,is_included) values ('".$value."')";
                    $execute = mysqli_query($con, $query) or die(mysqli_error($con));
                }
            //Excluded Item
                $id_book_ex = $last_id;
                $jm = count($_POST["item_excluded"]);
                for($e=0;$e<$jm;$e++){
                    $id_item_ex = $_POST["item_excluded"][$e];
                    $jumlah_ex = $_POST["jumlah_ex"][$e];
                    $harga_ex = $_POST["harga_ex"][$e];
                    
                    $data_ex = [$id_book_ex,$id_item_ex,$jumlah_ex,$harga_ex,"NO"];
                    $valu_ex = implode("','",$data_ex);
                    $kueri = "INSERT INTO TB_ITEM_ADD (id_booking,id_item,jml_item,price_item,is_included) values ('".$valu_ex."')";
                    $exe = mysqli_query($con, $kueri) or die(mysqli_error($con));
                }
                
            //Payment Schedule
                $query2 = mysqli_query($con,"INSERT INTO tb_payment_schedule
                                            (ID_BOOKING,
                                            NO_BUKTI,
                                            TGL_PAYMENT_SCHEDULE,
                                            ALOKASI_PAYMENT,
                                            LAST_UPDATE)
                                            values
                                            ('$last_id',
                                             '$NewID',
                                             '$pay_1',
                                             '$alokasi_1',
                                              NOW() ),
                                            ('$last_id',
                                             '$NewID',
                                             '$pay_2',
                                             '$alokasi_2',
                                              NOW() ),
                                            ('$last_id',
                                             '$NewID',
                                             '$pay_3',
                                             '$alokasi_3',
                                              NOW() )") or die(mysqli_error($con));
            #Get Room Name
                $nama_ruangan = mysqli_query($con, "SELECT NAMA_RUANGAN from tb_ruangan where id_ruangan = '$ruangan' ");
                $results_ruangan = mysqli_fetch_array($nama_ruangan);
            #get Today
                $tanggal_hari_ini = date('Y-m-d');
                
            //Journal
                $ins_jurnal = mysqli_query($con,"INSERT INTO tb_jurnal
						(
						NO_BUKTI,
						TGL_TRANSAKSI,
						AYAT_JURNAL,
						JENIS_JURNAL,
						REF_JURNAL)
                                                values
                                                (
                                                '$NewID',
						NOW(),
						'Sewa Ruangan $results_ruangan[0] untuk Event $nama_event pada $tanggal_hari_ini ',
						'PENDAPATAN SEWA',
						'$last_id'
                                                ) ") or die(mysqli_error($con));
                
                //$ins_detail = mysqli_query($con,"");
             
            if($query1&&$execute&&$exe&&$query2&&$ins_jurnal){
?>

<!-- Popup -->
<script>
    alert("Data Berhasil Disimpan");location.href="<?=MAIN_URL?>/beranda.php";
</script>
<?php
            }
?>