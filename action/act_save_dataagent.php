<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

    //Required File
        require_once dirname(__FILE__)."/../class/config.php";
        
    //Define
        $db = new Database();
        $db->connect();

    //Data from Previous Page
        $nama_agent = $db->escapeString($_POST["nama_agent"]);
        $alamat_agent = $db->escapeString($_POST["alamat_agent"]);
        $telepon_agent = $db->escapeString($_POST["telepon_agent"]);
        $hp_agent = $db->escapeString($_POST["hp_agent"]);
        $cp_agent = $db->escapeString($_POST["cp_agent"]);
        $email_agent = $db->escapeString($_POST["email_agent"]);
        $bank = $db->escapeString($_POST["bank"]);
        $no_rekening = $db->escapeString($_POST["no_rekening"]);
        $catatan = $db->escapeString($_POST["catatan"]);
        
    //Save to Database
        $db->insert("tb_agent",array("nama_agent"=>$nama_agent,
                                     "alamat_agent"=>$alamat_agent,
                                     "telepon_agent"=>$telepon_agent,
                                     "hp_agent"=>$hp_agent,
                                     "cp_agent"=>$cp_agent,
                                     "email_agent"=>$email_agent,
                                     "bank_agent"=>$bank,
                                     "no_rekening_agent"=>$no_rekening,
                                     "remark_Agent"=>$catatan));
        $result = $db->getResult();
        
    if($result){
        echo "<script>alert('Penambahan Data Vendor Berhasil');location.href='".MAIN_URL."/pages/form_tambah_agent.php'</script>";
    }else{
        echo "<script>alert('Penambahan Data Vendor Gagal');location.href='".MAIN_URL."/pages/form_tambah_agent.php'</script>";
    }
        
?>