<?php
  error_reporting(E_ALL | E_STRICT);
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

        //Required File
            require_once dirname(__FILE__)."/../class/config.php";

        //Define Connection -> Database
            $db = new Database();
            $db->connect();

        //Data from previous page
          $id_profil = $db->escapeString($_REQUEST["id_profil"]);
          $nama_perusahaan = $db->escapeString($_REQUEST["nama_perusahaan"]);
          $alamat_perusahaan = $db->escapeString($_REQUEST["alamat_perusahaan"]);
          $telepon_perusahaan = $db->escapeString($_REQUEST["telepon_perusahaan"]);
          $fax_perusahaan = $db->escapeString($_REQUEST["fax_perusahaan"]);
          $email_perusahaan = $db->escapeString($_REQUEST["email_perusahaan"]);
          $website_perusahaan = $db->escapeString($_REQUEST["website_perusahaan"]);
          $npwp_perusahaan = $db->escapeString($_REQUEST["npwp_perusahaan"]);
          $pkp_perusahaan = $db->escapeString($_REQUEST["pkp_perusahaan"]);

          $image = $_FILES['logo']['name'];
        	$size = $_FILES['logo']['size'];
        	$tmp = $_FILES['logo']['tmp_name'];

          $path = "../gambar/$image";

	        $type_file = pathinfo($path,PATHINFO_EXTENSION);

          if(!empty($tmp)){
		          move_uploaded_file($tmp, $path);

              $db->update("tb_profil",array("nama"=>$nama_perusahaan,"alamat"=>$alamat_perusahaan,"telepon"=>$telepon_perusahaan,"FAX"=>$fax_perusahaan,"email"=>$email_perusahaan,"website"=>$website_perusahaan,"npwp"=>$npwp_perusahaan,"pkp"=>$pkp_perusahaan,"logo"=>$image),"id_profil = '$id_profil' ");
              $result = $db->getResult();
              if(!$result){?>
          			<script>alert('Update Data Gagal');location.href='<?= MAIN_URL ?>/pages/form_info.php'</script>
             <?php
              }else{ ?>
          			<script>alert('Update Data Berhasil');location.href='<?= MAIN_URL ?>/pages/form_info.php'</script>
          <?php	}
          }else if(empty($image)&&empty($size)&&empty($tmp)){

            $db->update("tb_profil",array("nama"=>$nama_perusahaan,"alamat"=>$alamat_perusahaan,"telepon"=>$telepon_perusahaan,"FAX"=>$fax_perusahaan,"email"=>$email_perusahaan,"website"=>$website_perusahaan,"npwp"=>$npwp_perusahaan,"pkp"=>$pkp_perusahaan),"id_profil = '$id_profil' ");
            $result = $db->getResult();
            if(!$result){ ?>
              <script>alert('Update Data Gagal');location.href='<?= MAIN_URL ?>/pages/form_info.php'</script>
            <?php
            }else{ ?>
              <script>alert('Update Data Berhasil');location.href='<?= MAIN_URL ?>/pages/form_info.php'</script>
          <?php }
          }else{ ?>
            <script>alert('Terjadi Kesalahan');location.href='<?= MAIN_URL ?>/pages/form_info.php'</script>
        <?php }
?>
