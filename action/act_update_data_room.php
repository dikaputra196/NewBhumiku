
<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

    //Required File
        require_once dirname(__FILE__)."/../class/config.php";
        require_once dirname(__FILE__)."/../components/templates/main.php";
    
    //Call Template
        $template = new Template();
        
    //Define
        $db = new Database();
        $db->connect();

    //Data from Previous Page
        $id_ruangan = $db->escapeString($_REQUEST["id_room"]);
        $nama_ruangan = $db->escapeString($_REQUEST["nama_room"]);
        $kapasitas_ruangan = $db->escapeString($_REQUEST["kapasitas_room"]);    
        $panjang = $db->escapeString($_REQUEST["panjang"]);
        $lebar = $db->escapeString($_REQUEST["lebar"]);
        $full_day = $db->escapeString($_REQUEST["full_day"]);
        $half_day = $db->escapeString($_REQUEST["half_day"]);
        $hourly = $db->escapeString($_REQUEST["hourly"]);
    
    //Action
      $db->update("tb_ruangan",array("nama_ruangan"=>$nama_ruangan,"kapasitas_ruangan"=>$kapasitas_ruangan,"panjang_ruangan"=>$panjang,"lebar_ruangan"=>$lebar,"harga_sewa"=>$full_day,"harga_half_day"=>$half_day,"harga_hourly"=>$hourly),"id_ruangan='$id_ruangan'");
      $result = $db->getResult();

      if($result){ ?>
           <script>alert('Update Data Berhasil');location.href='<?= MAIN_URL ?>/pages/data_room.php'</script>
      <?php
      }else{
          echo "<script>alert('Update Data Berhasil');location.href='".MAIN_URL."/pages/data_room.php'</script>";
      }
      
?>