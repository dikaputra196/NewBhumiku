<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
    //Required
    require_once dirname(__FILE__)."/../class/config.php";
    require_once dirname(__FILE__)."/../class/manual_connect.php";
    require_once dirname(__FILE__)."/../class/native_connect.php";
    
    //Define Database
    $db = new Database();
    $db->connect();
    
    //Variabel from Previous Page
    $count_item_ex = $db->escapeString($_REQUEST["count_item_ex"]); 
    $total_ex=0;
    
    //Proccess
    for($no=1; $no<=$count_item_ex; $no++ ){
?>

<!-- Result -->
<div class="form-group">
    <div class="col-sm-3">
        <small><i>Nama Item</i></small>
        <select class="form-control select2" onchange="asd(this)" name="item_excluded[]" id="item_excluded<?= $no ?>" data-target="item_ex<?= $no ?>">
            <option value=""> ---</option>
            <?php
                //Query
                $db->select("tb_item","id_item,nama_item",NULL,"is_active='1' ");
                $result = $db->getResult();
                foreach($result as $show){
            ?>
                <option value="<?= $show['id_item'] ?>"> <?= $show['nama_item'] ?> </option>
            <?php
                }
            ?>
        </select>
    </div>
    
    <div class="col-sm-3">
        <small><i>Harga Item</i></small>
        <div class="input-group">
            <span class="input-group-addon">Rp.</span>
            <input type="text" class="form-control" name="harga_ex[]" id="item_ex<?= $no ?>" data-target="h_item_ex<?= $no ?>" readonly>
        </div>
    </div>
    
    <div class="col-sm-2">
        <small><i>Jumlah Item</i></small>
        <input type="number" class="form-control" name="jumlah_ex[]" id="jumlah_ex<?= $no ?>" data-target="j_item_ex<?= $no ?>">
    </div>
    
</div>

<?php   
    }
?>

<!-- Select2 -->
    <script>
        $(document).ready(function(){
            $(".select2").select2();
        });
    </script>
    
<!-- -->
    <script>
        function asd(change){
            var val = $(change).val();
            $.ajax({
                    url:'<?= MAIN_URL ?>/action/get_harga_item.php',
                    type:'POST',
                    async:true,
                    data:'id_barang='+val,
                    beforeSend : function(){
            $("#"+$(change).attr('data-target')).val('Loading...');
                    },
                    success : function(data){

            $("#"+$(change).attr('data-target')).val(data);
                    }
                });			
        }
    </script>