<?php
    error_reporting(0);
/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

    //Required File
        require_once dirname(__FILE__)."/../class/manual_connect.php";
        require_once dirname(__FILE__)."/../class/native_connect.php";
        
        //Data from Previous page
            $tanggal_transaksi = mysqli_real_escape_string($con, $_POST["tanggal_transaksi"]);
            $jenis_refund = mysqli_real_escape_string($con, $_POST["jenis_refund"]);
            $nama_event = mysqli_real_escape_string($con, $_POST["nama_event"]);
            $keterangan = mysqli_real_escape_string($con, $_POST["keterangan"]);
            $cara_pembayaran = mysqli_real_escape_string($con, $_POST["cara_pembayaran"]);
            $akun_kas= mysqli_real_escape_string($con, $_POST["akun_kas"]);
            $jumlah = mysqli_real_escape_string($con, $_POST["jumlah"]);
                    
        //Find No Bukti Terakhir from tb_booking
            date_default_timezone_set("Australia/Perth");
            $today = date('ymd');
            $query = mysqli_query($con,"SELECT MAX(id_refund),NO_BUKTI AS maxID FROM tb_refund WHERE NO_BUKTI LIKE('%".$today."%') GROUP BY id_refund ORDER BY id_refund DESC") or die(mysqli_error($con));
            $hasil = mysqli_fetch_array($query);
            $idMax = $hasil['maxID'];
            $noUrut = (int) substr($idMax,9, 4);
            $maxid=explode("-", $hasil['maxID']);
            $noUrut=$maxid[2];
            $noUrut++;
        
	//No Bukti Pemesanan
            $NewID = "RF"."-".$today."-".sprintf('%04s',$noUrut);

	//Find Nomor Harian Terakhir
            $qr = mysqli_query($con,"SELECT MAX(id_refund), MAX(no_harian) AS maxno FROM tb_refund where tgl_transaksi='$tanggal_transaksi' ");
            $result = mysqli_fetch_array($qr);
            $maxno = $result['maxno'];
            $maxno++;    
            
        //Save -> Database
            $query = "INSERT INTO tb_refund
                      (NO_HARIAN,
                       NO_BUKTI,
                       TGL_TRANSAKSI,
                       ID_BOOKING,
                       JML_TRANSAKSI,
                       WAKTU_POSTING)
                      values
                      ('$maxno',
                      '$NewID',
                      '$tanggal_transaksi',
                      '$nama_event',
                      '$jumlah',
                      NOW())";        
            $execute = mysqli_query($con, $query) or die(mysqli_error($con));
            
            $qry = "INSERT INTO tb_jurnal
                    (NO_BUKTI,
                    TGL_TRANSAKSI,
                    AYAT_JURNAL,
                    JENIS_JURNAL,
                    REF_JURNAL,
                    LAST_UPDATE)
                    values
                    ('$NewID',
                    '$tanggal_transaksi',
                    '$keterangan',
                    'REFUND',
                    '$nama_event',
                    NOW())";
            $exec = mysqli_query($con, $qry) or die(mysqli_error($con));
            $id_jurnal = mysqli_insert_id($con);
            
            $qr = "INSERT INTO tb_detail_jurnal
                   (ID_JURNAL,
                   KODE_COA,
                   POSISI,
                   JML_TRANSAKSI)
                   values
                   ('$id_jurnal',
                   '$jenis_refund',
                   'D',
                   '$jumlah'),
                   ('$id_jurnal',
                   '$akun_kas',
                   'K',
                   '$jumlah')";
            $ex = mysqli_query($con, $qr) or die(mysqli_error($con));
        
            if($query&&$qry&&$qr){
		echo "<script>alert('Refund Sukses');location.href='".MAIN_URL."/pages/form_refund_dana.php';</script>";
            }else{
                    echo "<script>alert('Refund Gagal');location.href='".MAIN_URL."/pages/form_refund_dana.php';</script>";
            }
            
            
            echo $today;
?>