<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

    //Required File
            require_once dirname(__FILE__)."/../class/manual_connect.php";
            require_once dirname(__FILE__)."/../class/native_connect.php";
            
    //Data from Previous Page
        $tanggal_transaksi = mysqli_real_escape_string($con, $_POST["tanggal_transaksi"]);
        $no_bukti = mysqli_real_escape_string($con, $_POST["no_bukti"]);
        $keterangan = mysqli_real_escape_string($con, $_POST["keterangan"]);
        $dari_akun = mysqli_real_escape_string($con, $_POST["dari_akun"]);
        $ke_akun = mysqli_real_escape_string($con, $_POST["ke_akun"]);
        $nominal_transaksi = mysqli_real_escape_string($con, $_POST["nominal_transaksi"]);
        
    //Proccess
        $query = "INSERT INTO tb_jurnal
                  (NO_BUKTI,
                  TGL_TRANSAKSI,
                  AYAT_JURNAL,
                  JENIS_JURNAL,
                  REF_JURNAL,
                  LAST_UPDATE)
                  values
                  ('$no_bukti',
                   '$tanggal_transaksi',
                   '$keterangan',
                   'MUTASI KAS',
                   '-',
                   NOW())";
        $execute = mysqli_query($con, $query) or die(mysqli_error($con));
        $last_id = mysqli_insert_id($con);
        
        $qry = "INSERT INTO tb_detail_jurnal
                (ID_JURNAL,
                KODE_COA,
                POSISI,
                JML_TRANSAKSI)
                values
                ('$last_id',
                '$ke_akun',
                'D',
                '$nominal_transaksi'),
                ('$last_id',
                 '$dari_akun',
                 'K',
                 '$nominal_transaksi')";
        $exec = mysqli_query($con, $qry) or die(mysqli_error($con));
        
        if($execute&&$exec){
            echo "<script>alert('Mutasi Berhasil Tersimpan');location.href='".MAIN_URL."/pages/form_mutasi_kas.php';</script>";
        }else{
            echo "<script>alert('Mutasi Gagal Tersimpan');location.href='".MAIN_URL."/pages/form_mutasi_kas.php';</script>";
        }

?>