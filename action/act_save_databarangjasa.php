<?php
    error_reporting(0);
/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

    //Required File
        require_once dirname(__FILE__)."/../class/config.php";
        
    //Define
        $db = new Database();
        $db->connect();
        
    //Data from Previous Page
        $kode_barang = $db->escapeString($_POST["kode_barang"]);
        $nama_barang = $db->escapeString($_POST["nama_barang"]);
        $satuan_besar = $db->escapeString($_POST["satuan_besar"]);
        $satuan_kecil = $db->escapeString($_POST["satuan_kecil"]);
        $konversi_satuan = $db->escapeString($_POST["konversi_satuan"]);
        $jenis = $db->escapeString($_POST["jenis"]);
        $harga_rata2 = $db->escapeString($_POST["harga_rata2"]);
        $on_stock = $db->escapeString($_POST["on_stock"]);
        $on_purchased = $db->escapeString($_POST["on_purchased"]);
        
    if(empty($_POST["inventory"])){
        $db->insert("tb_barang",
                    array("kode_barang"=>$kode_barang,
                          "nama_barang"=>$nama_barang,
                          "jenis_barang"=>$jenis,
                          "id_satuan_besar"=>$satuan_besar,
                          "id_satuan_kecil"=>$satuan_kecil,
                          "konversi_satuan"=>$konversi_satuan,
                          "is_inventory"=>'0',
                          "avg_price"=>$harga_rata2,
                          "on_stock"=>$on_stock,
                          "on_purchased"=>$on_purchased
                        ));
        $results = $db->getResult();
    }else{
        $db->insert("tb_barang",
                    array("kode_barang"=>$kode_barang,
                          "nama_barang"=>$nama_barang,
                          "jenis_barang"=>$jenis,
                          "id_satuan_besar"=>$satuan_besar,
                          "id_satuan_kecil"=>$satuan_kecil,
                          "konversi_satuan"=>$konversi_satuan,
                          "is_inventory"=>'1',
                          "avg_price"=>$harga_rata2,
                          "on_stock"=>$on_stock,
                          "on_purchased"=>$on_purchased
                        ));
        $result = $db->getResult();
    }
    if(($results) || ($result)){
        echo "<script>alert('Penambahan Data Barang / Jasa Berhasil');location.href='".MAIN_URL."/pages/form_tambah_barangjasa.php'</script>";
    }else{
        echo "<script>alert('Penambahan Data Barang / Jasa Gagal');location.href='".MAIN_URL."/pages/form_tambah_barangjasa.php'</script>";
    }
?>