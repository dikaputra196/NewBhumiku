<?php
    require_once dirname(__FILE__)."/../class/config.php";
    require_once dirname(__FILE__)."/../class/manual_connect.php";
    require_once dirname(__FILE__)."/../class/native_connect.php";
    
/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
     //Get Data from Previous Page
    $nama_event = mysqli_real_escape_string($con,$_REQUEST["nama_event"]);

    //Variable
    $no=0;
    
    //Konversi Tanggal Bulan Tahun
    function bulananindonesia($bulanan)
      {
        $bulan_angka = array('01','02','03','04','05','06','07','08','09','10','11','12');
        $nama_bulan = array('Januari','Februari','Maret','April','Mei','Juni','Juli','Agustus','September','Oktober','November','Desember');
        $convert = str_ireplace($bulan_angka, $nama_bulan, $bulanan);
        return $convert;
      }
    
    //Condition
    if(!empty($nama_event))
	{
                //Query
		$query = mysqli_query($con,"SELECT
                                            a.id_booking,
                                            a.tgl_event,
                                            a.nama_event,
                                            b.nama_ruangan,
                                            CONCAT(jam_mulai,' - ', jam_selesai) AS Waktu,
                                            c.nama_eo,
                                            a.nama_pemesan,
                                            a.status_booking
                                            FROM
                                            tb_booking a
                                            INNER JOIN tb_eo c
                                            ON c.id_eo = a.id_eo
                                            INNER JOIN tb_ruangan b
                                            ON b.id_ruangan = a.id_ruangan
                                            WHERE a.nama_event LIKE '%$nama_event%' AND is_final='0' AND a.status_booking='TENTATIVE'") or die (mysqli_error($con));
                while($result=mysqli_fetch_array($query))
                     {
                        $no++;
                ?>

                        <tr>
                            <td><?= $no ?></td>
                            <td><?= $result['tgl_event']; ?></td>
                            <td><?= $result['nama_event']; ?></td>
                            <td><?= $result['nama_ruangan']; ?></td>
                            <td><?= $result['Waktu']; ?></td>
                            <td><?= $result['nama_eo']; ?></td>
                            <td><?= $result['nama_pemesan']; ?></td>
                            <td><a class="delete-item" href="javascript:void(0)" data-id="<?= $result["id_booking"]; ?>"><button class="btn btn-sm btn-danger"><span class="glyphicon glyphicon-remove"></span> Delete</button></a></td>
                        </tr>

                <?php
                     }
                
                    if(mysqli_num_rows($query)===0){
                
                ?>
        <tr>
            <td colspan="8">
                <div class="alert alert-danger alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    <h4><i class="icon fa fa-ban"></i> Error!</h4>
                    <strong><center>Tidak ada Hasil untuk Pencarian <?= $nama_event; ?></center></strong>
                </div>  
            </td>
        </tr>
                 
        <?php
                }
                     
        } else {}
        ?>

        <!-- Dialog -->
        <script>
        $(document).ready(function(){
          $(".delete-item").click(function(e){
              e.preventDefault();
              var id_booking = $(this).attr('data-id');
              var parent = $(this).parent("td").parent("tr");
              bootbox.dialog({
                message: "Apakah anda yakin akan menghapus Data ini?",
                title: "<i class='glyphicon glyphicon-trash'></i> Hapus? ",
                buttons: {
                success: {
                label: "<i class='fa fa-times'></i> Tidak",
                className: "btn-success",
                callback: function(){
                  $(".bootbox").modal("hide");
                }
              },
              danger: {
                label: "<i class='fa fa-check'></i> Hapus!",
                className: "btn-danger",
                callback: function(){
                  $.ajax({
                    type: "POST",
                    url: "<?= MAIN_URL ?>/action/act_cancel_booking.php",
                    data: "id_booking="+id_booking
                  })
                  .done(function(response){
                    bootbox.alert(response);
                    parent.fadeOut('slow');
                  })
                  .fail(function(){
                    bootbox.alert('Error.....');
                  });
                }
              }
              }
              });
          });
        });
      </script>