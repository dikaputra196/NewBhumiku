<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

//Required File
        require_once dirname(__FILE__)."/../class/manual_connect.php";
        require_once dirname(__FILE__)."/../class/native_connect.php";
        
        //Data from Previous Page
        
        $tanggal_transaksi = mysqli_real_escape_string($con,$_POST["tanggal_transaksi"]);
        
        $catatan = mysqli_real_escape_string($con, $_POST["catatan"]);
        
        $nama_event = mysqli_real_escape_string($con, $_POST["nama_event"]);
        
        $nominal_transaksi = mysqli_real_escape_string($con, $_POST["nominal_transaksi"]);
        
        $akun_sumber = mysqli_real_escape_string($con, $_POST["akun_sumber"]);
        
        $penerima_dana = mysqli_real_escape_string($con, $_POST["penerima_dana"]);
        
        //Proccess [ No. Bukti Transaksi ]
            $today = date('ymd');
            $query = mysqli_query($con, "SELECT MAX(id_petty_cash),NO_BUKTI AS maxID FROM tb_petty_cash WHERE NO_BUKTI LIKE('%".$today."%') GROUP BY id_petty_cash ORDER BY ID_petty_cash DESC") or die(mysqli_error($con));
            $hasil = mysqli_fetch_array($query);
            $idMAX = $hasil['maxID'];
            $nourut = (int) substr($idMAX,10,4);
            $maxid = explode("-",$hasil['maxID']);
            $nourut = $maxid[2];
            $nourut++;
        
        //No Bukti Transaksi
            $NewID = "PC"."-".$today."-".sprintf('%04s',$nourut);
            
        //[proses]No Harian
            $query2 = mysqli_query($con,"SELECT MAX(id_petty_cash), NO_HARIAN AS maxhari FROM tb_petty_cash GROUP BY id_petty_cash ORDER BY id_petty_cash DESC");
            $result = mysqli_fetch_array($query2);
            $max = $result['maxhari'];
            $urut = $max;
            $urut++;

	//No harian
            $newno = $urut;
            
       
        //Save -> Database
            // -> tb petty cash
            $kueri = "INSERT INTO tb_petty_cash
                      (NO_HARIAN,
                      NO_BUKTI,
                      TGL_TRANSAKSI,
                      ID_BOOKING,
                      JML_TRANSAKSI,
                      WAKTU_POSTING)
                      values
                      ('$newno',
                       '$NewID',
                       '$tanggal_transaksi',
                       '$nama_event',
                       '$nominal_transaksi',
                       NOW())";
            $execute = mysqli_query($con, $kueri) or die(mysqli_error($con));
            $last_increment = mysqli_insert_id($con);
            
            // -> jurnal
            $kueri_j = "INSERT INTO tb_jurnal
                        (NO_BUKTI,
                        TGL_TRANSAKSI,
                        AYAT_JURNAL,
                        JENIS_JURNAL,
                        REF_JURNAL,
                        LAST_UPDATE)
                        values
                        ('$NewID',
                         '$tanggal_transaksi',
                         '$catatan',
                         'PETTY CASH',
                         '$last_increment',
                         NOW()) ";
            $exec = mysqli_query($con, $kueri_j) or die(mysqli_error($con));
            $last_id_j = mysqli_insert_id($con);
            
            // -> detail jurnal
            $kueri_d = "INSERT INTO tb_detail_jurnal
                        (ID_JURNAL,
                        KODE_COA,
                        POSISI,
                        JML_TRANSAKSI)
                        values
                        ('$last_id_j',
                         '110002',
                         'D',
                         '$nominal_transaksi'),
                        ('$last_id_j',
                         '$akun_sumber',
                         'K',
                         '$nominal_transaksi')";
            $ex = mysqli_query($con, $kueri_d) or die(mysqli_error($con));
            
            if($execute&&$exec&&$ex){
                echo "<script>alert('Transaksi Berhasil');location.href='".MAIN_URL."/pages/form_petty_cash.php'</script>";
            }else{
                echo "<script>alert('Transaksi Gagal');location.href='".MAIN_URL."/pages/form_petty_cash.php'</script>";
            }
?>

