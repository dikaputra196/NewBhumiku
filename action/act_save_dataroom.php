<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
    //Required File
    require_once dirname(__FILE__)."/../class/config.php";
    
    //Define
        $db = new Database();
        $db->connect();
    
    //Data from previous page
        $nama_room = $db->escapeString($_POST["nama_room"]);
        $kapasitas_room = $db->escapeString($_POST["kapasitas_room"]);
        $panjang = $db->escapeString($_POST["panjang"]);
        $lebar = $db->escapeString($_POST["lebar"]);
        $full_day = $db->escapeString($_POST["full_day"]);
        $half_day = $db->escapeString($_POST["half_day"]);
        $hourly = $db->escapeString($_POST["hourly"]);
    
    //Save
        $db->insert("tb_ruangan",array("nama_ruangan"=>$nama_room,"kapasitas_ruangan"=>$kapasitas_room,"panjang_ruangan"=>$panjang,"lebar_ruangan"=>$lebar,"harga_sewa"=>$full_day,"harga_half_day"=>$half_day,"harga_hourly"=>$hourly));
        $results = $db->getResult();
        
    //Alert
        if($results){
            echo "<script>alert('Penambahan Data Room Berhasil');location.href='".MAIN_URL."/pages/form_tambah_room.php';</script>";
        }else{
            echo "<script>alert('Penambahan Data Room Gagal');location.href='".MAIN_URL."/pages/form_tambah_room.php';</script>";
        }
?>

