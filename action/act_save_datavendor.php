<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

    //Required File
        require_once dirname(__FILE__)."/../class/config.php";
        
    //Define
        $db = new Database();
        $db->connect();

    //Data from Previous Page
        $nama_vendor = $db->escapeString($_POST["nama_vendor"]);
        $alamat_vendor = $db->escapeString($_POST["alamat_vendor"]);
        $telepon_vendor = $db->escapeString($_POST["telepon_vendor"]);
        $hp_vendor = $db->escapeString($_POST["HP_vendor"]);
        $contact_person = $db->escapeString($_POST["contact_person"]);
        $email_vendor = $db->escapeString($_POST["email_vendor"]);
        $npwp = $db->escapeString($_POST["npwp"]);
        $bank = $db->escapeString($_POST["bank"]);
        $no_rekening = $db->escapeString($_POST["no_rekening"]);
        $catatan = $db->escapeString($_POST["catatan"]);
  
    //Save -> Database
        $db->insert("tb_vendor",array("nama_vendor"=>$nama_vendor,"alamat_vendor"=>$alamat_vendor,"telepon_vendor"=>$telepon_vendor,"hp_Vendor"=>$hp_vendor,"cp_vendor"=>$contact_person,"email_vendor"=>$email_vendor,"npwp_vendor"=>$npwp,"bank_vendor"=>$bank,"remark_vendor"=>$catatan,"no_rekening_vendor"=>$no_rekening));
        $result = $db->getResult();
    
    if($result){
        echo "<script>alert('Penambahan Data Vendor Berhasil');location.href='".MAIN_URL."/pages/form_tambah_vendor.php';</script>";
    }else{
        echo "<script>alert('Penambahan Data Vendor Gagal');location.href='".MAIN_URL."/pages/form_tambah_vendor.php';</script>";
    }
        
?>