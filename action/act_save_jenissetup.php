<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

    //Required File
        require_once dirname(__FILE__)."/../class/config.php";

    //Define Connection -> Database
        $db = new Database();
        $db->connect();
        
    //Data from Previous Page
        $nama_jenis_setup = $db->escapeString($_POST["nama_jenis_setup"]);
        $deskripsi = $db->escapeString($_POST["deskripsi"]);
        
        $db->insert("tb_jenis_setup",array("nama_jenis_setup"=>$nama_jenis_setup,"deskripsi"=>$deskripsi));
        $result = $db->getResult();
        
        if($result){
            echo "<script>alert('Penambahan Data Jenis Setup Berhasil');location.href='".MAIN_URL."/pages/form_tambah_jenissetup.php'</script>";
        }else{
            echo "<script>alert('Penambahan Data Jenis Setup Gagal');location.href='".MAIN_URL."/pages/form_tambah_jenissetup.php'</script>";
        }
?>