<?php
    require_once dirname(__FILE__)."/../class/config.php";
    require_once dirname(__FILE__)."/../class/manual_connect.php";
    require_once dirname(__FILE__)."/../class/native_connect.php";

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
    //Get Data from Previous Page
    $tanggal_event = mysqli_real_escape_string($con,$_REQUEST["tanggal_event"]);

    //Variable
    $no=0;

    //Konversi Tanggal Bulan Tahun
    function bulananindonesia($bulanan)
      {
        $bulan_angka = array('01','02','03','04','05','06','07','08','09','10','11','12');
        $nama_bulan = array('Januari','Februari','Maret','April','Mei','Juni','Juli','Agustus','September','Oktober','November','Desember');
        $convert = str_ireplace($bulan_angka, $nama_bulan, $bulanan);
        return $convert;
      }

    //Query
    $query=mysqli_query($con," SELECT
                            a.id_booking,
                            a.tgl_event,
                            a.nama_event,
                            b.nama_ruangan,
                            CONCAT(jam_mulai,' - ', jam_selesai) AS Waktu,
                            c.nama_eo,
                            a.nama_pemesan,
                            a.status_booking
                            FROM
                            tb_booking a
                            INNER JOIN tb_eo c
                            ON c.id_eo = a.id_eo
                            INNER JOIN tb_ruangan b
                            ON b.id_ruangan = a.id_ruangan
                            WHERE a.tgl_event='$tanggal_event'");
                            while($result=mysqli_fetch_array($query))
                              {
                                $no++;
                            ?>
                                <tr>
                                    <td><?=$no?></td>
                                    <td><?=$result['tgl_event']?></td>
                                    <td><?=$result['nama_event']?></td>
                                    <td><?=$result['nama_ruangan']?></td>
                                    <td><?=$result['Waktu']?></td>
                                    <td><?=$result['nama_eo']?></td>
                                    <td><?=$result['nama_pemesan']?></td>
                                    <td><a href="<?= MAIN_URL ?>/pages/form_edit_booking.php?id_booking=<?=$result['id_booking']?>"><button class="btn btn-sm bg-navy"><span class="glyphicon glyphicon-edit"></span> Update</button></a></td>
                                </tr>
<?php
                              }
    //Hasil Konversi Tanggal Pemesanan
    $cari = explode('-',$tanggal_event);
    $tahun = $cari[0];
    $bulan = $cari[1];
    $tgl = $cari[2];

    $bulanan = $cari[1];
    $bulans = bulananindonesia($bulanan);
    $tgls = $tgl.(" ").$bulans.(" ").$tahun;
    if(mysqli_num_rows($query)===0){
?>
    <script>
        bootbox.alert({
            title: "<i class='fa fa-warning'></i>  Error!",
            message : "<center><strong>Maaf, Tidak ada Hasil untuk Event pada Tanggal <?=$tgls;?></strong></center>",
            backdrop : true
        });
    </script>
<?php
    }
?>
