<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

    //Required File
        require_once dirname(__FILE__)."/../class/config.php";
        
    //Define
        $db = new Database();
        $db->connect();

    //Data from previous page
        $kode_satuan = $db->escapeString($_POST["kode_satuan"]);
        $nama_satuan = $db->escapeString($_POST["nama_satuan"]);
        
    //Save -> Database
        $db->insert("tb_satuan",array("kode_satuan"=>$kode_satuan,"nama_satuan"=>$nama_satuan));
        $result = $db->getResult();
        
    if($result){
        echo "<script>alert('Penambahan Data Satuan Berhasil');location.href='".MAIN_URL."/pages/form_tambah_satuan.php'</script>";
    }else{
        echo "<script>alert('Penambahan Data Satuan Gagal');location.href='".MAIN_URL."/pages/form_tambah_satuan.php'</script>";
    }
?>