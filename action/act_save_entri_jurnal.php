<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

    //Required File
        require_once dirname(__FILE__)."/../class/manual_connect.php";
        require_once dirname(__FILE__)."/../class/native_connect.php";

    //Data from Previous Page
        $tanggal_transaksi = mysqli_real_escape_string($con, $_POST["tanggal_transaksi"]);
        $no_bukti = mysqli_real_escape_string($con, $_POST["no_bukti"]);
        $keterangan = mysqli_real_escape_string($con, $_POST["keterangan"]);
        $kode_akun = mysqli_real_escape_string($con, $_POST["kode_akun"]);
        $posisi = mysqli_real_escape_string($con, $_POST["posisi"]);
        $nominal_transaksi = mysqli_real_escape_string($con, $_POST["nominal_transaksi"]);
  
    // Save to Database
        $query = "INSERT INTO tb_jurnal
                 (NO_BUKTI,
                 TGL_TRANSAKSI,
                 AYAT_JURNAL,
                 JENIS_JURNAL,
                 REF_JURNAL,
                 LAST_UPDATE)
                 values
                 ('$no_bukti',
                 '$tanggal_transaksi',
                 '$keterangan',
                 'JURNAL MANUAL',
                 '-',
                 NOW())";
        $execute = mysqli_query($con, $query) or die(mysqli_error($con));
        $last_increment = mysqli_insert_id($con);
        
        $query_d = "INSERT INTO tb_detail_jurnal
                    (ID_JURNAL,
                    KODE_COA,
                    POSISI,
                    JML_TRANSAKSI)
                    values
                    ('$last_increment',
                    '$kode_akun',
                    '$posisi',
                    '$nominal_transaksi')";
        $exec = mysqli_query($con, $query_d) or die(mysqli_error($con));
        
        if($execute && $exec){
           echo "<script>alert('Jurnal Tersimpan');location.href='".MAIN_URL."/pages/form_entri_jurnal.php';</script>";
        }else{
           echo "<script>alert('Jurnal Gagal Tersimpan');location.href='".MAIN_URL."/pages/form_entri_jurnal.php';</script>";
       }
?>