<?php
    error_reporting(0);
    //Required File
    require_once dirname(__FILE__)."/../class/manual_connect.php";
    require_once dirname(__FILE__)."/../class/native_connect.php";
/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
    //Variable from previous page
    $nama_event = mysqli_real_escape_string($con,$_REQUEST["nama_event"]);
  
    $query2 = mysqli_query($con,"select * from tb_booking where id_booking ='$nama_event' ");
    $hasil=mysqli_fetch_array($query2);
    
    $query = mysqli_query($con,"SELECT
				p.id_booking,
				p.tgl_event,
				p.nama_pemesan,
				p.jaminan_kerusakan,
				p.no_bukti_pemesanan,
				z.nama_ruangan,
				(bayar_booking+total) AS 'Total Biaya',
				payed AS 'Telah Dibayar',
				(bayar_booking+total)-payed AS 'Sisa Pembayaran',
				p.status_booking
				FROM
				(SELECT id_booking,(harga_room-diskon_room+jaminan_kerusakan) AS bayar_booking FROM tb_booking WHERE id_booking=$nama_event )k
				JOIN
				(SELECT id_booking,SUM(tambah_item) AS total FROM (SELECT id_booking,(jml_item*price_item) AS tambah_item FROM tb_item_add
				WHERE id_booking=$nama_event) p) a
				ON k.id_booking=a.id_booking
				JOIN
				(SELECT id_booking,SUM(jml_payment_received) AS payed FROM tb_payment_schedule WHERE id_booking=$nama_event GROUP BY id_booking) m
				ON a.id_booking=m.id_booking
				JOIN
				(SELECT id_booking, tgl_event, id_ruangan,nama_pemesan, jaminan_kerusakan, no_bukti_pemesanan, status_booking FROM tb_booking WHERE id_booking=$nama_event) p
				JOIN
				(SELECT tb_ruangan.NAMA_RUANGAN FROM tb_ruangan JOIN tb_booking ON tb_ruangan.ID_RUANGAN = tb_booking.ID_RUANGAN WHERE tb_booking.id_ruangan='$hasil[ID_RUANGAN]' AND tb_booking.`ID_BOOKING`=$nama_event) z
				WHERE a.id_booking = '$nama_event'");
	$results = mysqli_fetch_array($query);
?>
        
        <input type="hidden" name="id_booking" id="id_booking" value="<?= $results['id_booking']; ?>" class='form-control'/>
        <div class="form-group">
            <label class="col-sm-2 control-label">Tanggal Event</label>
            
            <div class="col-sm-3">
                <input type="text" name='tgl_event' id="tgl_event" value="<?= $results['tgl_event']; ?>" class='form-control' readonly/>
            </div>
        </div>
        
        <div class="form-group">
            <label class="col-sm-2 control-label">Nama Pemesan</label>
            
            <div class="col-sm-4">
                <input type="text" name='nama_pemesan' id="nama_pemesan" value="<?= $results['nama_pemesan']; ?>" class='form-control' readonly/>
            </div>
        </div>
	
        <div class="form-group">
            <label class="col-sm-2 control-label">Jaminan Kerusakan</label>
            
            <div class="col-sm-3">
                <input type="text" name='jaminan_kerusakan' id="jaminan_kerusakan" value="<?= $results['jaminan_kerusakan']; ?>" class='form-control' readonly/>
            </div>
        </div>
        
        <div class="form-group">
            <label class="col-sm-2 control-label">No. Bukti Pemesanan</label>
            
            <div class="col-sm-3">
                <input type="text" name='no_bukti_pemesanan' id="no_bukti_pemesanan" value="<?= $results['no_bukti_pemesanan']; ?>" class='form-control' readonly/>
            </div>
        </div>
        
        <div class="form-group">
            <label class="col-sm-2 control-label">Nama Ruangan</label>
            
            <div class="col-sm-3">
                <input type="text" name='nama_ruangan' id="nama_ruangan" value="<?= $results['NAMA_RUANGAN']; ?>" class='form-control' readonly/>
            </div>
        </div>
        
        <div class="form-group">
            <label class="col-sm-2 control-label">Total Biaya</label>
	
            <div class="col-sm-3">
                <input type="text" name='total_biaya' id="total_biaya" value="<?= $results['Total Biaya']; ?>" class='form-control' readonly/>
            </div>
        </div>
        
        <div class="form-group">
            <label class="col-sm-2 control-label">Telah Dibayar</label>
            
            <div class="col-sm-3">
                <input type="text" name='telah_dibayar' id="telah_dibayar" value="<?= $results['Telah Dibayar']; ?>" class='form-control' readonly/>
            </div>
        </div>
        
        <div class="form-group">
            <label class="col-sm-2 control-label">Sisa Pembayaran</label>
        
            <div class="col-sm-3">
                <input type="text" name="sisa_pembayaran" id="sisa_pembayaran" value="<?= $results['Sisa Pembayaran']; ?>" class='form-control' readonly/>
            </div>
        </div>
        
        <div class="form-group">
            
            <label class="col-sm-2 control-label">Status Booking</label>
            
            <div class="col-sm-3">
                <input type="text" name='status_booking' id="status_booking" value="<?= $results['status_booking']; ?>" class='form-control' readonly/>
            </div>
        </div>