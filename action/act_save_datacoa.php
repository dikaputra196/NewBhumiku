<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
    //Required File
        require_once dirname(__FILE__)."/../class/config.php";

    //Define Connection -> Database
        $db = new Database();
        $db->connect();
        
    //Data from Previous Page
        $parent_akun = $db->escapeString($_POST["parent_akun"]);
        $kode_akun = $db->escapeString($_POST["kode_akun"]);
        $nama_akun = $db->escapeString($_POST["nama_akun"]);
        $jenis_akun = $db->escapeString($_POST["jenis_akun"]);
        $saldo_normal = $db->escapeString($_POST["saldo_normal"]);
        
        $db->insert("tb_coa",array("kode_coa"=>$kode_akun,"nama_coa"=>$nama_akun,"kode_parent"=>$parent_akun,"jenis_coa"=>$jenis_akun,"saldo_normal"=>$saldo_normal));
        $result = $db->getResult();
        
        if($result){
            echo "<script>alert('Penambahan Data Chart of Accounts Berhasil');location.href='".MAIN_URL."/pages/form_tambah_coa.php'</script>";
        }else{
            echo "<script>alert('Penambahan Data Chart of Accounts Gagal');location.href='".MAIN_URL."/pages/form_tambah_coa.php'</script>";
        }
?>