<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
    //Required
    require_once dirname(__FILE__)."/../class/manual_connect.php";
    require_once dirname(__FILE__)."/../class/native_connect.php";
        
    //Data from Previous Page
        $tanggal_pembayaran = mysqli_real_escape_string($con,$_POST['tanggal_pembayaran']);
	$pembayaran_ke = mysqli_real_escape_string($con,$_POST['pembayaran_ke']);
	$jumlah_pembayaran = mysqli_real_escape_string($con,$_POST['jumlah_pembayaran']);
	$cara_pembayaran = mysqli_real_escape_string($con,$_POST['cara_pembayaran']);
	$dibayar_ke = mysqli_real_escape_string($con,$_POST['dibayar_ke']);
	$no_bukti_pemesanan = mysqli_real_escape_string($con,$_POST['no_bukti_pemesanan']);
	$ref_jurnal = mysqli_real_escape_string($con,$_POST['id_booking']);
	$id_booking = mysqli_real_escape_string($con,$_POST['id_booking']);

    //Check
	$id_p = [];
	$sel = mysqli_query($con, "SELECT * FROM tb_payment_schedule where id_booking = '$id_booking' ");
	while($res = mysqli_fetch_array($sel)){
		$id_p[] = $res['ID_PAYMENT_SCHEDULE'];
	}    
     
        //Pembayaran 1
        if($pembayaran_ke=="1"){
            $query = "UPDATE TB_PAYMENT_SCHEDULE SET tgl_payment_received='$tanggal_pembayaran', jml_payment_received='$jumlah_pembayaran',status_payment='PAID',cara_payment='$cara_pembayaran where id_payment_schedule='$id_p[0]' ";
            $execute = mysqli_query($con, $query) or die(mysqli_error($con));
                
                if($execute){
                    $kueri = mysqli_query($con, "INSERT INTO TB_JURNAL
                                                (NO_BUKTI,
                                                TGL_TRANSAKSI,
                                                AYAT_JURNAL,
                                                JENIS_JURNAL,
                                                REF_JURNAL)
                                                values
                                                ('$no_bukti_pemesanan',
                                                '$tanggal_pembayaran',
                                                'Pembayaran Sewa Ruangan Termin 1',
                                                'PEMBAYARAN SEWA',
                                                '$ref_jurnal')") or die(mysqli_error($con));
                    $id_jurnal = mysqli_insert_id($con);
                    $ins_detail = mysqli_query($con,"INSERT INTO TB_DETAIL_JURNAL
                                                    (ID_JURNAL,
                                                    KODE_COA,
                                                    POSISI,
                                                    JML_TRANSAKSI)
                                                    values
                                                    ('$id_jurnal',
                                                    '$dibayar_ke',
                                                    'D',
                                                    '$jumlah_pembayaran'),
                                                    ('$id_jurnal',
                                                    '$dibayar_ke',
                                                    'K',
                                                    '$jumlah_pembayaran')"); 
                    echo "<script>
                            alert('Transaksi Berhasil');location.href='../beranda.php';
                        </script> ";
                    ?>
            <?php
                }
                else{
            
                    echo "<script>
                        alert('Transaksi Gagal');location.href='../beranda.php';
                    </script> ";?>
            <?php
                }
        }
        
        //Pembayaran 2
        else if($pembayaran_ke=="2"){
            $query1 = "UPDATE TB_PAYMENT_SCHEDULE SET tgl_payment_received='$tanggal_pembayaran', jml_payment_received='$jumlah_pembayaran',status_payment='PAID',cara_payment='$cara_pembayaran' where id_payment_schedule='$id_p[1]' ";
            $exec = mysqli_query($con, $query1) or die(mysqli_error($con));
            
            if($exec){
                    $kueri = mysqli_query($con, "INSERT INTO TB_JURNAL
                                                (NO_BUKTI,
                                                TGL_TRANSAKSI,
                                                AYAT_JURNAL,
                                                JENIS_JURNAL,
                                                REF_JURNAL)
                                                values
                                                ('$no_bukti_pemesanan',
                                                '$tanggal_pembayaran',
                                                'Pembayaran Sewa Ruangan Termin 2 dan Jaminan Kerusakan',
                                                'PEMBAYARAN SEWA',
                                                '$ref_jurnal')") or die(mysqli_error($con));
                    $id_jurnal = mysqli_insert_id($con);
                    $status = mysqli_query($con, "UPDATE tb_booking set status_booking='DEFINITIVE' where id_booking='$id_booking'");
                    $ins_detail = mysqli_query($con,"INSERT INTO TB_DETAIL_JURNAL
                                                    (ID_JURNAL,
                                                    KODE_COA,
                                                    POSISI,
                                                    JML_TRANSAKSI)
                                                    values
                                                    ('$id_jurnal',
                                                    '$dibayar_ke',
                                                    'D',
                                                    '$jumlah_pembayaran'),
                                                    ('$id_jurnal',
                                                    '$dibayar_ke',
                                                    'K',
                                                    '$jumlah_pembayaran')") or die(mysqli_error($con)); 
                    
                    echo "<script>
                            alert('Transaksi Berhasil');location.href='../beranda.php';
                         </script>";
                    
                    ?>
                    
            <?php
                }
                else{
            
                    echo "<script>
                        alert('Transaksi Gagal');location.href='../beranda.php';
                    </script>"; 
             ?>
            <?php
                }
            
        }
        
        //Pembayaran 3
        else if($pembayaran_ke=="3"){
            $query2 = "UPDATE TB_PAYMENT_SCHEDULE SET tgl_payment_received='$tanggal_pembayaran', jml_payment_received='$jumlah_pembayaran',status_payment='PAID',cara_payment='$cara_pembayaran' where id_payment_schedule='$id_p[2]' ";
            $exe = mysqli_query($con, $query2) or die(mysqli_error($con));
                
                if($exe){
                    $kueri = mysqli_query($con, "INSERT INTO TB_JURNAL
                                                (NO_BUKTI,
                                                TGL_TRANSAKSI,
                                                AYAT_JURNAL,
                                                JENIS_JURNAL,
                                                REF_JURNAL)
                                                values
                                                ('$no_bukti_pemesanan',
                                                '$tanggal_pembayaran',
                                                'Pembayaran Sewa Ruangan Termin 3',
                                                'PEMBAYARAN SEWA',
                                                '$ref_jurnal')") or die(mysqli_error($con));
                    $id_jurnal = mysqli_insert_id($con);
                    $ins_detail = mysqli_query($con,"INSERT INTO TB_DETAIL_JURNAL
                                                    (ID_JURNAL,
                                                    KODE_COA,
                                                    POSISI,
                                                    JML_TRANSAKSI)
                                                    values
                                                    ('$id_jurnal',
                                                    '$dibayar_ke',
                                                    'D',
                                                    '$jumlah_pembayaran'),
                                                    ('$id_jurnal',
                                                    '$dibayar_ke',
                                                    'K',
                                                    '$jumlah_pembayaran')");
                   echo "<script>
                            alert('Transaksi Berhasil');location.href='../beranda.php';
                         </script>";
                    
                    ?>
            <?php
                }
                else{
                    echo "<script>
                        alert('Transaksi Gagal');location.href='../beranda.php';
                    </script>"; 
             ?>
            <?php
                }
        }
        
        //Not Available Process
        else{?>
            <script>
                alert("Pembayaran Gagal");location.href="<?= MAIN_URL ?>/pages/form_pembayaran.php";
            </script>
        <?php
        }
?>
