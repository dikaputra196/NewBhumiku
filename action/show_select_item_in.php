<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
    //Required
    require_once dirname(__FILE__)."/../class/config.php";
    require_once dirname(__FILE__)."/../class/manual_connect.php";
    require_once dirname(__FILE__)."/../class/native_connect.php";
    
    //Define Database
    $db = new Database();
    $db->connect();
    
    //Variabel from Previous Page
    $count_item_in = $db->escapeString($_REQUEST["count_item_in"]); 
    $total=0;
    
    //Proccess
    for($no=1; $no<=$count_item_in; $no++ ){
?>

<!-- Result -->
<div class="form-group">
    <div class="col-sm-3">
        <small><i>Nama Item</i></small>
        <select class="form-control select2" name="item_included[]" id="item_included<?= $no ?>" data-target="item_i<?= $no ?>">
            <option value=""> ---</option>
            <?php
                //Query
                $db->select("tb_item","id_item,nama_item",NULL,"is_active='1' ");
                $result = $db->getResult();
                foreach($result as $show){
            ?>
                <option value="<?= $show['id_item'] ?>"> <?= $show['nama_item'] ?> </option>
            <?php
                }
            ?>
        </select>
    </div>
    
    <div class="col-sm-2">
        <small><i>Jumlah Item</i></small>
        <input type="number" class="form-control" name="jumlah_in[]" id="jumlah_in<?= $no ?>" data-target="h_item_i<?= $no ?>">
    </div>
    
</div>

<?php   
    }
?>

<!-- Select2 -->
    <script>
        $(document).ready(function(){
            $(".select2").select2();
        });
    </script>