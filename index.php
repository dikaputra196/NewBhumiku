<?php
    //Required File
    require_once dirname(__FILE__)."/components/templates/main_login.php";
    require_once dirname(__FILE__)."/class/config.php";
    require_once dirname(__FILE__)."/class/manual_connect.php";
    require_once dirname(__FILE__)."/class/LoginController.php";
    
    //Define Connection -> Database
    $db = new Database();
    $db->connect();
    
    //Call Template
    $template = new loginPage();
    
    //Define Config
    $config = new manual_connect();
    if(!empty($_SESSION['id_user']) && isset($_SESSION['id_user'])){
        $config->checkSession();
    }
    LoginController::login();
    
    //Start HTML
    $template->pageTitle="BHUMIKU Balai Pertemuan | Login";
    
    //Start Content
    $template->contentTitle="<span class='glyphicon glyphicon-check'></span> Beranda";
    $template->startContent();
?>
 
<!--container login form -->

<div class="row">
    <div class='col-xs-12'>
        <?= manual_connect::showFlash(); ?>                     
    </div>
</div>

<form action="<?= MAIN_URL.'/index.php'; ?>" method="POST">
    <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12">
            <div class="cont-mrg">
                <div class="font-ket">Username</div>
                <div class="input-group">
                    <span class="input-group-addon" id="sizing-addon1" style="background-color:#00a65a; color:#fff;">
                    <span class="glyphicon glyphicon-user" aria-hidden="true"></span>
                    </span>
                    <input type="text" class="form-control" placeholder="Masukan username ..." aria-describedby="sizing-addon1" name="login[username]" id="username" maxlength="50">
                </div>
                <div class="font-mrg font-ket">Password</div>
                <div class="input-group">
                    <span class="input-group-addon" id="sizing-addon1" style="background-color:#00a65a; color:#fff;">
                    <span class="glyphicon glyphicon-lock" aria-hidden="true"></span>
                    </span>
                    <input type="password" class="form-control" placeholder="Masukan password ..." aria-describedby="sizing-addon1" name="login[password]" id="password" maxlength="50">
                </div>
                <!--button-->
                <div class="row">
                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 sub-mrga">
                        <button type="submit" class="btn btn-block btn-orange login" id="submit">
                        <span class="glyphicon glyphicon-ok" aria-hidden="true"></span>
                        Login</button>
                    </div>
                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12" id="sub-mrgb">
                        <button type="reset" class="btn btn-default btn-block">
                        <span class="glyphicon glyphicon-remove" aria-hidden="true"></span> 
                        Cancel</button>
                    </div>
                </div>
                <!--end button-->
            </div>
        </div>
    </div>
</form>
<!--end login form container-->

<!-- End Content -->
<?php $template->endContent(); ?>

<!-- Place Script Here -->

  
<!-- // End Script Place-->

<!-- End HTML -->
<?php $template->endHtml(); ?>